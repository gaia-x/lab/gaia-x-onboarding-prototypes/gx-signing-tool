import React from 'react'
import { FormControl, FormErrorMessage, FormHelperText, FormLabel, Input, InputGroup, InputLeftElement, InputRightElement } from '@chakra-ui/react'
import { Field } from 'formik'
import { toObjectPath } from '@/utils/util'

export interface FormFieldProps {
  label: string
  name: string
  list?: string
  autocomplete?: string
  helperText?: string
  placeholder?: string
  required?: boolean
  rightElement?: React.ReactNode
  leftElement?: React.ReactNode
  radioGroup?: React.ReactNode
  zIndex?: number
  validationFunction?: (value: string) => string | undefined
}

export const FormField: React.FC<FormFieldProps> = ({
  label,
  helperText,
  placeholder,
  required = false,
  name,
  list,
  autocomplete,
  rightElement,
  leftElement,
  radioGroup,
  zIndex,
  validationFunction
}) => {
  return (
    <Field name={name} validate={validationFunction}>
      {({ field, form }: any) => {
        const touched = toObjectPath(name, form.touched)
        const error = toObjectPath(name, form.errors)
        return (
          <FormControl isInvalid={touched && error} isRequired={required}>
            <FormLabel>{label}</FormLabel>
            {radioGroup}
            <InputGroup zIndex={zIndex}>
              {leftElement && <InputLeftElement>{leftElement}</InputLeftElement>}
              <Input {...field} list={list} autoComplete={autocomplete} placeholder={placeholder} />
              {touched && error ? (
                <FormErrorMessage position={'absolute'} bottom={'-20px'}>
                  {error}
                </FormErrorMessage>
              ) : (
                !!helperText && <FormHelperText>{helperText}</FormHelperText>
              )}
              {rightElement && <InputRightElement>{rightElement}</InputRightElement>}
            </InputGroup>
          </FormControl>
        )
      }}
    </Field>
  )
}
