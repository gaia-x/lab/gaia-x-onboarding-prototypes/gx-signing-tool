import React from 'react'
import { FormFieldProps } from '@/components/ui/Fields/FormField'
import { FormControl, FormLabel, Select } from '@chakra-ui/react'
import { Field } from 'formik'

interface FormSelectFieldProps extends Omit<FormFieldProps, 'leftElement' | 'rightElement' | 'radioGroup'> {
  values: string[]
  value?: string
}
export const FormSelectField: React.FC<FormSelectFieldProps> = props => {
  return (
    <Field>
      {({ form }: any) => {
        return (
          <FormControl isRequired={props.required}>
            <FormLabel>{props.label}</FormLabel>
            <Select onChange={e => form.setFieldValue(props.name, e.target.value)} placeholder={props.placeholder}>
              {props.values.map(value => (
                <option key={value} value={value} style={{ color: 'black' }}>
                  {value}
                </option>
              ))}
            </Select>
          </FormControl>
        )
      }}
    </Field>
  )
}
