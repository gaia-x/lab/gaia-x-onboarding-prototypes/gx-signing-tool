import React, { useState } from 'react'
import { useShapeByKey } from '@/hooks/useShapeByKey'
import { FormField, FormFieldProps } from '@/components/ui/Fields/FormField'
import { Radio, RadioGroup } from '@chakra-ui/radio'
import { Stack } from '@chakra-ui/react'
import { LegalRegistrationNumberType } from '@/sharedTypes'

type LegalRegistrationNumberProperty = {
  'sh:path': {
    '@id': string
  }
  'sh:datatype': {
    '@id': string
  }
  'sh::minLength': number
}

const regexPatterns: { [type in LegalRegistrationNumberType]: RegExp } = {
  taxID: /^\d{2}-\d{7}$/,
  EUID: /^[A-Z]{2}\d{2}[A-Z0-9]{1,15}$/,
  EORI: /^[A-Z]{2}[A-Z0-9]{1,15}$/,
  vatID: /^[A-Z]{2}[A-Z0-9]{2,15}$/,
  leiCode: /^[A-Z0-9]{20}$/
}

interface LRNFieldProps extends FormFieldProps {
  setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void
  radioValue: string
  typeName: string
}

export const LegalRegistrationNumberField: React.FC<LRNFieldProps> = props => {
  const [selectedNumberType, setSelectedNumberType] = useState<LegalRegistrationNumberType>()

  const validateLRN = (value: string): string | undefined => {
    const matchLRN = (id: string, idType: LegalRegistrationNumberType): boolean => {
      return regexPatterns[idType].test(id)
    }

    let result: string | undefined
    if (selectedNumberType && !matchLRN(value, selectedNumberType)) {
      result = `Invalid ${selectedNumberType}`
    }
    return result
  }

  return (
    <FormField
      validationFunction={validateLRN}
      radioGroup={
        <LRNRadioGroup
          setFieldValue={value => {
            props.setFieldValue(props.typeName, value)
          }}
          setSelectedNumberType={setSelectedNumberType}
          radioValue={props.radioValue}
        />
      }
      {...props}
    />
  )
}

interface LRNRadioGroupProps {
  setFieldValue: (value: string) => void
  setSelectedNumberType: (value: LegalRegistrationNumberType) => void
  radioValue: string
}
function LRNRadioGroup({ setFieldValue, setSelectedNumberType, radioValue }: LRNRadioGroupProps) {
  const { data: shape, isFetched } = useShapeByKey('participant')

  const legalRegistrationNumberShape = isFetched && shape['@graph'].find((otherShape: any) => otherShape['@id'] === 'gx:legalRegistrationNumberShape')
  const getLrnLabel = (property: LegalRegistrationNumberProperty): LegalRegistrationNumberType => {
    return property['sh:path']['@id'].split(':')[1] as LegalRegistrationNumberType
  }
  return (
    <RadioGroup
      my={3}
      onChange={value => {
        setFieldValue(value)
        setSelectedNumberType(value as LegalRegistrationNumberType)
      }}
      value={radioValue === '' ? undefined : radioValue}
    >
      <Stack spacing={5} direction={'row'} flexWrap={'wrap'}>
        {legalRegistrationNumberShape &&
          legalRegistrationNumberShape['sh:property'].map((prop: LegalRegistrationNumberProperty, index: number) => (
            <Radio key={'lrn' + index} colorScheme="green" value={getLrnLabel(prop)}>
              {getLrnLabel(prop)}
            </Radio>
          ))}
      </Stack>
    </RadioGroup>
  )
}
