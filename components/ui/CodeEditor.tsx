import React, { useState } from 'react'
import Editor from '@monaco-editor/react'
import { Box } from '@chakra-ui/react'

interface CodeEditorProps {
  value: string
  onChange?: (value: string) => void
  height?: string
  width?: string
  readOnly?: boolean
  testId?: string
  language?: string
}

export const CodeEditor: React.FC<CodeEditorProps> = ({
  value,
  onChange,
  height = '50vh',
  width = '100%',
  readOnly = false,
  testId = 'code-editor',
  language = 'json'
}) => {
  const [isMounted, setIsMounted] = useState(false)

  return (
    <Box w={width}>
      <Editor
        onMount={() => setIsMounted(true)}
        height={height}
        defaultLanguage={language}
        theme="light"
        value={value}
        options={{
          readOnly,
          lineNumbers: 'off',
          minimap: { enabled: false }
        }}
        wrapperProps={{ 'data-testid': testId, 'data-mounted': isMounted, 'data-value': value }}
        onChange={value => (onChange ? onChange(value || '') : null)}
      />
    </Box>
  )
}
