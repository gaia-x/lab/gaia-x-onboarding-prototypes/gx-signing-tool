import React from 'react'
import { Flex, BoxProps } from '@chakra-ui/react'

export const Container: React.FC<BoxProps> = props => {
  return (
    <Flex
      bgGradient={'linear(to-br, rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.2))'}
      p={3}
      mx={2}
      mb={5}
      borderRadius={6}
      boxShadow={'inner'}
      color={'white'}
      direction={'column'}
      justifyContent={'start'}
      {...props}
    >
      {props.children}
    </Flex>
  )
}
