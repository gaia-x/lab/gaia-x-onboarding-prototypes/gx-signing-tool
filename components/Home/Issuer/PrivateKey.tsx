import React, { useContext, useEffect, useState } from 'react'
import { Container } from '../../ui/Container'
import { Button, Checkbox, Flex, Heading, IconButton, Link, Tooltip } from '@chakra-ui/react'
import { CodeEditor } from '../../ui/CodeEditor'
import { InfoOutlineIcon, RepeatIcon } from '@chakra-ui/icons'
import { generatePrivateKey, privateKeyToPem } from '@/web-crypto/cryptokey'
import { useErrorHandler } from '@/hooks/useError'
import { SignatureContext } from '@/contexts/SignatureContext'
import { useWebAuthn } from '@/hooks/useWebAuthn'
import { useEid } from '@/hooks/useEid'

interface PrivateKeyProps {
  refreshEnabled?: boolean
  readOnly?: boolean
  privateKeyChange?: (value: string) => void
}

export const PrivateKey: React.FC<PrivateKeyProps> = ({ refreshEnabled = true, readOnly = false, privateKeyChange }) => {
  const { issuerUse, privateKeyUse } = useContext(SignatureContext)
  const { disableWebauthn, enableWebauthn, encryptKey, isWebauthnEnabled } = useWebAuthn()
  const { isEidEnabled, toggleEID } = useEid()
  const { errorHandler } = useErrorHandler()
  const [isGenPK, setGenPk] = useState(false)
  const [isEncryptedChecked, setIsEncryptedChecked] = useState(false)
  const [isUsingEncryption, setUsingEncryption] = useState(false)
  const [isEidChecked, setIsEidChecked] = useState(false)

  useEffect(() => {
    if (isWebauthnEnabled()) {
      setUsingEncryption(true)
      setIsEncryptedChecked(true)
    }
  }, [])

  useEffect(() => {
    if (isEidEnabled()) {
      setIsEidChecked(true)
    }
  }, [])

  const refreshKey = async () => {
    try {
      let key = await privateKeyToPem(await generatePrivateKey())
      setGenPk(true)

      if (key) {
        if (isWebauthnEnabled()) {
          const encryptedKey = await encryptKey(key)
          if (!encryptedKey) {
            throw new Error('Error while encrypting private key')
          }
          key = encryptedKey
        }

        localStorage.setItem('privateKey', key)
        privateKeyUse.setPrivateKey(key)
      }

      if (!issuerUse.isCustomIssuer) {
        issuerUse.setVerificationMethod('')
      }
    } catch (err: any) {
      await unregister()
      errorHandler(err, 'Error while generating private key')
    } finally {
      setGenPk(false)
    }
  }

  const updatePrivateKey = (privateKey: string) => {
    // do not store the private key in the custom DID solution
    privateKeyUse.setPrivateKey(privateKey, refreshEnabled)
    privateKeyChange?.(privateKey)
  }

  const register = async () => {
    try {
      await enableWebauthn()
      setIsEncryptedChecked(true)
      setUsingEncryption(true)
      await refreshKey()
    } catch (err: any) {
      await unregister()
      errorHandler(err, 'Error while registering security key')
    }
  }

  const unregister = async () => {
    try {
      await disableWebauthn()
      setIsEncryptedChecked(false)
      setUsingEncryption(false)
      privateKeyUse.setPrivateKey('', true)
    } catch (err: any) {
      errorHandler(err, 'Error while unregistering security key')
    }
  }

  const checkEID = async () => {
    if (isEidChecked) {
      await toggleEID(false)
      setIsEidChecked(false)
    } else {
      const success = await toggleEID(true)
      if (success) {
        setIsEidChecked(true)
      }
    }
  }

  return (
    <Container w={'100%'} mt={4} background={'none'} p={0} mx={0}>
      <Flex direction={'column'}>
        <Flex justifyContent={'space-between'} alignItems={'baseline'}>
          <Flex justifyContent={'flex-start'} alignItems={'baseline'}>
            <Heading my={2} size={'lg'}>
              Private key
            </Heading>
            <Tooltip
              label={
                'Your private key will not be shared through the network, this application use Web Crypto API and sign your document on browser side only'
              }
            >
              <InfoOutlineIcon ml={2} />
            </Tooltip>
          </Flex>
          {refreshEnabled && (
            <IconButton
              mb={2}
              aria-label="Generate new private key"
              onClick={refreshKey}
              isLoading={isGenPK}
              colorScheme={'green'}
              icon={<RepeatIcon />}
            />
          )}
        </Flex>
        <CodeEditor
          value={privateKeyUse.privateKey}
          language={'pem'}
          onChange={updatePrivateKey}
          height={'20vh'}
          readOnly={isUsingEncryption || readOnly}
          testId={'privateKey'}
        />
        <Checkbox onChange={e => setIsEncryptedChecked(e.target.checked)} isChecked={isEncryptedChecked} isDisabled={isEidChecked} mb={2}>
          Enable private key encryption through a compatible Webauthn security key
          <Link
            href="https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/-/blob/development/README.md#private-key-encryption-through-webauthn-and-a-fido2-hardware-token"
            isExternal
          >
            <Tooltip
              label={`This feature requires both a security key with hmac-secret capabilities and a browser implementing the webauthn PRF extension. 
                      A new private key will be generated and encrypted before storing it in the local storage.
                      For more info, click on the tooltip icon.`}
            >
              <InfoOutlineIcon ml={2} />
            </Tooltip>
          </Link>
        </Checkbox>
        {isEncryptedChecked && (
          <Flex justifyContent={'center'} alignItems={'baseline'}>
            {refreshEnabled && (
              <Button m={2} colorScheme={'green'} isDisabled={isUsingEncryption} onClick={() => register()}>
                Register security key
              </Button>
            )}
            <Button m={2} colorScheme={'red'} onClick={() => unregister()}>
              {refreshEnabled ? 'Disable private key encryption' : 'To import your own private key you first need to disable webauthn encryption'}
            </Button>
          </Flex>
        )}
        <Checkbox onChange={() => checkEID()} isChecked={isEidChecked} isDisabled={isUsingEncryption}>
          Enable signing with an eIDAS eID.
          <Link href="https://ec.europa.eu/digital-building-blocks/wikis/display/DIGITAL/eID" isExternal>
            <Tooltip label={'What is eID?'}>
              <InfoOutlineIcon ml={2} />
            </Tooltip>
          </Link>
        </Checkbox>
      </Flex>
    </Container>
  )
}
