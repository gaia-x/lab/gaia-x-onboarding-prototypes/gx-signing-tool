import React, { ChangeEvent, useContext, useEffect } from 'react'
import {
  Box,
  Button,
  Divider,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Input,
  InputGroup,
  InputRightElement,
  Select,
  Stack,
  Switch,
  Text,
  Tooltip
} from '@chakra-ui/react'
import { Container } from '../../ui/Container'
import { CodeEditor } from '../../ui/CodeEditor'
import { PrivateKey } from './PrivateKey'
import { useExamples } from '@/hooks/useExamples'
import { RepeatIcon, WarningIcon } from '@chakra-ui/icons'
import { generateName } from '@/utils/util'
import { SignatureContext } from '@/contexts/SignatureContext'

export const Issuer: React.FC = () => {
  const { issuerUse, issuerDoc, setIssuerDoc, setDocName, docName, signDocument, signingIsLoading } = useContext(SignatureContext)

  const { data: examples, getExampleByKey, setSelectedExample, selectedExample } = useExamples()

  const errors = []
  if (issuerDoc) {
    try {
      const doc = JSON.parse(issuerDoc)
      if (!doc.type?.length) {
        errors.push('Document type is required')
      }
      if (issuerUse.isCustomIssuer) {
        if (!doc.issuer) {
          errors.push('Issuer is required')
        }
        if (!doc.id) {
          errors.push('Verifiable Credential ID is required')
        }
        if (!doc.credentialSubject?.id) {
          errors.push('Credential Subject ID is required')
        }
      }
    } catch {
      errors.push('Invalid JSON')
    }
  }

  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setDocName(e.target.value)
  }

  const handleGenerateName = () => setDocName(generateName())

  const handleExampleChange = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e.target.value) {
      setSelectedExample(undefined)
      setIssuerDoc('')
      return
    }
    const exampleTuple = getExampleByKey(e.target.value)
    if (exampleTuple) {
      setSelectedExample(exampleTuple)
      setIssuerDoc(JSON.stringify(exampleTuple.example.vcTemplateFilled, null, 2))
    }
  }

  useEffect(() => {
    if (!docName || signingIsLoading) {
      handleGenerateName()
    }
  }, [signingIsLoading])

  return (
    <Container minW={'40%'} maxW={'500px'} minH={'70vh'} className="issuer">
      <Flex p={3} fontSize={20} w="100%" alignItems={'center'} justifyContent={'space-between'}>
        <Heading size={'md'} fontWeight={'bold'}>
          Issue Verifiable Credential
        </Heading>
        <Flex gap={2} alignItems={'center'}>
          {errors.length > 0 && (
            <Tooltip
              label={
                <Stack>
                  {errors.map((err, idx) => (
                    <Text key={idx}>{err}</Text>
                  ))}
                </Stack>
              }
            >
              <WarningIcon color={'red'} />
            </Tooltip>
          )}
          <Button
            colorScheme="green"
            onClick={signDocument}
            isLoading={signingIsLoading}
            loadingText={'Signing...'}
            isDisabled={!issuerDoc || !issuerUse.isValid || !!errors.length}
          >
            Sign
          </Button>
        </Flex>
      </Flex>
      <Box padding={3}>
        <Flex alignItems={'center'} justifyContent={'space-between'} py={3} w={'100%'}>
          <InputGroup w={{ base: '60%', md: '40%' }}>
            <Input value={docName} onChange={handleNameChange} placeholder={'Document name'} _placeholder={{ color: 'white' }} />
            <InputRightElement width="4.5rem">
              <RepeatIcon cursor={'pointer'} aria-label={'Generate a name'} onClick={handleGenerateName}></RepeatIcon>
            </InputRightElement>
          </InputGroup>
          <Select
            w={{ base: '40%', sm: '30%' }}
            onChange={handleExampleChange}
            placeholder={'Examples'}
            value={selectedExample ? selectedExample.shape : undefined}
            data-testid="examples"
          >
            {examples.map(({ shape: key, example: value }) => (
              <option key={key} value={key} style={{ color: 'black' }}>
                {value.summary}
              </option>
            ))}
          </Select>
        </Flex>
        <CodeEditor value={issuerDoc} onChange={value => setIssuerDoc(value)} height={'35vh'} />
        <FormControl display="flex" justifyContent="space-between" mt={4} w={'100%'}>
          <Tooltip label="Only did:web are supported for now">
            <FormLabel htmlFor="custom-issuer" mb="0" data-testid="custom-issuer">
              I use my own DID solution
            </FormLabel>
          </Tooltip>
          <Switch
            size={'lg'}
            colorScheme={'green'}
            id="custom-issuer"
            isChecked={issuerUse.isCustomIssuer}
            onChange={() => issuerUse.setIsCustomIssuer(!issuerUse.isCustomIssuer)}
          />
        </FormControl>
        {issuerUse.isCustomIssuer && (
          <>
            <Flex direction={'column'} mt={2}>
              <Text>Verification method</Text>
              <Input mt={1} value={issuerUse.verificationMethod} onChange={e => issuerUse.setVerificationMethod(e.target.value)} />
            </Flex>
          </>
        )}
        {!issuerUse.isCustomIssuer && (
          <Text mt={2} fontSize={'sm'} data-testid="custom-did-notice">
            The Wizard will host a DID document for you.
            <br />
            VerifiableCredential and credentialSubject ids will be generated.
          </Text>
        )}
        <Divider mt={4} />
        <PrivateKey />
      </Box>
    </Container>
  )
}
