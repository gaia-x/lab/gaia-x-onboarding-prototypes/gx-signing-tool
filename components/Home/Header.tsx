import React, { useContext } from 'react'
import { Alert, Box, CloseButton, ListItem, Stack, Text, UnorderedList } from '@chakra-ui/react'
import { ConnectWallet } from '@/components/Home/ConnectWallet/ConnectWallet'
import { useDisclaimer } from '@/hooks/useDisclaimer'
import { SelectClearingHouse } from '@/components/SelectClearingHouse'
import { WalletContext } from '@/contexts/WalletContext'

export const Header: React.FC = () => {
  const { isWalletOpen } = useContext(WalletContext)
  const { shouldDisplayDisclaimer, acceptDisclaimer } = useDisclaimer()

  return (
    <>
      <Stack direction={{ base: 'column-reverse', md: 'row' }} justify={'flex-end'} alignItems={'flex-end'} py={5}>
        {!isWalletOpen && (
          <Box>
            <SelectClearingHouse />
          </Box>
        )}
        <Box>{!isWalletOpen && <ConnectWallet />}</Box>
      </Stack>
      {shouldDisplayDisclaimer && (
        <Box px={2}>
          <Alert position={'relative'} status="warning" borderRadius={4} mb={4} maxW={'620px'}>
            <Box>
              <Text fontWeight={'bold'}>Wizard disclaimer ! 🧙🏻‍</Text> This wizard has been developed for test purpose:
              <UnorderedList>
                <ListItem>Loire will NOT be supported by this tool.</ListItem>
                <ListItem>The provided certificate is not valid for production.</ListItem>
                <ListItem>It doesn't enforce key chain validation.</ListItem>
                <ListItem>It doesn't implement all classes validation.</ListItem>
                <ListItem>The provided keypair is for convenience. We advocate that you use your own.</ListItem>
              </UnorderedList>
            </Box>
            <CloseButton position="absolute" right="8px" top="8px" onClick={acceptDisclaimer} />
          </Alert>
        </Box>
      )}
    </>
  )
}
