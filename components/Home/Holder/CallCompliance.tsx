import React, { useContext, useEffect, useState } from 'react'
import { Box, Button, Flex, Input, Select } from '@chakra-ui/react'
import { CodeEditor } from '@/components/ui/CodeEditor'
import { StoredWalletEntry } from '@/models/wallet'
import { v4 as uuidv4 } from 'uuid'
import { VDocument } from '@/models/document'
import { JsonLdDocument } from 'jsonld'
import { AppContext } from '@/contexts/AppContext'
import { useExamples } from '@/hooks/useExamples'
import { useCompliance } from '@/hooks/useCompliance'
import { useCredentials } from '@/hooks/useCredentials'
import { publicKeyToDidId } from '@/web-crypto/did'
import { createVcId, joinPath } from '@/utils/util'
import { getRootUrl } from '@/env'
import { SignatureContext } from '@/contexts/SignatureContext'
import { ArrowLeftIcon } from '@chakra-ui/icons'
import { ComplianceFormat } from '@/sharedTypes'
import { useEid } from '@/hooks/useEid'

type VerifiablePresentation = {
  verifiableCredential: JsonLdDocument[]
}

interface CallComplianceProps {
  documents: StoredWalletEntry[]
  onComplete: (doc: VDocument) => void
  onCancel?: () => void
}

export const CallCompliance: React.FC<CallComplianceProps> = ({ documents, onComplete, onCancel }) => {
  const { addDocument } = useContext(AppContext)
  const { privateKeyUse } = useContext(SignatureContext)

  const { VPTemplate } = useExamples()
  const { uploadVC } = useCredentials()
  const { verifyWithCompliance, isLoading: verifyIsLoading, outputFormat, outputFormats, setOutputFormat } = useCompliance()
  const { isEidEnabled } = useEid()

  const [presentationName, setPresentationName] = useState<string>('')

  const [verifiablePresentation, setVerifiablePresentation] = useState<StoredWalletEntry>()
  const handleSubmitToCompliance = async () => {
    setPresentationName('')

    if (!verifiablePresentation) return console.error('No verifiable presentation')

    let privateKey = ''
    if (!isEidEnabled()) {
      privateKey = (await privateKeyUse.getPrivateKey()) ?? ''
    }
    const publicKey = await privateKeyUse.getPublicKey()
    if (!publicKey) return console.error('Could not get public key')

    const vcPath = publicKeyToDidId(publicKey)
    const didUrl = joinPath(location.origin, getRootUrl(location.pathname), 'api', 'credentials', vcPath)
    const vcId = createVcId(didUrl)

    const complianceResponse = await verifyWithCompliance(vcId, verifiablePresentation.document)

    if (!complianceResponse) return

    await uploadVC(complianceResponse.data as any)

    addDocument({
      document: complianceResponse.data,
      docName: presentationName,
      privateKey: privateKey,
      rawDocument: complianceResponse?.jwt,
      format: complianceResponse.format
    })
    onComplete(complianceResponse.data)
  }

  useEffect(() => {
    const handleCreatePresentation = async () => {
      const documentsContent = documents.map(doc => doc.document)
      const template: VerifiablePresentation = { ...VPTemplate }

      template.verifiableCredential = documentsContent
      // Create a new StoredWalletEntry with the selected documents
      const newPresentation: StoredWalletEntry = {
        key: uuidv4(),
        type: 'VerifiablePresentation',
        creation: new Date().toISOString(),
        name: presentationName || 'Document',
        document: template as VDocument,
        isVerified: false
      }

      setPresentationName(documents[0].name)
      setVerifiablePresentation(newPresentation)
    }
    handleCreatePresentation()
  }, [documents])

  return (
    <Box>
      <Flex flexDirection={'column'}>
        <Flex justifyContent={'space-between'} w={'100%'} alignItems={'center'}>
          <Input
            placeholder={'Document name'}
            value={presentationName}
            onChange={e => setPresentationName(e.target.value)}
            w={{ base: '50%', md: '40%' }}
            my={1}
            _placeholder={{ color: 'white' }}
          />
          <Select
            w={{ base: '50%', md: '25%' }}
            value={outputFormat}
            placeholder={'Output format'}
            onChange={e => setOutputFormat(e.target.value as ComplianceFormat)}
            data-testid={'compliance-output-format'}
          >
            {outputFormats.map(format => (
              <option key={format} style={{ color: 'black' }}>
                {format}
              </option>
            ))}
          </Select>
        </Flex>
        {verifiablePresentation && <CodeEditor value={JSON.stringify(verifiablePresentation.document, null, 2)} readOnly />}
      </Flex>
      <Flex pt={4} pb={2} justifyContent={'space-between'}>
        {onCancel && (
          <Button colorScheme={'yellow'} leftIcon={<ArrowLeftIcon />} onClick={onCancel}>
            Back
          </Button>
        )}
        <Button isLoading={verifyIsLoading} colorScheme="teal" mr={3} onClick={handleSubmitToCompliance}>
          Submit to compliance
        </Button>
      </Flex>
    </Box>
  )
}
