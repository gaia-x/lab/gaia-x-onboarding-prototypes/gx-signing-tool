import { Flex, Grid, GridItem, Heading } from '@chakra-ui/react'
import { SignedDocument } from '@/components/Home/Holder/SignedDocument'
import { StoredWalletEntry } from '@/models/wallet'
import { ImportDocument } from '@/components/Home/Holder/ImportDocument'
import { PrepareComplianceCall } from '@/components/Home/Holder/PrepareComplianceCall'
import React from 'react'

interface HolderColumnProps {
  documents: StoredWalletEntry[]
  title: string
}

export const HolderColumn: React.FC<HolderColumnProps> = ({ documents, title }) => {
  return (
    <Flex flexDirection={'column'} w={'100%'} alignItems={'center'}>
      <Heading>{title}</Heading>
      <Flex
        flexDirection={{ base: 'column-reverse', xl: 'row' }}
        alignItems={{ base: 'flex-end', lg: 'center' }}
        justifyContent={'space-between'}
        w={'100%'}
        px={6}
        mt={4}
      >
        <ImportDocument />
        <PrepareComplianceCall documents={documents} />
      </Flex>
      <Grid
        px={6}
        mt={6}
        templateColumns={{
          base: 'repeat(1, 1fr)',
          '2xl': 'repeat(2, 1fr)',
          '3xl': 'repeat(3, 1fr)'
        }}
        gap={4}
        w={'100%'}
        data-testid="documents"
      >
        {documents.map(doc => (
          <GridItem key={doc.key}>
            <SignedDocument id={doc.key} signedDocument={doc} />
          </GridItem>
        ))}
      </Grid>
    </Flex>
  )
}
