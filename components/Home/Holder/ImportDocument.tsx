import {
  Box,
  Button,
  Flex,
  Heading,
  Input,
  InputGroup,
  InputRightElement,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  Text,
  useDisclosure,
  useToast,
  VStack
} from '@chakra-ui/react'
import { v4 as uuidv4 } from 'uuid'
import { CodeEditor } from '@/components/ui/CodeEditor'
import React, { useCallback, useContext, useState } from 'react'
import { AddIcon, RepeatIcon } from '@chakra-ui/icons'
import type { StorableVDocument, VDocument } from '@/models/document'
import { AppContext } from '@/contexts/AppContext'
import { useDropzone } from 'react-dropzone'
import { gradients } from '@/customTheme'
import type { StoredWalletEntry } from '@/models/wallet'
import { generateName, isDocumentVerified } from '@/utils/util'
import { useWallet } from '@/hooks/useWallet'
import { PolicyStepperContext } from '@/contexts/PolicyStepperContext'

interface ImportDocumentProps {
  saveInWallet?: boolean
  saveInPolicyContext?: boolean
}

export const ImportDocument: React.FC<ImportDocumentProps> = ({ saveInWallet, saveInPolicyContext }) => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const { handleImport } = useWallet()
  const [name, setName] = useState<string>('')
  const [document, setDocument] = useState<string>('')

  const [importedDocuments, setImportedDocuments] = useState<StorableVDocument[]>([])
  const hasImportedDocuments = !!importedDocuments.length

  const { addDocument } = useContext(AppContext)
  const { addCredential } = useContext(PolicyStepperContext)

  const toast = useToast()

  const handleClose = () => {
    setName('')
    setDocument('')
    setImportedDocuments([])
    onClose()
  }

  const validateDocument = (document: string, documentName?: string): boolean => {
    let parsedDocument: VDocument

    // Document is a valid JSON
    try {
      parsedDocument = JSON.parse(document)
    } catch (error) {
      toast({
        title: 'Invalid document' + (documentName ? ` (${documentName})` : ''),
        description: 'Please enter a valid JSON document',
        status: 'error',
        duration: 5000,
        isClosable: true
      })
      return false
    }

    // Document is signed
    if (!parsedDocument.hasOwnProperty('proof')) {
      toast({
        title: 'No proof' + (documentName ? ` (${documentName})` : ''),
        description: 'Please enter a signed document',
        status: 'error',
        duration: 5000,
        isClosable: true
      })
      return false
    }

    return true
  }

  const handleGenerateName = () => {
    const name = generateName()
    setName(name)
  }

  const handleAddDocument = () => {
    if (!document)
      return toast({
        title: 'No document',
        description: 'Please enter a document',
        status: 'error',
        duration: 5000,
        isClosable: true
      })

    if (!validateDocument(document)) return

    const parsedDocument: VDocument = JSON.parse(document)
    const isDocVerified = isDocumentVerified(parsedDocument)

    if (saveInWallet) {
      addDocumentToWallet(parsedDocument, name, isDocVerified)
    } else if (saveInPolicyContext) {
      addCredential({
        document: parsedDocument,
        docName: name,
        privateKey: ''
      })
    } else {
      addDocument(
        {
          document: parsedDocument,
          docName: name,
          privateKey: ''
        },
        isDocVerified
      )
    }
    handleClose()
  }

  const handleAddImportedDocuments = () => {
    importedDocuments.forEach(importedDocument => {
      const isDocVerified = isDocumentVerified(importedDocument.document)

      if (saveInWallet) {
        addDocumentToWallet(importedDocument.document, importedDocument.docName, isDocVerified)
      } else if (saveInPolicyContext) {
        addCredential({
          document: importedDocument.document,
          docName: importedDocument.docName,
          privateKey: ''
        })
      } else {
        addDocument(importedDocument, isDocVerified)
      }
    })

    toast({
      title: 'Document(s) saved',
      description: 'Document(s) saved in wallet',
      status: 'success',
      duration: 3000,
      isClosable: true
    })
    handleClose()
  }

  const addDocumentToWallet = (importedDocument: VDocument, documentName: string, isDocVerified: any) => {
    const walletEntry: StoredWalletEntry = {
      document: importedDocument,
      name: documentName,
      privateKey: '',
      key: uuidv4(),
      type: 'VerifiableCredential',
      creation: new Date().toISOString(),
      isVerified: isDocVerified
    }
    handleImport(walletEntry, false)
  }

  const onDrop = useCallback((acceptedFiles: File[]) => {
    acceptedFiles.forEach((file, i) => {
      const fileReader = new FileReader()
      fileReader.onloadend = () => {
        const content = fileReader.result
        if (!validateDocument(content as string, acceptedFiles[i].name)) return

        const vDocument: VDocument = JSON.parse(content as string)
        const storableDocument: StorableVDocument = {
          document: vDocument,
          docName: acceptedFiles[i].name.split('.')[0],
          privateKey: ''
        }
        setImportedDocuments(prevState => [...prevState, storableDocument])
      }
      fileReader.readAsText(file)
    })
  }, [])

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop })

  return (
    <Box pb={1}>
      <Button leftIcon={<AddIcon />} colorScheme={'green'} onClick={onOpen} data-testid={'import-document-button'}>
        Import signed document
      </Button>
      <Modal isOpen={isOpen} onClose={handleClose} isCentered size={'3xl'}>
        <ModalOverlay />
        <ModalContent bgGradient={gradients.primary} color={'white'} data-testid={'import-document-modal'}>
          <ModalHeader>{'Import document'}</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            {!hasImportedDocuments && (
              <Stack justifyContent={'space-between'} direction={{ base: 'column', sm: 'row' }}>
                <InputGroup w={{ base: '70%', md: '40%' }}>
                  <Input value={name} onChange={e => setName(e.target.value)} placeholder={'Document name'} _placeholder={{ color: 'white' }} />
                  <InputRightElement width="4.5rem">
                    <RepeatIcon cursor={'pointer'} aria-label={'Generate a name'} onClick={handleGenerateName}></RepeatIcon>
                  </InputRightElement>
                </InputGroup>
                <div {...getRootProps()}>
                  <input {...getInputProps()}></input>
                  {isDragActive ? (
                    <Button colorScheme={'green'} data-testid={'upload-document-button'}>
                      Drop the files ...
                    </Button>
                  ) : (
                    <Button colorScheme={'green'} data-testid={'upload-document-button'}>
                      Drag & drop or click to select files
                    </Button>
                  )}
                </div>
              </Stack>
            )}
            {hasImportedDocuments && <Text>{importedDocuments.length} documents</Text>}
            <VStack maxH={'50vh'} mt={1} overflowY={'auto'} pr={5}>
              {hasImportedDocuments ? (
                importedDocuments.map((importedDocument, index) => (
                  <Flex w={'100%'} flexDirection={'column'}>
                    <Heading mb={1} size={'md'}>
                      {importedDocument.docName}
                    </Heading>
                    <CodeEditor
                      key={importedDocument.docName + index}
                      readOnly
                      value={JSON.stringify(importedDocument.document, null, 2)}
                      height={'30vh'}
                    />
                  </Flex>
                ))
              ) : (
                <Box {...getRootProps()} width={'100%'} onClick={() => null}>
                  <CodeEditor value={document} onChange={setDocument} height={'40vh'} />
                </Box>
              )}
            </VStack>
          </ModalBody>

          <ModalFooter>
            <Button
              colorScheme="green"
              onClick={hasImportedDocuments ? handleAddImportedDocuments : handleAddDocument}
              isDisabled={!(document || importedDocuments.length)}
            >
              Add
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  )
}
