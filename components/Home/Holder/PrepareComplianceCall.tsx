import React, { useContext, useEffect, useState } from 'react'
import {
  Badge,
  Box,
  Button,
  Checkbox,
  Container,
  Divider,
  Flex,
  IconButton,
  Link,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  Text,
  Tooltip,
  useDisclosure
} from '@chakra-ui/react'
import { StoredWalletEntry } from '@/models/wallet'
import { CodeEditor } from '@/components/ui/CodeEditor'
import { ArrowLeftIcon, ArrowRightIcon, CheckIcon, ChevronDownIcon, ChevronUpIcon, WarningIcon } from '@chakra-ui/icons'
import { CallCompliance } from './CallCompliance'
import { useVPValidation } from '@/hooks/useVPValidation'
import { gradients } from '@/customTheme'
import { useVCTypes } from '@/hooks/useVCTypes'
import { ClearingHousesContext } from '@/contexts/ClearingHousesContext'

interface PrepareComplianceCallProps {
  documents: StoredWalletEntry[]
}

export const PrepareComplianceCall: React.FC<PrepareComplianceCallProps> = ({ documents }) => {
  const complianceInputDisclosure = useDisclosure()
  const callComplianceDisclosure = useDisclosure()
  const complianceDisclaimerDisclosure = useDisclosure()
  const [isExpanded, setExpanded] = useState<string[]>([])
  const [selectedDocuments, setSelectedDocuments] = useState<StoredWalletEntry[]>([])
  const { isValid, validations } = useVPValidation(selectedDocuments)
  const { getVCTypes } = useVCTypes()
  const { selectedGxDeploymentPath } = useContext(ClearingHousesContext)

  const selectableDocuments = documents.filter(d => !d.isVerified)
  const isProductionEnv: boolean = selectedGxDeploymentPath === 'v1'
  const [isDisclaimerChecked, setIsDisclaimerChecked] = useState(false)

  useEffect(() => {
    const remains = selectedDocuments.filter(d => selectableDocuments.find(sd => sd.key === d.key))
    if (remains.length !== selectedDocuments.length) {
      setSelectedDocuments(remains)
    }
  }, [documents])

  const handleSubmitComplianceInput = () => {
    complianceInputDisclosure.onClose()
    isProductionEnv ? complianceDisclaimerDisclosure.onOpen() : callComplianceDisclosure.onOpen()
  }

  const backToComplianceInputs = () => {
    complianceDisclaimerDisclosure.onClose()
    complianceInputDisclosure.onOpen()
  }

  const acceptComplianceDisclaimer = () => {
    complianceDisclaimerDisclosure.onClose()
    callComplianceDisclosure.onOpen()
  }

  const backFromComplianceCall = () => {
    callComplianceDisclosure.onClose()
    isProductionEnv ? complianceDisclaimerDisclosure.onOpen() : complianceInputDisclosure.onOpen()
  }

  const toggleExpanded = (key: string) => {
    if (isExpanded.includes(key)) {
      setExpanded(isExpanded.filter(k => k !== key))
    } else {
      setExpanded([...isExpanded, key])
    }
  }

  const setDocumentSelected = (document: StoredWalletEntry, isSelected: boolean) => {
    if (isSelected) {
      setSelectedDocuments([...selectedDocuments, document])
    } else {
      setSelectedDocuments(selectedDocuments.filter(d => d.key !== document.key))
    }
  }

  return (
    <Box pb={1}>
      <Button colorScheme={'teal'} onClick={complianceInputDisclosure.onOpen}>
        Call compliance
      </Button>
      <Modal isOpen={complianceInputDisclosure.isOpen} onClose={complianceInputDisclosure.onClose} size={'3xl'}>
        <ModalOverlay />
        <ModalContent color={'white'} bgGradient={gradients.primary}>
          <ModalHeader>Select credentials to present to compliance</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Flex gap={2} wrap={'wrap'} dir={'row'} mb={4}>
              {validations.map((v, i) => (
                <Tooltip
                  key={i}
                  label={
                    v.messages.length > 0 && (
                      <Stack>
                        {v.messages.map((m, ii) => (
                          <Text key={ii}>{m}</Text>
                        ))}
                      </Stack>
                    )
                  }
                >
                  <Flex
                    align={'center'}
                    flex={1}
                    justifyContent={'space-between'}
                    color={v.isValid ? 'green.100' : 'red'}
                    borderRadius={4}
                    fontWeight={v.isValid ? 'normal' : '500'}
                    bgColor={v.isValid ? '#642CF7' : 'red.100'}
                    fontSize={'sm'}
                    p={2}
                    data-testid={`compliance-validation-tooltip_${i}`}
                  >
                    <Flex align={'center'}>
                      <Badge>{v.min > 1 ? `${v.count}/${v.min}` : v.count}</Badge>
                      <Text ml={2}>{v.name}</Text>
                    </Flex>
                    <Text>{v.isValid ? <CheckIcon aria-label={'Valid credential'} /> : <WarningIcon aria-label={'Missing credential'} />}</Text>
                  </Flex>
                </Tooltip>
              ))}
            </Flex>
            {selectableDocuments.map((doc, i) => (
              <Box key={doc.key}>
                {i !== 0 && <Divider my={2} />}
                <Flex align={'center'} justifyContent={'space-between'} width={'100%'}>
                  <Checkbox
                    data-testid={`compliance-document_${i}`}
                    onChange={e => setDocumentSelected(doc, e.target.checked)}
                    isChecked={selectedDocuments.includes(doc)}
                  >
                    <Box ml={2} wordBreak={'break-word'} overflow={'auto'}>
                      <Text>{doc.name}</Text>
                      <Text>{getVCTypes(doc.document).join(', ')}</Text>
                    </Box>
                  </Checkbox>
                  <IconButton
                    onClick={() => toggleExpanded(doc.key)}
                    icon={isExpanded.includes(doc.key) ? <ChevronUpIcon boxSize={5} /> : <ChevronDownIcon boxSize={5} />}
                    aria-label="toggle document"
                    colorScheme={'blue'}
                    size="sm"
                  />
                </Flex>
                {isExpanded.includes(doc.key) && (
                  <Box mt={2}>
                    <CodeEditor value={JSON.stringify(doc.document, null, 2)} height={'200px'} readOnly />
                  </Box>
                )}
              </Box>
            ))}
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="teal" mr={3} rightIcon={<ArrowRightIcon />} onClick={handleSubmitComplianceInput} isDisabled={!isValid}>
              Next ({selectedDocuments.length})
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
      <Modal isOpen={complianceDisclaimerDisclosure.isOpen} onClose={complianceDisclaimerDisclosure.onClose} size={'3xl'}>
        <ModalOverlay />
        <ModalContent color={'white'} bgGradient={gradients.primary}>
          <ModalHeader>Production requirements</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Container>
              <Checkbox
                isChecked={isDisclaimerChecked}
                onChange={() => setIsDisclaimerChecked(!isDisclaimerChecked)}
                data-testid={'compliance-disclaimer'}
              >
                Please note that this is a production environment, and{' '}
                <Link
                  style={{
                    textDecoration: 'underline',
                    fontWeight: 'bold'
                  }}
                  href={'https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/trust_anchors/#list-of-defined-trust-anchors'}
                >
                  a valid certificate is required.
                </Link>
              </Checkbox>
            </Container>
          </ModalBody>
          <ModalFooter>
            <Flex flexDirection={'row'} alignItems={'center'} justifyContent={'space-between'} w={'100%'}>
              <Button colorScheme={'yellow'} leftIcon={<ArrowLeftIcon />} onClick={backToComplianceInputs}>
                Back
              </Button>
              <Button isDisabled={!isDisclaimerChecked} colorScheme="teal" mr={3} rightIcon={<ArrowRightIcon />} onClick={acceptComplianceDisclaimer}>
                Next
              </Button>
            </Flex>
          </ModalFooter>
        </ModalContent>
      </Modal>
      <Modal isOpen={callComplianceDisclosure.isOpen} onClose={callComplianceDisclosure.onClose} size={'3xl'}>
        <ModalOverlay />
        <ModalContent color={'white'} bgGradient={gradients.primary}>
          <ModalHeader>Submit a presentation to compliance</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <CallCompliance documents={selectedDocuments} onComplete={callComplianceDisclosure.onClose} onCancel={backFromComplianceCall} />
          </ModalBody>
        </ModalContent>
      </Modal>
    </Box>
  )
}
