import React, { useContext, useMemo } from 'react'
import { Flex, Heading, IconButton } from '@chakra-ui/react'
import { Container } from '../../ui/Container'
import { AppContext } from '@/contexts/AppContext'
import { ArrowBackIcon } from '@chakra-ui/icons'
import { WalletContext } from '@/contexts/WalletContext'
import { HolderColumn } from '@/components/Home/Holder/HolderColumn'

export const Holder: React.FC = () => {
  const { signedDocuments } = useContext(AppContext)
  const { isWalletOpen, setWalletIsOpen } = useContext(WalletContext)
  const closeWallet = () => {
    setWalletIsOpen(false)
  }

  const documents = useMemo(() => {
    return Array.from(signedDocuments.values())
  }, [signedDocuments])

  return (
    <Container
      className={'holder'}
      minW={'40%'}
      maxW={isWalletOpen ? '100%' : { base: '100%', lg: '1000px' }}
      w={{ base: 'auto', xl: '60%' }}
      minH={'70vh'}
    >
      <Flex fontSize={20} w="100%" alignItems={'center'} justifyContent={'flex-start'} p={3}>
        {isWalletOpen && <IconButton variant={'outline'} aria-label={'Back'} icon={<ArrowBackIcon />} colorScheme={'white'} onClick={closeWallet} />}
        <Heading ml={3} size={'md'} fontWeight={'bold'}>
          Holder
        </Heading>
      </Flex>
      <Flex w={'100%'} h={'100%'}>
        <HolderColumn documents={documents} title={''} />
      </Flex>
    </Container>
  )
}
