import React, { useContext } from 'react'
import { Header } from '@/components/Home/Header'
import { Box, Flex } from '@chakra-ui/react'
import { Issuer } from '@/components/Home/Issuer/Issuer'
import { Holder } from '@/components/Home/Holder/Holder'
import { LocalWallet } from '@/components/Wallet/LocalWallet/LocalWallet'
import { WalletContext } from '@/contexts/WalletContext'
import { AvailableWallets } from '@/models/wallet'
import { DecentralizedWebNodeWallet } from '@/components/Wallet/DecentralizedWebNodeWallet/DecentralizedWebNodeWallet'

export const HomeContainer: React.FC = () => {
  const { isWalletOpen, connectedWallet } = useContext(WalletContext)

  return (
    <Box minH={'100vh'} px={{ base: 0, sm: 8 }} position={'relative'} bg={'transparent'}>
      <Header />
      <Flex direction={{ base: 'column', xl: 'row' }} justifyContent={isWalletOpen ? 'flex-start' : 'space-around'}>
        {!isWalletOpen && <Issuer />}
        <Holder />
      </Flex>
      {isWalletOpen && connectedWallet === AvailableWallets.LOCAL_WALLET && <LocalWallet />}
      {isWalletOpen && connectedWallet === AvailableWallets.DECENTRALIZED_WEB_NODE && <DecentralizedWebNodeWallet />}
    </Box>
  )
}
