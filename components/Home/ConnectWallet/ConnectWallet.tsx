import {
  Box,
  Button,
  Grid,
  GridItem,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure
} from '@chakra-ui/react'
import React, { useContext } from 'react'
import { StaticImageData } from 'next/image'
import waltIdIcon from '../../../public/waltid.png'
import localWalletIcon from '../../../public/white-wallet.png'
import difLogo from '../../../public/dif-logo.svg'
import { WalletTile } from '@/components/Home/ConnectWallet/WalletTile'
import { gradients } from '@/customTheme'
import { WalletContext } from '@/contexts/WalletContext'
import { AvailableWallets } from '@/models/wallet'

export type Wallet = {
  name: string
  icon: StaticImageData
  disabled: boolean
  openWallet: () => void
}

export const ConnectWallet: React.FC = () => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const { setWalletIsOpen, connectedWallet, setConnectedWallet } = useContext(WalletContext)

  const handleOpenLocalWallet = () => {
    onClose()
    setWalletIsOpen(true)
    setConnectedWallet(AvailableWallets.LOCAL_WALLET)
  }

  const handleOpenDWNWallet = () => {
    onClose()
    setWalletIsOpen(true)
    setConnectedWallet(AvailableWallets.DECENTRALIZED_WEB_NODE)
  }

  const walletsAvailable: Wallet[] = [
    {
      name: 'Local Wallet',
      icon: localWalletIcon,
      disabled: false,
      openWallet: handleOpenLocalWallet
    },
    {
      name: 'Decentralized Web Node',
      icon: difLogo,
      disabled: false,
      openWallet: handleOpenDWNWallet
    },
    {
      name: 'Walt.Id Wallet',
      icon: waltIdIcon,
      disabled: true,
      openWallet: () => null
    }
  ]

  return (
    <Box>
      {connectedWallet === AvailableWallets.NONE && (
        <Button
          colorScheme={'green'}
          onClick={() => {
            onOpen()
          }}
        >
          Connect Wallet
        </Button>
      )}
      {connectedWallet === AvailableWallets.LOCAL_WALLET && (
        <Button colorScheme={'blue'} aria-label={'wallet'} onClick={handleOpenLocalWallet}>
          🟢<Text ml={2}>Local Wallet</Text>
        </Button>
      )}
      {connectedWallet === AvailableWallets.DECENTRALIZED_WEB_NODE && (
        <Button colorScheme={'blue'} aria-label={'wallet'} onClick={handleOpenDWNWallet}>
          🟢<Text ml={2}>Decentralized Web Node</Text>
        </Button>
      )}
      <Modal isCentered isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent bgGradient={gradients.primary} color={'white'}>
          <ModalHeader>Available Wallets</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Grid templateColumns="repeat(2, 1fr)" gap={4}>
              {walletsAvailable.map(wallet => (
                <GridItem key={wallet.name}>
                  <WalletTile wallet={wallet} />
                </GridItem>
              ))}
            </Grid>
          </ModalBody>
          <ModalFooter />
        </ModalContent>
      </Modal>
    </Box>
  )
}
