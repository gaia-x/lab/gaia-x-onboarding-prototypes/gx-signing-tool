import {
  Button,
  Center,
  HStack,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Switch,
  Text,
  IconButton,
  Flex
} from '@chakra-ui/react'
import { gradients } from '@/customTheme'
import { CodeEditor } from '@/components/ui/CodeEditor'
import { VCGraph } from '@/components/VCGraph'
import React, { useState } from 'react'
import { VDocument } from '@/models/document'
import { FiCopy } from 'react-icons/fi'
import { useToasterHandler } from '@/hooks/useToast'

interface ViewDocumentModalProps {
  docName: string
  document: VDocument
  rawDocument: string | undefined
  isOpen: boolean
  onClose: () => void
}

export const ViewDocumentModal: React.FC<ViewDocumentModalProps> = ({ docName, document, rawDocument, isOpen, onClose }) => {
  const [isGraphViewSelected, setIsGraphViewSelected] = useState<boolean>(false)
  const { useToaster } = useToasterHandler()
  const copyToClipboard = () => {
    const value = !!rawDocument ? rawDocument : JSON.stringify(document)
    navigator.clipboard.writeText(value).then(() => useToaster('Copied to clipboard !'))
  }
  const handleClose = () => {
    setIsGraphViewSelected(false)
    onClose()
  }

  return (
    <Modal isOpen={isOpen} onClose={handleClose} isCentered size={'6xl'}>
      <ModalOverlay />
      <ModalContent bgGradient={gradients.primary} color={'white'} data-testid="doc-view-modal">
        <ModalHeader>{docName}</ModalHeader>
        <ModalCloseButton />
        <ModalBody>
          <Flex justifyContent={'space-between'}>
            <HStack mb={3}>
              <Text color={!isGraphViewSelected ? 'inherit' : 'gray'}>JSON</Text>
              <Switch onChange={e => setIsGraphViewSelected(e.target.checked)} />
              <Text color={isGraphViewSelected ? 'inherit' : 'gray'}>Graph</Text>
            </HStack>
            <IconButton variant="subtle" onClick={copyToClipboard} aria-label="copy to clipboard" icon={<FiCopy />} />
          </Flex>
          {!!rawDocument && (
            <Flex direction={'column'} pb={5}>
              JWT Payload:
              <CodeEditor value={rawDocument} height={'30px'} readOnly />
            </Flex>
          )}
          {!isGraphViewSelected ? (
            <CodeEditor value={JSON.stringify(document, null, 2)} height={'min(600px, calc(100vh - 200px))'} readOnly />
          ) : (
            <Center>
              <VCGraph json={JSON.stringify(document)} />
            </Center>
          )}
        </ModalBody>

        <ModalFooter>
          <Button colorScheme="blue" mr={3} onClick={handleClose}>
            Close
          </Button>
        </ModalFooter>
      </ModalContent>
    </Modal>
  )
}
