import React, { useContext } from 'react'
import { FormControl, FormLabel, HStack, Select } from '@chakra-ui/react'
import { capitalize } from '@/utils/util'
import { GaiaXDeploymentPath } from '@/sharedTypes'
import { ClearingHousesContext } from '@/contexts/ClearingHousesContext'

export const SelectClearingHouse: React.FC = () => {
  const {
    providers,
    gxDeploymentPaths,
    setSelectedClearingHouse,
    getClearingHouseByName,
    setSelectedGxDeploymentPath,
    selectedGxDeploymentPath,
    selectedClearingHouse,
    getPathVersionName
  } = useContext(ClearingHousesContext)

  const handleSelectClearingHouse = (provider: string) => {
    const clearingHouse = getClearingHouseByName(provider)
    if (clearingHouse) setSelectedClearingHouse(clearingHouse)
  }

  const handleSelectGxDeploymentPath = (version: GaiaXDeploymentPath) => setSelectedGxDeploymentPath(version)

  return (
    <HStack alignItems={'flex-end'} justifyContent={'flex-end'}>
      <FormControl color={'white'} w={'auto'}>
        {gxDeploymentPaths ? <FormLabel>Environment</FormLabel> : <FormLabel>Clearing House</FormLabel>}
        <Select
          w={{ base: '160px', md: '200px' }}
          value={selectedClearingHouse.name}
          onChange={e => handleSelectClearingHouse(e.target.value)}
          data-testid={'clearing-house-select'}
        >
          {providers.map(provider => (
            <option key={provider} value={provider} style={{ color: 'black' }}>
              {capitalize(provider)}
            </option>
          ))}
        </Select>
      </FormControl>
      <FormControl color={'white'} w={'auto'}>
        {gxDeploymentPaths ? <FormLabel>Path</FormLabel> : <FormLabel>Version</FormLabel>}
        <Select
          w={{ base: '160px', sm: '300px' }}
          value={gxDeploymentPaths ? selectedGxDeploymentPath : 'v1'}
          onChange={e => handleSelectGxDeploymentPath(e.target.value as GaiaXDeploymentPath)}
          data-testid={'version-select'}
        >
          {gxDeploymentPaths ? (
            gxDeploymentPaths.map(version => (
              <option key={version} value={version} style={{ color: 'black' }}>
                {version} ({getPathVersionName(version)})
              </option>
            ))
          ) : (
            <option key={'v1'} value={'v1'} style={{ color: 'black' }}>
              v1 ({getPathVersionName('v1')})
            </option>
          )}
        </Select>
      </FormControl>
    </HStack>
  )
}
