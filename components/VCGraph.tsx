import React, { useRef } from 'react'

interface VCGraphProps {
  json: string
  height?: string
  width?: string
}

export const VCGraph: React.FC<VCGraphProps> = ({ json, height = 'min(600px, calc(100vh - 200px))', width = '100%' }) => {
  const jsonCrackRef = useRef(null)

  const loadGraph = () => {
    if (jsonCrackRef.current) {
      const iframe = jsonCrackRef.current as HTMLIFrameElement
      iframe.contentWindow?.postMessage({ json }, '*')
    }
  }

  return <iframe height={height} width={width} onLoad={loadGraph} ref={jsonCrackRef} id="jsoncrackEmbed" src="https://jsoncrack.com/widget" />
}
