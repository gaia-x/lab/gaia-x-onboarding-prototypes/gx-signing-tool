import React, { ChangeEvent, useContext } from 'react'
import { Button, Flex, HStack, Select, Text } from '@chakra-ui/react'
import { ArrowLeftIcon, ArrowRightIcon } from '@chakra-ui/icons'
import { CodeEditor } from '@/components/ui/CodeEditor'
import { PolicyStepperContext } from '@/contexts/PolicyStepperContext'
import CanDisplay from '@/data/policyExamples/requests/CanDisplay.json'
import CanPlay from '@/data/policyExamples/requests/CanPlay.json'
import { Container } from '@/components/ui/Container'

export const ConsumerUsagePolicy: React.FC = () => {
  const { stepper, odrlRequest, setOdrlRequest, handleLastStep } = useContext(PolicyStepperContext)

  const policyExamples: any[] = [
    { key: 'Can Display', value: CanDisplay },
    { key: 'Can Play', value: CanPlay }
  ]

  const handleExampleChange = (e: ChangeEvent<HTMLSelectElement>) => {
    if (!e.target.value) {
      setOdrlRequest('')
      return
    }
    const example = policyExamples.find(obj => obj.key === e.target.value)?.value
    if (example) {
      setOdrlRequest(JSON.stringify(example, null, 2))
    }
  }

  const handleSubmit = async () => {
    await handleLastStep()
    stepper.goToNext()
  }

  return (
    <Flex flexDirection={'column'} alignItems={'center'} w={'100%'}>
      <Container>
        <Flex alignItems={'center'} justifyContent={'space-between'} py={2} w={'100%'}>
          <Text fontSize={16} alignSelf={'center'}>
            Consumer Usage Policy: (in ODRL)
          </Text>
          <Select w={{ base: '50%', sm: '30%' }} onChange={handleExampleChange} placeholder={'Examples'}>
            {policyExamples.map(({ key }) => (
              <option key={key} value={key} style={{ color: 'black' }}>
                {key}
              </option>
            ))}
          </Select>
        </Flex>
        <CodeEditor width={'800px'} value={odrlRequest} onChange={value => setOdrlRequest(value)} height={'50vh'} />
        <HStack mt={10} justifyContent={'center'}>
          <Button colorScheme={'yellow'} leftIcon={<ArrowLeftIcon />} onClick={stepper.goToPrevious}>
            Previous
          </Button>
          <Button isDisabled={!odrlRequest} type={'submit'} colorScheme={'green'} rightIcon={<ArrowRightIcon />} onClick={handleSubmit}>
            Next
          </Button>
        </HStack>
      </Container>
    </Flex>
  )
}
