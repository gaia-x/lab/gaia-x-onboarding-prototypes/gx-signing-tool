import React, { useContext, useMemo } from 'react'
import { Flex, Heading } from '@chakra-ui/react'
import { Container } from '../../ui/Container'
import { PolicyCredentialsHolderColumn } from '@/components/PolicyPlayground/PolicySteps/PolicyCredentialsHolderColumn'
import { PolicyStepperContext } from '@/contexts/PolicyStepperContext'

export const PolicyCredentialsHolder: React.FC = () => {
  const { credentials } = useContext(PolicyStepperContext)

  const documents = useMemo(() => {
    return Array.from(credentials.values())
  }, [credentials])

  return (
    <Container>
      <Flex fontSize={20} w="100%" alignItems={'center'} p={3}>
        <Heading ml={3} size={'sm'} fontWeight={'bold'}>
          Credentials used during policy reasoning:
        </Heading>
      </Flex>
      <Flex w={'100%'} h={'100%'}>
        <PolicyCredentialsHolderColumn credentials={documents} title={''} />
      </Flex>
    </Container>
  )
}
