import React, { useContext } from 'react'
import { Button, Flex, Heading } from '@chakra-ui/react'
import { CodeEditor } from '@/components/ui/CodeEditor'
import { ArrowLeftIcon } from '@chakra-ui/icons'
import { PolicyStepperContext } from '@/contexts/PolicyStepperContext'

export const AgreementStep: React.FC = () => {
  const { stepper, odrlAgreement } = useContext(PolicyStepperContext)

  return (
    <Flex flexDirection={'column'} w={'100%'} px={5}>
      {odrlAgreement ? (
        <Flex flexDirection={'column'} alignItems={'center'}>
          <Flex alignItems={'center'}>
            <Heading my={3} size={'md'}>
              ODRL Agreement
            </Heading>
          </Flex>
          <CodeEditor width={'60%'} readOnly value={JSON.stringify(odrlAgreement, null, 2)} />
        </Flex>
      ) : (
        <Flex alignItems={'center'} justifyContent={'center'} mt={'5'} mb={'5'} fontWeight={'bold'} fontSize={'18'}>
          No agreement reached !
        </Flex>
      )}
      <Flex justifyContent={'center'} alignItems={'center'} mt={5}>
        <Button colorScheme={'yellow'} leftIcon={<ArrowLeftIcon />} onClick={stepper.goToPrevious} mr={2}>
          Previous
        </Button>
      </Flex>
    </Flex>
  )
}
