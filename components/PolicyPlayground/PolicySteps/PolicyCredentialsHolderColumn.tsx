import { Flex, Grid, GridItem, Heading, Text } from '@chakra-ui/react'
import { StoredWalletEntry } from '@/models/wallet'
import { ImportDocument } from '@/components/Home/Holder/ImportDocument'
import React from 'react'
import { ConnectWallet } from '@/components/Home/ConnectWallet/ConnectWallet'
import { PolicyCredential } from '@/components/PolicyPlayground/PolicySteps/PolicyCredential'

interface HolderColumnProps {
  credentials: StoredWalletEntry[]
  title: string
}

export const PolicyCredentialsHolderColumn: React.FC<HolderColumnProps> = ({ credentials, title }) => {
  return (
    <Flex flexDirection={'column'} w={'100%'} alignItems={'center'}>
      <Heading>{title}</Heading>
      <Flex justifyContent={'space-between'} w={'100%'} px={6} mt={4}>
        <ImportDocument saveInPolicyContext />
        <Text ml={2} mr={2} alignSelf={'center'}>
          OR
        </Text>
        <ConnectWallet />
      </Flex>
      <Grid
        px={6}
        mt={6}
        templateColumns={{
          base: 'repeat(1, 1fr)',
          '2xl': 'repeat(2, 1fr)',
          '3xl': 'repeat(3, 1fr)'
        }}
        gap={4}
        w={'100%'}
      >
        {credentials.map(vc => (
          <GridItem key={vc.key}>
            <PolicyCredential id={vc.key} credential={vc} />
          </GridItem>
        ))}
      </Grid>
    </Flex>
  )
}
