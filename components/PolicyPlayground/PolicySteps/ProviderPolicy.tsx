import React, { ChangeEvent, useContext } from 'react'
import { Box, Button, Flex, Heading, Link, Select, Text } from '@chakra-ui/react'
import { ArrowRightIcon } from '@chakra-ui/icons'
import { CodeEditor } from '@/components/ui/CodeEditor'
import { PolicyStepperContext } from '@/contexts/PolicyStepperContext'
import LegalParticipantSubdivisionCode from '@/data/policyExamples/offers/LegalParticipantSubdivisionCode.json'
import LegalRegistrationNumber from '@/data/policyExamples/offers/LegalRegistrationNumber.json'
import { Container } from '../../ui/Container'
import { ImportDocument } from '@/components/Home/Holder/ImportDocument'
import { ConnectWallet } from '@/components/Home/ConnectWallet/ConnectWallet'

export const ProviderPolicy: React.FC = () => {
  const { stepper, odrlOffer, setOdrlOffer, handleOfferPolicy, serviceOffering, setServiceOffering } = useContext(PolicyStepperContext)

  const policyExamples: any[] = [
    { key: 'gx:LegalParticipant Address Constraint', value: LegalParticipantSubdivisionCode },
    { key: 'gx:LegalRegistrationNumber Constraint', value: LegalRegistrationNumber }
  ]
  const handleExampleChange = (e: ChangeEvent<HTMLSelectElement>) => {
    setServiceOffering(undefined)
    if (!e.target.value) {
      setOdrlOffer('')
      return
    }
    const example = policyExamples.filter(obj => obj.key === e.target.value)[0]?.value
    if (example) {
      setOdrlOffer(JSON.stringify(example, null, 2))
    }
  }

  const handleSubmit = async () => {
    await handleOfferPolicy()
    stepper.goToNext()
  }

  const handleDeleteServiceOffering = () => {
    setServiceOffering(undefined)
    setOdrlOffer('')
  }

  return (
    <Flex flexDirection={'column'} alignItems={'center'} w={'100%'}>
      <Heading>Policy Decision Point (Demonstration purposes)</Heading>
      <Link href={'https://gitlab.com/gaia-x/lab/policy-reasoning/odrl-vc-profile'} style={{ textDecoration: 'underline' }}>
        ODRL Profile Specifications
      </Link>
      <Text mt={5} maxW={'600px'}>
        Follow the steps in order to apply reasoning between a Provider's Service Offering Policy and a Consumer Usage Policy while also evaluating
        the Consumer's credentials. Credential enforceable verifications could be defined in JSONPath in an ODRL rule constraint. (Examples available
        below)
      </Text>

      <Container mt={'2'}>
        <Container>
          <Flex fontSize={20} w="100%" alignItems={'center'} p={3} justifyContent={'center'}>
            <Heading ml={3} size={'sm'} fontWeight={'bold'}>
              Import Service Offering VC:
            </Heading>
          </Flex>

          {!serviceOffering ? (
            <Flex justifyContent={'center'} w={'100%'} px={6} mt={4}>
              <ImportDocument saveInPolicyContext />
              <Text ml={2} mr={2} alignSelf={'center'} fontWeight={'bold'} textDecoration={'underline'}>
                OR
              </Text>
              <ConnectWallet />
            </Flex>
          ) : (
            <Button ml={3} colorScheme={'red'} onClick={() => handleDeleteServiceOffering()}>
              Remove Credential
            </Button>
          )}
        </Container>
        <Text fontSize={16} fontWeight={'bold'} textDecoration={'underline'} alignSelf={'center'}>
          OR
        </Text>
        <Box width={'40vw'}>
          <Flex alignItems={'center'} justifyContent={'space-between'} py={2}>
            <Text fontSize={16} alignSelf={'center'}>
              Provider Service Offering Policy: (in ODRL)
            </Text>
            <Select w={{ base: '50%', sm: '30%' }} onChange={handleExampleChange} placeholder={'Examples'}>
              {policyExamples.map(({ key }) => (
                <option key={key} value={key} style={{ color: 'black' }}>
                  {key}
                </option>
              ))}
            </Select>
          </Flex>
          <CodeEditor readOnly={!!serviceOffering} value={odrlOffer} onChange={value => setOdrlOffer(value)} height={'50vh'} />
        </Box>
        <Button
          isDisabled={!odrlOffer}
          w={'100px'}
          mt={2}
          colorScheme={'green'}
          rightIcon={<ArrowRightIcon />}
          onClick={handleSubmit}
          alignSelf={'center'}
        >
          Start
        </Button>
      </Container>
    </Flex>
  )
}
