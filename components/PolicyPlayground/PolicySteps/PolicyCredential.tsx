import React, { useContext } from 'react'
import { Button, Flex, Heading, HStack, Text, useDisclosure } from '@chakra-ui/react'
import { StoredWalletEntry } from '@/models/wallet'
import { ViewDocumentModal } from '@/components/ViewDocumentModal'
import { PolicyStepperContext } from '@/contexts/PolicyStepperContext'

interface SignedDocumentProps {
  id: string
  credential: StoredWalletEntry
}

export const PolicyCredential: React.FC<SignedDocumentProps> = ({ id, credential }) => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const { deleteCredential } = useContext(PolicyStepperContext)

  let type: string
  if (Array.isArray(credential.document.credentialSubject)) {
    type = credential.document.credentialSubject.map(s => s.type ?? s['@type'] ?? 'unknown').join(', ')
  } else {
    type = credential.document?.credentialSubject?.type || credential.type
  }

  return (
    <Flex backgroundColor={`rgba(255, 255, 255, 0.2)`} p={3} borderRadius={'md'} w={'100%'} h={'100%'}>
      <Flex flexDirection={'column'} justifyContent={'space-between'} w={'100%'}>
        <Flex w={'100%'} justifyContent={'space-between'} alignItems={'center'}>
          <Heading size={'sm'}>{credential.name}</Heading>
        </Flex>
        <Text mt={3}>{type}</Text>
        <HStack mt={3}>
          <Button colorScheme={'blue'} onClick={onOpen}>
            View
          </Button>
          <Button colorScheme={'red'} onClick={() => deleteCredential(id)}>
            Delete
          </Button>
        </HStack>
      </Flex>
      <ViewDocumentModal
        docName={credential.name}
        document={credential.document}
        rawDocument={credential.rawDocument}
        isOpen={isOpen}
        onClose={onClose}
      />
    </Flex>
  )
}
