import React, { useContext } from 'react'
import { Box, Button, Flex, HStack, Text } from '@chakra-ui/react'
import { ArrowLeftIcon, ArrowRightIcon } from '@chakra-ui/icons'
import { PolicyStepperContext } from '@/contexts/PolicyStepperContext'
import { PolicyCredentialsHolder } from '@/components/PolicyPlayground/PolicySteps/PolicyCredentialsHolder'

export const ConsumerCredentials: React.FC = () => {
  const { stepper, requestedCredentials, credentials } = useContext(PolicyStepperContext)

  const areCredentialsProvided = requestedCredentials.every(rc =>
    Array.from(credentials.values())
      .map(c => c.document.credentialSubject.type)
      .includes(rc)
  )

  return (
    <Flex flexDirection={'column'} alignItems={'center'} w={'100%'}>
      <Box mt={8} maxW={'800px'}>
        Please provide the following Credentials :
        {requestedCredentials.map(credential => (
          <Text fontWeight={'bold'} key={credential}>
            {credential}
          </Text>
        ))}
      </Box>
      <Flex p={4} m={{ base: 0, sm: 4 }} justifyContent={'flex-start'} flexDirection={'column'} alignItems={'center'}>
        <PolicyCredentialsHolder />
      </Flex>
      <HStack mt={10}>
        <Button colorScheme={'yellow'} leftIcon={<ArrowLeftIcon />} onClick={stepper.goToPrevious}>
          Previous
        </Button>
        <Button isDisabled={!areCredentialsProvided} type={'submit'} colorScheme={'green'} rightIcon={<ArrowRightIcon />} onClick={stepper.goToNext}>
          Next
        </Button>
      </HStack>
    </Flex>
  )
}
