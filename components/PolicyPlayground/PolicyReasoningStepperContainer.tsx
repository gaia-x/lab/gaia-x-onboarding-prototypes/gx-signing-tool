import { Flex, Step, StepIcon, StepIndicator, StepNumber, StepSeparator, StepStatus, StepTitle, Box } from '@chakra-ui/react'
import { Stepper as ChakraStepper } from '@chakra-ui/stepper'
import React, { useContext, useEffect } from 'react'
import { PolicyStepperContext } from '@/contexts/PolicyStepperContext'
import { ProviderPolicy } from '@/components/PolicyPlayground/PolicySteps/ProviderPolicy'
import { ConsumerUsagePolicy } from '@/components/PolicyPlayground/PolicySteps/ConsumerUsagePolicy'
import { ConsumerCredentials } from '@/components/PolicyPlayground/PolicySteps/ConsumerCredentials'
import { AgreementStep } from '@/components/PolicyPlayground/PolicySteps/AgreementStep'
import { AvailableWallets } from '@/models/wallet'
import { LocalWallet } from '@/components/Wallet/LocalWallet/LocalWallet'
import { WalletContext } from '@/contexts/WalletContext'
import { DecentralizedWebNodeWallet } from '@/components/Wallet/DecentralizedWebNodeWallet/DecentralizedWebNodeWallet'

export const PolicyReasoningStepperContainer: React.FC = ({}) => {
  const { stepper } = useContext(PolicyStepperContext)
  const { isWalletOpen, connectedWallet, setWalletInPolicyContext } = useContext(WalletContext)
  useEffect(() => {
    setWalletInPolicyContext(true)
  })

  return (
    <Box>
      <Flex flexDirection={'column'} minH={'100vh'} h={'100%'} w={isWalletOpen ? '50%' : '100%'}>
        <Flex p={4} justifyContent={'flex-start'} flexDirection={'column'} alignItems={'center'}>
          <ChakraStepper index={stepper.activeStep} colorScheme={'blue'} color={'white'} mt={8} flexWrap={'wrap'}>
            {stepper.steps.map((step, index) => (
              <Step key={index}>
                <StepIndicator mx={2}>
                  <StepStatus complete={<StepIcon />} incomplete={<StepNumber />} active={<StepNumber />} />
                </StepIndicator>
                <StepTitle>{step.description}</StepTitle>
                <StepSeparator />
              </Step>
            ))}
          </ChakraStepper>
          <Flex mt={8} color={'white'} w={'100%'} overflowX={'hidden'}>
            {stepper.activeStep === 0 && <ProviderPolicy />}
            {stepper.activeStep === 1 && <ConsumerCredentials />}
            {stepper.activeStep === 2 && <ConsumerUsagePolicy />}
            {stepper.activeStep === 3 && <AgreementStep />}
          </Flex>
        </Flex>
      </Flex>
      {isWalletOpen && connectedWallet === AvailableWallets.LOCAL_WALLET && <LocalWallet />}
      {isWalletOpen && connectedWallet === AvailableWallets.DECENTRALIZED_WEB_NODE && <DecentralizedWebNodeWallet />}
    </Box>
  )
}
