import React, { useContext, useMemo, useState } from 'react'
import { Box, Button, Center, Flex, FormControl, FormLabel, Heading, Stack, Switch, Tooltip, VStack } from '@chakra-ui/react'
import { Form, Formik, FormikProps } from 'formik'
import { LegalRegistrationNumberField } from '@/components/ui/Fields/LegalRegistrationNumberField'
import { FormField } from '@/components/ui/Fields/FormField'
import { CodeEditor } from '@/components/ui/CodeEditor'
import { WalletContext } from '@/contexts/WalletContext'
import { AvailableWallets, StoredWalletEntry } from '@/models/wallet'
import { VDocument } from '@/models/document'
import { generateName } from '@/utils/util'
import { useRouter } from 'next/router'
import { Container } from '@/components/ui/Container'
import { useErrorHandler } from '@/hooks/useError'
import { SignatureContext } from '@/contexts/SignatureContext'
import { AppContext } from '@/contexts/AppContext'
import { SelectClearingHouse } from '@/components/SelectClearingHouse'
import { useLRN } from '@/hooks/useLRN'
import { useWallet } from '@/hooks/useWallet'

export type ClaimNotaryVC = {
  '@context': string[]
  type: string
  id: string
  [key: string]: string | string[]
}

type LRNForm = {
  type: string
  value: string
  credentialSubjectId: string
  vcId: string
}

export const LegalRegistrationNumberContainer: React.FC = () => {
  const { privateKeyUse } = useContext(SignatureContext)
  const [notaryResponse, setNotaryResponse] = useState<VDocument>()
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [isCustomDid, setIsCustomDid] = useState<boolean>(false)
  const { addDocument } = useContext(AppContext)

  const documentName = useMemo(() => {
    return generateName()
  }, [])

  const { connectedWallet, setWalletIsOpen } = useContext(WalletContext)
  const { handleImport } = useWallet()

  const router = useRouter()
  const { errorHandler } = useErrorHandler()

  const { createLRN } = useLRN(isCustomDid)

  const handleSubmit = async (values: LRNForm) => {
    setIsLoading(true)

    try {
      const notaryResponse = await createLRN({
        type: values.type,
        value: values.value,
        csid: values.credentialSubjectId,
        vcid: values.vcId
      })
      setNotaryResponse(notaryResponse)
    } catch (error: any) {
      errorHandler(error, error.response.data)
    } finally {
      setIsLoading(false)
    }
  }

  const handleAddToHolder = async (saveInLocalWallet: boolean) => {
    if (!notaryResponse) return

    addDocument({
      document: notaryResponse,
      docName: documentName,
      privateKey: privateKeyUse.privateKey
    })

    if (saveInLocalWallet) {
      const entry: StoredWalletEntry = {
        type: notaryResponse.type,
        document: notaryResponse,
        creation: notaryResponse.proof.created,
        isVerified: false,
        name: documentName,
        key: crypto.randomUUID()
      }

      handleImport(entry)
      setWalletIsOpen(true)
    }

    await router.push('/')
  }

  const handleExit = async () => {
    setNotaryResponse(undefined)
  }

  return (
    <Flex
      h={{ base: 'auto', md: '80vh' }}
      px={{ base: 0, md: 5 }}
      flexDirection={'column'}
      alignItems={'center'}
      justifyContent={'space-between'}
      textAlign={'center'}
    >
      <Flex w={'100%'} justifyContent={'flex-end'} p={3}>
        <SelectClearingHouse />
      </Flex>
      <Container flexDirection={'row'} justifyContent={'space-around'} alignItems={'center'} p={{ base: 0, md: 10 }} mt={{ base: 5, md: 0 }}>
        <Flex flexDirection={'column'} alignItems={'center'} w={{ base: '100%', xl: '60%' }}>
          {!notaryResponse ? (
            <Heading color={'white'} maxW={'100%'}>
              Claim your Legal Registration Number Verifiable Credential from the Gaia-X Notary
            </Heading>
          ) : (
            <Heading color={'white'} maxW={'100%'}>
              Your Legal Registration Number Verifiable Credential ({documentName})
            </Heading>
          )}
          <Center mt={8}>
            {!notaryResponse ? (
              <Box color={'white'} maxW={'500px'}>
                <Formik
                  initialValues={{
                    type: '',
                    value: '',
                    vcId: '',
                    credentialSubjectId: ''
                  }}
                  onSubmit={async values => {
                    await handleSubmit(values)
                  }}
                >
                  {(props: FormikProps<any>) => (
                    <Form>
                      <VStack spacing={10} px={{ base: 2, md: 0 }}>
                        <Box>
                          <FormControl display="flex" justifyContent="space-between" mt={4} w={'100%'} data-testid={'did-custom-switch'}>
                            <Tooltip label="Only did:web are supported for now">
                              <FormLabel htmlFor="custom-issuer" mb="0">
                                I use my own DID solution
                              </FormLabel>
                            </Tooltip>
                            <Switch
                              size={'lg'}
                              colorScheme={'green'}
                              id="custom-issuer"
                              isChecked={isCustomDid}
                              onChange={() => setIsCustomDid(!isCustomDid)}
                            />
                          </FormControl>
                        </Box>
                        {isCustomDid && (
                          <>
                            <FormField label={'Verifiable Credential ID'} name={'vcId'} required />
                            <FormField label={'Credential subject ID'} name={'credentialSubjectId'} required />
                          </>
                        )}
                        <LegalRegistrationNumberField
                          name={'value'}
                          typeName={'type'}
                          label={'Legal registration number'}
                          required
                          setFieldValue={props.setFieldValue}
                          radioValue={props.values.type}
                        />
                        <Button type={'submit'} colorScheme={'green'} isLoading={isLoading} mb={{ base: 3, md: 0 }}>
                          Submit
                        </Button>
                      </VStack>
                    </Form>
                  )}
                </Formik>
              </Box>
            ) : (
              <Flex w={{ base: '80vw', md: '500px', lg: '800px', xl: '1000px' }} flexDirection={'column'}>
                <CodeEditor value={JSON.stringify(notaryResponse, null, 2)} readOnly />
                <Stack direction={{ base: 'column', md: 'row' }} mt={6} justify={'center'} spacing={5}>
                  {connectedWallet !== AvailableWallets.NONE && (
                    <>
                      <Button colorScheme={'green'} onClick={() => handleAddToHolder(false)}>
                        Add in holder
                      </Button>
                      <Button colorScheme={'green'} onClick={() => handleAddToHolder(true)}>
                        Add in holder & Save in local wallet
                      </Button>
                    </>
                  )}
                  <Button colorScheme={'red'} onClick={handleExit}>
                    Exit
                  </Button>
                </Stack>
              </Flex>
            )}
          </Center>
        </Flex>
      </Container>
    </Flex>
  )
}
