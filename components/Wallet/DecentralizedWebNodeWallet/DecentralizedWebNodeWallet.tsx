import { Alert, Box, CloseButton, Flex, IconButton, Input, Text } from '@chakra-ui/react'
import React, { useContext } from 'react'
import { WalletContext } from '@/contexts/WalletContext'
import { WalletHeader } from '@/components/Wallet/WalletHeader'
import { RepeatIcon } from '@chakra-ui/icons'
import { DecentralizedWebNodeContext } from '@/contexts/DecentralizedWebNodeContext'
import { DecentralizedWebNodeContent } from '@/components/Wallet/DecentralizedWebNodeWallet/DecentralizedWebNodeContent'
import { useDecentralizedWebNode } from '@/hooks/useDecentralizedWebNode'

export const DecentralizedWebNodeWallet: React.FC = () => {
  const { setWalletIsOpen } = useContext(WalletContext)
  const { refreshCredentials, decentralizedWebNodeUrl, setDecentralizedWebNodeUrl } = useContext(DecentralizedWebNodeContext)
  const { isLoading, acceptDisclaimer, shouldDisplayDisclaimer } = useDecentralizedWebNode()
  const onChangeNodeUrl = async (e: React.ChangeEvent<HTMLInputElement>) => {
    setDecentralizedWebNodeUrl(e.target.value)
  }
  const refreshNodeEntries = async () => {
    await refreshCredentials(decentralizedWebNodeUrl)
  }

  return (
    <Flex
      direction={'column'}
      alignItems={'center'}
      position={'absolute'}
      zIndex={2}
      top={['80px', '80px', '80px', '10px']}
      right={'10px'}
      minW={['auto', 'auto', 'auto', 'auto', 'auto', '500px']}
      minH={'800px'}
      maxH={'100vh'}
      p={3}
      color={'white'}
      bgGradient={'linear(to-br, rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.3))'}
      borderRadius={'md'}
      boxShadow={'lg'}
      data-testid="dwn-wallet"
    >
      <WalletHeader title={'Decentralized Web Node'} onClose={() => setWalletIsOpen(false)} />
      {shouldDisplayDisclaimer && (
        <Alert status="warning" borderRadius={4} mb={4} w={'90%'} color={'black'}>
          <Box>
            <Text fontWeight={'bold'}>Disclaimer</Text>
            <Text>Deleting credentials is not supported yet !</Text>
          </Box>
          <CloseButton position="absolute" right="8px" top="8px" onClick={acceptDisclaimer} />
        </Alert>
      )}
      <Flex justifyContent={'space-between'} w={'90%'}>
        <Input maxW={'85%'} value={decentralizedWebNodeUrl} onChange={onChangeNodeUrl} placeholder={'Node url'} data-testid="dwn-node-url" />
        <IconButton
          colorScheme={'green'}
          onClick={refreshNodeEntries}
          aria-label="Refresh wallet entries"
          icon={<RepeatIcon />}
          isLoading={isLoading}
        />
      </Flex>
      <DecentralizedWebNodeContent />
    </Flex>
  )
}
