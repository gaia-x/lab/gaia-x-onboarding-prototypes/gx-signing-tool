import { Button, Flex, HStack, Text, useDisclosure, VStack } from '@chakra-ui/react'
import React, { useContext } from 'react'
import { StoredWalletEntry } from '@/models/wallet'
import { ViewDocumentModal } from '@/components/ViewDocumentModal'
import { WalletContext } from '@/contexts/WalletContext'
import Image from 'next/image'
import jwtLogo from '@/public/jwt-logo.png'

export const DecentralizedWebNodeDocument: React.FC<{
  signedDocument: StoredWalletEntry
}> = ({ signedDocument }) => {
  const { handleExport } = useContext(WalletContext)

  const { isOpen, onOpen, onClose } = useDisclosure()

  return (
    <VStack w={'100%'} alignItems={'flex-start'} backgroundColor={'rgba(255, 255, 255, 0.2)'} p={3} borderRadius={'md'}>
      <Flex w={'100%'} justifyContent={'space-between'}>
        <Flex alignItems={'center'}>
          <Text fontWeight="bold">{signedDocument.name}</Text>
        </Flex>
        {signedDocument.format?.includes('jwt') && <Image src={jwtLogo} alt="jwt-logo" width={25} height={25} />}
      </Flex>
      <Text>{signedDocument.type}</Text>
      <Text>{new Date(signedDocument.creation).toLocaleString()}</Text>
      <HStack spacing={3}>
        <Button colorScheme={'green'} onClick={() => handleExport(signedDocument)}>
          Export
        </Button>
        <Button colorScheme={'blue'} onClick={onOpen}>
          View
        </Button>
      </HStack>
      <ViewDocumentModal
        docName={signedDocument.name}
        rawDocument={signedDocument.rawDocument}
        document={signedDocument.document}
        isOpen={isOpen}
        onClose={onClose}
      />
    </VStack>
  )
}
