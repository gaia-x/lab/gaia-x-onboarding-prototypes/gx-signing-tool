import { VStack } from '@chakra-ui/react'
import React, { useContext } from 'react'
import { DecentralizedWebNodeDocument } from '@/components/Wallet/DecentralizedWebNodeWallet/DecentralizedWebNodeDocument'
import { DecentralizedWebNodeContext } from '@/contexts/DecentralizedWebNodeContext'

export const DecentralizedWebNodeContent: React.FC = () => {
  const { walletEntries } = useContext(DecentralizedWebNodeContext)

  const sortedWalletEntries = walletEntries.sort((a, b) => {
    const aDate = new Date(a.creation)
    const bDate = new Date(b.creation)
    return bDate.getTime() - aDate.getTime()
  })

  return (
    <VStack maxH={'75vh'} overflowY={'auto'} w={'100%'} spacing={5} mt={5} px={5}>
      {sortedWalletEntries.map(walletEntry => (
        <DecentralizedWebNodeDocument key={walletEntry.key} signedDocument={walletEntry} />
      ))}
    </VStack>
  )
}
