import { Flex } from '@chakra-ui/react'
import React, { useContext } from 'react'
import { WalletHeader } from '@/components/Wallet/WalletHeader'
import { LocalWalletToolbar } from '@/components/Wallet/LocalWallet/LocalWalletToolbar'
import JSZip from 'jszip'
import { saveAs } from 'file-saver'
import { WalletContext } from '@/contexts/WalletContext'
import { LocalWalletContext } from '@/contexts/LocalWalletContext'
import { LocalWalletContent } from '@/components/Wallet/LocalWallet/LocalWalletContent'
import { gradients } from '@/customTheme'

export const LocalWallet: React.FC = () => {
  const { setWalletIsOpen } = useContext(WalletContext)
  const { selectedDocuments } = useContext(LocalWalletContext)

  const createZip = () => {
    const zip = new JSZip()
    const jsonFiles = selectedDocuments.map((entry, i) => ({
      filename: entry.name === 'Document' ? `Document-${i + 1}.json` : `${entry.name}.json`,
      content: JSON.stringify(entry.document, null, 2)
    }))

    jsonFiles.forEach(jsonFile => {
      zip.file(jsonFile.filename, jsonFile.content)
    })

    zip.generateAsync({ type: 'blob' }).then(content => {
      saveAs(content, 'credentials.zip')
    })
  }

  return (
    <Flex
      direction={'column'}
      alignItems={'center'}
      position={'absolute'}
      zIndex={2}
      top={{ base: '0px', md: '10px' }}
      right={{ base: 0, sm: '10px' }}
      minW={{ base: 'auto', xl: '500px' }}
      w={{ base: '100%', md: 'auto' }}
      minH={'800px'}
      maxH={'100vh'}
      p={3}
      color={'white'}
      bgGradient={{ base: gradients.primary, xl: 'linear(to-br, rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.3))' }}
      borderRadius={'md'}
      boxShadow={'lg'}
      data-testid="local-wallet"
    >
      <WalletHeader title={'Local Wallet'} onClose={() => setWalletIsOpen(false)} />
      <LocalWalletToolbar onDownload={createZip} />
      <LocalWalletContent />
    </Flex>
  )
}
