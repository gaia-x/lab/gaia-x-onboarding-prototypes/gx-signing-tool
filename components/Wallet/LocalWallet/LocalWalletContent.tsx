import { VStack } from '@chakra-ui/react'
import React from 'react'
import { LocalWalletDocument } from '@/components/Wallet/LocalWallet/LocalWalletDocument'
import { useLocalWallet } from '@/hooks/useLocalWallet'

export const LocalWalletContent: React.FC = () => {
  const { walletEntries } = useLocalWallet()

  const sortedWalletEntries = walletEntries.sort((a, b) => {
    const aDate = new Date(a.creation)
    const bDate = new Date(b.creation)
    return bDate.getTime() - aDate.getTime()
  })

  return (
    <VStack maxH={'75vh'} overflowY={'auto'} w={'100%'} spacing={5} mt={5} px={5}>
      {sortedWalletEntries.map(walletEntry => (
        <LocalWalletDocument key={walletEntry.key} signedDocument={walletEntry} />
      ))}
    </VStack>
  )
}
