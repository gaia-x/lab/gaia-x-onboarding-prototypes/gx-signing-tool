import {
  Button,
  Checkbox,
  Flex,
  HStack,
  IconButton,
  Input,
  Popover,
  PopoverArrow,
  PopoverBody,
  PopoverCloseButton,
  PopoverContent,
  PopoverHeader,
  PopoverTrigger,
  Text,
  useDisclosure,
  VStack
} from '@chakra-ui/react'
import React, { useContext, useEffect, useState } from 'react'
import { StoredWalletEntry } from '@/models/wallet'
import { CheckIcon, EditIcon } from '@chakra-ui/icons'
import { VerifiedByComplianceTag } from '@/components/VerifiedByComplianceTag'
import { ViewDocumentModal } from '@/components/ViewDocumentModal'
import { WalletContext } from '@/contexts/WalletContext'
import Image from 'next/image'
import jwtLogo from '@/public/jwt-logo.png'
import { LocalWalletContext } from '@/contexts/LocalWalletContext'

export const LocalWalletDocument: React.FC<{
  signedDocument: StoredWalletEntry
}> = ({ signedDocument }) => {
  const { handleExport } = useContext(WalletContext)
  const { handleDelete, handleLocalWalletImport, selectedDocuments, setSelectedDocuments } = useContext(LocalWalletContext)

  const { isOpen, onOpen, onClose } = useDisclosure()
  const [isEditing, setIsEditing] = useState(false)
  const [newName, setNewName] = useState(signedDocument.name)
  const [isSelected, setIsSelected] = useState<boolean>(selectedDocuments.includes(signedDocument))

  const handleUpdateName = () => {
    handleDelete(signedDocument.key)
    handleLocalWalletImport({ ...signedDocument, name: newName })
    setIsEditing(false)
  }

  const handleSelect = () => {
    if (isSelected) {
      setSelectedDocuments(prevState => prevState.filter(item => item.key !== signedDocument.key))
    } else {
      setSelectedDocuments(prevState => [...prevState, signedDocument])
    }
  }

  useEffect(() => {
    if (selectedDocuments.filter(item => item.key === signedDocument.key).length > 0) {
      setIsSelected(true)
    } else {
      setIsSelected(false)
    }
  }, [selectedDocuments])

  return (
    <VStack w={'100%'} alignItems={'flex-start'} backgroundColor={'rgba(255, 255, 255, 0.2)'} p={3} borderRadius={'md'}>
      {isEditing ? (
        <Flex>
          <Input value={newName} onChange={e => setNewName(e.target.value)} />
          <IconButton colorScheme={'green'} aria-label={'Save'} icon={<CheckIcon />} onClick={handleUpdateName} />
        </Flex>
      ) : (
        <Flex w={'100%'} justifyContent={'space-between'}>
          <Flex alignItems={'center'}>
            <Checkbox mr={2} isChecked={isSelected} onChange={handleSelect} />
            <Text fontWeight="bold">{signedDocument.name}</Text>
            <EditIcon cursor={'pointer'} ml={1} onClick={() => setIsEditing(true)} />
          </Flex>
          {!!signedDocument.isVerified && <VerifiedByComplianceTag version={signedDocument.isVerified} />}
          {signedDocument.format?.includes('jwt') && <Image src={jwtLogo} alt="jwt-logo" width={25} height={25} />}
        </Flex>
      )}
      <Text>{signedDocument.type}</Text>
      <Text>{new Date(signedDocument.creation).toLocaleString()}</Text>
      <HStack spacing={3}>
        <Button colorScheme={'green'} onClick={() => handleExport(signedDocument)}>
          Export
        </Button>
        <Button colorScheme={'blue'} onClick={onOpen}>
          View
        </Button>
        <Popover>
          <PopoverTrigger>
            <Button colorScheme={'red'}>Delete</Button>
          </PopoverTrigger>
          <PopoverContent color={'black'}>
            <PopoverArrow />
            <PopoverCloseButton />
            <PopoverHeader>Confirmation !</PopoverHeader>
            <PopoverBody>
              <Flex direction={'column'} justifyContent={'space-around'} alignItems={'center'}>
                <Text>Are you sure you want to delete this document ?</Text>
                <Button ml={3} colorScheme={'red'} onClick={() => handleDelete(signedDocument.key)}>
                  Yes
                </Button>
              </Flex>
            </PopoverBody>
          </PopoverContent>
        </Popover>
      </HStack>

      <ViewDocumentModal
        docName={signedDocument.name}
        rawDocument={signedDocument.rawDocument}
        document={signedDocument.document}
        isOpen={isOpen}
        onClose={onClose}
      />
    </VStack>
  )
}
