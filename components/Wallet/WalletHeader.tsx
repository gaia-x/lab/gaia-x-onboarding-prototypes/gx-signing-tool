import React, { useContext } from 'react'
import { CloseButton, Divider, Flex, Heading, IconButton } from '@chakra-ui/react'
import { FiLogOut } from 'react-icons/fi'
import { WalletContext } from '@/contexts/WalletContext'
import { AvailableWallets } from '@/models/wallet'

interface WalletHeaderProps {
  title: string
  onClose: () => void
}

export const WalletHeader: React.FC<WalletHeaderProps> = ({ title, onClose }) => {
  const { setConnectedWallet } = useContext(WalletContext)
  const logoutOfWallet = () => {
    setConnectedWallet(AvailableWallets.NONE)
    onClose()
  }
  return (
    <Flex direction={'column'} w={'100%'} alignItems={'center'}>
      <Flex justifyContent={'space-between'} w={'100%'}>
        <Heading size={'lg'} ml={3}>
          {title}
        </Heading>
        <Flex alignItems={'center'}>
          <IconButton variant="subtle" onClick={logoutOfWallet} aria-label="Logout of wallet" icon={<FiLogOut />} />
          <CloseButton color={'white'} onClick={onClose} />
        </Flex>
      </Flex>
      <Divider my={3} w={'100%'} />
    </Flex>
  )
}
