import React, { ChangeEvent, useEffect } from 'react'
import { Avatar, Box, Button, Card, CardHeader, Flex, Heading, IconButton, Link, ListItem, Select, Text, UnorderedList } from '@chakra-ui/react'
import { FiLogIn } from 'react-icons/fi'
import { FaAddressCard, FaCode, FaTrashAlt, FaWallet } from 'react-icons/fa'
import { CodeEditor } from '@/components/ui/CodeEditor'
import { Container } from '@/components/ui/Container'
import { useMembershipCredential } from '@/hooks/useMembershipCrendential'
import { getUrlParameter, cleanQueryParams } from '@/utils/util'
import { MembershipCredentialFormat, MembershipCredentialType } from '@/models/membershipCredentialInfo'
import Image from 'next/image'
import svgGaiaxLogo from '@/public/gaiax-logo.svg'
import { QRCodeSVG } from 'qrcode.react'

export const MembershipCredentialContainer: React.FC = () => {
  const {
    getMembershipCredential,
    membershipCredential,
    isLoading,
    credentialType,
    saveMembershipCredentialType,
    cleanMembershipCredential,
    displayFormat,
    setDisplayFormat,
    membershipCredentialInfo,
    requestOIDCUrl,
    oidcResponse,
    loginUrl
  } = useMembershipCredential()

  const isIOS = () => {
    return (
      ['iPad Simulator', 'iPhone Simulator', 'iPod Simulator', 'iPad', 'iPhone', 'iPod'].includes(navigator.platform) ||
      // iPad on iOS 13 detection
      (navigator.userAgent.includes('Mac') && 'ontouchend' in document)
    )
  }
  useEffect(() => {
    const code = getUrlParameter('code')
    cleanQueryParams()
    if (code) {
      getMembershipCredential(code)
    }
  }, [])
  const login = () => {
    window.location.replace(loginUrl)
  }

  const handleCredentialTypeChange = (e: ChangeEvent<HTMLSelectElement>) => {
    saveMembershipCredentialType(e.target.value as MembershipCredentialType)
  }

  const triggerOIDC = async () => {
    setDisplayFormat(MembershipCredentialFormat.OIDC)
    await requestOIDCUrl()
  }

  return (
    <Flex
      h={{ base: '100vh', md: '80vh' }}
      px={{ base: 0, md: 10 }}
      flexDirection={'column'}
      alignItems={'center'}
      justifyContent={'space-around'}
      textAlign={'center'}
      mt={6}
    >
      <Container flexDirection={'row'} justifyContent={'space-around'} alignItems={'center'} p="5" w={'100%'}>
        <Flex flexDirection={'column'} alignItems={'center'} w={'100%'}>
          <Heading>Membership Credential Portal</Heading>
          <Text mt={5} maxW={'600px'}>
            Access your Gaia-X Membership Verifiable Credential by logging into the Members' platform (Exclusive to members).
          </Text>
          {!!membershipCredential ? (
            <Container mt={6} w={'100%'}>
              <Flex flexDirection={'row'} alignItems={'center'} justifyContent={'space-between'} mb={'12px'}>
                {displayFormat !== MembershipCredentialFormat.CARD && (
                  <Button
                    w={'100px'}
                    colorScheme={'blue'}
                    m="6px"
                    rightIcon={<FaAddressCard />}
                    onClick={() => setDisplayFormat(MembershipCredentialFormat.CARD)}
                    alignSelf={'center'}
                  >
                    Card
                  </Button>
                )}
                {displayFormat !== MembershipCredentialFormat.JSON_LD && (
                  <Button
                    w={'110px'}
                    colorScheme={'blue'}
                    m={{ base: '2px', md: '6px' }}
                    rightIcon={<FaCode />}
                    onClick={() => setDisplayFormat(MembershipCredentialFormat.JSON_LD)}
                    alignSelf={'center'}
                  >
                    JSON-LD
                  </Button>
                )}
                {displayFormat !== MembershipCredentialFormat.OIDC && (
                  <Button
                    w={'100px'}
                    m={{ base: '2px', md: '6px' }}
                    colorScheme={'blue'}
                    rightIcon={<FaWallet />}
                    onClick={triggerOIDC}
                    alignSelf={'center'}
                  >
                    OIDC
                  </Button>
                )}
                <IconButton
                  colorScheme={'red'}
                  m={{ base: '2px', md: '6px' }}
                  icon={<FaTrashAlt />}
                  onClick={cleanMembershipCredential}
                  alignSelf={'center'}
                  aria-label="Reset"
                />
              </Flex>
              {displayFormat === MembershipCredentialFormat.CARD && (
                <Card maxW={{ base: '100%', lg: '60%' }} alignSelf={'center'}>
                  <CardHeader>
                    <Flex flexDirection={{ base: 'column', sm: 'row' }}>
                      <Flex flex="1" gap="4" alignItems="center" flexWrap="wrap">
                        <Avatar name={membershipCredentialInfo?.name} color={'black'} backgroundColor={'#E2E8F0'} />
                        <Box display={{ base: 'flex', sm: 'none' }} ml={'auto'}>
                          <Image src={svgGaiaxLogo} alt="gaia-x" width={60} height={60} />
                        </Box>
                        <Box>
                          <Heading textAlign={'start'} size="sm">
                            {membershipCredentialInfo?.name}
                          </Heading>
                          <Text textAlign={'start'}>{membershipCredentialInfo?.email}</Text>
                          <Text textAlign={'start'}>{membershipCredentialInfo?.membershipNumber}</Text>
                        </Box>
                      </Flex>
                      <Box display={{ base: 'none', sm: 'flex' }}>
                        <Image src={svgGaiaxLogo} alt="gaia-x" width={60} height={60} />
                      </Box>
                    </Flex>
                  </CardHeader>
                </Card>
              )}
              {displayFormat === MembershipCredentialFormat.JSON_LD && (
                <CodeEditor readOnly value={JSON.stringify(membershipCredential, null, 2)} height={'60vh'} />
              )}
              {displayFormat === MembershipCredentialFormat.OIDC && oidcResponse && (
                <Flex flexDirection={'column'} justifyContent={'space-around'} alignItems={'center'}>
                  <UnorderedList textAlign={'left'} mt={'10px'}>
                    <ListItem>
                      First, please install a wallet supporting OpenID for VC Issuance such as{' '}
                      {!isIOS() ? (
                        <Text as={'span'} style={{ fontWeight: 'bold' }}>
                          <Link
                            href={'https://play.google.com/store/apps/details?id=co.talao.wallet'}
                            style={{
                              textDecoration: 'underline',
                              fontWeight: 'bold'
                            }}
                          >
                            Talao
                          </Link>{' '}
                          or{' '}
                          <Link
                            href={'https://play.google.com/store/apps/details?id=co.altme.alt.me.altme'}
                            style={{ textDecoration: 'underline', fontWeight: 'bold' }}
                          >
                            Altme
                          </Link>
                        </Text>
                      ) : (
                        <Text as={'span'} style={{ fontWeight: 'bold' }}>
                          <Link
                            href={'https://apps.apple.com/us/app/talao-wallet/id1582183266'}
                            style={{ textDecoration: 'underline', fontWeight: 'bold' }}
                          >
                            Talao
                          </Link>{' '}
                          or{' '}
                          <Link
                            href={'https://apps.apple.com/be/app/altme-wallet/id1633216869'}
                            style={{ textDecoration: 'underline', fontWeight: 'bold' }}
                          >
                            Altme
                          </Link>
                        </Text>
                      )}
                    </ListItem>
                    <ListItem>
                      Then in the wallet profiles use:{' '}
                      <Text as={'span'} style={{ fontWeight: 'bold' }}>
                        Custom
                      </Text>
                    </ListItem>
                    <ListItem display={{ base: 'list-item', sm: 'none' }}>
                      <Link href={oidcResponse?.uri} style={{ textDecoration: 'underline', fontWeight: 'bold' }} mt={'10px'}>
                        Use this link
                      </Link>
                    </ListItem>
                    <ListItem>
                      Then input the PIN:{' '}
                      <Text as={'span'} style={{ fontWeight: 'bold' }}>
                        {oidcResponse?.userPin}
                      </Text>
                    </ListItem>
                    <ListItem>
                      Afterwards, select the correct Credential Type:{' '}
                      <Text as={'span'} style={{ fontWeight: 'bold' }}>
                        {credentialType} Gaia-X Membership
                      </Text>
                    </ListItem>
                    <ListItem>Keep in mind that this action can only be performed once</ListItem>
                  </UnorderedList>
                  <Flex flexDirection={'column'} display={{ base: 'none', sm: 'flex' }}>
                    <Text align="center" fontSize="1em" style={{ textDecoration: 'underline' }} mt={3}>
                      Scan this QR code:
                    </Text>
                    <QRCodeSVG
                      value={oidcResponse?.uri}
                      size={300}
                      level={'M'}
                      includeMargin={true}
                      imageSettings={{
                        src: svgGaiaxLogo.src,
                        width: 60,
                        height: 40,
                        excavate: true
                      }}
                    />
                  </Flex>
                </Flex>
              )}
            </Container>
          ) : (
            <Container flexDirection={'column'} justifyContent={'space-around'} alignItems={'center'} p="5">
              <Select value={credentialType} w={'60%'} onChange={handleCredentialTypeChange} id="membership-credential-type">
                {Object.values(MembershipCredentialType).map(type => (
                  <option key={type} value={type} style={{ color: 'black' }}>
                    {type}
                  </option>
                ))}
              </Select>
              <Button isDisabled={isLoading} mt={6} colorScheme={'green'} rightIcon={<FiLogIn />} onClick={login} alignSelf={'center'}>
                Login and retrieve credential
              </Button>
            </Container>
          )}
        </Flex>
      </Container>
    </Flex>
  )
}
