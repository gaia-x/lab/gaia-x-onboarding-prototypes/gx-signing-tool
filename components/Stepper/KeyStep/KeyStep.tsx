import { Box, Button, Flex, HStack } from '@chakra-ui/react'
import React, { useContext, useEffect } from 'react'
import { PrivateKey } from '@/components/Home/Issuer/PrivateKey'
import { ArrowLeftIcon, ArrowRightIcon } from '@chakra-ui/icons'
import { StepperContext } from '@/contexts/StepperContext'
import { SignatureContext } from '@/contexts/SignatureContext'
import { useWebAuthn } from '@/hooks/useWebAuthn'

export const KeyStep: React.FC = () => {
  const { issuerUse, privateKeyUse } = useContext(SignatureContext)
  const { privateKeyChanged, setPrivateKeyChanged } = useContext(StepperContext)

  const { steps, handleCompleteKeyStep } = useContext(StepperContext)

  const { isWebauthnEnabled } = useWebAuthn()

  useEffect(() => {
    if (issuerUse.isCustomIssuer && !privateKeyChanged && !isWebauthnEnabled()) {
      privateKeyUse.setPrivateKey('')
    }
  }, [issuerUse.isCustomIssuer])

  return (
    <Flex flexDirection={'column'} alignItems={'center'} w={'100%'}>
      <Box w={'50%'}>
        <PrivateKey refreshEnabled={!issuerUse.isCustomIssuer} privateKeyChange={() => setPrivateKeyChanged(true)} />
      </Box>
      <HStack mt={10}>
        <Button colorScheme={'yellow'} leftIcon={<ArrowLeftIcon />} onClick={steps.goToPrevious}>
          Previous
        </Button>
        <Button colorScheme={'green'} rightIcon={<ArrowRightIcon />} onClick={handleCompleteKeyStep} isDisabled={!privateKeyUse.privateKey}>
          Next
        </Button>
      </HStack>
    </Flex>
  )
}
