import React, { ReactElement, useContext } from 'react'
import {
  Button,
  Flex,
  Heading,
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Stack,
  Tab,
  TabList,
  TabPanel,
  TabPanels,
  Tabs,
  useDisclosure
} from '@chakra-ui/react'
import { CodeEditor } from '@/components/ui/CodeEditor'
import { AppContext } from '@/contexts/AppContext'
import { useSignature } from '@/hooks/useSignature'
import { useRouter } from 'next/router'
import { StepperContext } from '@/contexts/StepperContext'
import { SignatureContext } from '@/contexts/SignatureContext'
import { ArrowLeftIcon, CheckCircleIcon } from '@chakra-ui/icons'
import { useDocuments } from '@/hooks/stepper/useDocuments'
import { gradients } from '@/customTheme'
import { useEid } from '@/hooks/useEid'
import { useWallet } from '@/hooks/useWallet'

export const SignStep: React.FC = () => {
  const { isOpen, onOpen, onClose } = useDisclosure()
  const { handleImport } = useWallet()
  const { addDocument } = useContext(AppContext)
  const { issuerUse, privateKeyUse, isSignMultiVC } = useContext(SignatureContext)
  const { isEidEnabled } = useEid()
  const { vc, docName, steps, multiVcCreator } = useContext(StepperContext)
  const { documents, setDocuments } = useDocuments()
  const router = useRouter()

  const { signingIsLoading, signDocument } = useSignature({
    issuerDoc: JSON.stringify(vc),
    issuerUse,
    privateKeyUse,
    docName
  })

  const handleSign = async () => {
    const signed = [...(await signDocument())]
    if (isSignMultiVC && multiVcCreator.legalRegistrationNumberVC) {
      signed.push(multiVcCreator.legalRegistrationNumberVC)
    }
    setDocuments(signed)
    onOpen()
  }

  const handleAddSignedVCToHolder = async (saveInLocalWallet: boolean) => {
    let privateKey = ''
    if (!isEidEnabled()) {
      privateKey = (await privateKeyUse.getPrivateKey()) ?? ''
    }
    for (const signedDoc of documents) {
      addDocument({
        document: signedDoc,
        docName,
        privateKey: privateKey
      })

      if (saveInLocalWallet) {
        handleImport({
          document: signedDoc,
          privateKey: privateKey,
          name: docName,
          key: crypto.randomUUID(),
          creation: new Date().toISOString(),
          isVerified: false,
          type: signedDoc.type
        })
      }

      onClose()
      await router.push('/')
    }
  }
  const vcs = Array.isArray(vc) ? vc : [vc]

  let toSignEditor: ReactElement

  if (Array.isArray(vc)) {
    toSignEditor = (
      <Tabs mt={2} w={'100%'} isFitted variant="enclosed" colorScheme={'white'}>
        <TabList>
          {vcs.map((v, i) => (
            <Tab key={i}>{v?.credentialSubject?.type?.replace('gx:', '')}</Tab>
          ))}
        </TabList>
        <TabPanels>
          {vcs.map((v, i) => (
            <TabPanel key={i}>
              <CodeEditor readOnly value={JSON.stringify(v, null, 2)} />
            </TabPanel>
          ))}
        </TabPanels>
      </Tabs>
    )
  } else {
    toSignEditor = <CodeEditor readOnly value={JSON.stringify(vc, null, 2)} />
  }

  return (
    <Flex flexDirection={'column'} w={'100%'} px={5}>
      <Flex flexDirection={'column'} alignItems={'center'}>
        <Flex alignItems={'center'}>
          <Heading my={3} size={'md'}>
            Your Verifiable Credential{isSignMultiVC ? 's' : ''} ({docName})
          </Heading>
        </Flex>
        {toSignEditor}
      </Flex>
      <Flex justifyContent={'center'} alignItems={'center'} mt={5}>
        <Button colorScheme={'yellow'} leftIcon={<ArrowLeftIcon />} onClick={steps.goToPrevious} mr={2}>
          Previous
        </Button>
        <Button colorScheme={'green'} leftIcon={<CheckCircleIcon />} ml={2} onClick={handleSign} isLoading={signingIsLoading}>
          Sign
        </Button>
      </Flex>
      <Modal isOpen={isOpen} onClose={onClose} isCentered size={'3xl'}>
        <ModalOverlay />
        <ModalContent bgGradient={gradients.primary} color={'white'}>
          <ModalHeader>Your signed Verifiable Credential{isSignMultiVC ? 's' : ''}</ModalHeader>
          <ModalBody>
            <Tabs mt={2} w={'100%'} isFitted variant="enclosed" colorScheme={'white'}>
              <TabList flexWrap={'wrap'}>
                {documents.map((sd, i) => (
                  <Tab key={i}>{sd?.credentialSubject?.type?.replace('gx:', '')}</Tab>
                ))}
              </TabList>
              <TabPanels>
                {documents.map((sd, i) => (
                  <TabPanel key={i}>
                    <CodeEditor readOnly value={JSON.stringify(sd, null, 2)} />
                  </TabPanel>
                ))}
              </TabPanels>
            </Tabs>
          </ModalBody>

          <ModalFooter>
            <Stack direction={{ base: 'column', sm: 'row' }} spacing={{ base: 4, sm: 0 }}>
              <Button colorScheme="green" mr={3} onClick={() => handleAddSignedVCToHolder(false)}>
                Add in holder
              </Button>
              <Button colorScheme="green" mr={3} onClick={() => handleAddSignedVCToHolder(true)}>
                Add in holder & Save in local wallet
              </Button>
            </Stack>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Flex>
  )
}
