import { Button, Flex, HStack, VStack } from '@chakra-ui/react'
import { DocumentNameField } from '@/components/ui/Fields/DocumentNameField'
import React, { useContext } from 'react'
import { Form, Formik } from 'formik'
import { ArrowLeftIcon, ArrowRightIcon } from '@chakra-ui/icons'
import { FormField } from '@/components/ui/Fields/FormField'
import { StepperContext } from '@/contexts/StepperContext'
import { generateName } from '@/utils/util'
import { SignatureContext } from '@/contexts/SignatureContext'

export type IdentifierStepState = {
  id: string
  name: string
  issuer: string
  verificationMethod: string
  credentialSubjectId: string
  tncId?: string
  tncCredentialSubjectId?: string
}

export const IdentifierStep: React.FC = () => {
  const { issuerUse, isSignMultiVC } = useContext(SignatureContext)
  const { issuer, verificationMethod } = issuerUse
  const { handleCompleteIdentifierStep, steps, vcCreator, multiVcCreator } = useContext(StepperContext)

  const handleSubmit = (values: IdentifierStepState) => {
    if (isSignMultiVC) {
      multiVcCreator.setIdentifiers({
        participantVcId: values.id,
        participantCredentialSubjectId: values.credentialSubjectId,
        tncVcId: values.tncId,
        tncCredentialSubjectId: values.tncCredentialSubjectId
      })
    } else {
      vcCreator.setValues({ ...vcCreator.values, ...values })
    }
    handleCompleteIdentifierStep(values)
  }

  return (
    <Flex justifyContent={'center'} w={'100%'}>
      <Formik
        initialValues={
          isSignMultiVC
            ? {
                id: multiVcCreator.identifiers?.participantVcId ?? '',
                credentialSubjectId: multiVcCreator.identifiers?.participantCredentialSubjectId ?? '',
                tncId: multiVcCreator.identifiers?.tncVcId,
                tncCredentialSubjectId: multiVcCreator.identifiers?.tncCredentialSubjectId,
                issuer,
                verificationMethod,
                name: generateName()
              }
            : {
                id: vcCreator.values?.id,
                credentialSubjectId: vcCreator.values?.credentialSubjectId,
                issuer,
                verificationMethod,
                name: generateName()
              }
        }
        onSubmit={values => handleSubmit(values)}
      >
        {() => (
          <Form>
            <VStack spacing={8} w={'100%'} align={'center'} minW={{ base: '300px', md: '400px' }}>
              <DocumentNameField name={'name'} label={'Name'} required />
              <FormField label={'Issuer'} name={'issuer'} required />
              <FormField label={'Verification method'} name={'verificationMethod'} required />
              <FormField label={'Verifiable Credential ID'} name={'id'} required />
              <FormField label={'Credential subject ID'} name={'credentialSubjectId'} required />
              {isSignMultiVC && (
                <>
                  <FormField label={'Terms and Conditions Verifiable Credential ID'} name={'tncId'} required />
                  <FormField label={'Terms and Conditions Credential subject ID'} name={'tncCredentialSubjectId'} required />
                </>
              )}
              <HStack mt={10}>
                <Button colorScheme={'yellow'} leftIcon={<ArrowLeftIcon />} onClick={steps.goToPrevious}>
                  Previous
                </Button>
                <Button type={'submit'} colorScheme={'green'} rightIcon={<ArrowRightIcon />}>
                  Next
                </Button>
              </HStack>
            </VStack>
          </Form>
        )}
      </Formik>
    </Flex>
  )
}
