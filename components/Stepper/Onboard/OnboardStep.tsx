import React, { useContext } from 'react'
import { Box, Button, Flex, FormControl, FormLabel, Heading, ListItem, Switch, Text, Tooltip, UnorderedList } from '@chakra-ui/react'
import { SignatureContext } from '@/contexts/SignatureContext'
import { ArrowRightIcon } from '@chakra-ui/icons'
import { StepperContext } from '@/contexts/StepperContext'

export const OnboardHome: React.FC = () => {
  const { issuerUse } = useContext(SignatureContext)
  const { goToNext } = useContext(StepperContext).steps

  return (
    <Flex flexDirection={'column'} alignItems={'center'} w={'100%'}>
      <Heading>Participant VCs in 4 steps</Heading>
      <Text mt={8} maxW={'800px'}>
        Follow the steps to issue the Verifiable Credentials required to pass compliance. Once complete, you will get 3 Verifiable Credentials : Legal
        Participant, Legal Registration Number and Terms And Conditions.
      </Text>
      <Box border="1px solid #ccc" borderRadius="md" p={4} mt={8}>
        <Text fontWeight={'bold'}>Wizard disclaimer ! 🧙🏻‍</Text> This wizard has been developed for test purpose:
        <UnorderedList>
          <ListItem>It doesn't enforce key chain validation.</ListItem>
          <ListItem>It doesn't implement all classes validation.</ListItem>
          {!issuerUse.isCustomIssuer && <ListItem>The provided keypair is for convenience. We advocate that you use your own.</ListItem>}
        </UnorderedList>
      </Box>
      <FormControl display="flex" justifyContent="center" my={8}>
        <Tooltip label="Only did:web are supported for now">
          <FormLabel htmlFor="use-own-did" mb="0" fontSize={'lg'}>
            I'll use my own DID solution
          </FormLabel>
        </Tooltip>
        <Switch
          data-testid="use-own-did"
          size={'lg'}
          colorScheme={'green'}
          id="use-own-did"
          isChecked={issuerUse.isCustomIssuer}
          onChange={() => issuerUse.setIsCustomIssuer(!issuerUse.isCustomIssuer)}
        />
      </FormControl>
      <Button colorScheme={'green'} rightIcon={<ArrowRightIcon />} onClick={goToNext}>
        Start
      </Button>
    </Flex>
  )
}
