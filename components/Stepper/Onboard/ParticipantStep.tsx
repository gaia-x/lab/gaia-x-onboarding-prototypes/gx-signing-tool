import { Form, Formik, FormikProps } from 'formik'
import { Box, Button, Flex, FormControl, FormLabel, Grid, GridItem, HStack, Input } from '@chakra-ui/react'
import React, { useContext, useState } from 'react'
import { ArrowLeftIcon, ArrowRightIcon } from '@chakra-ui/icons'
import { FormField } from '@/components/ui/Fields/FormField'
import { CountryCodePopover } from '@/components/Stepper/CreateCredentialSubject/ParticipantForm/CountryCodePopover'
import { StepperContext } from '@/contexts/StepperContext'
import { CredentialSubjectValues, participantDefaultValues } from '@/hooks/stepper/useCreateVC'
import { LegalRegistrationNumberField } from '@/components/ui/Fields/LegalRegistrationNumberField'
import { useLRN } from '@/hooks/useLRN'
import { SignatureContext } from '@/contexts/SignatureContext'
import { useErrorHandler } from '@/hooks/useError'

export const ParticipantStep: React.FC = () => {
  const { multiVcCreator } = useContext(StepperContext)
  const { issuerUse } = useContext(SignatureContext)
  const { value: participantValue, setValue: setParticipantValue } = multiVcCreator.of('participant', participantDefaultValues)
  const { steps } = useContext(StepperContext)
  const { createLRN } = useLRN(issuerUse.isCustomIssuer)
  const { errorHandler } = useErrorHandler()
  const [lrnVcId, setLrnVcId] = useState<string>('')
  const [lrnVcSubjectId, setLrnVcSubjectId] = useState<string>('')

  const validateCountryCode = (value: string): string | undefined => {
    if (!new RegExp('^[A-Z]{2}-[A-Z0-9]{1,3}$').test(value)) {
      return 'Invalid ISO 3166-2 country code'
    }
  }

  const handleSubmit = async (value: CredentialSubjectValues) => {
    try {
      const cs: any = { ...value }
      const lrn = await createLRN({
        type: cs.type,
        value: cs.value,
        csid: lrnVcSubjectId,
        vcid: lrnVcId
      })
      delete cs.type
      delete cs.value
      cs.legalRegistrationNumber.id = lrn.credentialSubject.id
      multiVcCreator.setLegalRegistrationNumberVC(lrn)
      setParticipantValue(cs)
      steps.goToNext()
    } catch (err: any) {
      errorHandler(err, err.response?.data?.message || err.response?.data || err.message || 'Could not verify legal registration number')
    }
  }

  return (
    <Formik initialValues={participantValue} onSubmit={handleSubmit}>
      {(props: FormikProps<any>) => (
        <Box w={'100%'}>
          <Form>
            <Flex flexDirection={'column'} alignItems={'center'} justifyContent={'flex-start'}>
              <Flex gap={10} direction={'column'} w={'70%'}>
                <FormField name={'legalName'} label={'Legal name'} required />
                <Box
                  style={
                    issuerUse.isCustomIssuer
                      ? {
                          padding: '16px',
                          borderRadius: '8px',
                          backgroundColor: '#00007133'
                        }
                      : {}
                  }
                >
                  <LegalRegistrationNumberField
                    name={'value'}
                    typeName={'type'}
                    label={'Legal registration number (LRN)'}
                    required
                    setFieldValue={props.setFieldValue}
                    radioValue={props.values.type}
                  />
                  {issuerUse.isCustomIssuer && (
                    <Grid templateColumns={{ base: 'repeat(1, 1fr)', lg: 'repeat(2, 1fr)' }} gap={10} mt={4}>
                      <FormControl isRequired={true}>
                        <FormLabel>LRN Verifiable Credential ID</FormLabel>
                        <Input value={lrnVcId} onChange={e => setLrnVcId(e.target.value)} />
                      </FormControl>
                      <FormControl isRequired={true}>
                        <FormLabel>LRN Credential Subject ID</FormLabel>
                        <Input value={lrnVcSubjectId} onChange={e => setLrnVcSubjectId(e.target.value)} />
                      </FormControl>
                    </Grid>
                  )}
                </Box>
                <Grid templateColumns={{ base: 'repeat(1, 1fr)', lg: 'repeat(2, 1fr)' }} gap={10} alignItems={'flex-end'}>
                  <GridItem>
                    <FormField
                      name={'headquarterAddress.countrySubdivisionCode'}
                      label={'Headquarter address (ISO 3166-2)'}
                      placeholder={'BE-BRU'}
                      rightElement={<CountryCodePopover />}
                      required={true}
                      validationFunction={validateCountryCode}
                      zIndex={2}
                    />
                  </GridItem>
                  <GridItem>
                    <FormField
                      name={'legalAddress.countrySubdivisionCode'}
                      label={'Legal address (ISO 3166-2)'}
                      placeholder={'BE-BRU'}
                      rightElement={<CountryCodePopover />}
                      required={true}
                      validationFunction={validateCountryCode}
                      zIndex={2}
                    />
                  </GridItem>
                  <GridItem>
                    <FormField name={'parentOrganization'} label={'Parent organization'} />
                  </GridItem>
                  <GridItem>
                    <FormField name={'subOrganization'} label={'Sub organization'} />
                  </GridItem>
                </Grid>
              </Flex>
              <HStack mt={10}>
                <Button colorScheme={'yellow'} leftIcon={<ArrowLeftIcon />} onClick={steps.goToPrevious}>
                  Previous
                </Button>
                <Button type={'submit'} colorScheme={'green'} isDisabled={props.isSubmitting || !props.isValid} rightIcon={<ArrowRightIcon />}>
                  Next
                </Button>
              </HStack>
            </Flex>
          </Form>
        </Box>
      )}
    </Formik>
  )
}
