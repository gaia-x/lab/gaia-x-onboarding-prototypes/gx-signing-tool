import React, { useContext } from 'react'
import { StepperContext } from '@/contexts/StepperContext'
import { TermsAndConditionsForm } from '../CreateCredentialSubject/TermsAndConditionsForm/TermsAndConditionsForm'
import { CredentialSubjectValues } from '@/hooks/stepper/useCreateVC'

export const TermsAndConditionsStep: React.FC = () => {
  const { setValue } = useContext(StepperContext).multiVcCreator.of('termsAndConditions')
  const { steps } = useContext(StepperContext)

  const handleOnComplete = (value: CredentialSubjectValues) => {
    setValue(value)
    steps.goToNext()
  }

  return <TermsAndConditionsForm onComplete={handleOnComplete} onPrevious={steps.goToPrevious} />
}
