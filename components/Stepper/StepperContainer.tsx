import { Box, Flex, Step, StepIcon, StepIndicator, StepNumber, StepSeparator, StepStatus, StepTitle } from '@chakra-ui/react'
import { Stepper as ChakraStepper } from '@chakra-ui/stepper'
import { CreateCredentialSubject } from '@/components/Stepper/CreateCredentialSubject/CreateCredentialSubject'
import React, { useContext } from 'react'
import { KeyStep } from '@/components/Stepper/KeyStep/KeyStep'
import { SignStep } from '@/components/Stepper/SignStep/SignStep'
import { IdentifierStep } from '@/components/Stepper/IdentifierStep/IdentifierStep.tsx/IdentifierStep'
import { StepperContext } from '@/contexts/StepperContext'
import { SignatureContext } from '@/contexts/SignatureContext'

export const StepperContainer: React.FC = ({}) => {
  const { steps, vc } = useContext(StepperContext)
  const { isCustomIssuer } = useContext(SignatureContext).issuerUse

  return (
    <Flex flexDirection={'column'} minH={'100vh'} h={'100%'}>
      <Flex p={4} m={{ base: 0, sm: 4 }} bg={'transparent'} justifyContent={'flex-start'} flexDirection={'column'} alignItems={'center'}>
        <ChakraStepper index={steps.activeStep} size={'lg'} colorScheme={'blue'} color={'white'} mt={8} flexWrap={'wrap'}>
          {steps.steps.map((step, index) => (
            <Step key={index}>
              <StepIndicator mx={2}>
                <StepStatus complete={<StepIcon />} incomplete={<StepNumber />} active={<StepNumber />} />
              </StepIndicator>

              <Box flexShrink="0">
                <StepTitle>{step.description}</StepTitle>
              </Box>

              <StepSeparator />
            </Step>
          ))}
        </ChakraStepper>
        <Flex mt={12} color={'white'} w={'100%'} overflowX={'hidden'}>
          {isCustomIssuer ? (
            <>
              {steps.activeStep === 0 && <CreateCredentialSubject />}
              {steps.activeStep === 1 && <KeyStep />}
              {steps.activeStep === 2 && <IdentifierStep />}
              {steps.activeStep === 3 && !!vc && <SignStep />}
            </>
          ) : (
            <>
              {steps.activeStep === 0 && <CreateCredentialSubject />}
              {steps.activeStep === 1 && !!vc && <SignStep />}
            </>
          )}
        </Flex>
      </Flex>
    </Flex>
  )
}
