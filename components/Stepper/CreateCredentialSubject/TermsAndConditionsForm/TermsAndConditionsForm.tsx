import React, { useContext, useEffect, useState } from 'react'
import { FirstStepFormProps } from '@/components/Stepper/CreateCredentialSubject/ParticipantForm/ParticipantForm'
import { Box, Button, Checkbox, Flex, HStack, Stack, Text } from '@chakra-ui/react'
import { ArrowLeftIcon, ArrowRightIcon } from '@chakra-ui/icons'
import { StepperContext } from '@/contexts/StepperContext'
import { useExamples } from '@/hooks/useExamples'

export interface TermsAndConditionsFormValues {
  termsAndConditions: string
}

export const TermsAndConditionsForm: React.FC<FirstStepFormProps> = ({ onComplete, onPrevious }) => {
  const [agreed, setAgreed] = useState(false)
  const { values, setValues } = useContext(StepperContext).vcCreator

  const exampleTuple = useExamples().getExampleByKey('termsAndConditions')
  const cs = values.credentialSubjectValues as TermsAndConditionsFormValues

  useEffect(() => {
    if (!cs?.termsAndConditions && exampleTuple) {
      const termsAndConditions = exampleTuple.example.value.verifiableCredential[0].credentialSubject['gx:termsAndConditions']
      setValues({
        ...values,
        credentialSubjectValues: {
          termsAndConditions: termsAndConditions
        }
      })
    }
  }, [cs?.termsAndConditions, exampleTuple])

  return (
    <Box mt={10} w={'100%'}>
      <Flex flexDirection={'column'} alignItems={'center'} justifyContent={'flex-start'}>
        <Text whiteSpace={'pre-wrap'} maxW={'600px'}>
          {cs?.termsAndConditions}
        </Text>
        <Stack spacing={5} mt={10} direction="row">
          <Checkbox isChecked={agreed} onChange={e => setAgreed(e.target.checked)} isDisabled={!cs?.termsAndConditions} data-testid="agree-tnc">
            I agree to the terms and conditions
          </Checkbox>
        </Stack>
        <HStack mt={10}>
          <Button colorScheme={'yellow'} leftIcon={<ArrowLeftIcon />} onClick={onPrevious}>
            Previous
          </Button>
          <Button colorScheme={'green'} onClick={() => onComplete(cs)} isDisabled={!agreed} rightIcon={<ArrowRightIcon />}>
            Next
          </Button>
        </HStack>
      </Flex>
    </Box>
  )
}
