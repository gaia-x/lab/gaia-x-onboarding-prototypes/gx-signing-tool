import React, { useContext } from 'react'
import { FirstStepFormProps } from '@/components/Stepper/CreateCredentialSubject/ParticipantForm/ParticipantForm'
import { Box, Button, Flex, Grid, GridItem, HStack } from '@chakra-ui/react'
import { Form, Formik } from 'formik'
import { ArrowLeftIcon, ArrowRightIcon } from '@chakra-ui/icons'
import { FormField } from '@/components/ui/Fields/FormField'
import { FormSelectField } from '@/components/ui/Fields/FormSelectField'
import { useLocalWallet } from '@/hooks/useLocalWallet'
import { AutoCompleteField } from '@/components/ui/Fields/AutoCompleteField'
import { DataListEntry } from '@/models/dataListEntry'
import { StepperContext } from '@/contexts/StepperContext'
import { serviceOfferingDefaultValues } from '@/hooks/stepper/useCreateVC'

export interface ServiceOfferingFormValues {
  providedBy: {
    id: string
  }
  termsAndConditions: {
    URL: string
    hash: string
  }
  policy: string
  dataAccountExport: {
    requestType: string
    accessType: string
    formatType: string
  }
  aggregationOf?: string
  dependsOn?: string
  dataProtectionRegime?: string
}

export const ServiceOfferingForm: React.FC<FirstStepFormProps> = ({ onComplete, onPrevious }) => {
  const { values } = useContext(StepperContext).vcCreator
  const { getEntriesByType } = useLocalWallet()

  const walletParticipants: DataListEntry[] = getEntriesByType('gx:LegalParticipant').map(entry => ({
    key: entry.name,
    value: entry.document?.credentialSubject?.id
  }))

  return (
    <Formik
      initialValues={values.credentialSubjectValues || serviceOfferingDefaultValues}
      onSubmit={values => {
        onComplete(values)
      }}
    >
      {() => (
        <Box mt={10} w={'100%'}>
          <Form>
            <Flex flexDirection={'column'} alignItems={'center'} justifyContent={'flex-start'}>
              <Grid templateColumns={{ base: 'repeat(1, 1fr)', md: 'repeat(2, 1fr)' }} gap={10} alignItems={'flex-end'} w={'70%'}>
                <GridItem>
                  <AutoCompleteField name={'providedBy.id'} label={'Provided by'} datalistEntries={walletParticipants} required />
                </GridItem>
                <GridItem>
                  <FormField name={'policy'} label={'Policy'} required />
                </GridItem>
                <GridItem>
                  <FormField name={'termsAndConditions.URL'} label={'Terms and conditions url'} required />
                </GridItem>
                <GridItem>
                  <FormField name={'termsAndConditions.hash'} label={'Terms and conditions hash'} required />
                </GridItem>
                <GridItem>
                  <FormSelectField
                    values={['API', 'email', 'webform', 'unregisteredLetter', 'registeredLetter', 'supportCenter']}
                    name={'dataAccountExport.requestType'}
                    label={'Request type'}
                    placeholder={'Select request type'}
                    required
                  />
                </GridItem>
                <GridItem>
                  <FormSelectField
                    values={['digital', 'physical']}
                    name={'dataAccountExport.accessType'}
                    label={'Access type'}
                    placeholder={'Select access type'}
                    required
                  />
                </GridItem>
                <GridItem>
                  <FormField name={'dataAccountExport.formatType'} label={'Format type'} required />
                </GridItem>
                <GridItem>
                  <FormField name={'aggregationOf'} label={'Aggregation of'} />
                </GridItem>
                <GridItem>
                  <FormField name={'dependsOn'} label={'Depends on'} />
                </GridItem>
                <GridItem>
                  <FormSelectField
                    values={['GDPR2016', 'LGPD2019', 'PDPA2012', 'CCPA2018', 'VCDPA2021']}
                    name={'dataProtectionRegime'}
                    label={'Data protection regime'}
                    placeholder={'Select data protection regime'}
                  />
                </GridItem>
              </Grid>
              <HStack mt={10}>
                <Button colorScheme={'yellow'} leftIcon={<ArrowLeftIcon />} onClick={onPrevious}>
                  Previous
                </Button>
                <Button type={'submit'} colorScheme={'green'} rightIcon={<ArrowRightIcon />}>
                  Next
                </Button>
              </HStack>
            </Flex>
          </Form>
        </Box>
      )}
    </Formik>
  )
}
