import { Form, Formik } from 'formik'
import { Box, Button, Flex, Grid, GridItem, HStack } from '@chakra-ui/react'
import React, { useContext } from 'react'
import { ArrowLeftIcon, ArrowRightIcon } from '@chakra-ui/icons'
import { FormField } from '@/components/ui/Fields/FormField'
import { CountryCodePopover } from '@/components/Stepper/CreateCredentialSubject/ParticipantForm/CountryCodePopover'
import { StepperContext } from '@/contexts/StepperContext'
import { CredentialSubjectValues, participantDefaultValues } from '@/hooks/stepper/useCreateVC'
import { DataListEntry } from '@/models/dataListEntry'
import { useLocalWallet } from '@/hooks/useLocalWallet'
import { AutoCompleteField } from '@/components/ui/Fields/AutoCompleteField'

export type ParticipantFormValues = {
  legalRegistrationNumber: {
    id: string
    type?: string
  }
  legalName: string
  headquarterAddress: {
    countrySubdivisionCode: string
  }
  legalAddress: {
    countrySubdivisionCode: string
  }
  parentOrganization?: string
  subOrganization?: string
}

export interface FirstStepFormProps {
  onComplete: (values: CredentialSubjectValues) => void
  onPrevious: () => void
}

export const ParticipantForm: React.FC<FirstStepFormProps> = ({ onComplete, onPrevious }) => {
  const { values } = useContext(StepperContext).vcCreator

  const { getEntriesByType } = useLocalWallet()

  const validateCountryCode = (value: string): string | undefined => {
    let error
    const iso3166_2Regex = new RegExp('^[A-Z]{2}-[A-Z0-9]{1,3}$')
    if (!iso3166_2Regex.test(value)) {
      error = 'Invalid country code'
    }
    return error
  }

  const walletParticipants: DataListEntry[] = getEntriesByType('gx:legalRegistrationNumber').map(entry => ({
    key: entry.name,
    value: entry.document?.credentialSubject?.id
  }))

  return (
    <Formik
      initialValues={values.credentialSubjectValues || participantDefaultValues}
      onSubmit={values => {
        onComplete(values)
      }}
    >
      {() => (
        <Box mt={10} w={'100%'}>
          <Form>
            <Flex flexDirection={'column'} alignItems={'center'} justifyContent={'flex-start'}>
              <Grid templateColumns={{ base: 'repeat(1, 1fr)', lg: 'repeat(2, 1fr)' }} gap={10} alignItems={'flex-end'} w={'70%'}>
                <GridItem>
                  <FormField name={'legalName'} label={'Legal name'} required />
                </GridItem>
                <GridItem>
                  <AutoCompleteField
                    name={'legalRegistrationNumber.id'}
                    label={'Legal registration number'}
                    datalistEntries={walletParticipants}
                    required
                  />
                </GridItem>
                <GridItem>
                  <FormField
                    name={'headquarterAddress.countrySubdivisionCode'}
                    label={'Headquarter address'}
                    rightElement={<CountryCodePopover />}
                    required={true}
                    validationFunction={validateCountryCode}
                    zIndex={2}
                  />
                </GridItem>
                <GridItem>
                  <FormField
                    name={'legalAddress.countrySubdivisionCode'}
                    label={'Legal address'}
                    rightElement={<CountryCodePopover />}
                    required={true}
                    validationFunction={validateCountryCode}
                    zIndex={2}
                  />
                </GridItem>
                <GridItem>
                  <FormField name={'parentOrganization'} label={'Parent organization'} />
                </GridItem>
                <GridItem>
                  <FormField name={'subOrganization'} label={'Sub organization'} />
                </GridItem>
              </Grid>
              <HStack mt={10}>
                <Button colorScheme={'yellow'} leftIcon={<ArrowLeftIcon />} onClick={onPrevious}>
                  Previous
                </Button>
                <Button type={'submit'} colorScheme={'green'} rightIcon={<ArrowRightIcon />}>
                  Next
                </Button>
              </HStack>
            </Flex>
          </Form>
        </Box>
      )}
    </Formik>
  )
}
