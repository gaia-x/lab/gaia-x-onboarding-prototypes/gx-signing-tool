import React, { useContext } from 'react'
import {
  Box,
  Divider,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  ListItem,
  Stack,
  Switch,
  Text,
  Tooltip,
  UnorderedList,
  useSteps
} from '@chakra-ui/react'
import { useExamples } from '@/hooks/useExamples'
import { CreateCredentialSubjectShapeForm } from '@/components/Stepper/CreateCredentialSubject/CreateCredentialSubjectShapeForm'
import { ExampleTuple } from '@/sharedTypes'
import { StepperContext } from '@/contexts/StepperContext'
import { SignatureContext } from '@/contexts/SignatureContext'
import { CredentialSubjectValues } from '@/hooks/stepper/useCreateVC'

export type CreateCredentialSubjectState = {
  credentialSubjectValues: CredentialSubjectValues
  shape: string
}

export const CreateCredentialSubject: React.FC = () => {
  const { handleCompleteCreateCredentialSubject, selectedExample, setSelectedExample } = useContext(StepperContext)
  const { issuerUse } = useContext(SignatureContext)

  const { data: examples } = useExamples()

  const availableShapes = ['participant', 'service', 'termsAndConditions']
  const availableExamples = examples?.filter(example => availableShapes.indexOf(example.shape) > -1)

  // 0 = Choose shape, 1 = Form,
  const { activeStep, goToNext, goToPrevious } = useSteps({
    index: selectedExample ? 1 : 0,
    count: 2
  })

  const handlePreviousShapeForm = () => {
    if (activeStep === 1) {
      goToPrevious()
      setSelectedExample(undefined)
    } else {
      goToPrevious()
    }
  }

  const handleChooseShape = (example: ExampleTuple) => {
    setSelectedExample(example)
    goToNext()
  }

  const handleCompleteShapeForm = (newFormValues: CredentialSubjectValues) => {
    if (!selectedExample) throw new Error('No selected example')
    handleCompleteCreateCredentialSubject({
      credentialSubjectValues: newFormValues,
      shape: selectedExample.shape
    })
    goToNext()
  }

  if (activeStep === 0)
    return (
      <Flex flexDirection={'column'} alignItems={'center'} w={'100%'}>
        <Heading>Create Verifiable Credential</Heading>
        <Box border="1px solid #ccc" borderRadius="md" p={4} mt={4}>
          <Text fontWeight={'bold'}>Wizard disclaimer ! 🧙🏻‍</Text> This wizard has been developed for test purpose:
          <UnorderedList>
            <ListItem>It doesn't enforce key chain validation.</ListItem>
            <ListItem>It doesn't implement all classes validation.</ListItem>
            {!issuerUse.isCustomIssuer && <ListItem>The provided keypair is for convenience. We advocate that you use your own.</ListItem>}
          </UnorderedList>
        </Box>
        {!selectedExample && (
          <Flex mt={6} flexDirection={'column'} justifyContent={'center'} alignItems={'center'} data-testid="shapes">
            <Heading size={'lg'}>Choose a shape</Heading>
            <FormControl display="flex" justifyContent="center" mt={10} w={'100%'}>
              <Tooltip label="Only did:web are supported for now">
                <FormLabel htmlFor="use-own-did" mb="0" fontSize={'lg'}>
                  I'll use my own DID solution
                </FormLabel>
              </Tooltip>
              <Switch
                data-testid="use-own-did"
                size={'lg'}
                colorScheme={'green'}
                id="use-own-did"
                isChecked={issuerUse.isCustomIssuer}
                onChange={() => issuerUse.setIsCustomIssuer(!issuerUse.isCustomIssuer)}
              />
            </FormControl>
            <Stack mt={10} spacing={10} direction={{ base: 'column', xl: 'row' }} wrap={'wrap'}>
              {availableExamples?.map(exampleTuple => (
                <Flex
                  h={'180px'}
                  w={'230px'}
                  px={2}
                  textAlign={'center'}
                  justifyContent={'center'}
                  alignItems={'center'}
                  key={exampleTuple.shape}
                  backgroundColor={'rgba(255, 255, 255, 0.1)'}
                  cursor={'pointer'}
                  borderRadius={'sm'}
                  _hover={{
                    color: 'white',
                    backgroundColor: 'rgba(255, 255, 255, 0.2)'
                  }}
                  onClick={() => handleChooseShape(exampleTuple)}
                >
                  {exampleTuple.example.summary}
                </Flex>
              ))}
            </Stack>
          </Flex>
        )}
      </Flex>
    )

  if (activeStep === 1 && !!selectedExample) {
    const action = selectedExample.example.summary.toLowerCase() === 'terms and conditions' ? 'Sign ' : 'Create '
    const target = selectedExample.example.summary
    return (
      <Flex flexDirection={'column'} alignItems={'center'} w={'100%'}>
        <Heading>{action + ' ' + target}</Heading>
        <Divider mt={1} orientation={'horizontal'} w={'350px'} />
        {!!selectedExample && (
          <CreateCredentialSubjectShapeForm
            selectedExample={selectedExample}
            onComplete={handleCompleteShapeForm}
            onPrevious={handlePreviousShapeForm}
          />
        )}
      </Flex>
    )
  }

  return null
}
