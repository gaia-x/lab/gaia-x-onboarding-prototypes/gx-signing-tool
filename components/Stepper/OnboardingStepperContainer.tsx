import { Box, Flex, Step, StepIcon, StepIndicator, StepNumber, StepSeparator, StepStatus, StepTitle } from '@chakra-ui/react'
import { Stepper as ChakraStepper } from '@chakra-ui/stepper'
import React, { useContext } from 'react'
import { KeyStep } from '@/components/Stepper/KeyStep/KeyStep'
import { SignStep } from '@/components/Stepper/SignStep/SignStep'
import { IdentifierStep } from '@/components/Stepper/IdentifierStep/IdentifierStep.tsx/IdentifierStep'
import { StepperContext } from '@/contexts/StepperContext'
import { SignatureContext } from '@/contexts/SignatureContext'
import { OnboardHome } from './Onboard/OnboardStep'
import { ParticipantStep } from './Onboard/ParticipantStep'
import { TermsAndConditionsStep } from './Onboard/TermsAndConditionsStep'
import { SelectClearingHouse } from '@/components/SelectClearingHouse'

export const OnboardingStepperContainer: React.FC = ({}) => {
  const { steps } = useContext(StepperContext)
  const { isCustomIssuer } = useContext(SignatureContext).issuerUse

  return (
    <Flex flexDirection={'column'} minH={'100vh'} h={'100%'}>
      <Box p={3}>
        <SelectClearingHouse />
      </Box>
      <Flex p={4} m={{ base: 0, sm: 4 }} justifyContent={'flex-start'} flexDirection={'column'} alignItems={'center'}>
        <ChakraStepper index={steps.activeStep} colorScheme={'blue'} color={'white'} mt={8} flexWrap={'wrap'}>
          {steps.steps.map((step, index) => (
            <Step key={index}>
              <StepIndicator mx={2}>
                <StepStatus complete={<StepIcon />} incomplete={<StepNumber />} active={<StepNumber />} />
              </StepIndicator>
              <StepTitle>{step.description}</StepTitle>
              <StepSeparator />
            </Step>
          ))}
        </ChakraStepper>
        <Flex mt={12} color={'white'} w={'100%'} overflowX={'hidden'}>
          {isCustomIssuer ? (
            <>
              {steps.activeStep === 0 && <OnboardHome />}
              {steps.activeStep === 1 && <ParticipantStep />}
              {steps.activeStep === 2 && <TermsAndConditionsStep />}
              {steps.activeStep === 3 && <KeyStep />}
              {steps.activeStep === 4 && <IdentifierStep />}
              {steps.activeStep === 5 && <SignStep />}
            </>
          ) : (
            <>
              {steps.activeStep === 0 && <OnboardHome />}
              {steps.activeStep === 1 && <ParticipantStep />}
              {steps.activeStep === 2 && <TermsAndConditionsStep />}
              {steps.activeStep === 3 && <SignStep />}
            </>
          )}
        </Flex>
      </Flex>
    </Flex>
  )
}
