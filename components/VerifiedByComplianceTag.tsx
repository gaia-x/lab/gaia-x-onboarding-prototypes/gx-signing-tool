import React from 'react'
import { Tag, TagLabel, TagLeftIcon } from '@chakra-ui/react'
import { CheckCircleIcon } from '@chakra-ui/icons'
import { GaiaXDeploymentPath } from '@/sharedTypes'

interface VerifiedByComplianceTagProps {
  version: GaiaXDeploymentPath
}

export const VerifiedByComplianceTag: React.FC<VerifiedByComplianceTagProps> = ({ version }) => {
  return (
    <Tag variant={'solid'} colorScheme={'teal'}>
      <TagLeftIcon as={CheckCircleIcon} />
      <TagLabel>{version === 'v1' ? 'Compliant 22.10-rc1' : 'Verified'}</TagLabel>
    </Tag>
  )
}
