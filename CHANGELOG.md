## [1.13.3](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.13.2...v1.13.3) (2024-07-19)


### Bug Fixes

* change version name to v1 instead of v1-staging for production ([791c151](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/791c151d58c721106e35124616dce94538000891))

## [1.13.2](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.13.1...v1.13.2) (2024-06-13)


### Bug Fixes

* no load balanced gxdch as staging default ([fae3531](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/fae3531998fe3a34616c4089dd97238c4edbd924))
* reduce possible algorithms to the ones supported by JWS2020 ([3f22576](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/3f22576b2a3d51a598d026b2800e2d36b0798ebf))

## [1.13.1](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.13.0...v1.13.1) (2024-06-06)


### Bug Fixes

* **jsonld:** add vcard and schema contexts ([cb6c49a](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/cb6c49a4b7f5a94a10c520ea15533ddb2345690e))

# [1.13.0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.12.0...v1.13.0) (2024-05-30)


### Features

* add disclaimer when calling production compliance ([dacadf1](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/dacadf12aeb2413cae5803df9d5e72a73bad2bf7))

# [1.12.0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.11.2...v1.12.0) (2024-05-28)


### Bug Fixes

* add clear instructions to use OIDC with the memberhsip credential ([234b004](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/234b004aee57d9cfa5d5d41529ee91e9f8e8d36d))
* add v1 staging environment in the list ([3c94c72](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/3c94c72c30745542224fabf1c3dca5a2d3ee1ee2))
* fix next js version ([f8b423b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/f8b423b75b012573fcf0ccec8a9e6d3df726d9a7))
* fix wrong url mapping for keycloak login ([2cb3653](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/2cb3653f8791f30e450a0ac11d477c8fc9d2b995))
* improve oidc instructions ([564e2b0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/564e2b00f12e3eaebc94b54299656f5784a3e1d3))


### Features

* **LAB-488:** add support for EC signature ([decaa9b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/decaa9be9923cb4ca08c3804988a971363048291))
* **membership-credential:** add a page to issue the gaia-x membership credential ([0d956ac](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/0d956ac3736e156221b69e50c61eca55ff320d29))

## [1.11.2](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.11.1...v1.11.2) (2024-03-22)


### Bug Fixes

* **policy-stepper:** fix a few typos in example policies ([d357d4b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/d357d4b3237846c11314767eb1a06d664d09f670))

## [1.11.1](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.11.0...v1.11.1) (2024-03-01)


### Bug Fixes

* use correct registry shape URL ([dedb883](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/dedb8835207961a5e28d62899f950a4791987981))

# [1.11.0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.10.1...v1.11.0) (2024-02-28)


### Bug Fixes

* **CI:** use npm cache during sematic release job ([41a0b01](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/41a0b016ca0aa0c6e185f1f4861742afc42c2e79))
* **deployment:** fix deployment chart and post-install hook ([8e0e39f](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/8e0e39f0c366617b5db2cd2d8ff54be507f5cfcc))
* **deployment:** fix misconfiguration in deployment file ([f491a26](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/f491a2679d88eb146b51184dc8ec2e07b620a334))
* **policy:** update policy reasoning examples and code [LAB-517] ([da15bd5](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/da15bd50e99630ceeb67b814103856923e906b2b))
* **storage:** fix issue related to download for in memory storage service ([45b4c20](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/45b4c2078fb4287854a00e102196befe4b441e4c))
* wrong syntax in service offering shape ([1fee8c3](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/1fee8c328f07faecf967e0d2bfc7049c6ec073c8))
* wrong syntax in service offering shape ([6ac18b0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/6ac18b098c70bcd8c95312a4a954a7e0c807ef16))


### Features

* add trusted issuers url env var ([1839c26](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/1839c26f448a4884179196dda556d7bcd7700e8f))
* allow customization of S3 bucket names ([0b04169](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/0b04169f0307bda23073f27efaefb943137aed54)), closes [#6](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/issues/6)
* **helm:** allow specifying additional volumes and volume mounts in chart ([198ec7a](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/198ec7a8b6e91b1aaa10024bf1ed48b1d5d24d6a))
* **LAB-505:** remove kafka ([08314d5](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/08314d54615f949a280d079aa50a04b4a0a299ed))
* **policy:** add policy reasoning stepper ([42a4b47](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/42a4b47ada65789bb1105100bbe3f6c4f71c3cf5))

## [1.10.1](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.10.0...v1.10.1) (2023-12-06)


### Bug Fixes

* target the proper array for stepper examples ([e1c52fc](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/e1c52fcfffd08660a06f3f15ae6bb04a13225368))

# [1.10.0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.9.0...v1.10.0) (2023-11-16)


### Bug Fixes

* invalid-parameter-to-send-to-ces-v1 ([2a45d9a](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/2a45d9ae3864db895d121ef077e41b9819d07897))


### Features

* add a submit to ces button ([7a2293a](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/7a2293ab3396497849dbb2591c2613f0f9eb986d))

# [1.9.0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.8.0...v1.9.0) (2023-10-31)


### Bug Fixes

* change compliance request url query param key 'vcid' to 'uid' ([2d5ebf8](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/2d5ebf8b7b8b3e580580bf3da77ab6e082141caa))


### Features

* control available shapes in stepper ([9599920](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/9599920547e73dc072e3e0703278559806d9d35c))

# [1.8.0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.7.0...v1.8.0) (2023-10-19)


### Bug Fixes

* use LRN CS id for participant onboarding ([9d2c706](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/9d2c706ba13d5a74343cd5b9b8bd7941089075db))


### Features

* **TAG-104:** enable signing with eidas eid ([e1825f2](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/e1825f2e2a0f7d43b1aadb27d3d80dbb5cd32001))

# [1.7.0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.6.0...v1.7.0) (2023-10-18)


### Bug Fixes

* add newly managed types examples ([a6df081](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/a6df081d37f20c4f598a22ea54c3f277aea960e0))
* correct hook value ([9e7d138](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/9e7d1383314d1a08b3ad4b6536ebec1f8fb9629b))
* fix credential type retrieval for a verifiable presentation ([f0140fc](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/f0140fc03d9ab651826fd4ff6f6a723ede2638f5))
* handle shape error returned from compliance ([22b6d46](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/22b6d46662b0cf72f7d4fe437bfa54c5d4b500b1))
* manage multi-subject credentials ([d8696f4](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/d8696f4e7576e775a03af375c387c6c48e5d56b9))
* quote DID variable to avoid processing \n as EOL ([760ec63](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/760ec63d8234ab0878f2a9ff84d580a8f241407f))
* remove storage folder ([600c120](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/600c1204ff56bd3542e4c235a0f33f6269a617ed))


### Features

* add user guide page ([b23138b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/b23138b6e9260cf7969dbac9bfa42000f00d7080))
* change the query param key for vc identifier ([3ffa24d](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/3ffa24dd4f5bd6c1b4650a566bccc474da72152d))
* **LAB-384:** normalize using static document loader ([917c9d5](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/917c9d5e73580c1af1fabaa57f6f98702117d48c))
* persist holder's VCs ([2882d17](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/2882d1760bdcfd36d97ed3631d7e4ef32bda0bee))
* resolvable LRN id ([c158290](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/c15829004d5b5d2b8a1d3fe9fdec3e0c6aab0477))
* serve managed VC as application/ld+json ([3c9052a](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/3c9052a3031024e3bb37e49bbc6b189e1fc2c26e))
* **wallet:** add integration with decentralized web node [LAB-343] ([c623050](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/c623050d98776a29b530b2af0a8bcb25fb9301ec))

# [1.6.0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.5.0...v1.6.0) (2023-08-28)


### Bug Fixes

* adapt to new compliance examples structure ([547a11f](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/547a11fe49735994e43bd5e90ca9bc5368eb0a6a))
* add controller and JWS context to generated DID documents ([314f73b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/314f73b76dc942605389abe76052cdb953be9660))
* display compliance verified tag ([e7ba28a](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/e7ba28a15c020605687f2ca2e5ea4a9938a2f4c7))
* enable sign button when verification method set ([f34bddc](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/f34bddcb596083b9e138761db0b5f2bcfa14ffbf))
* get root url regular expression ([33e1cde](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/33e1cde66e27ecf038a82957bf67f9621a5503e4))
* increase compliance call timeout ([e6285dc](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/e6285dccf57d15989d3d1ab8860da82fd9992157))
* rely on NextLink for absolute navigation routing ([cfbc539](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/cfbc539eec909b13a79ece37a7a82776baa8923f))
* replace headingSize prop by size to avoid browser console error ([89ba236](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/89ba23688e7c1a8f9f8a76028cb851688f150650))
* unselect deleted documents for compliance submission ([16c9998](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/16c999879d25ff54cbd834cc07cfd368ffdb63d4))
* Use getter to access private key ([701d354](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/701d354215a8cc868cce537ed89e73e365098de9))
* use proper relative path for menu on development environment ([c588262](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/c588262d09539715579fbc02ee3e1d7248a9275d))
* Use the correct path for webauthn challenge endpoint ([b6e1af1](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/b6e1af12b3f4f61c1bfb06768c4971413aa3b1cc))
* use the right path for requests on development env ([172a206](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/172a206ccd2c0fa845dd46bfb5b6864a38778a47))


### Features

* add button to add LRN VC in holder & wallet ([d02b879](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/d02b879562c91a17aa0f17275fd69a4f66ea15d5))
* add graph view for VC ([b301a73](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/b301a732f06896eccb723bd87dfea980e710a0bf))
* add LRN claim page ([83b4a44](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/83b4a443bc7137a9f7b8ac7441cbcc47925bc224))
* **compliance:** add an option to retrieve jwt credentials from compliance [LAB-352] ([63fd95d](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/63fd95d2fab8981541640eff226b885cfdda554d))
* create API route to call notary ([f61a035](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/f61a035855117bb7a262c906d4e5250e710ff3ef))
* generate new doc name after signature is done ([e035c8f](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/e035c8f864d9654b55c90a1010d0de5075dd0cad))
* handle clearing house selection ([f6a182c](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/f6a182cf75676d93c62a92cd7c935fee4e9465c2))
* improve import signed documents feature ([772a717](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/772a717244f364abe24ab2466a843abf972739b7))
* improve wallet entries json exports ([b0d4c40](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/b0d4c405d24a87a28eeedd3b81cb3054b8987175))
* keep given issuer and ask for credential subject id ([6f740c3](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/6f740c3f155c580eaefc3817fbd93946d2c950a9))
* **LAB-353:** improve normalization error message ([7bb3e64](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/7bb3e649596899dfc90a87af096fbf4afe0b2778))
* sign terms and conditions VC ([d9e06a9](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/d9e06a9d1a482eb9e362eed38fb494f438bda78a))
* **TAG-180:** multi VC issuance workflow ([246058f](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/246058feae09aa3f64558309e4956eb190342cca))
* UI to select documents for compliance call ([79fe45b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/79fe45b0502bd5cf0ed46edcfe8ee6f86907d4e2))
* Use webauthn security key to encrypt the user private key ([e06996c](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/e06996c09ad9919966d212b45bfa730171e7ef48))

# [1.5.0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.4.0...v1.5.0) (2023-07-18)


### Bug Fixes

* generation of credential subject id hmac ([f452ffc](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/f452ffcd209102814e7a6301013dc7f4f1ebf5b0))
* upload the VC using the right ID ([9007f21](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/9007f21d17cb891fe3e71ec2c3fbe8f682a8680e))


### Features

* add tooltip about did support ([25a31fd](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/25a31fd34c3a3b8c845ce131eed7546371495a86))
* **stepper:** use wallet participant entries as suggestions in providedBy field [TAG-165] ([4c1f044](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/4c1f044f711b07f8e792a1f19da0120226b1a953))

# [1.4.0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.3.0...v1.4.0) (2023-06-29)


### Bug Fixes

* add package-lock.json to gitignore ([4cd745f](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/4cd745f61ca344c9a2deec7811e99b45a7120b19))
* correct last merge conflicts resolution ([ae82fb6](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/ae82fb69ffd483546a4f028febe35f70c870dd76))
* country code regex pattern ([c511763](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/c511763e8f436b49b041f926158768f845aea7e3))
* deployment baseurl typo ([eb0f3a3](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/eb0f3a307ec0ec4ce0d32a294c56588a2772a07e))
* display entire VC when selecting examples ([c8a8ffa](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/c8a8ffab80abb160db86123dbb25b596843ff0c7))
* don't refetch examples on focus & fix examples get url creation ([4526d07](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/4526d070fa4347e086933ed61e24fec4c88613f1))
* generate DID ([9916c3b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/9916c3b567da8f3d0bcb5d3cf4fe0be11d4d7a3d))
* handling location pathname ([5d3087c](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/5d3087cb66acd7e628d985b71f72f807c92def90))
* ignore public key generation error ([e2ea4d5](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/e2ea4d56e4486aadb04524bd1e1cffe7f3de4d3a))
* keep providedBy example value ([fee5f2c](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/fee5f2c51268e6845dbe3c37bcbae107b59acae6))
* leaf certificate must appear first in chain ([7e125ce](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/7e125ce3d0c70beead0a439872d458851f013cd6))
* local wallet box size ([691db8f](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/691db8f2a6c533869bf6454aaf7565188e35934b))
* make VC pre resolvable ([7e17059](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/7e1705993307930f53d94ab65ca13193dc12463d))
* prevent inexistant key error ([ede266d](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/ede266d259f4f24b27d3d3b97b703bc9f045a8e1))
* remove unused didkey ([9cf571d](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/9cf571d3e3db81815ea20062241818d4d69061c8))
* use the right did:key format ([83c6353](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/83c6353d6498744a0871486af1a3f08f8cda6c37))
* use the right did:key format ([4f6460e](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/4f6460e058ef9ed96c10145044cf19f1c6a627b7))
* use the right did:key format ([ff033b0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/ff033b060ade81ffb2dd0f7e1b164e01b472983e))
* use the right did:key format ([2cb46d6](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/2cb46d6fa81df1861a814edf2eba3a239b5458a4))
* use the right did:key format ([bba9198](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/bba9198faeefc680546d97b91a2622cba99baec4))
* use the right VC identifiers ([435d313](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/435d3139296d52a2830a4e154547ae096d970317))
* yaml deployment file ([1712b66](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/1712b665cb689749ea0f2a798a0629a1d2cc418e))
* yaml deployment file ([135efe4](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/135efe44d43db3133cc83bf4ac0ab010edcaa36b))


### Features

* accept terms and conditions field for participant ([5a1c732](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/5a1c7321055ded62bb614a2664e880fa6d1a4cfc))
* add button to download wallet entries as zip ([9fc398d](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/9fc398db931b3f270bb6a7930eb23cdb555e040b))
* add contribute button in header ([db048a6](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/db048a6b30caee67d72c34d73a69762a3fa4df35))
* add custom issuer field if did dirty ([2bd4ce2](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/2bd4ce281f23c4a3677d5a0ee17e815444f87646))
* add edit button at third step ([e8e9e60](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/e8e9e6070eadaafa2d37f4e3b8d6b691359924be))
* add guided stepper button in header ([b50f713](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/b50f7132469799470fa70a91bc63f852a7c83c94))
* add step 2 ([bc71ad2](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/bc71ad2914f14ffb41600e67ce2932c576812606))
* add validate  api route ([96526b1](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/96526b129342641808403c15490108ab6e075003))
* allow custom issuer ([3320810](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/332081050eee9e0b99d055971a4ab48571943864))
* change document name generation ([108460b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/108460b6f1b9ec2a282052be006788fb10318f9c))
* **compliance:** Use https for compliance Id instead of did web [TAG-166] ([5ce545d](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/5ce545d2a3894138776ea9710e561c29b811a0b8))
* connect to s3 bucket ([cf22b99](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/cf22b99a70a314d12c72204ba195d1126ba0bf44))
* connect to s3 bucket ([d979f30](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/d979f309436a56cc28a28088a8fa33cd026680e6))
* create 1st step of stepper ([b6a74bb](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/b6a74bb8f36d6af8bebb9c8c4847be3c10691994))
* create 1st step of stepper ([78f1e1b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/78f1e1b60bd142e32dae1c853ed5eeceb93a0499))
* did key generation ([c6d0157](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/c6d015789a1eb37b091b897a5286891e082f04e5))
* did key generation ([babaa5c](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/babaa5c491b1a3d2333702381e27bcc78ec7b246))
* did key generation ([7fed697](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/7fed6976005b81cd6cb7aa2ddc4eb376e715dfa8))
* generate doc name by default ([02bc255](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/02bc2555f1806422d35f82b83cbc6a40b53ae101))
* handle signing VC & adding to holder with saving in local wallet ([1478dd5](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/1478dd5e4bf45d25daa13d2784640dcef88013da))
* handle terms and conditions ([8b0d677](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/8b0d677a1f3b298d0887d739f63690270e12d7a7))
* Implicit DID creation ([788820b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/788820bcd92a6a237323137920f01101fd3bdfb6))
* improve disclaimer behavior ([4b0001a](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/4b0001a8c7e9264da04233661c35962e0e29af83))
* improve disclaimer displaying ([ae1cc14](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/ae1cc1457be88fa96c82615fdaddea776a7010cd))
* improve service offering wallet ([3cf1d26](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/3cf1d26ac1f87618e4cb17dff905a6752dead5b0))
* migrate to vitest ([4d47dc5](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/4d47dc5284d708b3a9ec3315000237752ba172fa))
* migrate to vitest ([2182bc2](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/2182bc273f841723e744c47a81b3599c8d7d03e0))
* publish changelogs on slack ([ef11057](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/ef11057c1cc0be90df5687c1a1e18559bf765419))
* refacto stepper features into a context ([4a30445](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/4a304456e6ad120cad18ba1c79bff1aca8719b9b))
* resolvable x5u certificate chain ([9f1dee6](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/9f1dee61fc66e11e5338f614132312d6903a36cb))
* resolvable x5u certificate chain ([cb4e3d4](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/cb4e3d40c9c0ab5b4d2c88689a3f84ffd1830fab))
* rework Holder UI ([0bc9517](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/0bc95171487fdca49f63e747622a7b66f8fd695c))
* save json did into s3 bucket ([975a633](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/975a633fcab413192c13b3f16549f593ea8b7688))
* **signature:** use http Ids instead of did web when not provided [TAG-166] ([eec0d79](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/eec0d79ad3420bf26f94b19f6a41261b454bcc10))
* signing step ([74720b6](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/74720b68c9a0f001bfd6b1b0033c9990c95069e8))
* sort wallet entries by date in local wallet ui ([03b698a](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/03b698af2323b57eef5a5bfbcdab8130064bd0b3))
* store and retrieve VC by ID ([7496b67](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/7496b675e7fb1e3a3de0bb3fb7359470cdc6e464))
* store did/vc/x509 into different s3 buckets ([a94fcb4](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/a94fcb4dac05c6ef17523f8ab436fc8393c97015))
* support for storage alternatives ([74fd281](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/74fd281aaced22e81060af424dc2a5c93b481ff8))
* upload compliant VC for managed DID only ([ceab685](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/ceab6852c48b22e8de9e10745fe0f3d6ada2eb5d))
* upload self-signed VC ([6f9bf43](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/6f9bf43cc86c811b452ae1f6de93c35aa98685ed))
* upload VC ([f97dc57](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/f97dc57219d57d665acedc00379fad68fb33bfb9))
* use generateName for Issuer component ([10b01ab](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/10b01ab1c506ce4a930deeb222a6897c7ce26423))
* VC id generation ([96de647](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/96de647578ad83909d061289cccb4c3d19fca7b1))

# [1.3.0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.2.1...v1.3.0) (2023-05-02)


### Features

* add wizard disclaimer ([148c88a](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/148c88adbba75dd5e0eb859536c99fb78ca92f31))
* improve mobile view ([cfcfa13](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/cfcfa137886b6acacc3b7e98d61a79f532e8277f))

## [1.2.1](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.2.0...v1.2.1) (2023-04-27)


### Bug Fixes

* yarn.lock and not yarn-lock.json ([33a28c7](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/33a28c7de30446da3d805736cca72a2d9edb4194))

# [1.2.0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.1.3...v1.2.0) (2023-04-27)


### Bug Fixes

* add package-lock.json to gitignore ([3330eb2](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/3330eb2261b98e2897bd2edf71656aefcc433821))
* bff urls ([561441f](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/561441fa05f9e9d54920343f2584861d91d80cfd))
* build ([0ae3386](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/0ae3386d4cc3d39535a50050244c38ed02eaa026))
* call compliance dev for examples ([cf55774](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/cf55774e5687907329887b8414af49b68129b9b5))
* clear code editor on selecting default example ([2123254](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/21232546cf466a989619d05c9758df4204dbd91d))
* error handling when calling compliance ([e7b4b4b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/e7b4b4bf4b347a64aed287ec9e8458b5e93c1353))
* examples select font color ([d0986ae](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/d0986ae570e234b06f3c60b5e2f18ebab1b0dd7f))
* fix env var + try/catch + logs ([38d1f84](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/38d1f8414ceb39733810b5e1a954febe356d455b))
* move vitest to dev dep ([1da86f5](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/1da86f55c27aca63ce28940e6ec702512f4ffa84))
* relative url for calling bff ([10c6a64](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/10c6a64134a717ca5d3547c6ce366d8813fa2fe6))
* select example font color ([e40600c](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/e40600cafaabb6e9407b4134e231850897313cd3))
* use the right did:key format ([e6b00bd](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/e6b00bd2770738bc4b9a4c31a075a8758739eb20))
* use the right did:key format ([22a1973](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/22a1973594647387a5e02f79b506e7e0a78334e6))


### Features

* add call compliance button ([79f880e](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/79f880eeaaf89dcafad9cba4bcd1c50453510f5b))
* add documents length in local wallet ([be3ed16](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/be3ed160e4a80f39611e235e09390d46095dd29e))
* add private key and verification method examples ([fe58844](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/fe58844aab26ec3cad6050e1acb79e4f53d14d95))
* add style to genDidKey ([597f8b4](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/597f8b47653e379c320cd8007c567d118337d089))
* add tag component in local wallat ui ([38cea2c](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/38cea2cca2c3965d5861acab9326bd81527672d4))
* add tag for verified doc in local wallet ([4c8b9f7](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/4c8b9f73fe755bfff5962f9fb581b3b7ab35449c))
* add title on add document modal ([5b11889](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/5b118892988e5f0b8fde629ff6306f0a0ae9bffb))
* add validate  api route ([5759be1](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/5759be16d37df9e88d9261f4dcc762eb26e9fd1f))
* add view feature in local wallet ([3a92e75](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/3a92e75ff11a8b4c6e93a0f432b5861c56fc3236))
* can't add no signed documents in the holder ([8c88b6b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/8c88b6b5e09feddbce2550fbff316d88c96c9cd0))
* create holder columns ([630e3d6](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/630e3d6a237be39589f7887ffb049d2c7f6ef16d))
* did key generation ([66434f2](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/66434f22f5439f9cd73bc90c2ac1e25743e128ad))
* did key generation ([fc6acdd](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/fc6acdde74e13a4573ab744a3d79fb93b985e98e))
* handle adding external signed vc or vp ([9abf0ae](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/9abf0ae19ede5799c58624a9207c3bea4acdfa74))
* handle creating VP from multiple VCs ([0088501](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/0088501b9250b429649a2b9c8e2b71323dd75c17))
* handle view for verified document ([7abe5b6](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/7abe5b624e70eea07d2dcecad800c1d41315cbc4))
* improve compliance return error handling & improve holder UI ([2fd9519](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/2fd9519a14637f713eabbb0ff48abdc9285a37a4))
* improve examples feature by displaying only credentialSubject & reassembling the VC ([3d7f958](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/3d7f958b943fc7d5ee763ba1caa46d8f9701c345))
* migrate to vitest ([5ce6793](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/5ce67930b98b00d1a66efeafb4d38953262b8c03))
* persistance for example select input ([08c87a4](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/08c87a40bc9e56f736509d0b32a60c18c8e22b9a))
* rename doc saved in local wallet ([240efc0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/240efc0564b0eeab0415a4d31514258d0dc31188))
* rework Holder UI ([5d804f8](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/5d804f8d67b54405cda1331b3bf9eb9c76b3c5f5))
* unselect selection after saving or creating presentation ([4279088](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/427908863ba5cc6f3150ae5eeab85817444e51e3))
* validation on adding document ([a345184](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/a34518499b602bae5b14aa00d4f0228264c8097f))

## [1.1.3](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.1.2...v1.1.3) (2023-04-19)


### Bug Fixes

* change kafka env to point on files mounted ([5dcf2c3](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/5dcf2c3ff023d884991759024a26f34ec27d4552))

## [1.1.2](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.1.1...v1.1.2) (2023-04-19)


### Bug Fixes

* change kafka env to point on files mounted ([53b4ff8](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/53b4ff8167a001523feb6b9e349537cbf2d0c92b))

## [1.1.1](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.1.0...v1.1.1) (2023-04-19)


### Bug Fixes

* add package-lock.json to gitignore ([050304d](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/050304da486a1f8b7cd1b4ad22842c929a750503))
* fix env var + try/catch + logs ([9baec9c](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/9baec9c502959a88869e9955529915c856fdd81a))

# [1.1.0](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.0.7...v1.1.0) (2023-04-19)


### Bug Fixes

* UI auto connect local wallet ([18139b5](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/18139b5c5c44996b95cc39d968a501411159392b))


### Features

* add date to saved documents ([fb750ac](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/fb750ac8bf8b078fadc7ad4e4c79208ad9e18b4b))
* add tooltip on private key heading ([fe7c857](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/fe7c857dae2d891096bdc4b8c5acf1afb1f7439c))
* change heading ([09f367d](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/09f367d22a2d55e9a88e0bc0a1fc0ccd0dc5989c))
* create api route for VP examples ([2eea355](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/2eea3559c93fd29b592c31aa755fe75f6f74ec39))
* handle choose example ([1cc4779](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/1cc4779e0807e368a16d6d143f63b66232dcfcf4))
* handle delete document in local wallet ([9f3370c](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/9f3370cd88fcb836de5719ce1c0b4c0a6d00f89c))
* Implement Web Wallet UI & handle import/Export documents from it ([f223068](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/f223068a3b692f02d4bcc42d70dd87f9aacd5206))
* improve holder UI ([82e79ad](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/82e79ad623b018e47d628eeae30bcae14a5bde5e))
* improve sign documents tabs style ([cc2b322](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/cc2b32285db855f8ed66e7a1512d8b5b49c1db33))
* local wallet ui ([bb8b133](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/bb8b1336f03227a3118137b01447dfb71cefef27))
* save documents into local wallet ([29fbbd2](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/29fbbd2d9f65a0a19502499f102856f338aefc30))
* save private key into local wallet ([7cbec82](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/7cbec825da415326c0f295b43d7345594a917d70))
* send credential to kafka after they're issued from compliance ([72a5df9](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/72a5df94d1a424d0c046f9ad5e501784b90b13d3))
* submit document to compliance ([2da1c55](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/2da1c553ccecccdbf068cbd23c016aae20447414))

## [1.0.7](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.0.6...v1.0.7) (2023-04-13)


### Bug Fixes

* move from master to main branch ([476fb84](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/476fb842710de8699bf6f0b71ec44db7dbe1a4fa))

## [1.0.6](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.0.5...v1.0.6) (2023-04-13)


### Bug Fixes

* deploy released versions on / instead of vX ([bc30fae](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/bc30fae9fe3a9bf407197577e7e7edc7113026c2))

## [1.0.5](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.0.4...v1.0.5) (2023-04-13)


### Bug Fixes

* ingress path in helm files ([853935b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/853935b4220b48f3889b1177ac8f17093e8011d2))

## [1.0.4](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.0.3...v1.0.4) (2023-04-12)


### Bug Fixes

* fix image name gen and deployment ([a968d2b](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/a968d2b2148fcceff09145b3f47ac6aab68d12a6))

## [1.0.3](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.0.2...v1.0.3) (2023-04-12)


### Bug Fixes

* fix image name gen and deployment ([19bbe21](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/19bbe214a4e60cd0d5efeaa3fdac2d9d69378ce6))

## [1.0.2](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.0.1...v1.0.2) (2023-04-12)


### Bug Fixes

* fix image name gen and deployment ([e5e5688](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/e5e568881681f588a0ad322995df53c3925888f5))

## [1.0.1](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/compare/v1.0.0...v1.0.1) (2023-04-12)


### Bug Fixes

* fix docker image tag ([8341cda](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/8341cdaf04edd7e428497a01c276b618a5a86a3b))

# 1.0.0 (2023-04-12)


### Bug Fixes

* add generic did in VP example ([38dffe1](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/38dffe102a14563f1c4d6e7a518ded47ddcca5b6))
* build ([212fd83](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/212fd8391f50e4d0e7c3a23c7c99615950b776ca))
* remove wrong import ([42599d6](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/42599d66d6e09290643b001dd358e21fd9ce37b5))
* return type of signVerfiablePresentation ([272923f](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/272923f5af4058a38241940580df2222c0e3e718))


### Features

* add gitignore ([4e9a724](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/4e9a724dfa3e78551c129a2f2514ae89ec8cef68))
* add gx favicon ([cae8d64](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/cae8d64511bee8eaaa73cdfe04a3dc8216f09bed))
* add README ([11b41ba](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/11b41ba364ee774d2e3c4acc97268cce5a7e960d))
* add title into html head ([f5d7a9f](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/f5d7a9fce1aa23a552adfe4a7481646f112973f0))
* add verification method field ([252d140](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/252d140e859a380b3a251d015dea04385b299a6d))
* client side VC signing ([426a838](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/426a83869205118c8fdcfda277b78a2d3edb55c4))
* create tool ([281c680](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/281c680fd4e08b17c2bbd5097a05020362d1186c))
* handle signing VP & VC ([9d9eea7](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/9d9eea7c050d779d6a82df9793239ffbb5d52926))
* migrate on nextjs ([a199e8c](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/a199e8c8c87ceb1adb4dfcfe55efe749892b9744))
* responsive style ([5b759b3](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/commit/5b759b30b98e48b35e0371b7d577149e5f614608))
