import React from 'react'
import ReactMarkdown from 'react-markdown'
import { Container } from '@/components/ui/Container'
import * as fs from 'fs'
import rehypeSlug from 'rehype-slug'

type UserGuideProps = {
  userGuideMd: string
}

const UserGuide: React.FC<UserGuideProps> = ({ userGuideMd }) => {
  return (
    <Container h={'95vh'} overflowY={'auto'} className={'markdown-body'} background={'transparent'}>
      <ReactMarkdown children={userGuideMd} rehypePlugins={[rehypeSlug]} />
    </Container>
  )
}

export default UserGuide

export async function getStaticProps(): Promise<{ props: UserGuideProps }> {
  const fileContent = fs.readFileSync('docs/USERGUIDE.md', 'utf8').toString()
  const tocPlaceholderIndex = findTOCPlaceholder(fileContent)

  if (tocPlaceholderIndex !== -1) {
    const toc = generateTOC(fileContent)
    const beforeTOC = fileContent.split('\n').slice(0, tocPlaceholderIndex).join('\n')
    const afterTOC = fileContent
      .split('\n')
      .slice(tocPlaceholderIndex + 1)
      .join('\n')
    const userGuideMd = `${beforeTOC}\n${toc}\n${afterTOC}`
    return {
      props: {
        userGuideMd
      }
    }
  } else {
    const userGuideMd = fileContent.toString()
    return {
      props: {
        userGuideMd
      }
    }
  }
}

function findTOCPlaceholder(content: string): number {
  const lines = content.split('\n')
  return lines.findIndex(line => line.trim() === '[TOC]')
}

function generateTOC(content: string): string {
  const lines = content.split('\n')
  let toc = ''
  lines.forEach(line => {
    const match = line.match(/^(#+)\s+(.+)$/)
    if (match) {
      const level = match[1].length
      const text = `[${match[2]}](#${match[2].toLowerCase().replace(/\s/g, '-').replaceAll('*', '')})`
      toc += `${'  '.repeat(level - 1)}- ${text}\n`
    }
  })
  return toc
}
