import { PolicyReasoningStepperContainer } from '@/components/PolicyPlayground/PolicyReasoningStepperContainer'
import { WalletProvider } from '@/contexts/WalletContext'
import { SignatureProvider } from '@/contexts/SignatureContext'

export default function PolicyReasoningStepper() {
  return (
    <SignatureProvider defaultIsCustomIssuer={false}>
      <WalletProvider>
        <PolicyReasoningStepperContainer />
      </WalletProvider>
    </SignatureProvider>
  )
}
