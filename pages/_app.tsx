import type { AppProps } from 'next/app'
import { AppProvider } from '@/contexts/AppContext'
import { Box, ChakraProvider } from '@chakra-ui/react'
import { WalletProvider } from '@/contexts/WalletContext'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
import { Sidebar } from '@/components/Sidebar'
import { customTheme } from '@/customTheme'
import { ClearingHousesProvider } from '@/contexts/ClearingHousesContext'
import 'github-markdown-css/github-markdown.css'
import './userGuide/index.css'
import { DecentralizedWebNodeProvider } from '@/contexts/DecentralizedWebNodeContext'
import { LocalWalletProvider } from '@/contexts/LocalWalletContext'
import { PolicyStepperProvider } from '@/contexts/PolicyStepperContext'

const queryClient = new QueryClient()

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider theme={customTheme}>
      <QueryClientProvider client={queryClient}>
        <AppProvider>
          <PolicyStepperProvider>
            <WalletProvider>
              <LocalWalletProvider>
                <DecentralizedWebNodeProvider>
                  <ClearingHousesProvider>
                    <Box>
                      <Sidebar>
                        <Component {...pageProps} />
                      </Sidebar>
                    </Box>
                  </ClearingHousesProvider>
                </DecentralizedWebNodeProvider>
              </LocalWalletProvider>
            </WalletProvider>
          </PolicyStepperProvider>
        </AppProvider>
      </QueryClientProvider>
    </ChakraProvider>
  )
}
