import { StepperContainer } from '@/components/Stepper/StepperContainer'
import { SignatureProvider } from '@/contexts/SignatureContext'
import { StepperProvider } from '@/contexts/StepperContext'

export default function Stepper() {
  return (
    <SignatureProvider defaultIsCustomIssuer={false}>
      <StepperProvider allRequiredVCs={false}>
        <StepperContainer />
      </StepperProvider>
    </SignatureProvider>
  )
}
