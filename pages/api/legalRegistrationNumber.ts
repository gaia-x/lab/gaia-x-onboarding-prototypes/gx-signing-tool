import type { NextApiRequest, NextApiResponse } from 'next'
import { credentialsService, didService, notaryService } from '@/pages/api/service/providers.service'
import Joi from 'joi'
import { JsonLdDocument } from 'jsonld'
import { getString, joinPathParts } from '@/utils/util'

const VC_PARAM = 'vcid'
const ID_PARAM = 'didId'
const UID_PARAM = 'uid'

export default async function handler(req: NextApiRequest, res: NextApiResponse<JsonLdDocument | string>) {
  if (req.method === 'POST') {
    const { error } = validateBody(req.body)
    if (error) return res.status(400).send('Bad request')

    if (!req.query[VC_PARAM] && !req.query[UID_PARAM]) return res.status(400).send('Bad request')

    try {
      const clearingHouse = req.body.clearingHouse

      if (req.query[VC_PARAM] && req.query[UID_PARAM]) {
        const notaryVC = await notaryService.postLegalRegistrationNumberVC(
          Array.isArray(req.query[VC_PARAM]) ? req.query[VC_PARAM][0] : req.query[VC_PARAM],
          req.body.values,
          clearingHouse
        )

        if (req.body.stored && UID_PARAM in req.query && ID_PARAM in req.query) {
          await handleSave(req, JSON.stringify(notaryVC))
        }

        res.status(200).setHeader('Content-Type', 'application/ld+json').send(notaryVC)
        res.send(notaryVC)
      } else if (req.query[VC_PARAM]) {
        const notaryVC = await notaryService.postLegalRegistrationNumberVC(
          Array.isArray(req.query[VC_PARAM]) ? req.query[VC_PARAM][0] : req.query[VC_PARAM],
          req.body.values,
          clearingHouse
        )

        res.send(notaryVC)
      } else {
        res.status(400).send('Missing Verifiable Credential ID (vcid)')
      }
    } catch (e: any) {
      return res.status(400).send('Invalid registration number')
    }
  } else {
    return res.status(405).send(req.method + ' not allowed')
  }
}

async function handleSave(req: NextApiRequest, vc: string) {
  const didId = didService.pathToDid(req.query[ID_PARAM])
  const uId = getString(req.query[UID_PARAM])
  if (!!uId) {
    if (!('no-did-update' in req.query)) {
      await credentialsService.addVCToDidDoc(didId, uId, joinPathParts(didId))
    }
    return credentialsService.storeVC(didId + uId, JSON.parse(vc))
  }
  throw new Error('Missing Unique Identifier (uid)')
}

function validateBody(input: any) {
  const schema = Joi.object({
    stored: Joi.boolean().required(),
    values: Joi.object({
      '@context': Joi.array().items(Joi.string()).required(),
      id: Joi.string().required(),
      type: Joi.string().required()
    })
      .pattern(Joi.string().valid('gx:taxID', 'gx:EUID', 'gx:EORI', 'gx:vatID', 'gx:leiCode'), Joi.string().required())
      .required(),
    clearingHouse: Joi.string()
  })

  return schema.validate(input)
}
