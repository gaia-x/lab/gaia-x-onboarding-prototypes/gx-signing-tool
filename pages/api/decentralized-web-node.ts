import type { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios'
import { DecentralizedWebNodeRequest, DecentralizedWebNodeResponse, DWNEntry, DWNMethods, DWNReply } from '@/models/decentralizedWebNode'
import { parseJWT } from '@/utils/util'
import { VDocument } from '@/models/document'
import { StoredWalletEntry } from '@/models/wallet'
import { CredentialFormats, VerifiableCredential } from '@/sharedTypes'

const DWN_URL = 'nodeUrl'

interface ApiRequestBody extends NextApiRequest {
  body: StoredWalletEntry
}

export default async function handler(
  req: ApiRequestBody,
  res: NextApiResponse<
    | StoredWalletEntry[]
    | {
        message: string
      }
  >
) {
  try {
    if (!req.query[DWN_URL]) return res.status(400).send({ message: 'Bad request, missing required nodeUrl' })
    if (req.method === 'POST') {
      const request: DecentralizedWebNodeRequest = buildDwnRequest(req.body)
      const response = await axios.post<DecentralizedWebNodeResponse>(req.query[DWN_URL] as string, request, {
        timeout: 15000
      })

      if (response.data.replies.every(r => r.status.code === 200)) {
        res.send({ message: 'Added credential successfully' })
      } else {
        res.status(500).json({ message: 'Response error: ' + response.data.replies.map(r => r.status.detail).join(', ') })
      }
    } else if (req.method === 'GET') {
      const request: DecentralizedWebNodeRequest = { messages: Array.of({ descriptor: { method: DWNMethods.QUERY } }) }
      const response = await axios.post<DecentralizedWebNodeResponse>(req.query[DWN_URL] as string, request, {
        timeout: 15000
      })

      if (response.data.status.code === 200) {
        res.send(response.data.replies.flatMap((reply: DWNReply) => reply.entries).map((entry: DWNEntry) => dwnEntryToWalletEntry(entry)))
      } else {
        res.status(500).json({ message: 'Response error: ' + response.data.status.detail })
      }
    } else {
      return res.status(405).json({ message: 'Method Not Allowed' })
    }
  } catch (error: any) {
    console.error(error)
    res.status(500).json({ message: error.response?.data?.status?.detail || 'Error while sending request to ' + req.query[DWN_URL] })
  }
}

const dwnEntryToWalletEntry = (entry: DWNEntry): StoredWalletEntry => {
  const jwtData = Buffer.from(entry.data as string, 'base64').toString('utf-8')
  let document
  let type
  if (entry.dataFormat === CredentialFormats.VP_LD_JWT) {
    const vp = parseJWT(jwtData).vp as VDocument
    const verifiableCredentials = vp.verifiableCredential?.map(vc => parseJWT(vc.toString()).vc as VerifiableCredential)
    type = verifiableCredentials?.map((vc: VerifiableCredential) => vc?.credentialSubject.type)
    document = { ...vp, verifiableCredentials }
  } else {
    document = parseJWT(jwtData).vc as VDocument
    type = document?.credentialSubject?.type
  }
  return {
    key: entry.id,
    name: entry.id,
    type,
    document,
    isVerified: false,
    creation: new Date(entry.createdAt).toISOString(),
    rawDocument: jwtData,
    format: entry.dataFormat as CredentialFormats
  }
}

const buildDwnRequest = (credential: StoredWalletEntry): DecentralizedWebNodeRequest => {
  const base64Data = Buffer.from(credential.rawDocument as string, 'utf-8').toString('base64')
  return {
    messages: [
      {
        descriptor: {
          method: DWNMethods.WRITE,
          dateCreated: new Date(credential.creation).getTime(),
          recordId: credential.key,
          dataFormat: credential.format
        },
        data: base64Data
      }
    ]
  }
}
