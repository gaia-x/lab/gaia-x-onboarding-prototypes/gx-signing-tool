import type { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios'
import { ComplianceFormat, CredentialFormats, CredentialOffersApiResponseData } from '@/sharedTypes'
import { parseJWT } from '@/utils/util'
import { VDocument } from '@/models/document'

interface ApiRequestBody extends NextApiRequest {
  body: {
    clearingHouse: string
    verifiablePresentation: string
  }
}

export default async function handler(
  req: ApiRequestBody,
  res: NextApiResponse<
    | CredentialOffersApiResponseData
    | {
        message: string
      }
  >
) {
  try {
    if (req.method !== 'POST') {
      return res.status(405).json({ message: 'Method Not Allowed' })
    }

    if (!req.body.verifiablePresentation) return res.status(400).json({ message: 'verifiablePresentation required' })

    const url = req.body.clearingHouse
      ? `https://${req.body.clearingHouse}/api/credential-offers`
      : `${process.env.COMPLIANCE_BASE_URL}/credential-offers`

    const complianceResponse = await axios.post(url, req.body.verifiablePresentation, {
      timeout: 15000,
      params: req.query,
      headers: { accept: req.headers.accept }
    })

    const response: CredentialOffersApiResponseData = convertJwtToVDocument(complianceResponse, req.body.clearingHouse)

    res.send(response)
  } catch (error: any) {
    res.status(500).json({ message: error.response?.data?.message || 'Error while submitting document to compliance' })
  }
}

const convertJwtToVDocument = (complianceResponse: any, clearingHouse: string) => {
  const contentType: ComplianceFormat = complianceResponse.headers['content-type']?.split(';')[0]
  const result: CredentialOffersApiResponseData = {
    data: complianceResponse.data,
    clearingHouse,
    format: contentType
  }

  // Even though the output is in JWT, we parse the compliance response to display a human-readable credential
  switch (contentType) {
    case CredentialFormats.VC_JWT:
      result.jwt = complianceResponse.data
      result.data = parseJWT(complianceResponse.data).vc as VDocument
      return result
    case CredentialFormats.VP_LD_JWT:
      result.jwt = complianceResponse.data
      const vp = parseJWT(complianceResponse.data).vp as VDocument
      vp.verifiableCredential = vp.verifiableCredential?.map(vc => parseJWT(vc.toString()).vc as VDocument)
      result.data = vp
      return result
    default:
      return result
  }
}
