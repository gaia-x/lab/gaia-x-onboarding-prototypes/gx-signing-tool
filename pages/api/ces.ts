import { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios'

export default async function handler(req: NextApiRequest, res: NextApiResponse<{ message: string } | null>) {
  if (req.method !== 'POST') {
    return res.status(405).json({ message: 'Method Not Allowed' })
  }
  const data = req.body

  if (!data.event || !data.deploymentPath) {
    return res.status(400).json({ message: 'Bad Request, missing event or deploymentPath' })
  }

  let cesURI = ''
  if ('v1' === data.deploymentPath) {
    cesURI = 'https://ces-v1.lab.gaia-x.eu/credentials-events'
  } else {
    cesURI = 'https://ces-development.lab.gaia-x.eu/credentials-events'
  }

  try {
    const response = await axios.post(cesURI, data.event)
    if (response.status === 201) {
      return res.status(201).send(null)
    } else {
      return res.status(400).json({ message: 'Unable to send credential to the CES due to ' + response.data })
    }
  } catch (error: any) {
    return res.status(502).json({ message: 'Unable to send credential to the CES due to ' + error?.response?.data?.message ?? error.message })
  }
}
