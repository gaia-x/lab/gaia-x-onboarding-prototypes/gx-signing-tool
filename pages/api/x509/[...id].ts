import type { NextApiRequest, NextApiResponse } from 'next'
import { keyChainService } from '../service/providers.service'

function getId(req: NextApiRequest): string {
  let id = req.query.id
  if (!id) {
    return ''
  }
  if (Array.isArray(id)) {
    id = id[0]
  }
  if (id.endsWith('.crt') || id.endsWith('.pem')) {
    id = id.slice(0, -4)
  }
  return id
}

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  try {
    if (req.method === 'GET') {
      res.status(200).send(await keyChainService.getX509ByName(getId(req)))
    } else {
      res.status(405).send(req.method + ' not allowed')
    }
  } catch (error: any) {
    console.log(error)
    res.status(error['statusCode'] ?? 400).send(error?.message ?? error)
  }
}
