import { EIDJWSResponseData } from '@/sharedTypes'
import { NextApiRequest, NextApiResponse } from 'next'
import { Buffer } from 'buffer'
import { X509 } from 'jsrsasign'
import * as ocsp from '@techteamer/ocsp'
import axios from 'axios'

export default async function handler(req: NextApiRequest, res: NextApiResponse<EIDJWSResponseData | { message: string }>) {
  try {
    if (req.method !== 'POST') {
      return res.status(405).json({ message: 'Method Not Allowed' })
    }

    const data = req.body

    if (!data.payload || !data.header || !data.signature || !data.certificate) {
      return res.status(400).json({ message: 'Bad Request' })
    }
    // convert the certificate from base64 to hex and build a X509 object
    const certDerHex = Buffer.from(data.certificate, 'base64').toString('hex')
    const cert = new X509()
    cert.readCertHex(certDerHex)

    // get the AIA extension
    const certAIAInfo = cert.getExtAIAInfo()

    if (!certAIAInfo?.caissuer.length) {
      return res.status(400).json({ message: 'Certificate does not include AIA extension' })
    }

    const response = await axios.get(certAIAInfo.caissuer[0], { responseType: 'arraybuffer' })

    // build the ocsp request
    ocsp.check(
      {
        cert: Buffer.from(data.certificate, 'base64'),
        issuer: response.data
      },
      function (error: any, result: ocsp.CertInfo) {
        if (error) throw error

        if (result.certStatus.type === 'good') {
          // OCSP response is valid, build the VC proof
          const jws = buildJws(data)
          return res.status(200).json({ jws, valid: true, status: result.certStatus.type })
        } else {
          // Success, but certificate is revoked or unknown, do not build the VC proof
          return res.status(200).json({ jws: '', valid: false, status: result.certStatus.type })
        }
      }
    )
  } catch (error: any) {
    res.status(500).send(error.message)
    console.log(error)
  }
}

function buildJws(data: { payload: string; header: string; signature: string }): string {
  const encodedHeader = Buffer.from(data.header).toString('base64')
  const encodedPayload = Buffer.from(data.payload).toString('base64')
  const jws = `${encodedHeader}.${encodedPayload}.${data.signature}`

  return jws
}
