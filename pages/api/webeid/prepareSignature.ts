import { EIDHashResponseData } from '@/sharedTypes'
import crypto from 'crypto'
import { NextApiRequest, NextApiResponse } from 'next'

export default async function handler(req: NextApiRequest, res: NextApiResponse<EIDHashResponseData | { message: string }>) {
  try {
    if (req.method !== 'POST') {
      return res.status(405).json({ message: 'Method Not Allowed' })
    }

    const data = req.body

    if (!data.payload || !data.header || !data.supportedSignatureAlgorithms?.hashFunction) {
      return res.status(400).json({ message: 'Bad Request' })
    }

    const resData = hashDocument(data)

    return res.send(resData)
  } catch (error: any) {
    if (error.message === 'Unsupported hash function') {
      return res.status(400).json({ message: error.message })
    }
    res.status(500).send(error.message)
    console.log(error)
  }
}

function hashDocument(data: { payload: string; header: string; supportedSignatureAlgorithms: { hashFunction: string } }): EIDHashResponseData {
  const encodedHeader = Buffer.from(data.header).toString('base64')
  const encodedPayload = Buffer.from(data.payload).toString('base64')
  const payloadAndHeader = `${encodedHeader}.${encodedPayload}`

  switch (data.supportedSignatureAlgorithms.hashFunction) {
    case 'SHA-224':
      return { hash: crypto.createHash('sha224').update(payloadAndHeader).digest('base64'), hashFunction: 'SHA-224' }
    case 'SHA-256':
      return { hash: crypto.createHash('sha256').update(payloadAndHeader).digest('base64'), hashFunction: 'SHA-256' }
    case 'SHA-384':
      return { hash: crypto.createHash('sha384').update(payloadAndHeader).digest('base64'), hashFunction: 'SHA-384' }
    case 'SHA-512':
      return { hash: crypto.createHash('sha512').update(payloadAndHeader).digest('base64'), hashFunction: 'SHA-512' }
    default:
      throw new Error('Unsupported hash function')
  }
}
