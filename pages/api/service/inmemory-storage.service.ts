import { StorageService } from './storage.service'

export class InMemoryStorageService implements StorageService {
  private store: { [bucketName: string]: { [fileName: string]: any } } = {}

  upload(file: any, fileName: string, bucketName: string): Promise<void> {
    ;(this.store[bucketName] ??= {})[fileName] = file
    return Promise.resolve()
  }

  download(fileName: string, bucketName: string): Promise<string | undefined> {
    return new Promise(r => r(this.store?.[bucketName]?.[fileName]))
  }
}
