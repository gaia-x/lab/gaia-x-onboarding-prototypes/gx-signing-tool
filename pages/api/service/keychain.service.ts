import base58 from 'bs58'
import forge from 'node-forge'
import { StorageService } from './storage.service'
import { joinPath } from '@/utils/util'
import { KEYUTIL, KJUR } from 'jsrsasign'

function parseProcessPem(pem: string): string {
  return pem?.replaceAll(/\\?\\n/g, '\n')
}

export class KeychainService {
  private readonly host: string
  private readonly x509Root: string
  private readonly rootPrivateKeyPem: string
  private readonly intermediateCert: forge.pki.Certificate

  constructor(
    private storageService: StorageService,
    private keychainPath = process.env.KEYCHAIN_ENDPOINT_PATH ?? 'api/x509'
  ) {
    if (!process.env.BASE_URL) throw new Error('Missing BASE_URL env variable')
    this.host = process.env.BASE_URL

    if (!process.env.DID_X509_ROOT_CERTIFICATE) throw new Error('Missing DID_X509_ROOT_CERTIFICATE env variable')
    this.x509Root = parseProcessPem(process.env.DID_X509_ROOT_CERTIFICATE)
    if (!this.x509Root) throw new Error('DID_X509_ROOT_CERTIFICATE env variable empty')

    if (!process.env.DID_X509_ROOT_PRIVATE_KEY) throw new Error('Missing DID_X509_ROOT_PRIVATE_KEY env variable')
    const rootPrivateKey = parseProcessPem(process.env.DID_X509_ROOT_PRIVATE_KEY)
    if (!rootPrivateKey) throw new Error('DID_X509_ROOT_PRIVATE_KEY env variable empty')

    this.intermediateCert = forge.pki.certificateFromPem(this.x509Root)
    this.rootPrivateKeyPem = rootPrivateKey
  }

  async createX509(id: string, publicKeyPem: string): Promise<{ url: string; x509: string }> {
    let x509Chain: string
    const publicKey = KEYUTIL.getKey(publicKeyPem)
    if (publicKey instanceof KJUR.crypto.ECDSA) {
      x509Chain = await this.createECDSAX509(id, publicKey)
    } else {
      x509Chain = await this.createRSAX509(id, publicKeyPem)
    }
    const x509Name = this.getX509Name(id)
    await this.storageService.upload(x509Chain, x509Name, 'x509')
    return {
      url: joinPath(this.host, this.keychainPath, x509Name + '.pem'),
      x509: x509Chain
    }
  }

  async createECDSAX509(id: string, publicKey: KJUR.crypto.ECDSA): Promise<string> {
    // Build CSR
    const csr = new KJUR.asn1.csr.CertificationRequest({
      subject: { str: '/C=BE/L=Brussels/O=Gaia-X AISBL/CN=' + id },
      sbjpubkey: KEYUTIL.getPEM(publicKey),
      sigalg: 'SHA256withRSA',
      sbjprvkey: this.rootPrivateKeyPem
    })
    csr.sign()
    const keyFromCsr = KEYUTIL.getKeyFromCSRPEM(csr.getPEM())

    // Generate certificate from CSR
    const cert = new KJUR.asn1.x509.Certificate({
      serial: Math.round(Math.random() * 1_000_000),
      issuer: { str: '/CN=' + id },
      notbefore: utcStrippedDate(new Date()),
      notafter: utcStrippedDate(futureDate(new Date())),
      subject: { str: '/CN=' + id },
      sbjpubkey: keyFromCsr,
      ext: [{ extname: 'basicConstraints', cA: false }],
      sigalg: 'SHA256withRSA',
      cakey: this.rootPrivateKeyPem
    })
    const pemCert = cert.getPEM()

    const intermediateCertPem = forge.pki.certificateToPem(this.intermediateCert)

    return `${pemCert}${intermediateCertPem}${this.x509Root}`
  }

  async createRSAX509(id: string, publicKeyPem: string): Promise<string> {
    const forgePublicKey = forge.pki.publicKeyFromPem(publicKeyPem)
    const privateKey = forge.pki.privateKeyFromPem(this.rootPrivateKeyPem)

    // Build CSR
    const csr = forge.pki.createCertificationRequest()
    csr.publicKey = forgePublicKey
    csr.setSubject([
      { name: 'commonName', value: id },
      { name: 'countryName', value: 'BE' },
      { name: 'organizationName', value: 'Gaia-X AISBL' }
    ])
    csr.sign(privateKey)

    // Generate certificate from CSR
    const cert = forge.pki.createCertificate()
    cert.setSubject(this.intermediateCert.subject.attributes)
    cert.setIssuer(this.intermediateCert.subject.attributes)
    cert.validity.notBefore = new Date()
    cert.validity.notAfter = futureDate(cert.validity.notBefore)
    cert.publicKey = csr.publicKey
    cert.serialNumber = forge.util.bytesToHex(forge.random.getBytesSync(16))
    cert.setExtensions([
      {
        name: 'basicConstraints',
        cA: false
      }
    ])
    cert.sign(privateKey)

    const intermediateCertPem = forge.pki.certificateToPem(this.intermediateCert)
    const pemCert = forge.pki.certificateToPem(cert)

    return `${pemCert}${intermediateCertPem}${this.x509Root}`
  }

  getX509Name(id: string) {
    return base58.encode(new TextEncoder().encode(id))
  }

  getX509ById(id: string): Promise<string | undefined> {
    return this.getX509ByName(this.getX509Name(id))
  }

  getX509ByName(x509Name: string): Promise<string | undefined> {
    return this.storageService.download(x509Name, 'x509')
  }
}

function futureDate(src: Date, addYears = 1) {
  const target = new Date(src)
  target.setFullYear(src.getFullYear() + addYears)
  return target
}

/**
 * @param date the date to format
 * @returns date in format YYMMDDhhmmssZ
 */
function utcStrippedDate(date: Date) {
  return date
    .toISOString()
    .substring(2)
    .replace(/-/g, '')
    .replace(/:/g, '')
    .replace('T', '')
    .replace(/\.\d{3}/, '')
}
