import { CredentialsService } from './credentials.service'
import { DidService } from './did.service'
import { DiskStorageService } from './disk-storage.service'
import { InMemoryStorageService } from './inmemory-storage.service'
import { KeychainService } from './keychain.service'
import { S3Service } from './s3.service'
import { StorageService } from './storage.service'
import { NotaryService } from '@/pages/api/service/notary.service'

export const storageService: StorageService = (() => {
  switch (process.env.STORAGE_PROVIDER || (process.env.AWS_ENDPOINT ? 's3' : 'inmemory')) {
    case 's3':
      return new S3Service()
    case 'disk':
      return new DiskStorageService()
    case 'inmemory':
    default:
      return new InMemoryStorageService()
  }
})()

export const keyChainService = new KeychainService(storageService)
export const didService = new DidService(keyChainService)
export const credentialsService = new CredentialsService(didService, storageService)

export const notaryService = new NotaryService()
