import AWS from 'aws-sdk'
import S3 from 'aws-sdk/clients/s3'
import { md5 } from 'node-forge'
import { StorageService } from './storage.service'

const KEY_LIMIT = 255

function filterKey(key: string) {
  if (key.length <= KEY_LIMIT) {
    return key
  }
  return md5.create().update(key).digest().toHex().substring(0, KEY_LIMIT)
}

export class S3Service implements StorageService {
  private s3: S3
  private bucketChecks: { [name: string]: Promise<void> } = {}
  private s3GlobalBucketNames: { [name: string]: string } = {}
  private readonly accessKeyId: string
  private readonly secretAccessKey: string
  private readonly endpoint: string
  private readonly region: string

  constructor(private preventOverride = process.env.AWS_PREVENT_OVERRIDE === 'true') {
    if (!process.env.AWS_ACCESS_KEY_ID) throw new Error('Missing AWS_ACCESS_KEY_ID env variable')
    this.accessKeyId = process.env.AWS_ACCESS_KEY_ID
    if (!process.env.AWS_SECRET_ACCESS_KEY) throw new Error('Missing AWS_SECRET_ACCESS_KEY env variable')
    this.secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY
    if (!process.env.AWS_ENDPOINT) throw new Error('Missing AWS_ENDPOINT env variable')
    this.endpoint = process.env.AWS_ENDPOINT
    if (!process.env.AWS_REGION) throw new Error('Missing AWS_REGION env variable')
    this.region = process.env.AWS_REGION

    this.s3 = new AWS.S3({
      accessKeyId: this.accessKeyId,
      secretAccessKey: this.secretAccessKey,
      endpoint: this.endpoint,
      region: this.region
    })
  }

  async findBucket(s3BucketName: string): Promise<S3.Bucket> {
    const r = await this.s3.listBuckets().promise()
    if (r.$response.error) {
      throw new Error(r.$response.error.message)
    }
    const bucket = r.Buckets?.find(b => b.Name === s3BucketName)
    if (!bucket) {
      throw new Error(`bucket ${s3BucketName} not found, available: ${r.Buckets?.map(b => b.Name).join(', ')}`)
    }
    return bucket
  }

  async connect(s3BucketName: string): Promise<void> {
    try {
      await this.findBucket(s3BucketName)
    } catch (e: any) {
      const r = await this.s3.createBucket({ Bucket: s3BucketName }).promise()
      if (r.$response.error) {
        throw new Error(e.message + ' >> ' + r.$response.error)
      }
    }
  }

  async useBucket(bucketAliasName: string): Promise<string> {
    const s3BucketName = this.resolveBucketAliasName(bucketAliasName)
    await (this.bucketChecks[bucketAliasName] ??= this.connect(s3BucketName))
    return s3BucketName
  }

  async upload(file: any, fileName: string, bucketAliasName: string) {
    if (this.preventOverride && (await this.download(fileName, bucketAliasName))) {
      throw new Error(`${fileName} already exists`)
    }
    await this.s3
      .upload({
        Bucket: await this.useBucket(bucketAliasName),
        Key: filterKey(fileName),
        Body: file
      })
      .promise()
  }

  async download(fileName: string, bucketAliasName: string) {
    try {
      return (
        await this.s3
          .getObject({
            Bucket: await this.useBucket(bucketAliasName),
            Key: filterKey(fileName)
          })
          .promise()
      ).Body?.toString()
    } catch {
      // key does not exists
    }
  }

  private resolveBucketAliasName(bucketName: string): string {
    return (this.s3GlobalBucketNames[bucketName] ??= process.env[`STORAGE_S3_${bucketName.toUpperCase()}_BUCKET`] || bucketName)
  }
}
