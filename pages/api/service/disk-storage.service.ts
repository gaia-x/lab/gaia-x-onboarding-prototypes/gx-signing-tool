import { joinPath } from '@/utils/util'
import { mkdirSync, readFileSync, writeFileSync } from 'fs'
import { md5 } from 'node-forge'
import { StorageService } from './storage.service'

export class DiskStorageService implements StorageService {
  async upload(file: any, fileName: string, bucketName: string): Promise<void> {
    writeFileSync(this.useFolder(bucketName) + '/' + this.filterFilename(fileName), file, { encoding: 'utf-8' })
  }

  async download(fileName: string, bucketName: string): Promise<string | undefined> {
    try {
      return readFileSync(this.useFolder(bucketName) + '/' + this.filterFilename(fileName), { encoding: 'utf-8' })
    } catch {
      return undefined
    }
  }

  private filterFilename(filename: string) {
    return md5.create().update(filename).digest().toHex().substring(0, 50)
  }

  private useFolder(name: string) {
    const path = joinPath(process.env.DISK_STORAGE_PATH || './storage', name)
    mkdirSync(path, { recursive: true })
    return path
  }
}
