export interface StorageService {
  upload(file: any, fileName: string, bucketName: string): Promise<void>
  download(fileName: string, bucketName: string): Promise<string | undefined>
}
