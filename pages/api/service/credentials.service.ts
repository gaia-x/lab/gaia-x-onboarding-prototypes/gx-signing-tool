import { DidDocRequest, DidService } from './did.service'
import { JsonLdDocument } from 'jsonld'
import { StorageService } from './storage.service'
import { joinPath } from '@/utils/util'

export class CredentialsService {
  constructor(
    private didService: DidService,
    private storageService: StorageService,
    private host = process.env.BASE_URL,
    private didPath = process.env.DID_ENDPOINT_PATH ?? '/api/credentials'
  ) {}

  async storeDidDoc(id: string, req: DidDocRequest, store = true): Promise<JsonLdDocument> {
    const did = await this.storageService.download(id, 'did')
    if (did) return JSON.parse(did)
    const didDoc = await this.didService.createDidDocument(id, req)
    if (store) {
      await this.storageService.upload(JSON.stringify(didDoc), id, 'did')
    }
    return didDoc
  }

  async addVCToDidDoc(id: string, uid: string, path: string): Promise<void> {
    if (!this.host) throw new Error('Missing BASE_URL env variable')

    const didDoc: any = await this.getDidDoc(id)
    if (didDoc) {
      const fullVcId = id + '?uid=' + uid
      if (didDoc.service && didDoc.service.find((s: any) => s.id === fullVcId)) {
        throw new Error('This VC already exists, use another name')
      }
      ;(didDoc.service ??= []).push({
        id: fullVcId,
        type: 'LinkedDomains',
        serviceEndpoint: joinPath(this.host, this.didPath, path) + '?uid=' + uid
      })
      await this.storageService.upload(JSON.stringify(didDoc), id, 'did')
    }
  }

  async getDidDoc(id: string): Promise<JsonLdDocument> {
    return JSON.parse((await this.storageService.download(id, 'did')) || '{}')
  }

  async storeVC(id: string, vc: JsonLdDocument): Promise<JsonLdDocument> {
    await this.storageService.upload(JSON.stringify(vc), id, 'vcs')
    return vc
  }

  async getVC(id: string): Promise<JsonLdDocument> {
    return JSON.parse((await this.storageService.download(id, 'vcs')) || '{}')
  }
}
