import type { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios'

type Data = { [key: string]: string[] }
type Error = { message: string }

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data | Error>) {
  try {
    if (req.method !== 'GET') {
      return res.status(405).json({ message: 'Method Not Allowed' })
    }

    const clearingHouses = (await axios.get(process.env.REGISTRY_TRUSTED_ISSUERS_URL || `https://registry.gaia-x.eu/v1/api/trusted-issuers`)).data
    res.send(clearingHouses)
  } catch (error: any) {
    res.status(500).send(error.message)
    console.log(error)
  }
}
