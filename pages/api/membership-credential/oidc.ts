import type { NextApiRequest, NextApiResponse } from 'next'
import { JsonLdDocument } from 'jsonld'
import axios from 'axios'

const oidcServiceUrl = () => {
  if (!process.env.OIDC_SERVICE_BASE_URL) throw new Error('Missing OIDC_SERVICE_BASE_URL env variable')
  return process.env.OIDC_SERVICE_BASE_URL
}

export default async function handler(req: NextApiRequest, res: NextApiResponse<JsonLdDocument | string>) {
  if (req.method !== 'POST') return res.status(405).send({ message: 'Method not allowed' })

  try {
    if (!req.body) return res.status(400).send({ message: 'Bad request, missing required credential in body' })

    const response = await axios.post<any>(oidcServiceUrl(), req.body, {
      timeout: 15000
    })
    if (response.status === 200) {
      res.send(response.data)
    } else {
      res.status(500).json({ message: 'Response error: ' + response.data })
    }
  } catch (error: any) {
    console.error(error)
    res.status(500).json({ message: 'Error while requesting OIDC authorization' })
  }
}
