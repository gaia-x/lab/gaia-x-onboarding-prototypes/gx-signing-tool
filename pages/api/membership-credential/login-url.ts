import type { NextApiRequest, NextApiResponse } from 'next'
import { JsonLdDocument } from 'jsonld'

const keycloakBaseUrl = () => {
  if (!process.env.KEYCLOAK_BASE_URL) throw new Error('Missing KEYCLOAK_BASE_URL env variable')
  return process.env.KEYCLOAK_BASE_URL
}

export default async function handler(req: NextApiRequest, res: NextApiResponse<JsonLdDocument | string>) {
  if (req.method !== 'GET') return res.status(405).send({ message: 'Method not allowed' })

  const baseUrlPath = `${keycloakBaseUrl()}/auth`
  const clientId = `?client_id=${process.env.KEYCLOAK_CLIENT_ID || 'wizard'}`
  const scope = '&scope=openid email profile'
  const responseType = '&response_type=code'
  const redirectUri = `&redirect_uri=${process.env.BASE_URL || 'https://wizard.lab.gaia-x.eu'}/membershipCredential`

  const url = [baseUrlPath, clientId, scope, responseType, redirectUri].join('')

  res.send(url)
}
