import type { NextApiRequest, NextApiResponse } from 'next'
import { JsonLdDocument } from 'jsonld'
import axios from 'axios'
import { getString } from '@/utils/util'

const membershipUrl = () => {
  if (!process.env.MEMBERSHIP_CREDENTIAL_PORTAL_URL) throw new Error('Missing MEMBERSHIP_CREDENTIAL_PORTAL_URL env variable')
  return process.env.MEMBERSHIP_CREDENTIAL_PORTAL_URL
}

const DEFAULT_CREDENTIAL_TYPE: string = 'VCARD'

export default async function handler(req: NextApiRequest, res: NextApiResponse<JsonLdDocument | string>) {
  if (req.method !== 'GET') return res.status(405).send({ message: 'Method not allowed' })

  try {
    const authorizationCode = getString(req.query['code'])
    const type = getString(req.query['type']) || DEFAULT_CREDENTIAL_TYPE
    if (!authorizationCode) return res.status(400).send({ message: 'Bad request, missing required code param' })

    const accessToken = await getAccessToken(authorizationCode)
    const headers = {
      'content-type': 'application/x-www-form-urlencoded',
      Authorization: 'Bearer ' + accessToken
    }

    const response = await axios.get<any>(membershipUrl() + '?type=' + type, {
      headers,
      timeout: 15000
    })
    if (response.status === 200) {
      res.status(200).setHeader('Content-Type', 'application/ld+json').send(response.data)
      res.send(response.data)
    } else {
      res.status(500).json({ message: 'Response error: ' + response.data })
    }
  } catch (error: any) {
    console.error(error)
    res.status(500).json({ message: 'Error while requesting membership credential' })
  }
}

const getAccessToken = async (authorizationCode: string): Promise<string> => {
  const headers = {
    'content-type': 'application/x-www-form-urlencoded'
  }

  const body = new URLSearchParams()
  body.append('grant_type', 'authorization_code')
  body.append('client_id', process.env.KEYCLOAK_CLIENT_ID || 'wizard')
  body.append('client_secret', process.env.KEYCLOAK_CLIENT_SECRET || '')
  body.append('redirect_uri', process.env.BASE_URL + '/membershipCredential')
  body.append('code', authorizationCode)

  const url: string = process.env.KEYCLOAK_BASE_URL + '/token' || 'https://auth.gaia-x.eu/realms/gaia-x/protocol/openid-connect/token'
  const response = await axios.post<any>(url, body, {
    headers,
    timeout: 15000
  })
  if (response.status === 200) {
    return response?.data?.access_token
  } else {
    throw new Error('Failed to retrieve access token from keycloak')
  }
}
