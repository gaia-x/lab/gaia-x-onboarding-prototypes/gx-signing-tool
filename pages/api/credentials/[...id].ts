import { getString, joinPathParts } from '@/utils/util'
import type { NextApiRequest, NextApiResponse } from 'next'
import { credentialsService, didService } from '../service/providers.service'

const VC_PARAM = 'vcid'
const UID_PARAM = 'uid'

async function handleSave(req: NextApiRequest) {
  const didId = didService.pathToDid(req.query.id)
  if (UID_PARAM in req.query) {
    const vcId = getString(req.query[UID_PARAM])
    if (!('no-did-update' in req.query) && vcId) {
      await credentialsService.addVCToDidDoc(didId, vcId, joinPathParts(req.query.id))
    }

    return credentialsService.storeVC(didId + vcId, JSON.parse(req.body))
  }
  return await credentialsService.storeDidDoc(
    didId,
    {
      publicKeyPem: req.body,
      certchainUri: getString(req.query.certchainUri)
    },
    !('storeless' in req.query)
  )
}

async function handleRetrieve(req: NextApiRequest) {
  const didId = didService.pathToDid(req.query.id)
  const vcParam = getString(req.query[VC_PARAM] || req.query[UID_PARAM])

  if (!!vcParam) {
    try {
      return await credentialsService.getVC(didId + vcParam)
    } catch (e: any) {
      // VC ID must be resolvable before the document is actually uploaded
      return {
        id: `${didId}?${VC_PARAM}=${getString(req.query[VC_PARAM])}`,
        exists: false,
        error: e.message
      }
    }
  }
  return credentialsService.getDidDoc(didService.pathToDid(req.query.id))
}

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  try {
    if (req.method === 'POST' || req.method === 'PUT' || req.method === 'PATCH') {
      res
        .status(200)
        .setHeader('Content-Type', 'application/ld+json')
        .send(JSON.stringify(await handleSave(req), null, 2))
    } else if (req.method === 'GET') {
      const data = await handleRetrieve(req)
      res
        .status(200)
        .setHeader('Content-Type', 'application/ld+json')
        .send(JSON.stringify(data || '{}', null, 2))
    } else {
      res.status(405).send(req.method + ' not allowed')
    }
  } catch (error: any) {
    console.log(error)
    res.status(error['statusCode'] ?? 400).send(error?.message ?? error)
  }
}
