import { EIDCertchainPostResponseData } from '@/sharedTypes'
import { NextApiRequest, NextApiResponse } from 'next'
import { Buffer } from 'buffer'
import { X509 } from 'jsrsasign'
import axios from 'axios'
import { storageService } from '../service/providers.service'

export default async function handler(req: NextApiRequest, res: NextApiResponse<EIDCertchainPostResponseData | string | { message: string }>) {
  try {
    const { id } = req.query
    const data = req.body

    if (typeof id !== 'string') {
      return res.status(400).json({ message: 'Invalid ID' })
    }

    if (req.method === 'POST') {
      const certificate = data
      if (!certificate) {
        return res.status(400).json({ message: 'Certificate not found in request body' })
      }
      const certificateChain = await handleSave(id, certificate)
      return res.status(200).json({ certChain: certificateChain })
    }
    if (req.method === 'GET') {
      const certChainPem = await handleRetrieve(id)
      if (!certChainPem) {
        return res.status(404).json({ message: 'Certificate chain not found' })
      }
      if (req.headers.accept === 'application/x-pem-file') {
        res.setHeader('Content-Type', 'application/x-pem-file')
        res.setHeader('Content-Disposition', 'attachment; filename=' + id + '.pem')
      } else {
        res.setHeader('Content-Type', 'text/plain')
      }

      return res.status(200).send(certChainPem)
    }
    return res.status(405).json({ message: 'Method Not Allowed' })
  } catch (error: any) {
    res.status(500).send(error.message)
    console.log(error)
  }
}

async function handleSave(id: string, certificate: any): Promise<string> {
  try {
    const intermediateCertResponse = await axios.get(getCaIssuerFromPem(certificate), { responseType: 'arraybuffer' })
    const intermediateCertPem = getPemFromBuffer(intermediateCertResponse.data)
    const rootCertResponse = await axios.get(getCaIssuerFromPem(intermediateCertPem), { responseType: 'arraybuffer' })
    const rootCertPem = getPemFromBuffer(rootCertResponse.data)
    const certChainPem = buildCertificateChain(certificate, intermediateCertPem, rootCertPem)
    await storageService.upload(certChainPem, id + '.pem', 'x509')
    return certChainPem
  } catch (error: any) {
    throw error
  }
}

async function handleRetrieve(id: string): Promise<string | undefined> {
  try {
    return await storageService.download(id + '.pem', 'x509')
  } catch (error: any) {
    throw error
  }
}

function buildCertificateChain(...certificates: string[]): string {
  return certificates.reduce((chain, certificate) => chain + certificate, '')
}

function getPemFromBuffer(buffer: string): string {
  const base64Certificate = Buffer.from(buffer).toString('base64')
  const base64WithLineBreaks = base64Certificate.includes('\n') ? base64Certificate : base64Certificate.replace(/(.{64})/g, '$1\n')
  const pemCertificate = `\n-----BEGIN CERTIFICATE-----\n${base64WithLineBreaks}\n-----END CERTIFICATE-----`
  return pemCertificate
}

function getCaIssuerFromPem(pemCertificate: string): string {
  const cert = new X509()
  cert.readCertPEM(pemCertificate)
  const caIssuer = cert.getExtAIAInfo()?.caissuer[0]
  if (!caIssuer) {
    throw new Error('Certificate does not include AIA extension')
  }
  return caIssuer
}
