import { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios'
import { Example, ExampleTuple, VerifiableCredential } from '@/sharedTypes'

const typeMapping: { [key: string]: string } = {
  participant: 'gx:LegalParticipant',
  service: 'gx:ServiceOffering',
  termsAndConditions: 'gx:GaiaXTermsAndConditions',
  legalRegistrationNumber: 'gx:legalRegistrationNumber',
  sOTermsAndConditions: 'gx:SOTermsAndConditions',
  serviceWithResources: 'gx:ServiceOffering',
  dataResource: 'gx:DataResource',
  softwareResource: 'gx:SoftwareResource',
  physicalResource: 'gx:PhysicalResource',
  virtualResource: 'gx:VirtualResource',
  legitimateInterest: 'gx:LegitimateInterest',
  serviceAccessPoint: 'gx:ServiceAccessPoint',
  instantiatedVirtualResource: 'gx:InstantiatedVirtualResource'
}

function getMappedType(type: string) {
  return typeMapping[type] ?? 'gx:' + type
}

export default async function handler(req: NextApiRequest, res: NextApiResponse<ExampleTuple[]>) {
  if (!process.env.COMPLIANCE_BASE_URL) throw new Error('Missing COMPLIANCE_BASE_URL env variable')

  function findCredentialByType(vc: VerifiableCredential, searchType: string) {
    return returnTypeOrAtTypeField(vc).indexOf(searchType) > -1 || returnTypeOrAtTypeField(vc.credentialSubject).indexOf(searchType) > -1
  }

  function returnTypeOrAtTypeField(entry: any): string[] {
    const types = []
    if (Array.isArray(entry)) {
      return entry.flatMap(ent => returnTypeOrAtTypeField(ent))
    }
    const entryType = entry['@type'] ?? entry.type ?? ''
    if (Array.isArray(entryType)) {
      types.push(...entryType)
    } else {
      types.push(entryType)
    }
    return types
  }

  try {
    // Fetch examples from compliance
    const url = process.env.COMPLIANCE_BASE_URL.replace('/api', '')
    const { data: jsonDocs } = await axios.get(`${url}/openapi.json`)

    const urlSplitted = url.split('/')
    const complianceVersion = urlSplitted[urlSplitted.length - 1]

    const vpExample: Record<string, Example> =
      jsonDocs.paths[`/${complianceVersion}/api/credential-offers`].post.requestBody.content['application/json'].examples

    // Remove VCs proof
    const examplesWithoutProof: ExampleTuple[] = Object.entries(vpExample).map(([key, data]) => {
      const newValue: Example = {
        ...data,
        value: {
          ...data.value,
          verifiableCredential: (data.value as any).verifiableCredential.map((vc: any) => {
            const newVC = { ...vc }
            delete newVC.proof
            return newVC
          })
        }
      }

      return { shape: key, example: newValue }
    })

    // Extract VP & VC templates
    const result: ExampleTuple[] = examplesWithoutProof.map(({ shape: key, example }) => {
      const searchType = getMappedType(key)
      const vcTemplate: any = example.value.verifiableCredential.find(vc => findCredentialByType(vc, searchType)) ?? {}
      ;(vcTemplate.credentialSubject ??= {}).id = ''
      const newValue: Example = {
        ...example,
        vpTemplate: {
          ...example.value,
          verifiableCredential: []
        },
        vcTemplate: {
          ...vcTemplate,
          credentialSubject: {},
          id: '',
          issuanceDate: '',
          issuer: ''
        },
        vcTemplateFilled: {
          ...vcTemplate,
          credentialSubject: vcTemplate.credentialSubject,
          id: '',
          issuanceDate: '',
          issuer: ''
        },
        credentialSubject: vcTemplate.credentialSubject
      }

      return { shape: key, example: newValue }
    })

    res.send(result)
  } catch (error) {
    console.log(error)
  }
}
