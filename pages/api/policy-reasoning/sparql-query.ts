import type { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios'

interface ApiRequestBody extends NextApiRequest {
  body: { query: string }
}
const graphdbUrl = () => {
  return 'http://' + process.env.GRAPHDB_HOST + ':' + process.env.GRAPHDB_PORT
}

export default async function handler(req: ApiRequestBody, res: NextApiResponse<any>) {
  try {
    if (req.method !== 'POST') return res.status(405).send({ message: 'Method not allowed' })
    const headers = {
      'content-type': 'application/x-www-form-urlencoded',
      Accept: 'application/sparql-results+json'
    }
    const body = 'query=' + req.body.query
    const url = graphdbUrl() + '/repositories/policy-reasoning'
    const response = await axios.post<any>(url, body, {
      headers,
      timeout: 15000
    })
    if (response.status === 200) {
      if (response.data.results.bindings.length) {
        res.send(response.data.results.bindings[0])
        return
      }
      res.send([])
    } else {
      res.status(500).json({ message: 'Response error: ' + response.data })
    }
  } catch (error: any) {
    console.error(error)
    res.status(500).json({ message: 'Error while sending request to database' })
  }
}
