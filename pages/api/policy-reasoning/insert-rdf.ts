import type { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios'

interface ApiRequestBody extends NextApiRequest {
  body: { rdf: string }
}

const graphdbUrl = () => {
  return 'http://' + process.env.GRAPHDB_HOST + ':' + process.env.GRAPHDB_PORT
}

export default async function handler(req: ApiRequestBody, res: NextApiResponse<any>) {
  try {
    if (req.method !== 'POST') return res.status(405).send({ message: 'Method not allowed' })

    const body = { name: 'rdf-policy', type: 'text', format: 'text/turtle', data: JSON.parse(JSON.stringify(req.body.rdf)) }
    const url = graphdbUrl() + '/rest/repositories/policy-reasoning/import/upload/text'
    const response = await axios.post<any>(url, body, {
      timeout: 15000
    })
    if (response.status === 202) {
      res.status(202).send({ message: response.data })
    } else {
      res.status(500).json({ message: 'Response error: ' + response.data })
    }
  } catch (error: any) {
    console.error(error)
    res.status(500).json({ message: 'Error while sending request to database' })
  }
}
