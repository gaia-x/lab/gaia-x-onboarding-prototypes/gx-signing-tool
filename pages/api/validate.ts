// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import axios from 'axios'
import { toRDF } from 'jsonld'
import { Readable } from 'stream'
import factory from 'rdf-ext'
import Parser from '@rdfjs/parser-n3'
import SHACLValidator from 'rdf-validate-shacl'

type Data = { conforms: boolean; results: any[] } | { message: string }

export default async function handler(req: NextApiRequest, res: NextApiResponse<Data>) {
  try {
    if (req.method !== 'POST') {
      return res.status(405).json({ message: 'Method Not Allowed' })
    }

    const data = req.body

    if (!data) {
      res.status(400).json({ message: 'Bad Request' })
    }

    const { conforms, results } = await validate(data)

    res.send({ conforms, results })
  } catch (error: any) {
    res.status(500).send(error.message)
    console.log(error)
  }
}

async function validate(data: object) {
  const shapeFromRegistry = (await axios.get(`${process.env.REGISTRY_API_BASE_URL}/trusted-shape-registry/v1/shapes/trustframework`)).data

  const shapeStr = new Readable()
  shapeStr.push(shapeFromRegistry)
  shapeStr.push(null)

  const dataRDF = await toRDF(data, { format: 'application/n-quads' })
  const input = new Readable()
  input.push(dataRDF)
  input.push(null)

  const parser = new Parser({ factory })

  const shapes = await factory.dataset().import(parser.import(shapeStr))
  const sds = await factory.dataset().import(parser.import(input))

  const validator = new SHACLValidator(shapes)

  return validator.validate(sds)
}
