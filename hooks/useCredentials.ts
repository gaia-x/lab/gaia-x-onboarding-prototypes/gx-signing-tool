import { useState } from 'react'
import axios from 'axios'
import { environment } from '@/env'
import { JsonLdDocument } from 'jsonld'
import { publicKeyToDidId } from '@/web-crypto/did'
import { useEid } from './useEid'

interface UseCredentialsReturnValues {
  isLoading: boolean
  isCreatingDid: boolean
  isUploadingVC: boolean

  createDid(publicKey: string): Promise<JsonLdDocument | undefined>

  uploadVC(vc: JsonLdDocument & { id: string }): Promise<void>
}

export const useCredentials = (): UseCredentialsReturnValues => {
  const { isEidEnabled, getCertChainURI } = useEid()
  const [isCreatingDid, setIsCreatingDid] = useState(false)
  const [isUploadingVC, setIsUploadingVC] = useState(false)

  const createDid = async (publicKey: string): Promise<JsonLdDocument | undefined> => {
    try {
      setIsCreatingDid(true)
      let url: string

      if (isEidEnabled()) {
        const certChainURI = await getCertChainURI()
        url = environment(window.location.href).requestDidEndpoint(publicKeyToDidId(publicKey), certChainURI)
      } else {
        url = environment(window.location.href).requestDidEndpoint(publicKeyToDidId(publicKey))
      }

      const { data } = await axios.post<JsonLdDocument>(url, publicKey, {
        headers: { 'Content-Type': 'text/plain' }
      })
      return data
    } finally {
      setIsCreatingDid(false)
    }
  }

  const uploadVC = async (vc: JsonLdDocument & { id: string }): Promise<void> => {
    try {
      setIsUploadingVC(true)
      let url: string
      if (/^https?:\/\//.test(vc.id)) {
        // VC id is a URL pointing to the upload location, we can use it as-is
        url = vc.id
      } else {
        // VC id is a did, we need to construct the URL from it
        const didId = vc.id.split(':').pop()
        if (!didId) throw new Error('Invalid didId')
        url = environment(window.location.href).requestDidEndpoint(didId)
      }
      await axios.post<JsonLdDocument>(url, vc, {
        headers: { 'Content-Type': 'text/plain' }
      })
    } finally {
      setIsUploadingVC(false)
    }
  }

  return {
    isLoading: isCreatingDid || isUploadingVC,
    isCreatingDid,
    isUploadingVC,
    uploadVC,
    createDid
  }
}
