import { VerifiableCredential } from '@/sharedTypes'
import { VDocument } from '@/models/document'

export const useVCTypes = () => {
  const getVCTypes = (vc: VDocument): string[] => {
    if (Array.isArray(vc.credentialSubject)) {
      return vc.credentialSubject.flatMap(cs => getCSTypes(cs))
    } else if (vc.credentialSubject) {
      return [getCSTypes(vc.credentialSubject)]
    } else {
      return (
        vc?.verifiableCredential
          ?.map(credential => {
            return (credential as VerifiableCredential).credentialSubject
          })
          .flatMap(cs => getCSTypes(cs)) || []
      )
    }
  }

  const getCSTypes = (credentialSubject: any) => {
    return credentialSubject['type'] ?? credentialSubject['@type'] ?? 'undefined'
  }

  return {
    getVCTypes
  }
}
