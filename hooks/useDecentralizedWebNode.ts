import { useEffect, useState } from 'react'
import axios from 'axios'
import { useErrorHandler } from '@/hooks/useError'
import { CredentialOffersApiResponseData } from '@/sharedTypes'
import { StoredWalletEntry } from '@/models/wallet'
import { environment } from '@/env'

interface UseComplianceReturnValues {
  addCredentials: (credential: StoredWalletEntry, url: string) => Promise<boolean | undefined>
  getCredentials: (url: string) => Promise<StoredWalletEntry[]>
  isLoading: boolean
  shouldDisplayDisclaimer: boolean
  acceptDisclaimer: () => void
}

export const useDecentralizedWebNode = (): UseComplianceReturnValues => {
  const [isLoading, setIsLoading] = useState(false)
  const [shouldDisplayDisclaimer, setShouldDisplayDisclaimer] = useState(false)
  const { errorHandler } = useErrorHandler()

  useEffect(() => {
    const hasAcceptedDisclaimer = localStorage.getItem(`wallet-disclaimer`)
    setShouldDisplayDisclaimer(!hasAcceptedDisclaimer)
  }, [])

  const acceptDisclaimer = () => {
    localStorage.setItem('wallet-disclaimer', 'true')
    setShouldDisplayDisclaimer(false)
  }

  const getCredentials = async (url: string): Promise<StoredWalletEntry[]> => {
    try {
      setIsLoading(true)
      if (!url) {
        errorHandler({ name: 'Missing required URL', message: 'A Node url must be filled' })
        return []
      }

      const { data } = await axios.get<StoredWalletEntry[]>(environment(window.location.href).requestDecentralizedWebNode(url))
      return data
    } catch (e: any) {
      errorHandler(e, e.response?.data?.message)
      return []
    } finally {
      setIsLoading(false)
    }
  }
  const addCredentials = async (credential: StoredWalletEntry, url: string): Promise<boolean | undefined> => {
    try {
      setIsLoading(true)
      if (!url) {
        errorHandler({ name: 'Missing required URL', message: 'A Node url must be filled' })
      }

      const response = await axios.post<CredentialOffersApiResponseData>(
        environment(window.location.href).requestDecentralizedWebNode(url),
        credential
      )

      return response.status === 200
    } catch (e: any) {
      errorHandler(e, e.response?.data?.message)
    } finally {
      setIsLoading(false)
    }
  }

  return {
    addCredentials,
    getCredentials,
    isLoading,
    acceptDisclaimer,
    shouldDisplayDisclaimer
  }
}
