import { useState } from 'react'
import { BlankNode, defaultGraph, Literal, NamedNode, Namespace, triple } from 'rdflib'
import axios from 'axios'
import { useToast } from '@chakra-ui/react'
import { JSONPath } from 'jsonpath-plus'
import { StoredWalletEntry } from '@/models/wallet'
import { environment } from '@/env'
import { ContractAgreement, ovcConstraint, Rule } from '@/models/dataExchange'

const odrlRuleTypes = ['permission', 'prohibition', 'duty']

interface UseOdrlEngineValues {
  offer: string
  setOffer: (offer: string) => void
  request: string
  setRequest: (odrlRequest: string) => void
  agreement?: ContractAgreement
  setAgreement: (odrlAgreement: ContractAgreement) => void
  handleReasoning: (credentials: StoredWalletEntry[]) => Promise<boolean>
  credentialsVerification: ovcConstraint[]
  setCredentialsVerification: (credentialsVerification: ovcConstraint[]) => void
  handleOfferPolicy: () => void
  consumer: string
  setConsumer: (consumer: string) => void
  provider: string
  setProvider: (provider: string) => void
}

export const usePolicyReasoning = (): UseOdrlEngineValues => {
  const [offer, setOffer] = useState<string>('')
  const [request, setRequest] = useState<string>('')
  const [agreement, setAgreement] = useState<ContractAgreement>()
  const [consumer, setConsumer] = useState<string>('')
  const [provider, setProvider] = useState<string>('')
  const [credentialsVerification, setCredentialsVerification] = useState<ovcConstraint[]>([])
  const toast = useToast()

  const handleReasoning = async (credentials: StoredWalletEntry[]): Promise<boolean> => {
    const credentialEvaluation = evaluateCredentials(credentialsVerification, credentials)
    const sparqlQuery = odrlToSparql(request, credentialEvaluation)

    const data = await executeSparqlQuery(sparqlQuery)
    if (data?.policy) {
      setAgreement(createAgreement(data, request))
      toast({
        title: 'Contract Agreement created',
        description: '',
        status: 'success',
        duration: 3000,
        isClosable: true
      })
      return true
    } else {
      setAgreement(undefined)
      toast({
        title: 'No Agreement reached',
        description: '',
        status: 'warning',
        duration: 3000,
        isClosable: true
      })
      return false
    }
  }

  const handleOfferPolicy = async () => {
    const { rdfTriples, credentialsVerification } = odrlToRDF()
    setCredentialsVerification(credentialsVerification)
    if (!provider) {
      const offerValue = JSON.parse(offer)
      const assigner = offerValue?.permission.map((p: any) => p.assigner)?.[0] || offerValue?.prohibition.map((p: any) => p.assigner)?.[0]
      setProvider(assigner)
    }
    await insertRDF(rdfTriples)
  }

  const createAgreement = (result: any, odrlRequest: string): ContractAgreement => {
    const request = JSON.parse(odrlRequest)
    const agreement: ContractAgreement = {
      '@type': 'http://www.w3.org/ns/odrl/2/Agreement',
      uid: result.policy.value
    }
    odrlRuleTypes.forEach(ruleType => {
      agreement[ruleType as keyof ContractAgreement] = request[ruleType]?.map((rule: Rule, index: number) => ({
        target: result[ruleType.concat(String(index)).concat('Target')].value,
        action: result[ruleType.concat(String(index)).concat('Action')].value,
        assigner: provider,
        assignee: consumer
      }))
    })

    return agreement
  }

  const evaluateCredentials = (credentialsVerification: ovcConstraint[], credentials: StoredWalletEntry[]) => {
    const credentialEvaluation: ovcConstraint[] = credentialsVerification.map(verification => {
      return {
        ...verification,
        rightOperand: credentials
          .filter(vc => vc.document.credentialSubject.type === verification['ovc:credentialSubjectType'])
          .map(vc => {
            return JSONPath({ path: verification['ovc:leftOperand'], json: vc.document })
          })
          .join(',')
      }
    })
    return credentialEvaluation
  }

  const insertRDF = async (odrlRDF: string) => {
    const { data } = await axios.post<{
      message: string
    }>(environment(window.location.href).insertRDF(), { rdf: odrlRDF })
    return data
  }

  async function executeSparqlQuery(sparqlQuery: string) {
    const { data } = await axios.post<any>(environment(window.location.href).requestSparQLQuery(), { query: sparqlQuery })
    return data
  }

  const odrlToRDF = () => {
    const RDF = Namespace('http://www.w3.org/1999/02/22-rdf-syntax-ns#')
    const ODRL = Namespace('http://www.w3.org/ns/odrl/2/')
    const OVC = Namespace('https://w3id.org/ovc/1/')

    const credentialsVerification: ovcConstraint[] = []
    const odrlExpression = JSON.parse(offer)
    const triples: any[] = []
    const addTriple = (subject: any, predicate: any, object: any) => {
      triples.push(triple(subject, predicate, object, defaultGraph()))
    }

    const expressionNode = new NamedNode(odrlExpression.uid)
    addTriple(expressionNode, RDF('type'), ODRL(odrlExpression['@type']))
    odrlRuleTypes
      .map(ruleType => odrlExpression[ruleType]!)
      .filter(rulesByType => rulesByType)
      .forEach(rulesByType => {
        rulesByType.forEach((rule: Rule) => {
          const expressionNode = new BlankNode()
          addTriple(expressionNode, RDF('type'), ODRL(rule['@type']!))
          addTriple(expressionNode, ODRL('target'), new NamedNode(rule.target))
          addTriple(expressionNode, ODRL('action'), new NamedNode(rule.action))
          addTriple(expressionNode, ODRL('hasPolicy'), new NamedNode(odrlExpression.uid))
          const assignee = rule.assignee
          if (assignee && assignee['ovc:constraint']) {
            assignee['ovc:constraint'].forEach((constraint: ovcConstraint) => {
              const assigneeNode = new BlankNode()
              addTriple(expressionNode, ODRL('assignee'), assigneeNode)
              const constraintNode = new BlankNode()
              addTriple(assigneeNode, OVC('constraint'), constraintNode)
              addTriple(constraintNode, OVC('leftOperand'), new Literal(constraint['ovc:leftOperand']))
              addTriple(constraintNode, ODRL('operator'), new NamedNode(constraint.operator))
              addTriple(constraintNode, OVC('credentialSubjectType'), new NamedNode(constraint['ovc:credentialSubjectType']))
              if (Array.isArray(constraint.rightOperand)) {
                constraint.rightOperand.forEach((rightOperand: any) => {
                  addTriple(constraintNode, ODRL('rightOperand'), new Literal(rightOperand))
                })
              } else {
                addTriple(constraintNode, ODRL('rightOperand'), new Literal(constraint.rightOperand!))
              }
              credentialsVerification.push({
                operator: constraint.operator,
                'ovc:leftOperand': constraint['ovc:leftOperand'],
                'ovc:credentialSubjectType': constraint['ovc:credentialSubjectType']
              })
            })
          }
        })
      })

    return { rdfTriples: triples.toString().replace(/,/g, ''), credentialsVerification }
  }

  const odrlToSparql = (odrlRequest: string, constraints: ovcConstraint[]): string => {
    const request = JSON.parse(odrlRequest)
    const sparqlPrefixes: string =
      'PREFIX odrl: <http://www.w3.org/ns/odrl/2/>\n PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n PREFIX ovc: <https://w3id.org/ovc/1/>\n'
    let selectStatement: string = 'SELECT ?policy '
    let ruleStatements = ''

    odrlRuleTypes.forEach((ruleType: string) => {
      request[ruleType]?.forEach((rule: Rule, index: number) => {
        const target = rule.target
        const action = rule.action
        const ruleName = ruleType.concat(String(index))
        const capitalizedRuleType = capitalize(ruleType)
        selectStatement += `?${ruleName}Target `
        selectStatement += `?${ruleName}Action `
        ruleStatements +=
          `?${ruleName} odrl:action ?${ruleName}Action .\n` +
          `?${ruleName} odrl:hasPolicy ?policy .\n` +
          `?${ruleName} odrl:target ?${ruleName}Target .\n` +
          `?${ruleName} rdf:type odrl:${capitalizedRuleType} .` +
          '{\n' +
          `?${ruleName} odrl:action <${action}> .\n` +
          '} \n' +
          'UNION \n' +
          '{\n' +
          `<${action}> odrl:includedIn ?${ruleName}Action .\n` +
          '}\n' +
          `?${ruleName} odrl:target <${target}> .\n`
      })
    })

    const queryConstraints = constraints
      .map(constraint => {
        return (
          '  \n' +
          '?permission odrl:assignee ?assignee .\n' +
          '?assignee ovc:constraint ?constraint .\n' +
          `?constraint ovc:leftOperand "${constraint['ovc:leftOperand']}" .\n` +
          `?constraint ovc:credentialSubjectType <${constraint['ovc:credentialSubjectType']}> .\n` +
          '?constraint odrl:rightOperand ?rightOperand .\n' +
          '{\n' +
          `?constraint odrl:rightOperand "${constraint.rightOperand}" .\n` +
          '} \n' +
          'UNION \n' +
          '{\n' +
          `"${constraint.rightOperand}" odrl:includedIn ?rightOperand .\n` +
          '}\n'
        )
      })
      .join('')

    return sparqlPrefixes
      .concat(selectStatement)
      .concat('\n WHERE {\n ?policy rdf:type odrl:Offer .\n')
      .concat(ruleStatements)
      .concat(queryConstraints)
      .concat('}')
  }

  const capitalize = (word: string) => word.charAt(0).toUpperCase() + word.slice(1)

  return {
    offer,
    setOffer,
    request,
    setRequest,
    agreement,
    setAgreement,
    handleReasoning,
    credentialsVerification,
    setCredentialsVerification,
    handleOfferPolicy,
    consumer,
    provider,
    setProvider,
    setConsumer
  }
}
