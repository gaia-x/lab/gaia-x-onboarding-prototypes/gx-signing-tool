import { getDidWeb, publicKeyToDidId } from '@/web-crypto/did'
import { useCredentials } from './useCredentials'
import { createCredentialSubjectId, createVcId, joinPath } from '@/utils/util'
import { getRootUrl } from '@/env'
import { signDocumentWithKey } from '@/web-crypto/signDocument'
import { usePrivateKey } from './usePrivateKey'
import { useEid } from './useEid'
import { VDocument } from '@/models/document'

interface SignAndUploadDocumentOptions {
  privateKey: string
  verificationMethod: string
  docToSign: any
  docName: string
}

interface UseGXSignatureReturnValues {
  signAndUploadDocument: (opts: SignAndUploadDocumentOptions) => Promise<any>
}

export const useGXSignature = (): UseGXSignatureReturnValues => {
  const { createDid, uploadVC } = useCredentials()
  const { getPublicKey } = usePrivateKey()
  const { isEidEnabled, signDocumentWithEid } = useEid()

  const copyCredentialSubject: (didUrl: string, docId: string, credentialSubject: object | []) => Promise<any> = async (
    didUrl: string,
    docId: string,
    credentialSubject: object | []
  ): Promise<[] | object> => {
    if (Array.isArray(credentialSubject)) {
      return Promise.all(
        credentialSubject.map(async cs => {
          return await copyCredentialSubject(didUrl, docId, cs)
        })
      )
    } else {
      const csCopy: any = { ...(credentialSubject ?? {}) }
      if (!csCopy.id && !csCopy['@id']) {
        csCopy.id = await createCredentialSubjectId(didUrl, csCopy, docId)
      }

      return csCopy
    }
  }

  const signAndUploadDocument = async (opts: SignAndUploadDocumentOptions) => {
    const publicKey = await getPublicKey()
    if (!publicKey) throw new Error('Could not get public key')
    const didURIPath = joinPath(getRootUrl(location.pathname), 'api', 'credentials', publicKeyToDidId(publicKey))
    const didUrl = joinPath(location.origin, didURIPath)
    const didDoc = await createDid(publicKey)

    if (!didDoc) throw new Error('DID document creation failed')
    if (!(didDoc as any).verificationMethod?.length) throw new Error('No verification method found')
    const docId = createVcId(didUrl)
    const doc = {
      ...opts.docToSign,
      id: docId,
      issuer: getDidWeb(location.host, didURIPath),
      credentialSubject: await copyCredentialSubject(didUrl, docId, opts.docToSign.credentialSubject)
    }

    let signedDocument: VDocument | undefined
    if (isEidEnabled()) {
      signedDocument = await signDocumentWithEid(doc, (didDoc as any).verificationMethod[0].id)
    } else {
      signedDocument = await signDocumentWithKey(doc, opts.privateKey, (didDoc as any).verificationMethod[0].id)
    }
    if (!signedDocument) throw new Error('Could not sign document')

    await uploadVC(signedDocument as any)
    return signedDocument
  }

  return {
    signAndUploadDocument
  }
}
