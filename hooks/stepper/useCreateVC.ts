import { ParticipantFormValues } from '@/components/Stepper/CreateCredentialSubject/ParticipantForm/ParticipantForm'
import { ServiceOfferingFormValues } from '@/components/Stepper/CreateCredentialSubject/ServiceOfferingForm/ServiceOfferingForm'
import { TermsAndConditionsFormValues } from '@/components/Stepper/CreateCredentialSubject/TermsAndConditionsForm/TermsAndConditionsForm'
import { VDocument } from '@/models/document'
import { ExampleTuple } from '@/sharedTypes'
import { useState } from 'react'
import { createCredentialSubjectId, createVcId } from '@/utils/util'
import { ContractAgreement } from '@/models/dataExchange'

export type CreateVCState<T> = {
  credentialSubjectValues: T | undefined
  shape: string
  id: string
  credentialSubjectId: string
  example?: ExampleTuple
}

export type CredentialSubjectValues = ParticipantFormValues | ServiceOfferingFormValues | TermsAndConditionsFormValues | ContractAgreement

export const participantDefaultValues: ParticipantFormValues = {
  legalName: '',
  headquarterAddress: {
    countrySubdivisionCode: ''
  },
  legalRegistrationNumber: {
    id: ''
  },
  legalAddress: {
    countrySubdivisionCode: ''
  }
}

export const serviceOfferingDefaultValues: ServiceOfferingFormValues = {
  providedBy: {
    id: ''
  },
  termsAndConditions: {
    URL: '',
    hash: ''
  },
  policy: '',
  dataAccountExport: {
    requestType: '',
    accessType: '',
    formatType: ''
  },
  aggregationOf: '',
  dependsOn: '',
  dataProtectionRegime: ''
}

export interface UseCreateVC {
  values: CreateVCState<CredentialSubjectValues>
  setValues: (values: CreateVCState<CredentialSubjectValues>) => void
  createVC: (isCustomIssuer: boolean, issuer: string) => Promise<VDocument>
}

export const useCreateVC = (source?: ExampleTuple): UseCreateVC => {
  const [values, setValues] = useState<CreateVCState<CredentialSubjectValues>>({
    credentialSubjectValues: source && source.shape ? getDefaultCredentialSubjectValues(source.shape) : undefined,
    id: '',
    credentialSubjectId: '',
    shape: ''
  })

  const createVC = async (isCustomIssuer: boolean, issuer: string) =>
    buildVerifiableCredential({
      isCustomIssuer,
      issuer,
      credentialSubjectValues: values.credentialSubjectValues,
      shape: values.shape,
      id: values.id,
      credentialSubjectId: values.credentialSubjectId,
      source
    })

  return {
    values,
    setValues,
    createVC
  }
}

export async function buildVerifiableCredential(opts: {
  isCustomIssuer: boolean
  issuer: string
  credentialSubjectValues?: CredentialSubjectValues
  shape: string
  id: string
  credentialSubjectId: string
  source?: ExampleTuple
}) {
  const { credentialSubjectValues, shape, id, credentialSubjectId } = opts

  const vcTemplate = {
    ...opts.source?.example.vcTemplate,
    id: opts.isCustomIssuer ? id : createVcId(opts.issuer),
    issuer: opts.issuer,
    issuanceDate: new Date().toISOString(),
    credentialSubject: getGxPrefixed(credentialSubjectValues)
  }

  vcTemplate.credentialSubject.type ||= opts.source?.example.credentialSubject.type

  // Remove empty props
  Object.entries(vcTemplate.credentialSubject).forEach(([key, value]) => {
    if (value === '') {
      delete vcTemplate.credentialSubject[key]
    }
  })

  if (shape === 'participant') {
    // Add terms and conditions
    vcTemplate.credentialSubject['gx-terms-and-conditions:gaiaxTermsAndConditions'] =
      '70c1d713215f95191a11d38fe2341faed27d19e083917bc8732ca4fea4976700'
  }

  if (!opts.isCustomIssuer) {
    delete vcTemplate.credentialSubject.id
    vcTemplate.credentialSubject.id = await createCredentialSubjectId(vcTemplate.issuer, vcTemplate.credentialSubject, vcTemplate.id)
  } else if (credentialSubjectId) {
    vcTemplate.credentialSubject.id = credentialSubjectId
  }

  return vcTemplate as VDocument
}

function getGxPrefixed(obj: CredentialSubjectValues | undefined): any {
  return Object.entries(obj ?? {}).reduce((subject, [key, value]) => {
    const newKey = `gx:${key}`
    let newValue = value

    if (typeof value === 'object') {
      newValue = Object.entries(value).reduce((innerObject, [key, value]) => {
        if (key === 'id') return { ...innerObject, [key]: value }
        const newKey = `gx:${key}`
        return { ...innerObject, [newKey]: value }
      }, {})
    }
    return { ...subject, [newKey]: newValue }
  }, {})
}

function getDefaultCredentialSubjectValues(shape: string) {
  if (shape === 'participant') {
    return participantDefaultValues
  } else if (shape === 'service') {
    return serviceOfferingDefaultValues
  }
}
