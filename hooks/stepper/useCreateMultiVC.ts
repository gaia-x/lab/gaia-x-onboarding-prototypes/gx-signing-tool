import { VDocument } from '@/models/document'
import { useState } from 'react'
import { useExamples } from '../useExamples'
import { buildVerifiableCredential, CreateVCState, CredentialSubjectValues } from './useCreateVC'

export type MultiVCState = CreateVCState<CredentialSubjectValues>[]

export type MultiVCStateIds = {
  participantVcId: string
  participantCredentialSubjectId: string
  tncVcId?: string
  tncCredentialSubjectId?: string
}

export interface UseCreateMultiVC {
  legalRegistrationNumberVC: VDocument | null
  setLegalRegistrationNumberVC: (lrnVC: VDocument) => void
  valuesByType: { [type: string]: CredentialSubjectValues }
  setValue(type: string, value: CredentialSubjectValues): void
  createVCs: (isCustomIssuer: boolean, issuer: string) => Promise<VDocument[]>
  of: (
    type: string,
    defaultValue?: CredentialSubjectValues
  ) => {
    value: CredentialSubjectValues
    setValue: (value: CredentialSubjectValues) => void
  }
  identifiers: MultiVCStateIds | null
  setIdentifiers: (ids: MultiVCStateIds) => void
}

export const useCreateMultiVC = (): UseCreateMultiVC => {
  const [values, setValues] = useState<MultiVCState>([])
  const [identifiers, setIdentifiers] = useState<MultiVCStateIds | null>(null)
  const [legalRegistrationNumberVC, setLegalRegistrationNumberVC] = useState<VDocument | null>(null)
  const { getExampleByKey } = useExamples()

  const getStateId = (c: CreateVCState<CredentialSubjectValues>, csId = false): string | undefined => {
    if (c.shape === 'participant') {
      return csId ? c.credentialSubjectId || identifiers?.participantCredentialSubjectId : c.id || identifiers?.participantVcId
    }
    if (c.shape === 'termsAndConditions') {
      return csId ? c.credentialSubjectId || identifiers?.tncCredentialSubjectId : c.id || identifiers?.tncVcId
    }
  }

  const createVCs = async (isCustomIssuer: boolean, issuer: string) =>
    Promise.all(
      values.map(value =>
        buildVerifiableCredential({
          isCustomIssuer,
          issuer,
          credentialSubjectValues: value.credentialSubjectValues,
          shape: value.shape,
          id: getStateId(value) ?? '',
          credentialSubjectId: getStateId(value, true) ?? '',
          source: getExampleByKey(value.shape)
        })
      )
    )

  const setValue = (type: string, value: CredentialSubjectValues) => {
    const newValues = [...values]
    const index = newValues.findIndex(v => v.shape === type)
    if (index === -1) {
      newValues.push({
        shape: type,
        id: '',
        credentialSubjectId: '',
        credentialSubjectValues: value
      })
    } else {
      newValues[index].credentialSubjectValues = value
    }
    setValues(newValues)
  }

  const valuesByType = values.reduce(
    (acc, cur) => {
      acc[cur.shape] = cur.credentialSubjectValues!
      return acc
    },
    {} as { [type: string]: CredentialSubjectValues }
  )

  const getForType = (type: string, defaultValue = {}) => ({
    value: valuesByType[type] ?? defaultValue,
    setValue: (value: CredentialSubjectValues) => setValue(type, value)
  })

  return {
    valuesByType,
    setValue,
    createVCs,
    of: getForType,
    legalRegistrationNumberVC,
    setLegalRegistrationNumberVC,
    identifiers,
    setIdentifiers
  }
}
