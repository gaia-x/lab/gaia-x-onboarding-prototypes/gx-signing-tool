import { VDocument } from '@/models/document'
import { useState } from 'react'

export interface UseDocuments {
  documents: VDocument[]
  setDocuments: (documents: VDocument[]) => void
  addDocument: (document: VDocument) => void
}

export const useDocuments = (): UseDocuments => {
  const [documents, setDocuments] = useState<VDocument[]>([])

  const addDocument = (doc: VDocument) => {
    setDocuments([...documents, doc])
  }

  return {
    documents,
    setDocuments,
    addDocument
  }
}
