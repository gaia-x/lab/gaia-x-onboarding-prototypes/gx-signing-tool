import { useSteps as useStepsChakra } from '@chakra-ui/react'

export interface Step {
  title: string
  description: string
}

export interface UseSteps {
  steps: Step[]
  activeStep: number
  setActiveStep: (step: number) => void
  goToNext: () => void
  goToPrevious: () => void
}

function stepTitle(index: number) {
  return index + ''
}

export const useSteps = (isCustomIssuer: boolean, askAllRequiredVCs = false): UseSteps => {
  const steps: Step[] = []

  if (askAllRequiredVCs) {
    steps.push({ title: stepTitle(steps.length), description: 'Start' })
    steps.push({ title: stepTitle(steps.length), description: 'Participant' })
    steps.push({ title: stepTitle(steps.length), description: 'Terms & Conditions' })
  } else {
    steps.push({ title: stepTitle(steps.length), description: 'Create VC' })
  }
  if (isCustomIssuer) {
    steps.push({ title: stepTitle(steps.length), description: 'Private key' })
    steps.push({ title: stepTitle(steps.length), description: 'Identity' })
  }

  steps.push({ title: stepTitle(steps.length), description: 'Sign' })

  const { activeStep, goToNext, goToPrevious, setActiveStep } = useStepsChakra({
    index: 0,
    count: steps.length
  })

  return {
    steps,
    activeStep,
    setActiveStep,
    goToNext,
    goToPrevious
  }
}
