import { useSteps as useStepsChakra } from '@chakra-ui/react'

export interface Step {
  title: string
  description: string
}

export interface UseSteps {
  steps: Step[]
  activeStep: number
  setActiveStep: (step: number) => void
  goToNext: () => void
  goToPrevious: () => void
}

function stepTitle(index: number) {
  return index + ''
}

export const useSteps = (): UseSteps => {
  const steps: Step[] = []
  steps.push({ title: stepTitle(steps.length), description: "Provider's Policy" })
  steps.push({ title: stepTitle(steps.length), description: 'Consumer Credentials' })
  steps.push({ title: stepTitle(steps.length), description: 'Consumer Usage Policy' })
  steps.push({ title: stepTitle(steps.length), description: 'Contract Agreement' })

  const { activeStep, goToNext, goToPrevious, setActiveStep } = useStepsChakra({
    index: 0,
    count: steps.length
  })

  return {
    steps,
    activeStep,
    setActiveStep,
    goToNext,
    goToPrevious
  }
}
