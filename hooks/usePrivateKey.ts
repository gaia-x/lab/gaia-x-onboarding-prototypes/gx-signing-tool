import { getRootUrl } from '@/env'
import { joinPath } from '@/utils/util'
import { derivePublicKeyFromString, generatePrivateKey, privateKeyToPem } from '@/web-crypto/cryptokey'
import { getDidWeb, publicKeyToDidId } from '@/web-crypto/did'
import { useEffect, useState } from 'react'
import { useWebAuthn } from './useWebAuthn'
import { useEid } from './useEid'
import { useErrorHandler } from './useError'

export interface PrivateKeyUse {
  privateKey: string
  setPrivateKey: (privateKey: string, store?: boolean) => void
  buildWizardDidWeb: () => Promise<string | undefined>
  getPrivateKey: () => Promise<string | undefined>
  getPublicKey: () => Promise<string | undefined>
}

export const usePrivateKey = (): PrivateKeyUse => {
  const [privateKey, setPrivateKey] = useState('')
  const { decryptKey, isWebauthnEnabled } = useWebAuthn()
  const { isEidEnabled, getEidPublicKey } = useEid()
  const { errorHandler } = useErrorHandler()

  const updateKey = (newKey: string, store = !!newKey) => {
    if (store) {
      localStorage.setItem('privateKey', newKey)
    }
    setPrivateKey(newKey)
  }

  const getPemKeyAndUpdate = async () => {
    const key = await generatePrivateKey()
    const pemKey = await privateKeyToPem(key)
    if (pemKey) updateKey(pemKey)
  }

  useEffect(() => {
    const key = localStorage.getItem('privateKey')
    if (key) {
      setPrivateKey(key)
    } else {
      getPemKeyAndUpdate()
    }
  }, [])

  const buildWizardDidWeb = async () => {
    try {
      const publicKey = await getPublicKey()
      if (!publicKey) throw new Error('Could not get public key')
      return getDidWeb(location.host, joinPath(getRootUrl(location.pathname), 'api', 'credentials', publicKeyToDidId(publicKey)))
    } catch (error: any) {
      errorHandler(error)
    }
  }

  const getPrivateKey = async () => {
    try {
      if (isEidEnabled()) {
        throw new Error('EID is enabled, cannot access private key')
      }
      if (isWebauthnEnabled()) {
        const key = await decryptKey(privateKey)
        if (!key) throw new Error('Could not decrypt key')
        return key
      }
      return privateKey
    } catch (error: any) {
      errorHandler(error)
    }
  }

  const getPublicKey = async () => {
    try {
      if (isEidEnabled()) {
        const eidPublicKey = await getEidPublicKey()
        if (!eidPublicKey) throw new Error('Could not get public key from eid')
        return eidPublicKey
      } else {
        const privateKey = await getPrivateKey()
        if (!privateKey) throw new Error('Could not get private key')
        const publicKey = await derivePublicKeyFromString(privateKey)
        if (!publicKey) throw new Error('Could not get public key from private key')
        return publicKey
      }
    } catch (error: any) {
      errorHandler(error)
    }
  }

  return {
    privateKey,
    setPrivateKey: updateKey,
    buildWizardDidWeb,
    getPrivateKey,
    getPublicKey
  }
}
