import { useContext } from 'react'
import { LocalWalletContext } from '@/contexts/LocalWalletContext'
import { DecentralizedWebNodeContext } from '@/contexts/DecentralizedWebNodeContext'
import { AvailableWallets, StoredWalletEntry } from '@/models/wallet'
import { WalletContext } from '@/contexts/WalletContext'
import { useToast } from '@chakra-ui/react'

export const useWallet = () => {
  const toast = useToast()
  const { handleLocalWalletImport } = useContext(LocalWalletContext)
  const { handleDecentralizedWebNodeImport } = useContext(DecentralizedWebNodeContext)
  const { connectedWallet } = useContext(WalletContext)

  const handleImport = (document: StoredWalletEntry, confirmationAlert = true) => {
    if (connectedWallet === AvailableWallets.DECENTRALIZED_WEB_NODE) {
      if (document.format?.includes('jwt')) {
        handleDecentralizedWebNodeImport(document)
      } else {
        toast({
          title: 'Unsupported format',
          description: 'This wallet only handles credentials in JWT format at the moment',
          status: 'error',
          duration: 8000,
          isClosable: true
        })
        return
      }
    } else {
      handleLocalWalletImport(document)
    }
    if (confirmationAlert) {
      toast({
        title: 'Document saved',
        description: 'Document saved in wallet',
        status: 'success',
        duration: 3000,
        isClosable: true
      })
    }
  }

  return {
    handleImport
  }
}
