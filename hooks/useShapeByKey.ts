import { useQuery } from '@tanstack/react-query'
import axios from 'axios'
import { useErrorHandler } from '@/hooks/useError'

export const fetchShapeByKey = async (key: string): Promise<any> => {
  const { data } = await axios.get('https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/' + key)

  return data
}

export const useShapeByKey = (key: string) => {
  const { errorHandler } = useErrorHandler()

  return useQuery({
    queryKey: [key],
    queryFn: () => fetchShapeByKey(key),
    initialData: [],
    onError: (error: Error) => errorHandler(error, 'Could not fetch shapes')
  })
}
