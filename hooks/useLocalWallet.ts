import { StoredWalletEntry } from '@/models/wallet'
import { useCallback, useEffect, useState } from 'react'
import { localWalletService } from '../web-crypto/localWallet'

export const useLocalWallet = () => {
  const [walletEntries, setWalletsEntries] = useState<StoredWalletEntry[]>([])

  const refreshWallet = useCallback(async () => setWalletsEntries(await localWalletService.list()), [localWalletService])

  const persistWalletEntry = async (entry: StoredWalletEntry) => {
    await localWalletService.persist(entry)
    await refreshWallet()
  }

  const deleteWalletEntry = async (entryOrKey: StoredWalletEntry | string) => {
    await localWalletService.delete(typeof entryOrKey === 'string' ? entryOrKey : entryOrKey.key)
    await refreshWallet()
  }

  const getEntriesByType = (type: string) => {
    return walletEntries.filter(entry => entry.document?.credentialSubject?.type === type)
  }

  useEffect(() => {
    refreshWallet()
  }, [refreshWallet])

  useEffect(() => {
    const subscription = localWalletService.subscribe(refreshWallet)

    return () => {
      subscription.unsubscribe()
    }
  }, [refreshWallet, localWalletService, walletEntries])

  return {
    walletEntries,
    persistWalletEntry,
    deleteWalletEntry,
    getEntriesByType
  }
}
