import { useToast } from '@chakra-ui/react'

export const useToasterHandler = () => {
  const toast = useToast()

  const useToaster = (message: string, status: any = 'info', title?: string) => {
    toast({
      title,
      description: message,
      status,
      duration: 5000,
      isClosable: true
    })
  }

  return { useToaster }
}
