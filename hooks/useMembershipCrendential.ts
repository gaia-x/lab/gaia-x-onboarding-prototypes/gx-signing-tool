import { useEffect, useState } from 'react'
import axios from 'axios'
import { useErrorHandler } from '@/hooks/useError'
import { environment } from '@/env'
import { VDocument } from '@/models/document'
import { MembershipCredentialFormat, MembershipCredentialInfo, MembershipCredentialType } from '@/models/membershipCredentialInfo'
import { OIDCResponse } from '@/sharedTypes'
import { cleanQueryParams } from '@/utils/util'

const MEMBERSHIP_CREDENTIAL_KEY = 'gx-membership-credential'
const MEMBERSHIP_CREDENTIAL_TYPE_KEY = 'gx-membership-credential-type'

interface UseMembershipCredentialValues {
  getMembershipCredential: (authorizationCode: string) => Promise<VDocument>
  isLoading: boolean
  membershipCredential: VDocument | undefined
  credentialType: MembershipCredentialType
  saveMembershipCredentialType: (credentialType: MembershipCredentialType) => void
  cleanMembershipCredential: () => void
  displayFormat: MembershipCredentialFormat
  setDisplayFormat: (displayFormat: MembershipCredentialFormat) => void
  membershipCredentialInfo: MembershipCredentialInfo | undefined
  requestOIDCUrl: () => Promise<OIDCResponse>
  oidcResponse: OIDCResponse | undefined
  loginUrl: string
}

export const useMembershipCredential = (): UseMembershipCredentialValues => {
  const [membershipCredential, setCredential] = useState<VDocument>()
  const [credentialType, setCredentialType] = useState<MembershipCredentialType>(MembershipCredentialType.VCARD)
  const [displayFormat, setDisplayFormat] = useState<MembershipCredentialFormat>(MembershipCredentialFormat.CARD)
  const [membershipCredentialInfo, setMembershipCredentialInfo] = useState<MembershipCredentialInfo>()
  const [isLoading, setIsLoading] = useState(false)
  const [oidcResponse, setOidcResponse] = useState<OIDCResponse>()
  const [loginUrl, setLoginUrl] = useState<string>('')
  const { errorHandler } = useErrorHandler()

  useEffect(() => {
    getLoginUrl()
    const type: MembershipCredentialType = getStoredMembershipCredentialType()
    if (type) {
      setCredentialType(type)
    }
    const credential = getStoredMembershipCredential()
    if (credential) {
      setCredential(credential)
      setMembershipCredentialInfo(buildMembershipCredentialInfo(credential))
    }
  }, [])

  const getMembershipCredential = async (authorizationCode: string): Promise<VDocument> => {
    try {
      setIsLoading(true)
      const type = getStoredMembershipCredentialType()
      const { data } = await axios.get<VDocument>(environment(window.location.href).requestMembershipCredential(authorizationCode, type))
      saveMembershipCredential(data)
      setMembershipCredentialInfo(buildMembershipCredentialInfo(data))
      return data
    } catch (e: any) {
      errorHandler(e, e.response?.data?.message)
      return e
    } finally {
      cleanQueryParams()
      setIsLoading(false)
    }
  }

  const saveMembershipCredential = (credential: VDocument) => {
    setCredential(credential)
    localStorage.setItem(MEMBERSHIP_CREDENTIAL_KEY, JSON.stringify(credential))
  }

  const cleanMembershipCredential = () => {
    setCredential(undefined)
    localStorage.removeItem(MEMBERSHIP_CREDENTIAL_KEY)
    sessionStorage.removeItem(MEMBERSHIP_CREDENTIAL_TYPE_KEY)
  }

  const getStoredMembershipCredential = (): VDocument | null => {
    const credential = localStorage.getItem(MEMBERSHIP_CREDENTIAL_KEY)
    return !!credential ? (JSON.parse(credential) as VDocument) : null
  }

  const saveMembershipCredentialType = (type: MembershipCredentialType) => {
    setCredentialType(type)
    sessionStorage.setItem(MEMBERSHIP_CREDENTIAL_TYPE_KEY, type)
  }
  const getStoredMembershipCredentialType = (): MembershipCredentialType => {
    return (sessionStorage.getItem(MEMBERSHIP_CREDENTIAL_TYPE_KEY) as MembershipCredentialType) || MembershipCredentialType.VCARD
  }

  const buildMembershipCredentialInfo = (credential: VDocument): MembershipCredentialInfo => {
    return {
      membershipNumber: credential?.credentialSubject?.['vcard:hasMember']?.id || credential?.credentialSubject?.['org:membershipNumber'],
      name: credential?.credentialSubject?.['vcard:hasMember']?.['vcard:fn'] || credential?.credentialSubject?.['org:member']?.['org:name'],
      email: credential?.credentialSubject?.['vcard:hasMember']?.['vcard:hasEmail'] || credential?.credentialSubject?.['org:member']?.['org:email']
    }
  }

  const requestOIDCUrl = async (): Promise<OIDCResponse> => {
    try {
      setIsLoading(true)
      const response = await axios.post<OIDCResponse>(environment(window.location.href).requestOIDCAuthorization(), membershipCredential)
      setOidcResponse(response.data)
      return response.data
    } catch (e: any) {
      errorHandler(e, e.response?.data?.message)
      return e
    } finally {
      setIsLoading(false)
    }
  }

  const getLoginUrl = async (): Promise<string> => {
    try {
      setIsLoading(true)
      const { data } = await axios.get<string>(environment(window.location.href).requestKeycloakLoginUrl())
      setLoginUrl(data)
      return data
    } catch (e: any) {
      errorHandler(e, e.response?.data?.message)
      return e
    } finally {
      setIsLoading(false)
    }
  }

  return {
    getMembershipCredential,
    membershipCredential,
    credentialType,
    saveMembershipCredentialType,
    cleanMembershipCredential,
    displayFormat,
    setDisplayFormat,
    isLoading,
    membershipCredentialInfo,
    requestOIDCUrl,
    oidcResponse,
    loginUrl
  }
}
