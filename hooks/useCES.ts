import { useContext } from 'react'
import { ClearingHousesContext } from '@/contexts/ClearingHousesContext'
import { VDocument } from '@/models/document'
import axios from 'axios'
import { useToasterHandler } from '@/hooks/useToast'
import { environment } from '@/env'

type SubmitToCESProps = { document: VDocument }

type CloudEvent = {
  specversion: string
  type: string
  source: string
  time: string
  datacontenttype: string
  data: any
}

export const useCES = () => {
  const { selectedGxDeploymentPath } = useContext(ClearingHousesContext)
  const { useToaster } = useToasterHandler()
  const handleCESSubmit = async ({ document }: SubmitToCESProps) => {
    const cloudEvent: CloudEvent = {
      specversion: '1.0',
      type: 'eu.gaia-x.credential',
      source: '/wizard',
      time: new Date().toISOString(),
      datacontenttype: 'application/json',
      data: {
        ...document
      }
    }
    const submitToCESURI = environment(window.location.href).requestCESSubmitEndpoint()
    try {
      const response = await axios.post(submitToCESURI, {
        event: cloudEvent,
        deploymentPath: selectedGxDeploymentPath
      })
      if (response.status === 201) {
        useToaster('Event successfully submitted to the CES', 'success')
      } else {
        useToaster('Unable to send credential to the CES \n' + response.data, 'error')
      }
    } catch (error: any) {
      useToaster(error?.response?.data?.message ?? 'Unable to send credential to the CES \n' + error.message, 'error')
    }
  }

  return {
    handleCESSubmit
  }
}
