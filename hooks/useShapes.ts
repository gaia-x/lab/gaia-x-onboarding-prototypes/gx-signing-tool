import { useQuery } from '@tanstack/react-query'
import axios from 'axios'
import { useErrorHandler } from '@/hooks/useError'

export const fetchShapes = async (): Promise<any> => {
  const { data } = await axios.get('https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework')

  return data
}

export const useShapes = () => {
  const { errorHandler } = useErrorHandler()

  return useQuery({
    queryKey: ['shapes'],
    queryFn: fetchShapes,
    initialData: [],
    onError: (error: Error) => errorHandler(error, 'Could not fetch shapes')
  })
}
