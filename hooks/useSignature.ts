import { VDocument } from '@/models/document'
import { useState } from 'react'
import { useErrorHandler } from './useError'
import { UseIssuer } from './useIssuer'
import { PrivateKeyUse } from './usePrivateKey'
import { useGXSignature } from './useGXSignature'
import { signDocumentWithKey } from '@/web-crypto/signDocument'
import { useEid } from './useEid'

interface UseSignatureReturnValues {
  signDocument: () => Promise<VDocument[]>
  signingIsLoading: boolean
}

export const useSignature = (sign: {
  issuerUse: UseIssuer
  privateKeyUse: PrivateKeyUse
  issuerDoc: string
  docName: string
  onDocSigned?: (doc: VDocument) => void
}): UseSignatureReturnValues => {
  const [signingIsLoading, setSigningIsLoading] = useState<boolean>(false)
  const { errorHandler } = useErrorHandler()
  const { signAndUploadDocument } = useGXSignature()
  const { isEidEnabled, signDocumentWithEid } = useEid()

  const signSingleDocument = async (privateKey: string, docToSign: any, docName: string) => {
    if (!docToSign.issuanceDate) {
      docToSign.issuanceDate = new Date().toISOString()
    }

    let signedDocument: VDocument

    if (sign.issuerUse.isCustomIssuer) {
      if (isEidEnabled()) {
        signedDocument = await signDocumentWithEid(docToSign, sign.issuerUse.verificationMethod)
      } else {
        signedDocument = await signDocumentWithKey(docToSign, privateKey, sign.issuerUse.verificationMethod)
      }
    } else {
      docToSign.issuer = sign.issuerUse.issuer
      signedDocument = await signAndUploadDocument({
        privateKey: privateKey,
        docToSign,
        docName,
        verificationMethod: sign.issuerUse.verificationMethod
      })
    }

    if (signedDocument && sign.onDocSigned) {
      sign.onDocSigned(signedDocument)
    }
    return signedDocument
  }

  const getSignatureErrorMessage = (err: any) => {
    if (err.name !== 'jsonld.ValidationError') {
      return err.response?.data?.message || err.response?.data || err.message || 'Error while signing document'
    }
    return err.details.event.message
  }

  const signDocument: () => Promise<VDocument[]> = async () => {
    try {
      const signed: VDocument[] = []
      if (!sign.issuerDoc || !sign.issuerUse.isValid) throw new Error('Please provide a document and a verification method')
      setSigningIsLoading(true)

      let privateKey = ''
      if (!isEidEnabled()) {
        privateKey = (await sign.privateKeyUse.getPrivateKey()) ?? ''
      }

      const docToSign = JSON.parse(sign.issuerDoc)

      if (Array.isArray(docToSign)) {
        // sign multiple related documents in a batch (e.g. participant, lrn and t&c)
        let i = 0
        for (const doc of docToSign) {
          signed.push(await signSingleDocument(privateKey, doc, sign.docName + '-' + ++i))
        }
      } else {
        signed.push(await signSingleDocument(privateKey, docToSign, sign.docName))
      }
      return signed
    } catch (err: any) {
      errorHandler(err, getSignatureErrorMessage(err))
      return []
    } finally {
      setSigningIsLoading(false)
    }
  }
  return {
    signDocument,
    signingIsLoading
  }
}
