import { ClaimNotaryVC } from '@/components/LegalRegistrationNumber/LegalRegistrationNumberContainer'
import { environment, getRootUrl } from '@/env'
import { createCredentialSubjectId, joinPath } from '@/utils/util'
import { publicKeyToDidId } from '@/web-crypto/did'
import { v4 as uuidv4 } from 'uuid'
import { useContext } from 'react'
import { ClearingHousesContext } from '@/contexts/ClearingHousesContext'
import axios from 'axios'
import { SignatureContext } from '@/contexts/SignatureContext'
import { useCredentials } from '@/hooks/useCredentials'

type CreateLrnProps = { type: string; value: string; vcid: string; csid: string }

export interface UseLRN {
  createLRN: (props: CreateLrnProps) => Promise<any>
}

export const useLRN = (isCustomIssuer: boolean): UseLRN => {
  const { selectedClearingHouse } = useContext(ClearingHousesContext)
  const { privateKeyUse } = useContext(SignatureContext)
  const { createDid } = useCredentials()

  const buildLRNRequest = async (
    type: string,
    value: string,
    id?: string
  ): Promise<{
    credentialSubject: ClaimNotaryVC
    vcId: string | undefined
    didId: string | undefined
    uuid: string | undefined
    publicKey: string
  }> => {
    const credentialSubject: any = {
      '@context': ['https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/participant'],
      type: 'gx:legalRegistrationNumber',
      [`gx:${type}`]: value,
      id
    }
    const publicKey = await privateKeyUse.getPublicKey()
    if (!publicKey) throw new Error('Could not get public key')

    let vcId: string | undefined
    let didId: string | undefined
    let uuid: string | undefined
    if (!isCustomIssuer) {
      didId = publicKeyToDidId(publicKey).split(':').pop()
      const URIPath = joinPath(getRootUrl(location.pathname), 'api', 'credentials', didId ?? '')
      delete credentialSubject.id
      credentialSubject.id = await createCredentialSubjectId(
        joinPath(location.origin, URIPath),
        credentialSubject,
        encodeURIComponent(`${type}=${value}`)
      )
      uuid = uuidv4()
      vcId = await createCredentialSubjectId(joinPath(location.origin, URIPath) + '?uid=' + uuid, credentialSubject, uuid)
    }

    return { credentialSubject, vcId, didId, uuid, publicKey }
  }

  const createLRN = async (props: CreateLrnProps) => {
    const { credentialSubject, didId, vcId: generatedVcID, uuid, publicKey } = await buildLRNRequest(props.type, props.value, props.csid)
    if (isCustomIssuer) {
      const { data } = await axios.post(
        environment(window.location.href).requestNotaryRegistrationNumberEndpoint({
          vcid: encodeURIComponent(props.vcid)
        }),
        {
          stored: !isCustomIssuer,
          clearingHouse: selectedClearingHouse.registrationNotaryEndpoint,
          values: credentialSubject
        }
      )

      return data
    } else {
      await createDid(publicKey)

      const { data } = await axios.post(
        environment(window.location.href).requestNotaryRegistrationNumberEndpoint({
          didId: didId ?? '',
          uid: uuid ?? '',
          vcid: encodeURIComponent(generatedVcID ?? '')
        }),
        {
          stored: !isCustomIssuer,
          clearingHouse: selectedClearingHouse.registrationNotaryEndpoint,
          values: credentialSubject
        }
      )

      return data
    }
  }

  return {
    createLRN
  }
}
