import axios from 'axios'
import { useErrorHandler } from './useError'
import { environment } from '@/env'
import { getPemCertificateFromDERCertificate, getPublicKeyFromDERCertificate } from '@/web-crypto/cryptokey'
import { useEffect, useState } from 'react'
import { EIDHashResponseData, VerifiablePresentation, EIDJWSResponseData, EIDCertchainPostResponseData } from '@/sharedTypes'
import { VDocument } from '@/models/document'
import { normalize } from 'jsonld'
import { joinPath, stringToSHA256String } from '@/utils/util'

export type EIDSigningCertificate = { certificate: string; supportedSignatureAlgorithms: any }
export type EIDSupportedSignatureAlgorithms = { cryptoAlgorithm: string; hashFunction: string; paddingScheme: string } | null

type SignatureAlgorithm = {
  cryptoAlgorithm: string
  hashFunction: string
  paddingScheme: string
}

export const useEid = () => {
  const { errorHandler } = useErrorHandler()
  const [webEid, setWebEid] = useState<any>(null)
  const [eidCertificate, setEidCertificate] = useState<string>('')
  const [certChainURI, setcertChainURI] = useState<string>('')
  const [supportedSignatureAlgorithms, setSupportedSignatureAlgorithms] = useState<EIDSupportedSignatureAlgorithms>(null)

  useEffect(() => {
    const loadLibrary = async () => {
      if (typeof window !== 'undefined') {
        const module = await import('@web-eid/web-eid-library/web-eid')
        setWebEid(module)
      }
    }
    loadLibrary()
  }, [])

  const getCertChainURI = async (): Promise<string> => {
    if (certChainURI) {
      return certChainURI
    } else {
      const signingCertificate = await getCertificate()
      const pemCertificate = getPemCertificateFromDERCertificate(signingCertificate.certificate)
      const pemCertificateHash = await stringToSHA256String(pemCertificate)
      let builtURI = environment(window.location.href).requestCertChainEndpoint(pemCertificateHash)
      if (!builtURI.includes(window.location.href)) {
        builtURI = joinPath(window.location.href, builtURI)
      }
      setcertChainURI(builtURI)
      return builtURI
    }
  }

  const hostCertificateChain = async (): Promise<string | undefined> => {
    try {
      const chainURI = await getCertChainURI()
      const signingCertificate = await getCertificate()
      const pemCertificate = getPemCertificateFromDERCertificate(signingCertificate.certificate)
      const certChainUriResponse = await axios.post<EIDCertchainPostResponseData>(chainURI, pemCertificate, {
        headers: { 'Content-Type': 'text/plain' }
      })
      return certChainUriResponse.data.certChain
    } catch (error: any) {
      errorHandler(error, error.response?.data?.message || error.message)
    }
  }

  const toggleEID = async (enable: boolean) => {
    if (enable) {
      const status = await getEidStatus()

      if (!status) {
        localStorage.setItem('eid', 'false')
        return status
      }
    }
    // all good, enable or disable
    localStorage.setItem('eid', enable ? 'true' : 'false')
    return true
  }

  const getDERCertificateFromToken = async (): Promise<EIDSigningCertificate | undefined> => {
    try {
      const { certificate, supportedSignatureAlgorithms } = await webEid.getSigningCertificate()
      setEidCertificate(certificate)
      setSupportedSignatureAlgorithms(supportedSignatureAlgorithms)
      return { certificate, supportedSignatureAlgorithms }
    } catch (error: any) {
      errorHandler(error, 'Could not get the signing certificate from the eID card')
    }
  }

  async function getCertificate(): Promise<EIDSigningCertificate> {
    if (!eidCertificate || !supportedSignatureAlgorithms) {
      // we don't have them yet
      const signingCertificate = await getDERCertificateFromToken()
      if (!signingCertificate) {
        throw new Error('Could not get the signing certificate and supported signature algorithms from the eID card')
      }
      return signingCertificate
    } else {
      // we had them aldready
      const signingCertificate = { certificate: eidCertificate, supportedSignatureAlgorithms }
      return signingCertificate
    }
  }

  const getEidPublicKey = async (): Promise<string | undefined> => {
    try {
      const signingCertificate = await getCertificate()

      const pemPublicKey = await getPublicKeyFromDERCertificate(signingCertificate.certificate)
      return pemPublicKey
    } catch (error: any) {
      errorHandler(error, 'Could not get the public key from the eID certificate')
    }
  }

  const getHash = async (
    payload: string,
    header: string,
    supportedSignatureAlgorithms: EIDSupportedSignatureAlgorithms
  ): Promise<string | undefined> => {
    try {
      const hashResponse = await axios.post<EIDHashResponseData>(environment(window.location.href).requestPrepareSignatureEndpoint(), {
        payload,
        header,
        supportedSignatureAlgorithms
      })
      return hashResponse.data.hash
    } catch (error: any) {
      errorHandler(error, error.response?.data?.message || error.message)
    }
  }

  const checkOCSPAndGetCredential = async (
    payload: string,
    header: string,
    signature: string,
    signingCertificate: EIDSigningCertificate
  ): Promise<EIDJWSResponseData | undefined> => {
    try {
      const ocspResponse = await axios.post<EIDJWSResponseData>(environment(window.location.href).requestGetEIDJWSEndpoint(), {
        payload,
        header,
        signature,
        certificate: signingCertificate.certificate
      })
      return ocspResponse.data
    } catch (error: any) {
      errorHandler(error, error.response?.data?.message || error.message)
    }
  }

  const getEidStatus = async (): Promise<boolean | undefined> => {
    try {
      const status = await webEid.status()
      // might be useful to keep this for debugging
      console.log('WebEid Status', status)
      return true
    } catch (error: any) {
      // When the status check fails, in addition to the usual name, message and stack properties,
      // the error object contains the additional error code field code.
      errorHandler(error, error.message + ' ' + error.code)
    }
  }

  const isEidEnabled = (): boolean => {
    return localStorage.getItem('eid') === 'true'
  }

  const signWithEidAndBuildJws = async (payload: string): Promise<string | undefined> => {
    try {
      const signingCertificate = await getCertificate()

      const signatureAlgorithm = findPreferredHashAlgorithm(signingCertificate.supportedSignatureAlgorithms)
      const headerAlg = mapHashToAlg(signatureAlgorithm.hashFunction)

      if (!headerAlg) {
        throw new Error('Could not map the hash function to an algorithm')
      }

      const header = {
        alg: headerAlg,
        b64: false,
        crit: ['b64']
      }

      const hash = await getHash(payload, JSON.stringify(header), signatureAlgorithm)
      if (!hash) {
        throw new Error('Could not get the hash of the document')
      }
      // sign
      const { signature } = await webEid.sign(signingCertificate.certificate, hash, signatureAlgorithm.hashFunction)

      // verify certificate validity
      const ocspResponse = await checkOCSPAndGetCredential(payload, JSON.stringify(header), signature, signingCertificate)
      if (!ocspResponse?.valid) {
        throw new Error('The certificate is not valid or could not be verified: ' + ocspResponse?.status)
      }

      // build and host the certificate chain on the server
      const certChain = await hostCertificateChain()
      if (!certChain) {
        throw new Error('Could not build and host the certificate chain on the server')
      }

      return ocspResponse.jws
    } catch (error: any) {
      // we might get an error code if the native signing app returns one
      errorHandler(error, 'Could not sign the data with the eID card: ' + error.message + ' ' + error.code || '')
    }
  }

  async function signVerifiablePresentationWithEid(verifiablePresentation: VerifiablePresentation, verificationMethod: string): Promise<VDocument> {
    const signedVerifiableCredentials = await Promise.all(
      verifiablePresentation.verifiableCredential.map(
        async verifiableCredential => await signVerifiableCredentialWithEid(verifiableCredential, verificationMethod)
      )
    )

    return {
      ...verifiablePresentation,
      verifiableCredential: signedVerifiableCredentials
    }
  }

  async function signVerifiableCredentialWithEid(verifiableCredential: any, verificationMethod: string): Promise<VDocument> {
    const credentialNormalized = await normalize(verifiableCredential)

    const proof = await signWithEidAndBuildJws(credentialNormalized)
    const signedVerifiableCredential = {
      ...verifiableCredential,
      proof: {
        type: 'JsonWebSignature2020',
        created: new Date().toISOString(),
        proofPurpose: 'assertionMethod',
        verificationMethod: verificationMethod,
        jws: proof
      }
    }

    // Step 4: Add the signature to the verifiable credential
    return signedVerifiableCredential
  }

  function findPreferredHashAlgorithm(supportedAlgorithms: SignatureAlgorithm[], preferredHash = 'SHA-256'): SignatureAlgorithm {
    const preferredAlgorithm = supportedAlgorithms.find(algorithm => algorithm.hashFunction === preferredHash)

    return preferredAlgorithm ?? supportedAlgorithms[0]
  }

  function mapHashToAlg(hashFunction: string): string {
    switch (hashFunction) {
      case 'SHA-224':
        return 'ES224' // Custom identifier for ECDSA with SHA-224
      case 'SHA-256':
        return 'ES256' // Standard identifier for ECDSA with SHA-256
      case 'SHA-384':
        return 'ES384' // Standard identifier for ECDSA with SHA-384
      case 'SHA-512':
        return 'ES512' // Standard identifier for ECDSA with SHA-512
      default:
        throw new Error(`Unsupported hash function: ${hashFunction}`)
    }
  }

  async function signDocumentWithEid(doc: any, verificationMethodId: string): Promise<VDocument> {
    if (doc.type.includes('VerifiablePresentation')) {
      return await signVerifiablePresentationWithEid(doc, verificationMethodId)
    }
    if (doc.type.includes('VerifiableCredential')) {
      return await signVerifiableCredentialWithEid(doc, verificationMethodId)
    }
    throw new Error('Unsupported document type')
  }

  return {
    toggleEID,
    getCertChainURI,
    getEidPublicKey,
    getEidStatus,
    isEidEnabled,
    signDocumentWithEid
  }
}
