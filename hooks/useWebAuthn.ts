import { decrypt, encrypt, isCredentialIdSet, registerWebAuthn, unregisterWebAuthn } from '@/webauthn/encryption'
import { useQuery } from '@tanstack/react-query'
import axios from 'axios'
import { useErrorHandler } from './useError'
import { arrayBufferToBase64, bufferToCryptoKey, privateKeyToPem, stripPemHeaderAndFooter } from '@/web-crypto/cryptokey'
import { environment } from '@/env'

const fetchChallenge = async () => {
  const { data: challenge } = await axios.get<number[]>(environment(window.location.href).requestWebauthnChallengeEndpoint())
  return challenge
}

export const useWebAuthn = () => {
  const { errorHandler } = useErrorHandler()

  const { data, refetch } = useQuery({
    queryKey: ['challenge'],
    queryFn: fetchChallenge,
    onError: (error: Error) => errorHandler(error, 'Could not fetch challenge'),
    initialData: [],
    refetchOnWindowFocus: false,
    enabled: false
  })

  const getChallenge = async () => {
    await refetch()
    if (!data) throw new Error('Could not fetch challenge')
  }

  const enableWebauthn = async () => {
    try {
      await getChallenge()
      const challenge = new Uint8Array(data)
      await registerWebAuthn(challenge)
    } catch (error: any) {
      errorHandler(error, 'Could not register webauthn, platform or hardware not supported')
    }
  }

  const encryptKey = async (keyData: string): Promise<string | undefined> => {
    try {
      await getChallenge()

      const plainText = Uint8Array.from(window.atob(stripPemHeaderAndFooter(keyData)), c => c.charCodeAt(0))
      const challenge = new Uint8Array(data)
      const cipherText = await encrypt(challenge, plainText)

      if (cipherText) {
        return arrayBufferToBase64(cipherText)
      }

      throw new Error('Could not encrypt key')
    } catch (error: any) {
      errorHandler(error, 'Could not encrypt key')
    }
  }

  const decryptKey = async (keyData: string): Promise<string | undefined> => {
    try {
      await getChallenge()

      const cipherText = Uint8Array.from(window.atob(keyData), c => c.charCodeAt(0))
      const challenge = new Uint8Array(data)
      const plainText = await decrypt(challenge, cipherText)

      if (plainText) {
        const cryptoKey = await bufferToCryptoKey(plainText)
        return await privateKeyToPem(cryptoKey)
      }

      throw new Error('Could not decrypt key')
    } catch (error: any) {
      errorHandler(error, 'Could not decrypt key real')
    }
  }

  const disableWebauthn = async () => {
    try {
      if (isCredentialIdSet()) {
        unregisterWebAuthn()
      }
    } catch (error: any) {
      errorHandler(error, 'Could not unregister webauthn')
    }
  }

  const isWebauthnEnabled = () => {
    return isCredentialIdSet()
  }

  return {
    decryptKey,
    disableWebauthn,
    enableWebauthn,
    encryptKey,
    isWebauthnEnabled
  }
}
