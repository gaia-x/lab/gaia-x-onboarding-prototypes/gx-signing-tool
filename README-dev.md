# Developers documentation

[[_TOC_]]

## Requirements

- Access to internet
- http on port 3000
- asymmetric keypair & x509 certificate
- nodeJS 18 or NVM to install it

## Optional

- Docker (to run the graph-db)
- Docker compose (to run the graph-db)

## Run the wizard locally

### Prepare the environment variables

An example of env file is provided `.env.example`

```shell
cp .env.example .env
```

In this env file, you'll need to add your private key & certificate.
This will also allow you to change the storage mechanism for the wizard credentials & dids (see [storage](#storage))

A detailed explanation of each variable is accessible on the [helm documentation](./README.md#helm-chart)

### Using nodeJS

> ⚠️ Requires nodeJS 18.

If you are using [NVM](https://github.com/nvm-sh/nvm), you can do

```shell
nvm install
nvm use
```

You can then install dependencies using npm, hence the requirement on internet connection

```shell
npm install
```

The wizard requires an ontology-graphdb to run if you want to use the Policy Decision Point.
If you have one, you can set env variables accordingly, otherwise, one is provided via a docker-compose file

```shell
docker compose up -d graph-db
```

You can then start the process

```shell
npm run dev
```

### Using Docker

You call solely rely on docker & docker compose to run the component. You still have to prepare your .env file as
mentioned in [environment](#prepare-the-environment-variables)

```shell
docker compose up
```

### Storage

The wizard comes with several way of storing credentials, allowing a wide range of situations.
The setting is `STORAGE_PROVIDER` and takes the following values:

### inmemory

Pretty self-explanatory, every credential & did are stored in-memory of the Node process. Everything is lost on restart

### disk

This storage provider allows you to store on disk the credentials and dids generated via the wizard.
It comes with an additional parameter `DISK_STORAGE_PATH` allowing to set where to store credential on disk

### s3

This setting allows you to rely on a storage implementing S3 object-storage (AWS, OVH and others provide it).
It comes with additional parameters :

- `AWS_ENDPOINT` your S3 api endpoint
- `AWS_REGION` your s3 region (ex: eu-west-1, gra)
- `AWS_SDK_JS_SUPPRESS_MAINTENANCE_MODE_MESSAGE=1` This one is set to one to ignore the maintenance message from S3
  library
- `AWS_ACCESS_KEY_ID` your s3 access key id
- `AWS_SECRET_ACCESS_KEY` your s3 access key secret
- `STORAGE_S3_VCS_BUCKET` the name of the Bucket used to store Verifiable Credentials
- `STORAGE_S3_DID_BUCKET` the name of the Bucket used to store DID documents
- `STORAGE_S3_X509_BUCKET` the name of the Bucket used to store certificate chains

Please note that when selecting bucket names, it's important to ensure uniqueness at region level, as public cloud providers typically require this.

## Deploy for production

The Gaia-X European Association for Data and Cloud AISBL provides ready to deploy docker image and helm charts.
cf [Helm Chart](README.md#helm-chart) and [image tags](README.md#images-tags)

You are free to deploy manually, using docker-compose, swarm, but we recommend deploying on a k8s cluster using the helm
chart.

## Membership Credential feature

A dedicated page is used to issue, display and interact with the Gaia-X membership credential.
It is not mandatory to specify the following variables if the page is not used:

- KEYCLOAK_BASE_URL
- KEYCLOAK_CLIENT_ID
- KEYCLOAK_CLIENT_SECRET
- OIDC_SERVICE_BASE_URL
- MEMBERSHIP_CREDENTIAL_PORTAL_URL
