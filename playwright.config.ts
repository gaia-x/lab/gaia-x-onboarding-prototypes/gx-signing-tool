import { defineConfig, devices } from '@playwright/test'
import * as dotenv from 'dotenv'

const env: any = { ...process.env, ...(dotenv.config({ path: './tests/e2e/.env' }).parsed ?? {}) }

// https://playwright.dev/docs/test-configuration.
export default defineConfig({
  testDir: './tests/e2e',
  fullyParallel: true,
  forbidOnly: !!env.CI,
  retries: env.CI ? 5 : 0,
  workers: env.CI ? 3 : 4,
  reporter: 'html', // https://playwright.dev/docs/test-reporters
  expect: {
    timeout: env.CI ? 15000 : 5000
  },
  use: {
    baseURL: 'http://127.0.0.1:3000',
    trace: 'on-first-retry',
    video: 'on-first-retry'
  },

  projects: [
    {
      name: 'chromium',
      use: { ...devices['Desktop Chrome'] }
    },
    {
      name: 'firefox',
      use: { ...devices['Desktop Firefox'] }
    },
    {
      name: 'webkit',
      use: { ...devices['Desktop Safari'] }
    }
  ],

  webServer: {
    command: 'npm run build && npm run start',
    timeout: 180000,
    url: 'http://127.0.0.1:3000',
    reuseExistingServer: !env.CI,
    env
  }
})
