# Gaia-X Signing tool

main
branch: [![main pipeline status](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/badges/main/pipeline.svg)](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/-/commits/main)

development
branch: [![development pipeline status](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/badges/development/pipeline.svg)](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/-/commits/development)

[[_TOC_]]

You can use an instance of the Gaia-X Wizard [here](https://wizard.lab.gaia-x.eu).

This simple UI addresses common uses cases when emitting Verifiable Credentials:

- Generate a private key and keep it stored locally ;
- Sign Verifiable Credentials on client side using a private key ;
- Create a DID document and make it resolvable ;
- Create Verifiable Credentials a make them resolvable ;
- Submit Verifiable Crendentials to compliance service through a Verifiable Presentation, and get a signed VC back ;
- Make signed VC resolvable
- Store Verifiable Crendentials onto local browser IndexedDB storage

## [Development](README-dev.md)

## Images tags

This repo provides
several [images tags](https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool/container_registry/4094351).

| tag           | content              | example |
|---------------|----------------------|---------|
| `vX`          | latest major version | v1      |
| `vX.Y`        | latest minor version | v1.1    |
| `vX.Y.Z`      | specific version     | v1.1.1  |
| `main`        | latest stable        |         |
| `development` | latest unstable      |         |

Feature branches are also build and push to the container registry.
Versions are also referenced in the [changelog](CHANGELOG.md)

## Helm chart

A helm chart is provided inside <a href="k8s/Chart.yaml">k8s/Chart.yaml</a> folder.

It provides several environment variables for the application:

| Env Variable                        | Name in values file                 | Default value                                                      | Note                                                                                       | STORAGE_PROVIDER |
|-------------------------------------|-------------------------------------|--------------------------------------------------------------------|--------------------------------------------------------------------------------------------|------------------|
| BASE_URL                            |                                     | https://<ingress.hosts[0].host>/<ingress.hosts[0].paths[0].path>   | URL of the deployed application                                                            |                  |
| REGISTRY_API_BASE_URL               | REGISTRY_API_BASE_URL               | https://registry.lab.gaia-x.eu/main/api                            | URL of the registry to get shapes from                                                     |                  |
| COMPLIANCE_BASE_URL                 | COMPLIANCE_BASE_URL                 | https://compliance.lab.gaia-x.eu/main/api                          | URL of the compliance to submit VC to                                                      |                  |
| REGISTRATION_NUMBER_NOTARY_BASE_URL | REGISTRATION_NUMBER_NOTARY_BASE_URL | https://registrationnumber.notary.gaia-x.eu/v1                     | URL of the notary to submit VC to                                                          |                  |
| DID_X509_ROOT_CERTIFICATE           | DID_X509_ROOT_CERTIFICATE           |                                                                    | The root certificate for DID document certchain creation                                   |                  |
| DID_X509_ROOT_PRIVATE_KEY           | DID_X509_ROOT_CERTIFICATE           |                                                                    | The private key in PEM format for DID document certchain creation                          |                  |
| STORAGE_PROVIDER                    | STORAGE_PROVIDER                    | s3                                                                 | The storage provider to use, can be "s3", "disk" or "inmemory"                             |                  |
| AWS_ENDPOINT                        | AWS_ENDPOINT                        | https://s3.gra.io.cloud.ovh.net/                                   | AWS S3 endpoint for VC storage                                                             | s3               |
| AWS_REGION                          | AWS_REGION                          | gra                                                                | AWS S3 region endpoint for VC storage                                                      | s3               |
| AWS_ACCESS_KEY_ID                   | AWS_ACCESS_KEY_ID                   | 00000000000000000000000                                            | AWS S3 access key for VC storage                                                           | s3               |
| AWS_SECRET_ACCESS_KEY               | AWS_ACCESS_KEY_ID                   | 00000000000000000000000                                            | AWS S3 secret for VC storage                                                               | s3               |
| STORAGE_S3_VCS_BUCKET               | STORAGE_S3_VCS_BUCKET               | vcs                                                                | S3 bucket name for VC storage                                                              | s3               |
| STORAGE_S3_DID_BUCKET               | STORAGE_S3_DID_BUCKET               | did                                                                | S3 bucket name for DID document storage                                                    | s3               |
| STORAGE_S3_X509_BUCKET              | STORAGE_S3_X509_BUCKET              | x509                                                               | S3 bucket name for certificate chain storage                                               | s3               |
| DISK_STORAGE_PATH                   | DISK_STORAGE_PATH                   | ./storage                                                          | Storage path. The targeted container volume must be granted with read + write permissions. | disk             |
| GRAPHDB_ENABLED                     | GRAPHDB_ENABLED                     | true                                                               | True means that GraphDB is enabled and that policy reasoning features are enabled          |                  |
| GRAPHDB_PORT                        | GRAPHDB_PORT                        | 7200                                                               | Port for the GraphDB                                                                       |                  |
| GRAPHDB_HOST                        | GRAPHDB_HOST                        |                                                                    | GraphDB host (already supplied in deployment.yaml)                                         |                  |
| KEYCLOAK_BASE_URL                   | KEYCLOAK_BASE_URL                   | https://auth.gaia-x.eu/realms/gaia-x/protocol/openid-connect       | Base Url for keycloak authentication                                                       |                  |
| KEYCLOAK_CLIENT_ID                  | KEYCLOAK_CLIENT_ID                  | wizard                                                             | Keycloak client id                                                                         |                  |
| KEYCLOAK_CLIENT_SECRET              | KEYCLOAK_CLIENT_SECRET              | A `base64` of the client secret value                              | Keycloak client secret                                                                         |                  |
| OIDC_SERVICE_BASE_URL               | OIDC_SERVICE_BASE_URL               | https://wallet.lab.gaia-x.eu/development/requestCredential         | OpenId Connect service for VC issuance                                                    |                  |
| MEMBERSHIP_CREDENTIAL_PORTAL_URL    | MEMBERSHIP_CREDENTIAL_PORTAL_URL    | https://membership.lab.gaia-x.eu/development/membership-credential | Membership Credential portal                                                               |                  |

## Private key encryption through webauthn and a FIDO2 hardware token

In order to use a hardware token (eg: Yubikey) to encrypt the private key stored browser side, you will need both:

- A hardware token supporting
  the [hmac-secret extension](https://fidoalliance.org/specs/fido-v2.0-rd-20180702/fido-client-to-authenticator-protocol-v2.0-rd-20180702.html#sctn-hmac-secret-extension).
- A browser supporting the [PRF extension](https://w3c.github.io/webauthn/#prf-extension). At this point in time, this
  is only supported by Chrome Canary, starting from version 116, and you will need to enable
  the [Experimental Web Platform features flag](chrome://flags/#enable-experimental-web-platform-features). If on
  Windows, note that you may need a recent version of Windows to get PRF support in Windows itself, which Chrome depends
  on.

## Private key algorithms

The Wizard defaults to generating a RSA-PSS private key with SHA-256 hashing.
It supports RSA and ECDSA algorithms through the Web Crypto API.
There is no support for EdDSA as it is not supported by browsers yet.

## Prettify code

`.prettierrc` and `.prettierignore` contains the configuration for prettier.

You'll need to install/activate prettier in your IDE if it is available to make it work.

All the code pushed to the repository needs to be prettified, you can run prettier with the following command:

```bash
npm run prettify
```

## Policy Reasoning features

In order to run policy reasoning features a graph Database is required, for now only `GraphDB` is supported.
It is possible to either use the provided `docker-compose.yaml` or use a local instance.
Relevant configuration variables: `GRAPHDB_HOST`, `GRAPHDB_PORT` and `GRAPHDB_ENABLED`
More about how this feature works on the user guide.
