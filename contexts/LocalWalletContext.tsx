import React, { createContext, Dispatch, SetStateAction, useState } from 'react'
import { useLocalWallet } from '@/hooks/useLocalWallet'
import { StoredWalletEntry } from '@/models/wallet'

interface LocalWalletContextProps {
  handleLocalWalletImport: (document: StoredWalletEntry) => void
  handleDelete: (documentId: string) => void
  savedDocuments: StoredWalletEntry[]
  selectedDocuments: StoredWalletEntry[]
  setSelectedDocuments: Dispatch<SetStateAction<StoredWalletEntry[]>>
}

export const LocalWalletContext = createContext<LocalWalletContextProps>({
  handleLocalWalletImport: () => null,
  savedDocuments: [],
  handleDelete: () => null,
  selectedDocuments: [],
  setSelectedDocuments: () => null
})

interface WalletProviderProps {
  children: React.ReactNode
}

export const LocalWalletProvider: React.FC<WalletProviderProps> = ({ children }) => {
  const { walletEntries, persistWalletEntry, deleteWalletEntry } = useLocalWallet()
  const [savedDocuments, setSavedDocuments] = useState<StoredWalletEntry[]>(walletEntries)
  const [selectedDocuments, setSelectedDocuments] = useState<StoredWalletEntry[]>([])

  const handleLocalWalletImport = async (document: StoredWalletEntry) => {
    await persistWalletEntry(document)
    const index = savedDocuments.findIndex(d => d.key === document.key)
    if (index === -1) {
      setSavedDocuments([...savedDocuments, document])
    } else {
      const newDocs = [...savedDocuments]
      newDocs[index] = document
      setSavedDocuments(newDocs)
    }
  }

  const handleDelete = async (documentId: string) => {
    await deleteWalletEntry(documentId)
    const newSavedDocuments = savedDocuments.filter(document => document.key !== documentId)
    setSavedDocuments(newSavedDocuments)

    const newSelectedDocuments = selectedDocuments.filter(document => document.key !== documentId)
    setSelectedDocuments(newSelectedDocuments)
  }

  return (
    <LocalWalletContext.Provider
      value={{
        selectedDocuments,
        setSelectedDocuments,
        handleLocalWalletImport,
        savedDocuments,
        handleDelete
      }}
    >
      {children}
    </LocalWalletContext.Provider>
  )
}
