import { createContext, ReactNode, useContext, useEffect, useState } from 'react'
import { VDocument } from '@/models/document'
import { CreateCredentialSubjectState } from '@/components/Stepper/CreateCredentialSubject/CreateCredentialSubject'
import { generateName } from '@/utils/util'
import { IdentifierStepState } from '@/components/Stepper/IdentifierStep/IdentifierStep.tsx/IdentifierStep'
import { useExamples } from '@/hooks/useExamples'
import { ExampleTuple } from '@/sharedTypes'
import { UseSteps, useSteps } from '@/hooks/stepper/useSteps'
import { useCreateVC, UseCreateVC } from '@/hooks/stepper/useCreateVC'
import { SignatureContext } from './SignatureContext'
import { useErrorHandler } from '@/hooks/useError'
import { generatePrivateKey, privateKeyToPem } from '@/web-crypto/cryptokey'
import { useCreateMultiVC, UseCreateMultiVC } from '@/hooks/stepper/useCreateMultiVC'

interface StepperContextProps {
  docName: string
  vcCreator: UseCreateVC
  multiVcCreator: UseCreateMultiVC
  steps: UseSteps
  handleCompleteCreateCredentialSubject: (createCredentialSubjectState: CreateCredentialSubjectState) => void
  handleCompleteKeyStep: () => void
  handleCompleteIdentifierStep: (identifierStepState: IdentifierStepState) => void
  setSelectedExample: (example: ExampleTuple | undefined) => void
  selectedExample?: ExampleTuple
  vc?: VDocument | VDocument[]
  privateKeyChanged: boolean
  setPrivateKeyChanged: (changed: boolean) => void
}

export const StepperContext = createContext<StepperContextProps>({
  docName: '',
  steps: {
    steps: [],
    activeStep: 0,
    setActiveStep: () => null,
    goToNext: () => null,
    goToPrevious: () => null
  },
  vcCreator: {
    values: {
      credentialSubjectId: '',
      credentialSubjectValues: undefined,
      shape: '',
      id: ''
    },
    setValues: () => null,
    createVC: () => new Promise(() => null)
  },
  multiVcCreator: {
    valuesByType: {},
    setValue: () => null,
    createVCs: () => Promise.resolve([]),
    of: () => ({
      value: {} as any,
      setValue: () => null
    }),
    legalRegistrationNumberVC: null,
    setLegalRegistrationNumberVC: () => null,
    identifiers: null,
    setIdentifiers: () => null
  },
  handleCompleteCreateCredentialSubject: () => null,
  handleCompleteKeyStep: () => null,
  handleCompleteIdentifierStep: () => null,
  selectedExample: undefined,
  setSelectedExample: () => null,
  vc: undefined,
  privateKeyChanged: false,
  setPrivateKeyChanged: () => null
})

export const StepperProvider = ({ allRequiredVCs = false, children }: { children: ReactNode; allRequiredVCs: boolean }) => {
  const { privateKeyUse, issuerUse, isSignMultiVC } = useContext(SignatureContext)
  const steps = useSteps(issuerUse.isCustomIssuer, allRequiredVCs)
  const [privateKeyChanged, setPrivateKeyChanged] = useState(false)
  const [docName, setDocName] = useState<string>('')
  const { selectedExample, setSelectedExample } = useExamples()
  const { errorHandler } = useErrorHandler()
  const vcCreator = useCreateVC(selectedExample)
  const multiVcCreator = useCreateMultiVC()

  const [vc, setVc] = useState<VDocument | VDocument[]>()

  const handleCompleteCreateCredentialSubject = (createCredentialSubjectState: CreateCredentialSubjectState) => {
    vcCreator.setValues({ ...vcCreator.values, ...createCredentialSubjectState })
    steps.goToNext()
  }

  const handleCompleteCreateVC = async () => {
    setDocName(generateName())
    const buildVC = async () => {
      const issuer = issuerUse.isCustomIssuer ? issuerUse.issuer : await privateKeyUse.buildWizardDidWeb()
      if (!issuer) {
        throw new Error('No issuer')
      }
      if (isSignMultiVC) {
        setVc(await multiVcCreator.createVCs(issuerUse.isCustomIssuer, issuer))
      } else {
        setVc(await vcCreator.createVC(issuerUse.isCustomIssuer, issuer))
      }
    }
    try {
      await buildVC()
    } catch (err: any) {
      try {
        if (issuerUse.isCustomIssuer) {
          throw err
        }
        const privateKey = await generatePrivateKey()
        const pemKey = await privateKeyToPem(privateKey)
        if (pemKey) {
          privateKeyUse.setPrivateKey(pemKey)
        }
        await buildVC()
      } catch {
        errorHandler(err, 'Your private key is not valid')
      }
    }
  }

  const handleCompleteKeyStep = () => steps.goToNext()

  const handleCompleteIdentifierStep = (identifierStepState: IdentifierStepState) => {
    issuerUse.setIssuer(identifierStepState.issuer)
    issuerUse.setVerificationMethod(identifierStepState.verificationMethod)
    steps.goToNext()
  }

  useEffect(() => {
    if (steps.activeStep === steps.steps.length - 1) {
      handleCompleteCreateVC()
    }
  }, [issuerUse.isCustomIssuer, steps.activeStep])

  return (
    <StepperContext.Provider
      value={{
        vcCreator,
        multiVcCreator,
        docName,
        steps,
        handleCompleteCreateCredentialSubject,
        handleCompleteKeyStep,
        handleCompleteIdentifierStep,
        selectedExample,
        setSelectedExample,
        vc,
        privateKeyChanged,
        setPrivateKeyChanged
      }}
    >
      {children}
    </StepperContext.Provider>
  )
}
