import React, { createContext, useEffect, useState } from 'react'
import { v4 as uuidv4 } from 'uuid'
import type { StoredWalletEntry } from '@/models/wallet'
import type { StorableVDocument } from '@/models/document'
import { GaiaXDeploymentPath } from '@/sharedTypes'

export interface AppContextProps {
  signedDocuments: Map<string, StoredWalletEntry>
  addDocument: (doc: StorableVDocument, isVerified?: false | GaiaXDeploymentPath) => void
  deleteDocument: (key: string) => void
}

export const AppContext = createContext<AppContextProps>({
  signedDocuments: new Map(),
  addDocument: () => null,
  deleteDocument: () => null
})

const HOLDER_LOCAL_STORAGE_KEY = 'gxWizardHolderVCs'

export const AppProvider = ({ children }: { children: React.ReactNode }) => {
  const [signedDocuments, setSignedDocuments] = useState<Map<string, StoredWalletEntry>>(new Map())

  const addDocument = (doc: StorableVDocument, isVerified: false | GaiaXDeploymentPath = false) => {
    const documentUuid = doc.key || uuidv4()
    setSignedDocuments(
      new Map(
        signedDocuments.set(documentUuid, {
          key: documentUuid,
          name: doc.docName || 'Document',
          creation: new Date().toISOString(),
          type: doc.document.type,
          privateKey: doc.privateKey,
          document: doc.document,
          rawDocument: doc.rawDocument,
          format: doc.format,
          isVerified
        })
      )
    )
  }

  const deleteDocument = (key: string) => {
    const newMap = new Map(signedDocuments)
    newMap.delete(key)
    setSignedDocuments(newMap)
    localStorage.setItem(HOLDER_LOCAL_STORAGE_KEY, JSON.stringify(Array.from(newMap.entries())))
  }

  useEffect(() => {
    if (signedDocuments.size !== 0) localStorage.setItem(HOLDER_LOCAL_STORAGE_KEY, JSON.stringify(Array.from(signedDocuments.entries())))
  }, [signedDocuments])

  useEffect(() => {
    const VCsArray = localStorage.getItem(HOLDER_LOCAL_STORAGE_KEY)
    if (VCsArray) {
      const VCsMap = new Map(JSON.parse(VCsArray)) as Map<string, StoredWalletEntry>
      setSignedDocuments(VCsMap)
    }
  }, [])

  return (
    <AppContext.Provider
      value={{
        signedDocuments,
        addDocument,
        deleteDocument
      }}
    >
      {children}
    </AppContext.Provider>
  )
}
