import { createContext, ReactNode, useState } from 'react'
import { useSteps, UseSteps } from '@/hooks/stepper/usePolicySteps'
import { usePolicyReasoning } from '@/hooks/usePolicyReasoning'
import { StoredWalletEntry } from '@/models/wallet'
import { StorableVDocument } from '@/models/document'
import { v4 as uuidv4 } from 'uuid'
import { useErrorHandler } from '@/hooks/useError'
import { useToast } from '@chakra-ui/react'
import { ContractAgreement } from '@/models/dataExchange'

interface PolicyStepperContextProps {
  credentials: Map<string, StoredWalletEntry>
  serviceOffering?: StorableVDocument
  setServiceOffering: (vc: StorableVDocument | undefined) => void
  stepper: UseSteps
  handleLastStep: () => void
  odrlOffer: string
  setOdrlOffer: (offer: string) => void
  odrlRequest: string
  setOdrlRequest: (request: string) => void
  odrlAgreement?: ContractAgreement
  setOdrlAgreement: (agreement: ContractAgreement) => void
  addCredential: (doc: StorableVDocument) => void
  deleteCredential: (key: string) => void
  requestedCredentials: string[]
  handleOfferPolicy: () => void
  provider: string
  consumer: string
}

export const PolicyStepperContext = createContext<PolicyStepperContextProps>({
  stepper: {
    steps: [],
    activeStep: 0,
    setActiveStep: () => null,
    goToNext: () => null,
    goToPrevious: () => null
  },
  serviceOffering: undefined,
  setServiceOffering: () => null,
  credentials: new Map(),
  handleLastStep: () => null,
  odrlOffer: '',
  setOdrlOffer: () => null,
  odrlRequest: '',
  setOdrlRequest: () => null,
  odrlAgreement: undefined,
  setOdrlAgreement: () => null,
  addCredential: () => null,
  deleteCredential: () => null,
  requestedCredentials: [],
  handleOfferPolicy: () => null,
  consumer: '',
  provider: ''
})

export const PolicyStepperProvider = ({ children }: { children: ReactNode }) => {
  const toast = useToast()
  const { errorHandler } = useErrorHandler()
  const stepper = useSteps()
  const [credentials, setCredentials] = useState<Map<string, StoredWalletEntry>>(new Map())
  const [serviceOffering, setServiceOffering] = useState<StorableVDocument>()
  const {
    offer,
    setOffer,
    request,
    setRequest,
    agreement,
    setAgreement,
    credentialsVerification,
    handleOfferPolicy,
    handleReasoning,
    consumer,
    setProvider,
    setConsumer,
    provider
  } = usePolicyReasoning()

  const addCredential = (doc: StorableVDocument) => {
    if (stepper.activeStep === 0) {
      extractServiceOfferingPolicy(doc)
    } else {
      if (credentialsVerification.map(cv => cv['ovc:credentialSubjectType']).includes(doc.document.credentialSubject.type)) {
        if (doc.document.credentialSubject.type === 'gx:LegalParticipant') {
          setConsumer(doc.document.id)
        }
        addCredentialToAMap(doc)
      } else {
        toast({
          title: 'Non-required credential',
          description: 'This credential type is not required for the reasoning',
          status: 'warning',
          duration: 8000,
          isClosable: true
        })
      }
    }
  }

  const extractServiceOfferingPolicy = (serviceOffering: StorableVDocument) => {
    try {
      if (serviceOffering.document.credentialSubject.type === 'gx:ServiceOffering') {
        setServiceOffering(serviceOffering)
        setOffer(serviceOffering.document.credentialSubject['gx:policy'])
        setProvider(serviceOffering.document.credentialSubject['gx:providedBy']?.['id'])
      } else {
        toast({
          title: 'Unsupported type',
          description: 'Must be a gx:ServiceOffering Verifiable Credential',
          status: 'error',
          duration: 8000,
          isClosable: true
        })
      }
    } catch (err: any) {
      errorHandler(err)
    }
  }

  const addCredentialToAMap = (doc: StorableVDocument) => {
    const documentUuid = doc.key || uuidv4()
    setCredentials(
      new Map(
        credentials.set(documentUuid, {
          key: documentUuid,
          name: doc.docName || 'Document',
          creation: new Date().toISOString(),
          type: doc.document.type,
          privateKey: doc.privateKey,
          document: doc.document,
          rawDocument: doc.rawDocument,
          format: doc.format,
          isVerified: false
        })
      )
    )
  }

  const handleLastStep = async () => {
    return await handleReasoning(Array.from(credentials.values()))
  }

  const deleteCredential = (key: string) => {
    const newMap = new Map(credentials)
    newMap.delete(key)
    setCredentials(newMap)
  }

  return (
    <PolicyStepperContext.Provider
      value={{
        stepper,
        serviceOffering,
        setServiceOffering,
        credentials,
        addCredential,
        deleteCredential,
        handleLastStep,
        odrlOffer: offer,
        setOdrlOffer: setOffer,
        odrlRequest: request,
        setOdrlRequest: setRequest,
        odrlAgreement: agreement,
        setOdrlAgreement: setAgreement,
        requestedCredentials: credentialsVerification.map(cv => cv['ovc:credentialSubjectType']),
        handleOfferPolicy,
        consumer,
        provider
      }}
    >
      {children}
    </PolicyStepperContext.Provider>
  )
}
