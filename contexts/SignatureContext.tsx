import React, { createContext, useContext, useState } from 'react'
import { UseIssuer, useIssuer } from '@/hooks/useIssuer'
import { PrivateKeyUse, usePrivateKey } from '@/hooks/usePrivateKey'
import { useSignature } from '@/hooks/useSignature'
import { AppContext } from './AppContext'
import { VDocument } from '@/models/document'
import { useEid } from '@/hooks/useEid'

export interface SignatureContextProps {
  privateKeyUse: PrivateKeyUse
  issuerUse: UseIssuer
  issuerDoc: string
  docName: string
  setDocName: (name: string) => void
  setIssuerDoc: (doc: string) => void
  signDocument: () => void
  signingIsLoading: boolean
  isSignMultiVC: boolean
}

export const SignatureContext = createContext<SignatureContextProps>({
  privateKeyUse: {
    privateKey: '',
    setPrivateKey: () => null,
    getPrivateKey: () => new Promise(() => null),
    getPublicKey: () => new Promise(() => null),
    buildWizardDidWeb: () => new Promise(() => null)
  },
  issuerDoc: '',
  issuerUse: {
    issuer: '',
    setIssuer: () => null,
    isCustomIssuer: false,
    setIsCustomIssuer: () => null,
    verificationMethod: '',
    setVerificationMethod: () => null,
    isValid: false
  },
  docName: '',
  setDocName: () => null,
  setIssuerDoc: () => null,
  signDocument: () => null,
  signingIsLoading: false,
  isSignMultiVC: false
})

export interface SignatureProviderOptions {
  defaultIsCustomIssuer: boolean
  isSignMultiVC?: boolean
}

export const SignatureProvider = ({ children, defaultIsCustomIssuer, isSignMultiVC }: SignatureProviderOptions & { children: React.ReactNode }) => {
  const { addDocument } = useContext(AppContext)
  const issuerUse = useIssuer(defaultIsCustomIssuer)
  const privateKeyUse = usePrivateKey()
  const { isEidEnabled } = useEid()
  const [issuerDoc, setIssuerDoc] = useState<string>('')
  const [docName, setDocName] = useState<string>('')

  const onDocSigned = async (document: VDocument) => {
    let privateKey = ''
    if (!isEidEnabled()) {
      privateKey = (await privateKeyUse.getPrivateKey()) ?? ''
    }
    addDocument({
      document,
      docName,
      privateKey
    })
  }

  const { signingIsLoading, signDocument } = useSignature({
    issuerDoc,
    issuerUse,
    privateKeyUse,
    docName,
    onDocSigned
  })

  return (
    <SignatureContext.Provider
      value={{
        issuerDoc,
        issuerUse,
        privateKeyUse,
        docName,
        setDocName,
        setIssuerDoc,
        signDocument,
        signingIsLoading,
        isSignMultiVC: isSignMultiVC ?? false
      }}
    >
      {children}
    </SignatureContext.Provider>
  )
}
