import React, { createContext, Dispatch, SetStateAction, useCallback, useEffect, useState } from 'react'
import { StoredWalletEntry } from '@/models/wallet'
import { useDecentralizedWebNode } from '@/hooks/useDecentralizedWebNode'

const DECENTRALIZED_NODE_URL_KEY = 'decentralizedWebNodeUrl'

interface DecentralizedWebNodeContextProps {
  handleDecentralizedWebNodeImport: (document: StoredWalletEntry) => void
  decentralizedWebNodeUrl: string
  setDecentralizedWebNodeUrl: (url: string) => void
  walletEntries: StoredWalletEntry[]
  setWalletEntries: Dispatch<SetStateAction<StoredWalletEntry[]>>
  refreshCredentials: (url: string) => Promise<void>
}

export const DecentralizedWebNodeContext = createContext<DecentralizedWebNodeContextProps>({
  handleDecentralizedWebNodeImport: () => null,
  decentralizedWebNodeUrl: '',
  setDecentralizedWebNodeUrl: () => null,
  walletEntries: [],
  setWalletEntries: () => null,
  refreshCredentials: async () => undefined
})

interface DecentralizedWebNodeProviderProps {
  children: React.ReactNode
}

export const DecentralizedWebNodeProvider: React.FC<DecentralizedWebNodeProviderProps> = ({ children }) => {
  const { addCredentials, getCredentials } = useDecentralizedWebNode()
  const [decentralizedWebNodeUrl, setDecentralizedWebNodeUrl] = useState<string>('')
  const [walletEntries, setWalletEntries] = useState<StoredWalletEntry[]>([])

  const handleDecentralizedWebNodeImport = async (document: StoredWalletEntry) => {
    await addCredentials(document, decentralizedWebNodeUrl)
    await refreshCredentials(decentralizedWebNodeUrl)
  }

  const refreshCredentials = useCallback(async (url: string) => setWalletEntries(await getCredentials(url)), [])

  const saveDecentralizedWebNodeUrl = (url: string) => {
    localStorage.setItem(DECENTRALIZED_NODE_URL_KEY, url)
    setDecentralizedWebNodeUrl(url)
  }

  useEffect(() => {
    const storedUrl = localStorage.getItem(DECENTRALIZED_NODE_URL_KEY)
    if (storedUrl) {
      setDecentralizedWebNodeUrl(storedUrl)
    }
  }, [])

  return (
    <DecentralizedWebNodeContext.Provider
      value={{
        handleDecentralizedWebNodeImport,
        decentralizedWebNodeUrl,
        setDecentralizedWebNodeUrl: saveDecentralizedWebNodeUrl,
        walletEntries,
        setWalletEntries,
        refreshCredentials
      }}
    >
      {children}
    </DecentralizedWebNodeContext.Provider>
  )
}
