import React, { createContext, useContext, useEffect, useState } from 'react'
import { AppContext } from '@/contexts/AppContext'
import { AvailableWallets, StoredWalletEntry } from '@/models/wallet'
import { PolicyStepperContext } from '@/contexts/PolicyStepperContext'

const CONNECTED_WALLET_KEY = 'connectedWallet'

interface WalletContextProps {
  isWalletOpen: boolean
  setWalletIsOpen: (isOpen: boolean) => void
  isWalletInPolicyContext: boolean
  setWalletInPolicyContext: (isInPolicyContext: boolean) => void
  connectedWallet: string
  setConnectedWallet: (wallet: AvailableWallets) => void
  handleExport: (document: StoredWalletEntry) => void
}

export const WalletContext = createContext<WalletContextProps>({
  isWalletOpen: false,
  setWalletIsOpen: () => null,
  isWalletInPolicyContext: false,
  setWalletInPolicyContext: () => null,
  connectedWallet: AvailableWallets.NONE,
  setConnectedWallet: () => null,
  handleExport: () => null
})

interface WalletProviderProps {
  children: React.ReactNode
}

export const WalletProvider: React.FC<WalletProviderProps> = ({ children }) => {
  const { addDocument } = useContext(AppContext)
  const { addCredential } = useContext(PolicyStepperContext)
  const [isWalletOpen, setWalletIsOpen] = useState<boolean>(false)
  const [isWalletInPolicyContext, setWalletInPolicyContext] = useState<boolean>(false)
  const [connectedWallet, setConnectedWallet] = useState<string>(AvailableWallets.NONE)

  const saveConnectedWallet = (wallet: AvailableWallets) => {
    localStorage.setItem(CONNECTED_WALLET_KEY, wallet)
    setConnectedWallet(wallet)
  }

  useEffect(() => {
    const storedWallet = localStorage.getItem(CONNECTED_WALLET_KEY)
    if (storedWallet) {
      setConnectedWallet(storedWallet)
    }
  }, [])

  const handleExport = (document: StoredWalletEntry) => {
    const credential = {
      docName: document.name,
      document: document.document,
      privateKey: document.privateKey || '',
      key: document.key,
      rawDocument: document.rawDocument,
      format: document.format
    }
    if (isWalletInPolicyContext) {
      addCredential(credential)
      setWalletIsOpen(false)
    } else {
      addDocument(credential)
    }
  }

  return (
    <WalletContext.Provider
      value={{
        isWalletOpen,
        setWalletIsOpen,
        connectedWallet,
        setConnectedWallet: saveConnectedWallet,
        isWalletInPolicyContext,
        setWalletInPolicyContext,
        handleExport
      }}
    >
      {children}
    </WalletContext.Provider>
  )
}
