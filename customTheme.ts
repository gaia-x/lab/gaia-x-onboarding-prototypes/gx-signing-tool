import { extendTheme } from '@chakra-ui/react'

const customColors = {
  gxBlue: '#000094',
  darkBlue: '#000071',
  mediumBlue: '#465AFF',
  purple: '#B900FF',
  turquoise: '#46DAFF'
}

export const gradients = {
  primary: `linear(to-br, ${customColors.purple} -5%, ${customColors.gxBlue} 85%, ${customColors.turquoise} 110%)`
}

const breakpoints = {
  base: '0em', // 0px
  sm: '30em', // ~480px. em is a relative unit and is dependant on the font size.
  md: '48em', // ~768px
  lg: '62em', // ~992px
  xl: '80em', // ~1280px
  '2xl': '96em', // ~1536px
  '3xl': '120em' // ~1920px
}

export const customTheme = extendTheme({
  colors: { ...customColors },
  breakpoints,
  styles: {
    global: () => ({
      'html, body': {
        backgroundAttachment: 'fixed',
        bgGradient: gradients.primary
      }
    })
  }
})
