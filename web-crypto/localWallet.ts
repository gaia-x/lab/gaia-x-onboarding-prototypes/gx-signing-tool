import { StoredWalletEntry } from '@/models/wallet'

export interface LocalWalletService {
  fetch(key: string): Promise<StoredWalletEntry>
  persist(value: StoredWalletEntry): Promise<void>
  delete(key: string): Promise<void>
  list(): Promise<StoredWalletEntry[]>
  subscribe(listener: (change: LocalWalletChange) => unknown): {
    unsubscribe: () => void
  }
}

export type LocalWalletChange =
  | {
      type: 'persist'
      value: StoredWalletEntry
    }
  | {
      type: 'delete'
      key: string
    }

interface LocalStore<K extends keyof V, V> {
  type: 'localStorage' | 'indexedDB'

  fetch(key: V[K]): Promise<V>
  persist(value: V): Promise<void>
  delete(key: V[K]): Promise<void>
  list(): Promise<V[]>
}

class LocalStorageStore<K extends keyof V, V> implements LocalStore<K, V> {
  readonly type = 'localStorage'

  constructor(private key: K) {}

  fetch(key: V[K]): Promise<V> {
    const item = localStorage.getItem(key + '')
    return item ? JSON.parse(item) : null
  }

  async persist(value: V): Promise<void> {
    const key = value[this.key]
    localStorage.setItem(key + '', JSON.stringify(value))
  }

  async delete(key: V[K]): Promise<void> {
    localStorage.removeItem(key + '')
  }

  async list(): Promise<V[]> {
    const entries: V[] = []
    for (let i = 0; i < localStorage.length; i++) {
      const key = localStorage.key(i)
      if (key) {
        const item = localStorage.getItem(key)
        if (item) {
          entries.push(JSON.parse(item))
        }
      }
    }
    return entries
  }
}

class IndexedDBStore<K extends keyof V, V> implements LocalStore<K, V> {
  readonly type = 'indexedDB'

  constructor(
    private db: IDBDatabase,
    private storeName: string
  ) {}

  async fetch(key: V[K] & IDBValidKey): Promise<V> {
    const transaction = this.db.transaction([this.storeName], 'readonly')
    const objectStore = transaction.objectStore(this.storeName)
    const request = objectStore.get(key)
    await transactionToPromise(transaction)
    return request.result ?? null
  }

  async persist(value: V): Promise<void> {
    const transaction = this.db.transaction([this.storeName], 'readwrite')
    const objectStore = transaction.objectStore(this.storeName)
    objectStore.put(value)
    await transactionToPromise(transaction)
  }

  async delete(key: V[K] & IDBValidKey): Promise<void> {
    const transaction = this.db.transaction([this.storeName], 'readwrite')
    const objectStore = transaction.objectStore(this.storeName)
    objectStore.delete(key)
    await transactionToPromise(transaction)
  }

  async list(): Promise<V[]> {
    const transaction = this.db.transaction([this.storeName], 'readonly')
    const objectStore = transaction.objectStore(this.storeName)
    const request = objectStore.getAll()
    await transactionToPromise(transaction)
    return request.result
  }
}

class LocalWalletServiceImpl implements LocalWalletService {
  private store?: Promise<LocalStore<'key', StoredWalletEntry>>
  private listeners: ((change: LocalWalletChange) => unknown)[] = []

  constructor(
    private idbVersion = 1,
    private dbName = 'wallet',
    private objectStoreName = 'wallet-entries'
  ) {}

  async fetch(key: string): Promise<StoredWalletEntry> {
    return (await this.getStore()).fetch(key)
  }

  async persist(value: StoredWalletEntry): Promise<void> {
    ;(await this.getStore()).persist(value)
    this.emitChange({ type: 'persist', value })
  }

  async delete(key: string): Promise<void> {
    ;(await this.getStore()).delete(key)
    this.emitChange({ type: 'delete', key })
  }

  async list(): Promise<StoredWalletEntry[]> {
    return (await this.getStore()).list()
  }

  getStore(): Promise<LocalStore<'key', StoredWalletEntry>> {
    return (this.store ??= this.createStore())
  }

  subscribe(listener: (change: LocalWalletChange) => unknown): {
    unsubscribe: () => void
  } {
    this.listeners.push(listener)
    return {
      unsubscribe: () => {
        const index = this.listeners.indexOf(listener)
        if (index > -1) {
          this.listeners.splice(index, 1)
        }
      }
    }
  }

  private emitChange(change: LocalWalletChange): void {
    this.listeners.forEach(l => l(change))
  }

  private async createStore(): Promise<LocalStore<'key', StoredWalletEntry>> {
    const indexedDBInstance = await new Promise<IDBDatabase | null>(resolve => {
      const request = typeof window?.['indexedDB'] === 'undefined' ? null : indexedDB.open(this.dbName, this.idbVersion)
      if (request == null) {
        resolve(null)
      } else {
        request.onerror = () => resolve(null)
        request.onsuccess = () => resolve(request.result)
        request.onupgradeneeded = () => {
          request.onsuccess = null
          const db = request.result
          db.createObjectStore(this.objectStoreName, { keyPath: 'key' })
          if (request.transaction) {
            transactionToPromise(request.transaction)
              .then(() => resolve(request.result))
              .catch(() => resolve(null))
          }
        }
      }
    })
    return indexedDBInstance ? new IndexedDBStore(indexedDBInstance, this.objectStoreName) : new LocalStorageStore('key')
  }
}

function transactionToPromise(transaction: IDBTransaction): Promise<Event> {
  return new Promise((r, e) => {
    transaction.oncomplete = r
    transaction.onerror = e
  })
}

const instance: LocalWalletService = new LocalWalletServiceImpl()

if (typeof window !== 'undefined') {
  ;((window as any)['gx'] ??= {})['localWallet'] = instance
}

export { instance as localWalletService }
