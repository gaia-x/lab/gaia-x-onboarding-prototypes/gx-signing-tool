import base58 from 'bs58'

export function publicKeyToDidId(publicKey: string) {
  return base58.encode(new TextEncoder().encode(publicKey))
}

export function getDidWeb(host: string, path: string) {
  return (
    'did:web:' +
    `${host}${path}`
      .replace(/http[s]?:\/\//, '')
      .replace(/\/+/g, '/')
      .replace(':', '%3A') // encode port ':' as '%3A' in did:web
      .replace(/\//g, ':')
  )
}

export function getDidWebUrl(did: string, protocol = 'https') {
  return protocol + '//' + did.substring(8).replaceAll(':', '/').replaceAll('%3A', ':') + '/did.json'
}

export const JCAMapping = new Map([
  [
    'HmacSHA256',
    {
      JWS: 'HS256',
      OID: '1.2.840.113549.2.9'
    }
  ],
  [
    'HmacSHA384',
    {
      JWS: 'HS384',
      OID: '1.2.840.113549.2.10'
    }
  ],
  [
    'HmacSHA512',
    {
      JWS: 'HS512',
      OID: '1.2.840.113549.2.11'
    }
  ],
  [
    'SHA256withRSA',
    {
      JWS: 'RS256',
      OID: '1.2.840.113549.1.1.11'
    }
  ],
  [
    'SHA384withRSA',
    {
      JWS: 'RS384',
      OID: '1.2.840.113549.1.1.12'
    }
  ],
  [
    'SHA512withRSA',
    {
      JWS: 'RS512',
      OID: '1.2.840.113549.1.1.13'
    }
  ],
  [
    'SHA256withECDSA',
    {
      JWS: 'ES256',
      OID: '1.2.840.10045.4.3.2'
    }
  ],
  [
    'SHA384withECDSA',
    {
      JWS: 'ES384',
      OID: '1.2.840.10045.4.3.3'
    }
  ],
  [
    'SHA512withECDSA',
    {
      JWS: 'ES512',
      OID: '1.2.840.10045.4.3.4'
    }
  ],
  [
    'SHA256withRSAandMGF1',
    {
      JWS: 'PS256',
      OID: '1.2.840.113549.1.1.10'
    }
  ],
  [
    'SHA384withRSAandMGF1',
    {
      JWS: 'PS384',
      OID: '1.2.840.113549.1.1.10'
    }
  ],
  [
    'SHA512withRSAandMGF1',
    {
      JWS: 'PS512',
      OID: '1.2.840.113549.1.1.10'
    }
  ]
])
