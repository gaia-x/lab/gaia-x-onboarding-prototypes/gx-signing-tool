import { importPKCS8 } from 'jose'
import { KEYUTIL, X509, KJUR, RSAKey } from 'jsrsasign'

export function getPossiblePrivateKeyAlgs(privateKey: string): string[] {
  // Based on JOSE conformance for JWS
  // https://www.w3.org/community/reports/credentials/CG-FINAL-lds-jws2020-20220721/
  const key = KEYUTIL.getKey(privateKey)
  if (key instanceof KJUR.crypto.ECDSA) {
    return ['ES256', 'ES384', 'ES256K']
  } else if (key instanceof RSAKey) {
    return ['PS256']
  }
  // fallback to any supported alg
  return ['EdDSA', 'EdDSA256K']
}

export async function getPrivateKeyAlg(privateKey: string) {
  for (const alg of getPossiblePrivateKeyAlgs(privateKey)) {
    try {
      await importPKCS8(privateKey, alg)
      return alg
    } catch {
      // not the right algorithm
    }
  }
  throw new Error('No valid private key algorithm found')
}

/**
 * Derive the public key from a private key
 * @param privateKey the private key to derive the public key from
 * @returns the public key as a string
 */
export async function derivePublicKeyFromString(privateKey: string): Promise<string> {
  const key = KEYUTIL.getKey(privateKey)
  if (key instanceof KJUR.crypto.ECDSA) {
    key.generatePublicKeyHex()
    const publicKey = KEYUTIL.getKey(key, null, 'pkcs8pub')
    return KEYUTIL.getPEM(publicKey)
  }
  const alg = 'PS256'
  const cryptoKey = await importPKCS8<CryptoKey>(privateKey, alg, { extractable: true })
  const publicKey = await derivePublicKey(cryptoKey)
  return await publicKeyToPem(publicKey)
}

/**
 * Retrieves the public key from a base64 DER encoded certificate
 * @param certificate the certificate to retrieve the public key from
 * @returns the public key as a PEM string
 */
export function getPublicKeyFromDERCertificate(base64DerEncodedCertificate: string): string | undefined {
  try {
    const pemCertificate = getPemCertificateFromDERCertificate(base64DerEncodedCertificate)
    // Read the certificate into a jsrsasign X509 object
    const x509 = new X509()
    x509.readCertPEM(pemCertificate)

    const publicKey = x509.getPublicKey()
    const pemPublicKey = KEYUTIL.getPEM(publicKey)

    return pemPublicKey
  } catch (error) {
    console.error('Error while getting public key from DER certificate:', error)
    return undefined
  }
}

/**
 * Converts a base64 DER encoded certificate to a PEM encoded certificate
 * @param certificate the base64 string to convert to PEM
 * @returns the certificate as a PEM string
 */
export function getPemCertificateFromDERCertificate(base64DerEncodedCertificate: string): string {
  try {
    return (base64DerEncodedCertificate = `-----BEGIN CERTIFICATE-----\n${base64DerEncodedCertificate}\n-----END CERTIFICATE-----`)
  } catch (error) {
    throw new Error('Error while getting converting from DER certificate:' + error)
  }
}

export async function generatePrivateKey() {
  const { privateKey } = await crypto.subtle.generateKey(
    {
      name: 'RSA-PSS',
      modulusLength: 2048,
      publicExponent: new Uint8Array([1, 0, 1]),
      hash: 'SHA-256'
    },
    true,
    ['sign', 'verify']
  )
  return privateKey
}

export async function derivePublicKey(privateKey: CryptoKey): Promise<CryptoKey> {
  const jwk = await crypto.subtle.exportKey('jwk', privateKey)

  // remove private data from JWK
  delete jwk.d
  delete jwk.dp
  delete jwk.dq
  delete jwk.q
  delete jwk.qi
  jwk.key_ops = ['verify']

  // import public key
  return await crypto.subtle.importKey(
    'jwk',
    jwk,
    {
      name: 'RSA-PSS',
      hash: { name: 'SHA-256' }
    },
    true,
    ['verify']
  )
}

export function arrayBufferToBase64(buffer: ArrayBuffer): string {
  let binary = ''
  const bytes = new Uint8Array(buffer)
  for (let i = 0; i < bytes.byteLength; i++) {
    binary += String.fromCharCode(bytes[i])
  }
  return (btoa as any)(binary)
}

export function stripPemHeaderAndFooter(pem: string) {
  let lines = pem.split('\n')
  lines = lines.slice(1, -1)

  return lines.join('\n')
}

export async function publicKeyToPem(publicKey: CryptoKey) {
  const spkiBuffer = await crypto.subtle.exportKey('spki', publicKey)
  const str = arrayBufferToBase64(spkiBuffer)
  return publicKeyB64ToPem(str)
}

export function publicKeyB64ToPem(b64: string) {
  let finalString = '-----BEGIN PUBLIC KEY-----\n'
  while (b64.length > 0) {
    finalString += b64.substring(0, 64) + '\n'
    b64 = b64.substring(64)
  }
  finalString = finalString + '-----END PUBLIC KEY-----'
  return finalString
}

export async function privateKeyToPem(privateKey: CryptoKey) {
  const pkcs8Buffer = await crypto.subtle.exportKey('pkcs8', privateKey)
  const pemBody = arrayBufferToBase64(pkcs8Buffer).match(/.{1,64}/g)
  if (pemBody) return `-----BEGIN PRIVATE KEY-----\n${pemBody.join('\n')}\n-----END PRIVATE KEY-----`
}

export async function bufferToCryptoKey(buffer: BufferSource): Promise<CryptoKey> {
  return await crypto.subtle.importKey(
    'pkcs8',
    buffer,
    {
      name: 'RSA-PSS',
      hash: 'SHA-256'
    },
    true,
    ['sign']
  )
}
