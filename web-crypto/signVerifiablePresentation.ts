import { VerifiablePresentation } from '@/sharedTypes'
import { signVerifiableCredential } from './signVerifiableCredential'

export const signVerifiablePresentation = async (
  pemPrivateKey: string,
  verifiablePresentation: VerifiablePresentation,
  verificationMethod: string
): Promise<VerifiablePresentation> => {
  const signedVerifiableCredentials = await Promise.all(
    verifiablePresentation.verifiableCredential.map(
      async verifiableCredential => await signVerifiableCredential(pemPrivateKey, verifiableCredential, verificationMethod)
    )
  )

  return {
    ...verifiablePresentation,
    verifiableCredential: signedVerifiableCredentials
  }
}
