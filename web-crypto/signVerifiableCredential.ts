import { VDocument } from '@/models/document'
import { importPKCS8 } from 'jose'
import { JsonWebSignature2020Signer } from '@gaia-x/json-web-signature-2020'
import { getPrivateKeyAlg } from './cryptokey'
import { staticDocumentLoader } from '@/utils/static-document-loader'

export const signVerifiableCredential = async (pemPrivateKey: string, verifiableCredential: any, verificationMethod: string): Promise<VDocument> => {
  const alg = await getPrivateKeyAlg(pemPrivateKey)
  const signer = new JsonWebSignature2020Signer({
    privateKey: await importPKCS8(pemPrivateKey, alg),
    privateKeyAlg: alg,
    verificationMethod,
    documentLoader: staticDocumentLoader
  })
  const signedVc = await signer.sign(verifiableCredential)
  return signedVc as VDocument
}
