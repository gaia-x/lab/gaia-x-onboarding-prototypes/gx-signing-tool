import { ShapeName, VDocument } from './document'
import { CredentialFormats, GaiaXDeploymentPath } from '@/sharedTypes'

export interface StoredWalletEntry {
  key: string
  name: string
  type: ShapeName
  creation: string
  document: VDocument
  isVerified: GaiaXDeploymentPath | false
  privateKey?: string
  rawDocument?: string
  format?: CredentialFormats
}

export enum AvailableWallets {
  LOCAL_WALLET = 'LocalWallet',
  DECENTRALIZED_WEB_NODE = 'DecentralizedWebNode',
  NONE = 'None'
}
