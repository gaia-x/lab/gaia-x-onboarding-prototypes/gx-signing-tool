export type DecentralizedWebNodeRequest = {
  messages: DWNMessage[]
}

export type DWNMessage = {
  descriptor: {
    method: DWNMethods
    dateCreated?: number
    recordId?: string
    dataFormat?: string
  }
  data?: string
}

export enum DWNMethods {
  QUERY = 'CollectionsQuery',
  WRITE = 'CollectionsWrite'
}

export type DecentralizedWebNodeResponse = {
  status: DWNStatus
  replies: DWNReply[]
}

export type DWNReply = {
  status: DWNStatus
  entries: DWNEntry[]
}

export type DWNEntry = {
  id: string
  data: string
  dataFormat: string
  createdAt: string
}

export type DWNStatus = {
  code: number
  detail: string
}
