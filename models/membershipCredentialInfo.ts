export interface MembershipCredentialInfo {
  membershipNumber: string
  name: string
  email: string
}

export enum MembershipCredentialType {
  VCARD = 'VCARD',
  SCHEMA_ORG = 'SCHEMA_ORG'
}

export enum MembershipCredentialFormat {
  CARD = 'CARD',
  JSON_LD = 'JSON_LD',
  OIDC = 'OIDC'
}
