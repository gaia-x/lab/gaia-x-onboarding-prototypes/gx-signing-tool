export type Rule = {
  target: string
  action: string
  assigner: string
  assignee: ovcAssignee
  '@type'?: string
  constraint?: odrlConstraint[]
}

export type ContractAgreement = {
  '@type': string
  uid: string
  permission?: Rule[]
  prohibition?: Rule[]
  duty?: Rule[]
}

export type odrlConstraint = {
  operator: string
  leftOperand: string
  rightOperand?: string
  rightOperandReference: string
}

export type ovcConstraint = {
  operator: string
  'ovc:leftOperand': string
  rightOperand?: string
  'ovc:credentialSubjectType': string
}

export type ovcAssignee = {
  'ovc:constraint'?: ovcConstraint[]
}
