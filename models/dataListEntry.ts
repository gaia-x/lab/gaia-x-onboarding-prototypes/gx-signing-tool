export type DataListEntry = {
  key: string
  value: string
}
