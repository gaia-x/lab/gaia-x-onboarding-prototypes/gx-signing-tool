# Gaia-X Wizard User Guide

## Introduction

This document is intended to help users get started with the Gaia-X Wizard and understand its purpose and functionality.

**Gaia-X Wizard** is a web application that allows you to create and sign **Verifiable Presentations** as well as obtaining the **Verifiable Credentials** that attest Gaia-X compliance. Its purpose is to make users familiar with how VPs and VCs are used in Gaia-X, as well as provide a simple way to get **Gaia-X Compliant VCs**.


## Table of contents 📖

[TOC]

## Select Your Environment

The wizard provides a way to select the environment you want to use for calling Gaia-X services.

In the top right corner of the app, you will find a select input where you can choose between:
- **Clearing Houses** and its **version** (v1)

  or

- **Gaia-X environment** and its **path** (v1, main, development)

**Clearing Houses** offers a production environment and only accept certificates from [Trust Anchors](https://docs.gaia-x.eu/policy-rules-committee/trust-framework/22.10/trust_anchors/) (e.g., eIDAS, EV SSL).

**Gaia-X development environment** allows you to use certificates from other providers (e.g., Let's Encrypt).

## Playground

The **Playground** is a place for Gaia-X users where you can:

### **Create and sign** any type of **Verifiable Credential**

The **Playground** provides you some JSON-LD
examples to help you create **Verifiable Credentials**. You can also start from scratch.

1. In the **"Issue Verifiable Credential"** section, select an example and fill the JSON-LD or paste your filled JSON-LD
2. If you use your own **DID solution**, fill the verification method field with your DID (only did:web is supported for
   now)
3. Fill the private key field with your **private key** (PKCS#1 RSA format) or use the one provided by the wizard by
   default
4. Click on **"Sign"** button to sign your **Verifiable Credential**
5. If everything is ok, you will see your signed **Verifiable Credential** in the **"Holder"**
   section

### Import your own signed Verifiable Credential

In the **"Holder"** section or in the **Local Wallet**, click on the **"Import signed document"
** button, then in the modal opened you have 2 options:

- Fill the code editor input with your **signed Verifiable Credential** in JSON-LD format, name your document or
  generate one by clicking on the refresh button

or

- **Select or drag & drop file(s)** from your computer in the code editor input

then

- Click on the **"Add"** button


### Call the Gaia-X Compliance API to verify Legal Participant and Service Offering

To get **Gaia-X Compliance** on a **Legal Participant** or a **Service Offering**, you need these **Verifiable Credentials**:

For a **Legal Participant**:

- **Legal Participant**
- **Legal Registration Number**
- **Terms & Conditions**

For a **Service Offering**:

- **Legal Participant**
- **Legal Registration Number**
- **Terms & Conditions**
- **Service Offering**

You can create it all through the **Playground**, the **Stepper**, the **Onboarding** page or by importing signed documents.

Once you have all the **Verifiable Credentials** you need in the **"Holder"** section, you can call the **Gaia-X
Compliance API** to get your
**Verified Verifiable Credentials** by clicking in the **"Call compliance** button in the **"Holder"** section.

Once clicked a modal will open, select the 3 or more **Verifiable Credentials** needed and click on the
**"Submit to compliance"** button.

If everything is ok, the **Gaia-X Compliance** will return a **Verified Verifiable Credential** that you can save in
your **Local Wallet**.


## Onboarding

The Onboarding page will help you to create the following **Verifiable Credentials**:

- **Legal Participant**
- **Legal Registration Number**
- **Terms & Conditions**

To create a **Verifiable Credential** through the **Onboarding** page, click on the **"Onboarding"** tab in the sidebar

Then:

1. Click on the switch button to inform if you will use your own [DID solution](#understand-our-did-solution) and click on the **"Start"** button
2. Fill the form with the Legal Participant information needed, click on the **"Next"** button
3. Agree the **Terms & Conditions** by clicking on the checkbox and click on the **"Next"** button 
4. If you are using your own DID solution, fill in your private key and click on the **"Next"** button
5. If you are using your own DID solution, you will be asked to inform the **"Issuer"**, the **"Verification Method"** from your DID and a **Verifiable Credential ID** + **Credential subject ID** for each of the 3 **Verifiable Credentials** who will be created, then click on the **"Next"** button
6. At this point, you will be able to watch the **Verifiable Credentials** created, click on the **"Sign"** button to sign them, then you can add them in the **"Holder"** section and in the **Local Wallet**


## Stepper

The **Stepper** is a place for Gaia-X users where you can create **Verifiable Credentials** through a step by step form

To create a **Verifiable Credential** through the **Stepper**, click on the **"Stepper"** tab in the sidebar

Then:

1. Choose the type of **Verifiable Credential** you want to create
2. Inform if you are using your own DID solution or the Wizard's one
3. Fill the form with the information needed
4. You will get a **Verifiable Credential** , click on the **"Sign"** button to sign it
5. You will get your **signed Verifiable Credential**, you can add it in the **"Holder"** section and in the **Local
   Wallet**

## Get a Legal Registration Number

To get a **Legal Registration Number**:

1. Click on the **"Get Legal Registration Number"** tab in the sidebar.
2. If you are using you own **DID solution** click on switch button and fill the **"Verifiable Credential ID"** and **"Credential subject ID"** fields
3. Select the type of number you will use
4. Fill the **"Legal registration number"** field with your number
5. Click on the **"Submit"** button
6. If everything is ok, you will get your **Legal Registration Number** and be adle to add it in the **"Holder"** section and in the **Local Wallet**


## Create a Legal Participant or a Service Offering

To create a **Legal Participant** or a **Service Offering**, you have 2 options:

Use the [Playground](#playground):

1. Choose the type of shape in **"Example"** select field in the **"Issue Verifiable Credential"** section
2. Fill the JSON-LD or paste your filled JSON-LD
3. If you use your own **DID solution**, fill the verification method field with your DID (only did:web is supported for
   now)
4. Select if you are using providing your own DID solution or use the Wizard's one
   (If you are providing your own DID solution, you need to fill the **Verification Method** field with the path to your
   DID)
5. Fill the private key field with your **private key** (PKCS#1 RSA format) or use the one provided by the wizard by
   default
6. Click on **"Sign"** button to sign your **Verifiable Credential**

Use the [Stepper](#stepper):

1. Click on the **"Stepper"** tab in the sidebar
2. Choose the **type of shape** you need
3. Fill the form with the information needed
4. You will get a **Verifiable Credential** , click on the **"Sign"** button to sign it
5. You will get your **signed Verifiable Credential**, you can add it in the **"Holder"** section and in the **Local
   Wallet**

## Local Wallet

Gaia-X Wizard provide you a **Local wallet** solution, this wallet is a place where you can save your **signed
Verifiable Credentials**

This wallet is using the indexedDB of your browser to save your **Verifiable Credentials**.

Local Wallet **features**:

- **Import** signed **Verifiable Credentials** from the **"Holder"** in the **Playground** or from the **"Import signed
  document"** button in the **Local Wallet**
- **Export** **signed Verifiable Credentials** from the **Local Wallet** to the **"Holder"** in the **Playground**
- **Download** **signed Verifiable Credentials** JSON from the **Local Wallet** in a zip folder

## Understand our DID solution

Gaia-X Wizard provide you a **DID solution** to help you create and host your **Verifiable Credentials**.

What is a DID (Decentralized Identifier) ?
https://www.w3.org/TR/did-core/

Gaia-X Wizard will handle for you the hosting of your DID document, it's linked to the **private key** you will use.

Your DID document will at least contains the **Verification Method** needed to verify any document you will sign through the wizard

Your DID document will contain references about the **Verifiable Credentials** you will create through the wizard, it's using the **Verifiable Credentials IDs** and provide a service endpoint URL to fetch the linked domain resources.

## Policy reasoning (Policy Decision Point)

Policy Reasoning is provided in the wizard as a Playground and for testing purposes only.
For ease of use the whole procedure is split into 4 steps:
1. First of all, the provider must define a policy for each service, this could be done by putting the policy directly into the “gx:policy” attribute of a Service Offering Verifiable Credential.
   The policy is a standard ODRL Policy in a JSON-LD format, however, each Rule’s (Permission, Prohibition, Duty) might have an assignee for which constraints are in a specific format:
   - The “ovc:leftOperand” contains a JSONPath value indicating for which credential attribute the constraint is evaluated.
   - The “operator” contains a defined ODRL Operator
   - The “rightOperand” contains the actual values that the claims will be compared against
   - The “ovc:credentialSubjectType” refers to the credential type, which could be defined in the context.
   At the end of this step, the reasoning engine starts by transforming this policy into RDF triples and inserting that into a Graph Database.
2. The next step is using the consumer’s credentials (usually from a wallet) to evaluate the JSON Path supplied in the first step in the constraints part.
3. After that, the consumer has to convey their usage intentions, expressed as an ODRL request policy.
   Before going to the last step, using inputs from steps 2 and 3, a SPARQL query is built to query the database mentioned in step 1, it is also worth noting that the Database already contains the ODRL ontology and possibly others which enable the ability to perform RDFS or OWL reasoning.
4. Finally, if the query from step 3 returns any results, then an agreement can be reached between the consumer and the provider, otherwise either the consumer usage intentions are not allowed by the provider, or the content of the consumer’s credentials is not what the provider requires.


More details can be found in this [Article](https://gaia-x.eu/news/latest-news/gaia-x-and-the-policy-reasoning-engine/)
