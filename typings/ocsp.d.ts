declare module '@techteamer/ocsp' {
  export type CheckOptions = {
    cert: Buffer
    issuer: Buffer
  }

  export type CertId = {
    hashAlgorithm: {
      algorithm: any[]
    }
    issuerNameHash: Buffer
    issuerKeyHash: Buffer
    serialNumber: {
      negative: number
      words: any[]
      length: number
      red: null
    }
  }

  export type CertStatus = {
    type: 'good' | 'revoked' | 'unknown'
    value: any
  }

  export type SingleExtension = {
    extnID: any[]
    critical: boolean
    extnValue: Buffer
  }

  export type CertInfo = {
    certId: CertId
    certStatus: CertStatus
    thisUpdate: number
    singleExtensions: SingleExtension[]
  }

  export function check(options: CheckOptions, callback: (error: any, result: CertInfo) => void): void
}
