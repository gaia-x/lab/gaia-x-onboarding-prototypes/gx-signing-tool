import { describe, expect, it } from 'vitest'
import { signVerifiableCredential } from '../../web-crypto/signVerifiableCredential'
import { PemPrivateKeyExample } from '../../data/keys/pemPrivateKeyExample'
import VerifiableCredentialExample from '../../data/doc/LegalParticipantExample.json'
import { Buffer } from 'buffer'

window.Buffer = Buffer

describe('signVerifiableCredential', () => {
  const pemPrivateKey: string = PemPrivateKeyExample
  const verifiableCredential: any = VerifiableCredentialExample
  const verificationMethod = 'did:example:123#key-1'

  it('should sign a verifiable credential with a proof', async () => {
    const result = await signVerifiableCredential(pemPrivateKey, verifiableCredential, verificationMethod)

    expect(result).toHaveProperty('proof')
    expect(result.proof.created).not.toBeNull()
    expect(result.proof.jws).not.toBeNull()
    expect(result.proof.proofPurpose).toEqual('assertionMethod')
    expect(result.proof.type).toEqual('JsonWebSignature2020')
    expect(result.proof.verificationMethod).toEqual(verificationMethod)
  })
})
