import { defineConfig } from 'vite'
import path from 'path'

export default defineConfig({
  test: {
    include: ['**/front/*.{test,spec}.{js,mjs,cjs,ts,mts,cts,jsx,tsx}'],
    browser: {
      provider: 'playwright',
      enabled: true,
      headless: true,
      name: 'firefox'
    },
    coverage: {
      enabled: true,
      reportsDirectory: 'coverage/front',
      provider: 'istanbul',
      reporter: ['text', 'lcovonly', 'cobertura']
    }
  },
  resolve: {
    alias: {
      '@/': path.resolve(__dirname, '../../') + '/'
    }
  }
})
