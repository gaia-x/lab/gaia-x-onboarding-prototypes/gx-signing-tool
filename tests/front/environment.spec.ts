import { describe, it, expect } from 'vitest'
import { environment, getRootUrl } from '@/env'

describe('environment', () => {
  it('app path should be aware of the environment', async () => {
    const tests: { [url: string]: string } = {
      'http://localhost:3000': '',
      'http://localhost:3000/stepper': '',
      'http://localhost:3000/any-path': '',
      'http://localhost:3000/development': 'development',
      'http://localhost:3000/development/stepper': 'development',
      'https://wizard.lab.gaia-x.eu/': '',
      'https://wizard.lab.gaia-x.eu/stepper': '',
      'https://wizard.lab.gaia-x.eu/any/path': '',
      'https://wizard.lab.gaia-x.eu/development': 'development',
      'https://wizard.lab.gaia-x.eu/development/stepper': 'development'
    }
    for (const [url, expectedAppPath] of Object.entries(tests)) {
      expect(environment(url).appPath).toBe(expectedAppPath)
    }
  })

  it('root url should be aware of the environment', async () => {
    const tests: { [url: string]: string } = {
      '': '',
      '/': '',
      '/stepper': '',
      '/any/path': '',
      '/development': '/development',
      '/development/stepper': '/development',
      'http://localhost:3000': '',
      'http://localhost:3000/stepper': '',
      'http://localhost:3000/any-path': '',
      'http://localhost:3000/development': 'http://localhost:3000/development',
      'http://localhost:3000/development/stepper': 'http://localhost:3000/development',
      'https://wizard.lab.gaia-x.eu/': '',
      'https://wizard.lab.gaia-x.eu/stepper': '',
      'https://wizard.lab.gaia-x.eu/any/path': '',
      'https://wizard.lab.gaia-x.eu/development': 'https://wizard.lab.gaia-x.eu/development',
      'https://wizard.lab.gaia-x.eu/development/stepper': 'https://wizard.lab.gaia-x.eu/development'
    }
    for (const [url, expectedRootUrl] of Object.entries(tests)) {
      expect(getRootUrl(url)).toBe(expectedRootUrl)
    }
  })
})

export {}
