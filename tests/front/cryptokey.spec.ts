import {
  derivePublicKeyFromString,
  generatePrivateKey,
  privateKeyToPem,
  publicKeyToPem,
  getPublicKeyFromDERCertificate
} from '../../web-crypto/cryptokey'
import { describe, it, expect } from 'vitest'

describe('key management', () => {
  it('should generate a private key', async () => {
    const privateKey = await generatePrivateKey()
    const privateKeyPem = await privateKeyToPem(privateKey)
    expect(privateKeyPem).toContain('-----BEGIN PRIVATE KEY-----')
  })

  it('should derive a valid public key from a private one', async () => {
    const { privateKey, publicKey } = await crypto.subtle.generateKey(
      {
        name: 'RSA-OAEP',
        modulusLength: 4096,
        publicExponent: new Uint8Array([1, 0, 1]),
        hash: 'SHA-256'
      },
      true,
      ['encrypt', 'decrypt']
    )
    const derivedPublicKey = await derivePublicKeyFromString((await privateKeyToPem(privateKey)) ?? '')
    const originPem = await publicKeyToPem(publicKey)
    expect(derivedPublicKey).toEqual(originPem)
  })

  it('should return a public key from a der encoded certificate if the certificate is valid', () => {
    const pemPublicKey = `-----BEGIN PUBLIC KEY-----
      MHYwEAYHKoZIzj0CAQYFK4EEACIDYgAEo8miYxTaDIVGhcip7whe+J7VS5UCCuut
      Y0h8DSHEOcb8jLi24ii+0tJcUcpBzi/kegcYFgsdx6HZD+dIV9FaQAS8t99QbBSQ
      YZilL6JEMfbiQGsZg1ze5+MmAeHmT90B
      -----END PUBLIC KEY-----`.replace(/\s/g, '')

    const derBase64Cert = `MIID6DCCA0mgAwIBAgIQSTw53RelfEdf/p7Bt06JPjAKBggqhkjOPQQDBDBYMQsw
    CQYDVQQGEwJFRTEbMBkGA1UECgwSU0sgSUQgU29sdXRpb25zIEFTMRcwFQYDVQRh
    DA5OVFJFRS0xMDc0NzAxMzETMBEGA1UEAwwKRVNURUlEMjAxODAeFw0yMTAxMTMw
    NzE4MjVaFw0yNjAxMTIyMTU5NTlaMIGPMQswCQYDVQQGEwJFRTEyMDAGA1UEAwwp
    R1JPTkxJRVIsUElFUlJFIEFOVE9JTkUgTE9VSVMsMzg0MDEwMjAwNjExETAPBgNV
    BAQMCEdST05MSUVSMR0wGwYDVQQqDBRQSUVSUkUgQU5UT0lORSBMT1VJUzEaMBgG
    A1UEBRMRUE5PRUUtMzg0MDEwMjAwNjEwdjAQBgcqhkjOPQIBBgUrgQQAIgNiAASj
    yaJjFNoMhUaFyKnvCF74ntVLlQIK661jSHwNIcQ5xvyMuLbiKL7S0lxRykHOL+R6
    BxgWCx3HodkP50hX0VpABLy331BsFJBhmKUvokQx9uJAaxmDXN7n4yYB4eZP3QGj
    ggGeMIIBmjAJBgNVHRMEAjAAMA4GA1UdDwEB/wQEAwIGQDBIBgNVHSAEQTA/MDIG
    CysGAQQBg5EhAQECMCMwIQYIKwYBBQUHAgEWFWh0dHBzOi8vd3d3LnNrLmVlL0NQ
    UzAJBgcEAIvsQAECMB0GA1UdDgQWBBSqtaHuP6qUUBUblleHFCpE73QurjCBigYI
    KwYBBQUHAQMEfjB8MAgGBgQAjkYBATAIBgYEAI5GAQQwEwYGBACORgEGMAkGBwQA
    jkYBBgEwUQYGBACORgEFMEcwRRY/aHR0cHM6Ly9zay5lZS9lbi9yZXBvc2l0b3J5
    L2NvbmRpdGlvbnMtZm9yLXVzZS1vZi1jZXJ0aWZpY2F0ZXMvEwJFTjAfBgNVHSME
    GDAWgBTZrHDbX36+lPig5L5HotA0rZoqEjBmBggrBgEFBQcBAQRaMFgwJwYIKwYB
    BQUHMAGGG2h0dHA6Ly9haWEuc2suZWUvZXN0ZWlkMjAxODAtBggrBgEFBQcwAoYh
    aHR0cDovL2Muc2suZWUvZXN0ZWlkMjAxOC5kZXIuY3J0MAoGCCqGSM49BAMEA4GM
    ADCBiAJCAMVuy54WILl5ZES9CwaTfQ1MK0cw7ziEV0hfyb3u3iS/KpmZj0w8Amsz
    tfVqha2ahbvpLEjAvbphu677y7riWGdEAkIBeJyuRVlvYXMMJb4e+WWePez+8FEh
    ilfHto0FEJOLg8QwuOHsVzJBHpUnqyhI3iHaad2NkNB6kUt96ognbKo3YhM=`

    const result = getPublicKeyFromDERCertificate(derBase64Cert)?.replace(/\s/g, '')

    expect(result).toBeDefined()
    expect(result).toEqual(pemPublicKey)
  })

  it('should return undefined if an error occurs', () => {
    const result = getPublicKeyFromDERCertificate('Invalid DER certificate')

    expect(result).toBeUndefined()
  })
})

export {}
