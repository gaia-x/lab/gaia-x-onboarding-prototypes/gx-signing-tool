import { test, expect } from '@playwright/test'
import { acceptTnc, fillParticipantForm, getDocumentValue, setPrivateKey, verifyVCPresenceInHolder } from './util'

const validParticipantValues = {
  name: 'Gaia-X European Association for Data and Cloud AISBL',
  registrationNumber: 'FR79537407926',
  headquarter: 'FR-HDF',
  legalAddress: 'FR-HDF',
  lrnType: 'vatID'
}

const validIssuer = 'did:example:123456789'
const validVerifMethod = `${validIssuer}:key-1`
const validParticipantVCId = `${validIssuer}:participant:vc`
const validParticipantCSId = `${validIssuer}:participant:cs`
const validLrnVCId = `${validIssuer}:lrn:vc`
const validLrnCSId = `${validIssuer}:lrn:cs`
const validTncVCId = `${validIssuer}:tnc:vc`
const validTncCSId = `${validIssuer}:tnc:cs`

test.beforeEach(async ({ page }) => {
  await page.goto('/onboarding')
})

test.describe('Onboarding', () => {
  test('Disclaimer is visible', async ({ page }) => {
    await expect(page.getByText('disclaimer')).toBeVisible()
  })

  test('Onboard with managed issuer', async ({ page }) => {
    test.setTimeout(60000)
    const next = () => page.getByRole('button', { name: 'Next' }).click()

    // Start
    const button = page.getByRole('button', { name: 'Start' })
    await expect(button).toBeVisible()
    await button.click()

    // Participant
    await fillParticipantForm(page, validParticipantValues)
    await next()

    // Accept Terms and Conditions
    await acceptTnc(page)
    await next()

    // Sign
    await page.getByRole('button', { name: 'Sign' }).click()

    // Add to holder
    await page.getByRole('button', { name: 'Add in holder', exact: true }).click()

    await verifyVCPresenceInHolder(page)
  })

  test('Onboard with custom issuer', async ({ page }) => {
    test.setTimeout(60000)
    const next = () => page.getByRole('button', { name: 'Next' }).click()

    // Start
    const customDid = page.getByTestId('use-own-did')
    await expect(customDid).toBeVisible()
    await customDid.click()
    await page.getByRole('button', { name: 'Start' }).click()

    // Participant + LRN ids
    await fillParticipantForm(page, validParticipantValues)
    await page.getByLabel('verifiable credential id').fill(validLrnVCId)
    await page.getByLabel('credential subject id').fill(validLrnCSId)
    await next()

    // Accept Terms and Conditions
    await acceptTnc(page)
    await next()

    // Set private key
    await setPrivateKey(page)
    await next()

    // Fill issuer, verification method and identifiers
    await page.getByLabel('issuer').fill(validIssuer)
    await page.getByLabel('verification method').fill(validVerifMethod)
    await page.getByLabel('verifiable credential id').first().fill(validParticipantVCId)
    await page.getByLabel('credential subject id').first().fill(validParticipantCSId)
    await page.getByLabel('terms and conditions verifiable credential id').fill(validTncVCId)
    await page.getByLabel('terms and conditions credential subject id').fill(validTncCSId)
    await next()

    // Sign
    await page.getByRole('button', { name: 'Sign' }).click()

    // Add to holder
    await page.getByRole('button', { name: 'Add in holder', exact: true }).click()

    const participantValue = await getDocumentValue(page, 'gx:LegalParticipant', 2)
    expect(participantValue.id).toBe(validParticipantVCId)
    expect(participantValue.issuer).toBe(validIssuer)
    expect(participantValue.credentialSubject.id).toBe(validParticipantCSId)
    expect(participantValue.proof.verificationMethod).toBe(validVerifMethod)

    const tncValue = await getDocumentValue(page, 'gx:GaiaXTermsAndConditions', 2)
    expect(tncValue.id).toBe(validTncVCId)
    expect(tncValue.issuer).toBe(validIssuer)
    expect(tncValue.credentialSubject.id).toBe(validTncCSId)
    expect(tncValue.proof.verificationMethod).toBe(validVerifMethod)

    const lrnValue = await getDocumentValue(page, 'gx:legalRegistrationNumber', 2)
    expect(lrnValue.id).toBe(validLrnVCId)
    expect(lrnValue.issuer).toContain('did:web:registration.lab.gaia-x.eu:v1-staging')
    // expect(lrnValue.credentialSubject.id).toBe(validLrnCSId) <- for now LRN uses the VC id for both
    expect(lrnValue.proof.verificationMethod).toContain('did:web:registration.lab.gaia-x.eu:v1-staging')
  })
})
