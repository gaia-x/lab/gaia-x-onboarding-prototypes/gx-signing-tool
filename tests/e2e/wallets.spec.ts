import { expect, Page, test } from '@playwright/test'
import { connectLocalWallet, monacoEditorSelector, setMonacoEditorValue } from './util'
import signedParticipant from './fixtures/signedParticipant.json'
import decentralizedWebNodeResponse from './fixtures/decentralizedWebNodeResponse.json'

test.beforeEach(async ({ page }) => {
  await page.route('**/api/v1/identity/identity-hub**', route =>
    route.fulfill({
      status: 200,
      json: decentralizedWebNodeResponse
    })
  )
  await page.goto('/')
})

test.describe('Local Wallet', () => {
  test('Local Wallet is available to connect', async ({ page }) => {
    const walletButton = await page.getByText('Connect Wallet')
    await expect(page.getByText('Local Wallet')).not.toBeVisible()
    await walletButton.click()
    await expect(page.getByText('Available Wallets')).toBeVisible()
  })

  test('Local Wallet is connected', async ({ page }) => {
    await connectLocalWallet(page)
    const walletButton = page.getByText('Local Wallet')
    await expect(page.getByText('Connect Wallet')).not.toBeVisible()
    await expect(walletButton).toBeVisible()
    await walletButton.click()
    const wallet = page.getByTestId('local-wallet')
    await expect(wallet).toBeVisible()
    await wallet.getByRole('button', { name: 'Close' }).click()
    await expect(wallet).not.toBeVisible()
  })

  test('Import signed document into wallet', async ({ page }) => {
    await connectLocalWallet(page)
    const localWallet = page.getByTestId('local-wallet')
    await importDocumentIntoWallet(page)
    await expect(localWallet.getByText('VerifiableCredential')).toBeVisible()
  })

  test('Handle wallet entry operations', async ({ page }) => {
    await connectLocalWallet(page)
    const localWallet = page.getByTestId('local-wallet')
    await importDocumentIntoWallet(page)

    // View credentials and close dialog
    await expect(localWallet.getByRole('button', { name: 'View' })).toBeVisible()
    await localWallet.getByRole('button', { name: 'View' }).click()
    const dialog = await page.getByRole('dialog')
    await expect(dialog).toBeVisible()
    await dialog.getByRole('button', { name: 'Close' }).first().click()

    //Export credentials to holder
    await expect(localWallet.getByText('Export')).toBeVisible()
    await localWallet.getByText('Export').click()
    const holder = page.locator('.holder')
    await expect(holder.getByText('gx:LegalParticipant')).toBeVisible()

    //Delete wallet entry
    await expect(localWallet.getByRole('button', { name: 'Delete' })).toBeVisible()
    await localWallet.getByRole('button', { name: 'Delete' }).click()
    await localWallet.getByRole('button', { name: 'Yes' }).click()
    await expect(localWallet.getByText('VerifiableCredential')).not.toBeVisible()
  })

  test('Logout from Local Wallet', async ({ page }) => {
    await connectLocalWallet(page)
    const wallet = page.getByTestId('local-wallet')
    const logoutButton = wallet.locator('[aria-label="Logout of wallet"]')
    await expect(logoutButton).toBeVisible()
    await logoutButton.click()
    await expect(wallet).not.toBeVisible()
    await expect(page.getByText('Connect Wallet')).toBeVisible()
  })

  const importDocumentIntoWallet = async (page: Page) => {
    const localWallet = page.getByTestId('local-wallet')
    await localWallet.getByTestId('import-document-button').first().click()
    const importDocumentModal = page.getByTestId('import-document-modal')
    await expect(monacoEditorSelector(importDocumentModal)).toBeVisible()
    await setMonacoEditorValue(page, 0, JSON.stringify(signedParticipant, null, 2))
    const addButton = await importDocumentModal.getByRole('button', { name: 'Add' }).first()
    await expect(addButton).toBeEnabled()
    await addButton.click()
  }
})

test.describe('Decentralized Web Node Wallet', () => {
  test('Decentralized Web Node is available to connect', async ({ page }) => {
    const connectButton = await page.getByText('Connect Wallet')
    await expect(page.getByText('Decentralized Web Node')).not.toBeVisible()
    await connectButton.click()
    const dialog = await page.getByRole('dialog')
    await expect(dialog).toBeVisible()
    await expect(dialog.getByText('Available Wallets')).toBeVisible()
    await expect(dialog.getByText('Decentralized Web Node')).toBeEnabled()
  })

  test('Decentralized Web Node Wallet is connected', async ({ page }) => {
    await connectDwnWallet(page)
    const walletButton = page.getByText('Decentralized Web Node')
    await expect(page.getByText('Connect Wallet')).not.toBeVisible()
    await expect(walletButton).toBeVisible()
    await walletButton.click()
    const wallet = page.getByTestId('dwn-wallet')
    await expect(wallet).toBeVisible()

    // Disclaimer visible
    await expect(wallet.getByText('Disclaimer')).toBeVisible()
    await wallet.getByRole('alert').getByLabel('Close').first().click()
    await expect(wallet.getByText('Disclaimer')).not.toBeVisible()

    // Close wallet
    await wallet.getByRole('button', { name: 'Close' }).first().click()
    await expect(wallet).not.toBeVisible()
  })

  test('Handle wallet entry operations', async ({ page }) => {
    await connectDwnWallet(page)
    await refreshDwnWallet(page)
    const wallet = page.getByTestId('dwn-wallet')

    // View credentials and close dialog
    await expect(wallet.getByRole('button', { name: 'View' })).toBeVisible()
    await wallet.getByRole('button', { name: 'View' }).click()
    const dialog = await page.getByRole('dialog')
    await expect(dialog).toBeVisible()
    await dialog.getByRole('button', { name: 'Close' }).first().click()

    //Export credentials to holder
    await expect(wallet.getByText('Export')).toBeVisible()
    await wallet.getByText('Export').click()
    const holder = page.locator('.holder')
    await expect(holder.getByText('gx:legalRegistrationNumber')).toBeVisible()
  })

  test('Logout from Decentralized Web Node Wallet', async ({ page }) => {
    await connectDwnWallet(page)
    const wallet = page.getByTestId('dwn-wallet')
    const logoutButton = wallet.locator('[aria-label="Logout of wallet"]')
    await expect(logoutButton).toBeVisible()
    await logoutButton.click()
    await expect(wallet).not.toBeVisible()
    await expect(page.getByText('Connect Wallet')).toBeVisible()
  })

  const refreshDwnWallet = async (page: Page) => {
    const wallet = page.getByTestId('dwn-wallet')
    const refreshButton = wallet.locator('[aria-label="Refresh wallet entries"]')
    await expect(refreshButton).toBeVisible()
    await wallet.getByTestId('dwn-node-url').fill('http://mock/api/v1/identity/identity-hub')
    await refreshButton.click()
  }

  const connectDwnWallet = async (page: Page) => {
    await page.getByText('Connect Wallet').click()
    await page.getByText('Decentralized Web Node').click()
  }
})
