import { expect, test } from '@playwright/test'

test.beforeEach(async ({ page }) => {
  await page.goto('/')
})

test.describe('Sidebar', () => {
  test('Sidebar is visible ', async ({ page }) => {
    const sidebar = page.getByTestId('sidebar')
    await expect(sidebar).toBeVisible()
  })

  test('Clicking on logo return to Playground page', async ({ page }) => {
    await page.goto('/onboarding')
    await page.getByRole('link', { name: 'logo Gaia-X Wizard' }).click()
    await expect(page).toHaveURL('/')
  })

  test('Clicking on Playground redirect to / page', async ({ page }) => {
    await page.goto('/onboarding')
    await page.locator('a').filter({ hasText: 'Playground' }).click()
    await expect(page).toHaveURL('/')
  })

  test('Clicking on Participant VCs in 4 steps redirect to /onboarding page', async ({ page }) => {
    await page.goto('/')
    await page.locator('a').filter({ hasText: 'Participant VCs in 4 steps' }).click()
    await expect(page).toHaveURL('/onboarding')
  })

  test('Clicking on Stepper redirect to /stepper page', async ({ page }) => {
    await page.goto('/')
    await page.locator('a').filter({ hasText: 'Stepper' }).click()
    await expect(page).toHaveURL('/stepper')
  })

  test('Clicking on Get Legal Registration Number redirect to /legalRegistrationNumber page', async ({ page }) => {
    await page.goto('/')
    await page.locator('a').filter({ hasText: 'Get Legal Registration Number' }).click()
    await expect(page).toHaveURL('/legalRegistrationNumber')
  })

  test('Clicking on Contribute redirect to gitlab repository page', async ({ page }) => {
    await page.goto('/')

    page.on('popup', async (newPage: any) => {
      expect(await newPage.url()).toBe('https://gitlab.com/gaia-x/lab/gaia-x-onboarding-prototypes/gx-signing-tool')
    })

    await page.locator('a').filter({ hasText: 'Contribute' }).click()
  })
})
