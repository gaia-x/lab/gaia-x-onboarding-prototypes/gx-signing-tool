import { expect, test } from '@playwright/test'
import { getMonacoEditorValue, monacoEditorSelector } from './util'
import { VerifiableCredential } from '../../sharedTypes'

async function fillLRNInputAndSubmit(page: any, value: string, radioLabel: string) {
  await page.getByLabel('Legal registration number*').click()
  await page.getByLabel('Legal registration number*').fill(value)
  const lrnRadio = await page.locator('label').filter({ hasText: radioLabel })
  await expect(lrnRadio).toBeVisible()
  await lrnRadio.locator('span').first().click()
  const submitButton = await page.getByRole('button', { name: 'Submit' }).last()
  await expect(submitButton).toBeVisible()
  await submitButton.click()
  await expect(submitButton).not.toBeVisible()
}

async function getLrnMonacoEditorValue(page: any, index: number): Promise<VerifiableCredential> {
  const monacoEditor = monacoEditorSelector(page)
  await expect(monacoEditor).toBeVisible()
  const text = await getMonacoEditorValue(page, index)
  return JSON.parse(text ?? 'no json found')
}

test.beforeEach(async ({ page }) => {
  await page.goto('/legalRegistrationNumber')
})

test.describe('Legal Registration Number', () => {
  test('Claim Legal Registration Number with wizard DID', async ({ page }) => {
    await fillLRNInputAndSubmit(page, 'FR79537407926', 'vatID')

    const monacoEditor = monacoEditorSelector(page)
    await expect(monacoEditor).toBeVisible()

    const text = await getMonacoEditorValue(page, 0)
    const lrn = JSON.parse(text ?? 'no json found')

    expect(lrn.credentialSubject).toHaveProperty('type', 'gx:legalRegistrationNumber')
  })

  test('Claim Legal Registration Number with custom DID', async ({ page }) => {
    await page.getByTestId('did-custom-switch').locator('span').first().click()

    await page.getByLabel('Verifiable Credential ID*').click()
    await page.getByLabel('Verifiable Credential ID*').fill('vcId')

    await page.getByLabel('Credential subject ID*').click()
    await page.getByLabel('Credential subject ID*').fill('csId')

    await fillLRNInputAndSubmit(page, 'FR79537407926', 'vatID')

    const lrn = await getLrnMonacoEditorValue(page, 0)

    expect(lrn.id).toBe('vcId')
    expect(lrn.credentialSubject.id).toBe('csId')
    expect(lrn.credentialSubject).toHaveProperty('type', 'gx:legalRegistrationNumber')
  })
})
