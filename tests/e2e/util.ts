import { expect, Locator, type Page } from '@playwright/test'
import signedParticipant from './fixtures/signedParticipant.json'
import legalRegistrationNumber from './fixtures/legalRegistrationNumber.json'
import termsAndConditions from './fixtures/termsAndConditions.json'

export function monacoEditorSelector(locator: Locator | Page, testId = 'code-editor') {
  return locator.locator(`[data-testid="${testId}"][data-mounted="true"]`)
}

export async function getMonacoEditorValue(page: Page, index: number): Promise<string | undefined> {
  return await page.evaluate(async (i: number) => {
    while (!('monaco' in window && (window as any).monaco.editor.getEditors().length > 0)) {
      await new Promise(resolve => setTimeout(resolve, 100))
    }
    return (window as any).monaco.editor.getEditors()[i]?.getValue()
  }, index)
}

export async function setMonacoEditorValue(page: Page, index: number, value: string): Promise<void> {
  return page.evaluate(
    async ({ i, v }) => {
      while (!('monaco' in window && (window as any).monaco.editor.getEditors().length > 0)) {
        await new Promise(resolve => setTimeout(resolve, 100))
      }
      const editor = (window as any).monaco.editor.getEditors()[i]
      editor?.focus()
      editor?.setValue(v)
    },
    { i: index, v: value }
  )
}

export async function verifyVCPresenceInHolder(
  page: Page,
  expected = ['gx:LegalParticipant', 'gx:legalRegistrationNumber', 'gx:GaiaXTermsAndConditions']
) {
  const holder = page.locator('.holder')
  await Promise.all(expected.map(exp => expect(holder).toContainText(exp)))
}

export async function fillParticipantForm(
  cn: Locator | Page,
  values: {
    name: string
    registrationNumber: string
    headquarter: string
    legalAddress: string
    lrnType?: string
  }
) {
  await cn.getByLabel('name').fill(values.name)
  if (values.lrnType) {
    await cn.getByText(values.lrnType).click()
  }
  await cn.getByLabel('registration number').fill(values.registrationNumber)
  await cn.getByLabel('headquarter').fill(values.headquarter)
  await cn.getByLabel('legal address').fill(values.legalAddress)
}

export async function getDocumentValue(page: Page, type: string, monacoEditorIndex: number) {
  const vc = await page.getByTestId('documents').locator('div', { hasText: type }).first()
  await expect(vc).toBeVisible()
  const viewButton = await vc.getByRole('button', { name: 'View' })
  await expect(viewButton).toBeEnabled()
  await viewButton.click()
  const modal = page.getByTestId('doc-view-modal')
  await expect(modal).toBeVisible()
  const editor = modal.locator(monacoEditorSelector(page))
  await expect(editor).toBeVisible()
  const rawValue = await getMonacoEditorValue(page, monacoEditorIndex)
  await modal.getByRole('button', { name: 'Close' }).first().click()
  return JSON.parse(rawValue ?? '{}')
}

export async function setEnv(page: Page, label = 'Gaia-x', version = 'development') {
  const clearingHouses = page.getByTestId('clearing-house-select')
  await expect(clearingHouses.selectOption(label)).toBeTruthy()
  await clearingHouses.selectOption({ label })
  await page.getByLabel(/(?:path|version)/i).selectOption(version)
}

export async function acceptTnc(cn: Page | Locator) {
  await expect(cn.getByText('The PARTICIPANT signing the Self-Description agrees as follows')).toBeVisible()
  await cn.getByTestId('agree-tnc').click()
}

export async function setPrivateKey(page: Page, editorIndex = 0, value = process.env.DID_X509_ROOT_PRIVATE_KEY?.replace(/\\+n/g, '\n') ?? '') {
  const editor = monacoEditorSelector(page, 'privateKey')
  await expect(editor).toBeVisible()
  await setMonacoEditorValue(page, editorIndex, value)
}

export const connectLocalWallet = async (page: Page) => {
  await page.getByText('Connect Wallet').click()
  await page.getByText('Local Wallet').click()
}

export const importDocumentIntoHolder = async (page: Page, signedDocument: any) => {
  await page.getByTestId('import-document-button').first().click()
  const importDocumentModal = page.getByTestId('import-document-modal')
  await expect(monacoEditorSelector(importDocumentModal)).toBeVisible()
  await setMonacoEditorValue(page, 2, JSON.stringify(signedDocument, null, 2))
  const addButton = await page.getByRole('button', { name: 'Add' }).first()
  await expect(addButton).toBeEnabled()
  await addButton.click()
}

export const prepareComplianceCall = async (page: Page) => {
  await importDocumentIntoHolder(page, signedParticipant)
  await importDocumentIntoHolder(page, termsAndConditions)
  await importDocumentIntoHolder(page, legalRegistrationNumber)
  await page.getByRole('button', { name: 'Call compliance' }).click()
  await page.getByTestId('compliance-document_0').check()
  await page.getByTestId('compliance-document_1').check()
  await page.getByTestId('compliance-document_2').check()
}
