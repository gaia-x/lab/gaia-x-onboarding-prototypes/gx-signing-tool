import { expect, Page, test } from '@playwright/test'
import {
  getMonacoEditorValue,
  importDocumentIntoHolder,
  monacoEditorSelector,
  prepareComplianceCall,
  setEnv,
  setMonacoEditorValue,
  verifyVCPresenceInHolder
} from './util'
import signedParticipant from './fixtures/signedParticipant.json'
import unsignedParticipant from './fixtures/unsignedParticipant.json'
import legalRegistrationNumber from './fixtures/legalRegistrationNumber.json'
import termsAndConditions from './fixtures/termsAndConditions.json'

test.beforeEach(async ({ page }) => {
  await page.route('**/api/credential-offers**', route =>
    route.fulfill({
      status: 200
    })
  )
  await page.goto('/')
})

test.describe('Playground', () => {
  test('Disclaimer is visible and dismissible', async ({ page }) => {
    await expect(page.getByText('disclaimer')).toBeVisible()
    await page.getByRole('alert').getByLabel('Close').click()
    await expect(page.getByText('disclaimer')).not.toBeVisible()
  })
})

test.describe('Issue credentials', () => {
  test('Private key is set', async ({ page }) => {
    const editor = monacoEditorSelector(page, 'privateKey')
    await expect(editor).toContainText('-----BEGIN PRIVATE KEY-----')
  })

  test('Document name is generated', async ({ page }) => {
    await expect(page.getByPlaceholder('Document name')).toHaveValue(/[a-z]+/)
  })

  test('Can select VC example', async ({ page }) => {
    const issuer = page.locator('.issuer')
    const examples = issuer.getByTestId('examples')
    const options = examples.getByRole('option')
    const editor = monacoEditorSelector(page)
    await expect(editor).toBeVisible()
    await expect(options).toContainText(['Participant'])
    await expect(options).toContainText(['Service Offering'])
    await expect(options).toContainText(['Terms and Conditions'])
    await examples.selectOption('service')
    await expect(editor.getByRole('code')).toContainText('"type": "gx:ServiceOffering"')
  })

  test('Sign a Participant VC with custom DID', async ({ page }) => {
    const issuer = page.locator('.issuer')

    await expect(monacoEditorSelector(issuer)).toBeVisible()
    await issuer.getByTestId('examples').selectOption('participant')
    await issuer
      .locator('div')
      .filter({ hasText: /^Verification method$/ })
      .getByRole('textbox')
      .fill('did:web:example.com#verif1')

    const text = await getMonacoEditorValue(page, 0)
    const participant = JSON.parse(text ?? 'no json found')
    participant.id = 'https://example.com/credentials/3732'
    participant.credentialSubject.id = 'https://example.com/participants/3732#cs'
    participant.issuer = 'did:web:example.com'

    await setMonacoEditorValue(page, 0, JSON.stringify(participant, null, 2))
    const submit = issuer.getByRole('button', { name: 'Sign' })
    await submit.click()
    await expect(submit).toBeEnabled({ timeout: 20000 })
    await verifyVCPresenceInHolder(page, ['gx:LegalParticipant'])
  })

  async function signParticipantWithWizardIssuer(page: Page) {
    const issuer = page.locator('.issuer')
    await expect(monacoEditorSelector(issuer)).toBeVisible()
    await issuer.getByTestId('examples').selectOption('participant')
    await issuer.getByTestId('custom-issuer').click()
    await expect(issuer.getByTestId('custom-did-notice')).toBeVisible()
    const submit = issuer.getByRole('button', { name: 'Sign' })
    await submit.click()
    await expect(submit).toBeEnabled({ timeout: 20000 })
    await verifyVCPresenceInHolder(page, ['gx:LegalParticipant'])
  }

  test('Sign a Participant VC with Wizard issuer', async ({ page }) => {
    await signParticipantWithWizardIssuer(page)
  })

  test('Sign a Participant VC with Wizard issuer and check resolvability of the vcId', async ({ page }) => {
    await signParticipantWithWizardIssuer(page)
    await page.getByRole('button', { name: 'View' }).click()
    const viewEditorValue = await getMonacoEditorValue(page, 2)
    const vc = JSON.parse(viewEditorValue ?? 'no json found')

    const resolvedVc = await page.evaluate(async vcId => {
      try {
        const response = await fetch(vcId)
        return await response.text()
      } catch (error) {
        return 'no json found'
      }
    }, vc.id)

    expect(JSON.parse(resolvedVc)).toEqual(vc)
  })
})

test.describe('Import documents', () => {
  test('Import signed document into holder', async ({ page }) => {
    const holder = page.locator('.holder')
    await importDocumentIntoHolder(page, signedParticipant)
    await expect(holder.getByText('gx:LegalParticipant')).toBeVisible()
  })

  test('Failed import of invalid document', async ({ page }) => {
    const importButton = await page.getByTestId('import-document-button')
    await importButton.click()
    const importDocumentModal = page.getByTestId('import-document-modal')
    await expect(monacoEditorSelector(importDocumentModal)).toBeVisible()
    await setMonacoEditorValue(page, 2, JSON.stringify(unsignedParticipant))
    const addButton = await importDocumentModal.getByRole('button', { name: 'Add' }).first()
    await expect(addButton).toBeEnabled()
    await addButton.click()
    const toast = await page.locator('.chakra-alert').first()
    await expect(toast).toBeVisible()
    await expect(await toast.getAttribute('data-status')).toEqual('warning')
  })
})
test.describe('Choose a GXDCH', () => {
  test('Default Digital clearing house selection is available', async ({ page }) => {
    const clearingHouseSelect = page.getByTestId('clearing-house-select')
    await expect(clearingHouseSelect).toBeVisible()
    await expect(clearingHouseSelect.locator('option').first()).toContainText('Any')
    await expect(clearingHouseSelect.locator('option').getByText('Gaia-x')).toHaveCount(1)
    await expect(clearingHouseSelect.locator('option').getByText('Aruba')).toHaveCount(1)
    const versionSelect = page.getByTestId('version-select')
    await expect(versionSelect).toBeVisible()
    await expect(versionSelect.locator('option').first()).toContainText('v1')
  })

  test('Choose a Digital clearing house', async ({ page }) => {
    await page.waitForLoadState('networkidle')
    const clearingHouseSelect = page.getByTestId('clearing-house-select')
    const versionSelect = page.getByTestId('version-select')

    await clearingHouseSelect.selectOption('gaia-x')
    await expect(versionSelect.locator('option').first()).toContainText('v1')
    await expect(versionSelect.locator('option').getByText('v1-staging')).toHaveCount(1)
    await expect(versionSelect.locator('option').getByText('development')).toHaveCount(1)
    await expect(versionSelect.locator('option').getByText('main')).toHaveCount(1)
    await versionSelect.selectOption('development')

    const selectedClearingHouse = await page.evaluate(() => JSON.parse(window.localStorage.getItem('clearingHouse') || ''))
    await expect(selectedClearingHouse.clearingHouse.name).toEqual('gaia-x')
    await expect(selectedClearingHouse.gxVersion).toEqual('development')
  })
})

test.describe('Call the compliance', () => {
  test('Compliance call required credentials validation', async ({ page }) => {
    test.setTimeout(60000)
    const complianceButton = page.getByRole('button', { name: 'Call compliance' })
    await expect(complianceButton).toBeVisible()
    await complianceButton.click()
    const dialog = await page.getByRole('dialog')
    await expect(dialog).toBeVisible()

    const participantBadge = await page.getByTestId('compliance-validation-tooltip_0')
    await expect(participantBadge).toBeVisible()
    await expect(participantBadge.locator('[aria-label="Valid credential"]')).not.toBeVisible()
    await expect(participantBadge.locator('[aria-label="Missing credential"]')).toBeVisible()
    const tAndCBadge = await page.getByTestId('compliance-validation-tooltip_1')
    await expect(tAndCBadge).toBeVisible()
    await expect(tAndCBadge.locator('[aria-label="Missing credential"]')).toBeVisible()
    const lrnBadge = await page.getByTestId('compliance-validation-tooltip_2')
    await expect(lrnBadge).toBeVisible()
    await expect(lrnBadge.locator('[aria-label="Missing credential"]')).toBeVisible()

    await dialog.getByRole('button', { name: 'Close' }).first().click()
    await expect(dialog).not.toBeVisible()
  })

  test('Submit compliance presentation', async ({ page }) => {
    test.setTimeout(60000)
    await prepareComplianceCall(page)
    await setEnv(page)

    const participantBadge = await page.getByTestId('compliance-validation-tooltip_0')
    await expect(participantBadge.locator('[aria-label="Valid credential"]')).toBeVisible()
    const tAndCBadge = await page.getByTestId('compliance-validation-tooltip_1')
    await expect(tAndCBadge.locator('[aria-label="Valid credential"]')).toBeVisible()
    const lrnBadge = await page.getByTestId('compliance-validation-tooltip_2')
    await expect(lrnBadge.locator('[aria-label="Valid credential"]')).toBeVisible()

    await page.getByRole('button', { name: 'Next' }).first().click()
    const response = page.waitForResponse('**/credential-offers**')
    await page.getByRole('button', { name: 'Submit to compliance' }).first().click()
    const requestBody = (await response).request().postData()

    await expect(requestBody).toContain(JSON.stringify(signedParticipant))
    await expect(requestBody).toContain(JSON.stringify(termsAndConditions))
    await expect(requestBody).toContain(JSON.stringify(legalRegistrationNumber))
  })

  test('Should display disclaimer when calling production compliance', async ({ page }) => {
    test.setTimeout(60000)
    await prepareComplianceCall(page)
    await setEnv(page, 'Gaia-x', 'v1')

    const participantBadge = await page.getByTestId('compliance-validation-tooltip_0')
    await expect(participantBadge.locator('[aria-label="Valid credential"]')).toBeVisible()
    const tAndCBadge = await page.getByTestId('compliance-validation-tooltip_1')
    await expect(tAndCBadge.locator('[aria-label="Valid credential"]')).toBeVisible()
    const lrnBadge = await page.getByTestId('compliance-validation-tooltip_2')
    await expect(lrnBadge.locator('[aria-label="Valid credential"]')).toBeVisible()
    const nextButton = page.getByRole('button', { name: 'Next' }).first()

    await nextButton.click()

    await expect(page.getByText('Please note that this is a production environment')).toBeVisible()
    await expect(nextButton.first()).toBeDisabled()
    await expect(page.getByTestId('compliance-disclaimer')).toBeVisible()
    await page.getByTestId('compliance-disclaimer').check()
    await expect(nextButton).toBeEnabled()
    await nextButton.click()
    await expect(page.getByRole('button', { name: 'Submit to compliance' }).first()).toBeVisible()
  })
})
