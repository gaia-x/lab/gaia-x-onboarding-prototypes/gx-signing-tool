import { test, expect, type Page } from '@playwright/test'
import { acceptTnc, fillParticipantForm, getDocumentValue, setPrivateKey, verifyVCPresenceInHolder } from './util'

const validParticipantValues = {
  name: 'Gaia-X European Association for Data and Cloud AISBL',
  registrationNumber: 'did:web:example:123456789:lrn:vc',
  headquarter: 'BE-BRU',
  legalAddress: 'BE-BRU'
}

const validIssuer = 'did:example:123456789'
const validVerifMethod = `${validIssuer}:key-1`
const validVCId = `${validIssuer}:vc`
const validCSId = `${validIssuer}:cs`

test.beforeEach(async ({ page }) => {
  await page.goto('/stepper')
})

test.describe('Stepper', () => {
  test('Disclaimer is visible', async ({ page }) => {
    await expect(page.getByText('disclaimer')).toBeVisible()
  })

  test('Create participant VC with managed issuer', async ({ page }) => {
    const { selectShape, next, sign, addToHolder, verifyManagedDocumentValues } = stepperPage(page)

    await selectShape('Participant')
    await fillParticipantForm(page, validParticipantValues)
    await next()
    await sign()
    await addToHolder()

    await verifyVCPresenceInHolder(page, ['gx:LegalParticipant'])
    await verifyManagedDocumentValues('gx:LegalParticipant')
  })

  test('Create participant VC with custom issuer', async ({ page }) => {
    const { setCustomIssuer, selectShape, next, sign, fillIdentity, addToHolder, verifyIssuerDocumentValues } = stepperPage(page)

    await setCustomIssuer()
    await selectShape('Participant')

    await fillParticipantForm(page, validParticipantValues)
    await next()
    await setPrivateKey(page)
    await next()
    await fillIdentity()
    await next()
    await sign()
    await addToHolder()

    await verifyVCPresenceInHolder(page, ['gx:LegalParticipant'])
    await verifyIssuerDocumentValues('gx:LegalParticipant')
  })
})

test('Create service offering VC with managed issuer', async ({ page }) => {
  const { selectShape, next, sign, addToHolder, fillServiceOfferingForm, verifyManagedDocumentValues } = stepperPage(page)

  await selectShape('Service Offering')
  await fillServiceOfferingForm()
  await next()
  await sign()
  await addToHolder()

  await verifyVCPresenceInHolder(page, ['gx:ServiceOffering'])
  await verifyManagedDocumentValues('gx:ServiceOffering')
})

test('Create service offering VC with custom issuer', async ({ page }) => {
  const { setCustomIssuer, fillServiceOfferingForm, selectShape, next, sign, fillIdentity, addToHolder, verifyIssuerDocumentValues } =
    stepperPage(page)

  await setCustomIssuer()
  await selectShape('Service Offering')

  await fillServiceOfferingForm()
  await next()
  await setPrivateKey(page)
  await next()
  await fillIdentity()
  await next()
  await sign()
  await addToHolder()

  await verifyVCPresenceInHolder(page, ['gx:ServiceOffering'])
  await verifyIssuerDocumentValues('gx:ServiceOffering')
})

test('Create T&C VC with managed issuer', async ({ page }) => {
  const { selectShape, next, sign, addToHolder, verifyManagedDocumentValues } = stepperPage(page)

  await selectShape('Terms and Conditions')
  await acceptTnc(page)
  await next()
  await sign()
  await addToHolder()

  await verifyVCPresenceInHolder(page, ['gx:GaiaXTermsAndConditions'])
  await verifyManagedDocumentValues('gx:GaiaXTermsAndConditions')
})

test('Create T&C VC with custom issuer', async ({ page }) => {
  const { setCustomIssuer, selectShape, next, fillIdentity, sign, addToHolder, verifyIssuerDocumentValues } = stepperPage(page)

  await setCustomIssuer()
  await selectShape('Terms and Conditions')
  await acceptTnc(page)
  await next()

  await setPrivateKey(page)
  await next()
  await fillIdentity()
  await next()
  await sign()
  await addToHolder()

  await verifyVCPresenceInHolder(page, ['gx:GaiaXTermsAndConditions'])
  await verifyIssuerDocumentValues('gx:GaiaXTermsAndConditions')
})

const stepperPage = (page: Page) => ({
  setCustomIssuer: async () => {
    const customDid = page.getByTestId('use-own-did')
    await expect(customDid).toBeVisible()
    await customDid.click()
  },

  selectShape: async (name: string) => {
    await page.getByTestId('shapes').getByText(name).click()
  },

  addToHolder: async () => {
    await page.getByRole('button', { name: 'Add in holder', exact: true }).click()
  },

  sign: async () => {
    await page.getByRole('button', { name: 'Sign' }).click()
  },

  next: async () => {
    await page.getByRole('button', { name: 'Next' }).click()
  },

  fillIdentity: async () => {
    await page.getByLabel('issuer').fill(validIssuer)
    await page.getByLabel('verification method').fill(validVerifMethod)
    await page.getByLabel('verifiable credential id').first().fill(validVCId)
    await page.getByLabel('credential subject id').first().fill(validCSId)
  },

  verifyIssuerDocumentValues: async (type: string, editorIndex = 2) => {
    const value = await getDocumentValue(page, type, editorIndex)
    expect(value.id).toBe(validVCId)
    expect(value.issuer).toBe(validIssuer)
    expect(value.credentialSubject.id).toBe(validCSId)
    expect(value.proof.verificationMethod).toBe(validVerifMethod)
    return value
  },

  verifyManagedDocumentValues: async (type: string, editorIndex = 2) => {
    const value = await getDocumentValue(page, type, editorIndex)
    expect(value.id).toContain('http://')
    expect(value.issuer).toContain('did:web')
    // subject ID has to be there, either filled by the user or automatically by the wizard
    expect(value.credentialSubject.id).toBeDefined()
    expect(value.proof.verificationMethod).toContain('did:web')
    return value
  },

  fillServiceOfferingForm: async () => {
    await page.getByLabel('Provided by').fill(validIssuer)
    await page.getByLabel('Policy').fill('https://gaia-x.eu/privacy-policy')
    await page.getByLabel('Terms and conditions url').fill('http://termsandconds.com')
    await page.getByLabel('Terms and conditions hash').fill('d8402a23de560f5ab34b22d1a142feb9e13b3143')
    await page.getByLabel('Request type').selectOption('API')
    await page.getByLabel('Access type').selectOption('digital')
    await page.getByLabel('Format type').fill('application/json')
  }
})
