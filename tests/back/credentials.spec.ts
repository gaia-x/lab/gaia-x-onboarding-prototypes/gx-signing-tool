import { beforeAll, describe, expect, it } from 'vitest'
import { KeychainService } from '../../pages/api/service/keychain.service'
import { DidService } from '../../pages/api/service/did.service'
import { readFileSync } from 'fs'
import { InMemoryStorageService } from '../../pages/api/service/inmemory-storage.service'

describe('handle DID and credentials', () => {
  let didService: DidService
  let keyChainService: KeychainService

  beforeAll(() => {
    const storageService = new InMemoryStorageService()
    keyChainService = new KeychainService(storageService)
    didService = new DidService(keyChainService)
  })

  it('should create a DID document from a public key', async () => {
    const publicKeyPem = readFileSync('./tests/back/util/publicKey.pem', 'utf8')
    const doc = JSON.stringify(await didService.createDidDocument('test', { publicKeyPem }), null, 2)
    const expectedDoc = JSON.stringify(JSON.parse(readFileSync('./tests/back/util/did.json', 'utf8')), null, 2)

    expect(doc).toEqual(expectedDoc)
  })

  it('should create a valid x509 certchain from a public key', async () => {
    const publicKeyPem = readFileSync('./tests/back/util/publicKey.pem', 'utf8')
    const x509Url = (await keyChainService.createX509('testId', publicKeyPem)).url
    const resolvedx509 = await keyChainService.getX509ById('testId')

    expect(x509Url).toEqual(`${process.env.BASE_URL}/api/x509/zxo1qXWT.pem`) // zxo1qXWT is the base58 of "testId"
    const certCount = (resolvedx509?.split('-----BEGIN CERTIFICATE-----').length ?? 1) - 1
    expect(certCount).toEqual(3)
  })
})

export {}
