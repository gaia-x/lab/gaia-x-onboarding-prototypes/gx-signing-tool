import { defineConfig } from 'vite'
import path from 'path'

export default defineConfig({
  test: {
    include: ['**/back/*.{test,spec}.{js,mjs,cjs,ts,mts,cts,jsx,tsx}'],
    setupFiles: ['./tests/back/util/setup-teardown.js'],
    coverage: {
      enabled: true,
      reportsDirectory: 'coverage/back',
      provider: 'istanbul',
      reporter: ['text', 'lcovonly', 'cobertura']
    }
  },
  resolve: {
    alias: {
      '@/': path.resolve(__dirname, '../../') + '/'
    }
  }
})
