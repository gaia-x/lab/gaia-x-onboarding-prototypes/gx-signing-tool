import { Url } from 'jsonld/jsonld-spec'
import vcCtx from '@/public/credentials_v1_context.json'
import jwsCtx from '@/public/jws2020_v1_context.json'
import trustframeworkCtx from '@/public/trustframework_context.json'
import schemaCtx from '@/public/schema_context.json'
import vcardCtx from '@/public/schema_context.json'

const CACHED_CONTEXTS: { [url: string]: string } = {
  'https://www.w3.org/2018/credentials/v1': JSON.stringify(vcCtx),
  'https://w3id.org/security/suites/jws-2020/v1': JSON.stringify(jwsCtx),
  'https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#': JSON.stringify(trustframeworkCtx),
  'https://schema.org': JSON.stringify(schemaCtx),
  'http://schema.org': JSON.stringify(schemaCtx),
  'http://www.w3.org/2006/vcard/ns#': JSON.stringify(vcardCtx),
  'https://www.w3.org/2006/vcard/ns#': JSON.stringify(vcardCtx)
}

export const staticDocumentLoader: (url: Url) => Promise<{ document: string; documentUrl: string }> = async documentUrl => {
  if (documentUrl in CACHED_CONTEXTS) {
    return {
      document: CACHED_CONTEXTS[documentUrl],
      documentUrl
    }
  }
  const document = await (await fetch(documentUrl)).text()
  return {
    document,
    documentUrl
  }
}
