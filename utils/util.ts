import { generateSlug, RandomWordOptions } from 'random-word-slugs'
import { GaiaXDeploymentPath, VerifiableCredential } from '@/sharedTypes'
import * as jose from 'jose'
import { v4 as uuidv4 } from 'uuid'

export function joinPathParts(path: string | string[] | undefined): string {
  return Array.isArray(path) ? path.join('/') : path || ''
}

export function joinPath(host: string, ...path: string[]): string {
  if (!path.length) {
    return host
  }
  const p = path[0]
  let resolved: string
  if (host.endsWith('/')) {
    resolved = p.startsWith('/') ? host + p.substring(1) : host + p
  } else {
    resolved = p.startsWith('/') ? host + p : host + '/' + p
  }
  if (path.length === 1) {
    return resolved
  }
  return joinPath(resolved, ...path.slice(1))
}

export const generateName = () => {
  const options: RandomWordOptions<2> = {
    format: 'kebab',
    partsOfSpeech: ['adjective', 'noun'],
    categories: {
      adjective: ['color', 'appearance'],
      noun: ['animals', 'food']
    }
  }

  return generateSlug(2, options)
}

export const toObjectPath = (path: string, object: any) => {
  const parts = path.split('.')
  let current = object

  for (const part of parts) {
    current = current[part]

    // Exit the loop if the path isn't found in the object
    if (current === undefined) {
      break
    }
  }

  return current
}

export function getString(param: string | string[] | undefined): string | undefined {
  return Array.isArray(param) ? param?.[0] : param
}

export const isDocumentVerified = (document: VerifiableCredential) => {
  if (document.issuer.includes('did:web:compliance.lab.gaia-x.eu')) {
    return document.issuer.split(':').pop() as GaiaXDeploymentPath
  } else return false
}

export function capitalize(inputString: string): string {
  if (inputString.length === 0) {
    return inputString
  }

  const firstLetter = inputString.charAt(0).toUpperCase()
  const restOfString = inputString.slice(1)

  return firstLetter + restOfString
}

export const parseJWT = (jwt: string) => {
  return jose.decodeJwt(jwt)
}

export async function createCredentialSubjectId(didWeb: string, credentialSubject: any, docId: string) {
  const encoder = new TextEncoder()
  const data = encoder.encode(JSON.stringify(credentialSubject))
  const key = await crypto.subtle.importKey('raw', encoder.encode(docId), { name: 'HMAC', hash: 'SHA-256' }, false, ['sign'])
  const hmac = Array.from(new Uint8Array(await crypto.subtle.sign('HMAC', key, data)))
    .map(b => b.toString(16).padStart(2, '0'))
    .join('')
  return didWeb + '#' + hmac
}

export function createVcId(did: string) {
  return did + '?uid=' + uuidv4()
}

export function stringToSHA256String(str: string) {
  const encoder = new TextEncoder()
  const data = encoder.encode(str)
  return crypto.subtle.digest('SHA-256', data).then(buffer => {
    const hashArray = Array.from(new Uint8Array(buffer))
    const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join('')
    return hashHex
  })
}

export const getUrlParameter = (param: string) => {
  const urlParams = new URLSearchParams(window.location.search)
  const value = urlParams.get(param)
  return value ? decodeURIComponent(value.replace(/\+/g, ' ')) : ''
}

export const cleanQueryParams = () => {
  const currentUrl = window.location.href
  const urlWithoutParams = currentUrl.split('?')[0]
  window.history.replaceState(null, '', urlWithoutParams)
}
