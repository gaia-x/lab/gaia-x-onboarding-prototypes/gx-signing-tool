stages:
  - test
  - sonar
  - build
  - release
  - deploy

variables:
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

## Prepare node_modules cache
cache:
  - &global_cache_node_mods
    key:
      files:
        - package-lock.json
    paths:
      - node_modules/
    policy: pull

install:
  image: node:18
  stage: .pre
  cache:
    - <<: *global_cache_node_mods
      when: on_success
      policy: pull-push
    - key: ${CI_JOB_NAME}
      paths:
        - .npm/
      when: on_success
      policy: pull-push
  script:
    - npm ci --cache .npm --prefer-offline

test-back:
  image: node:18
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
      when: never
    - if: $CI_COMMIT_BRANCH == "main" && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
      when: never
    - if: $CI_COMMIT_BRANCH
  script:
    - npm run --ignore-scripts test:back
  coverage: /All files[^|]*\|[^|]*\s+([\d\.]+)/
  artifacts:
    when: on_success
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/back/cobertura-coverage.xml
    paths:
      - coverage/back/lcov.info


test-front:
  image: node:18
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
      when: never
    - if: $CI_COMMIT_BRANCH == "main" && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
      when: never
    - if: $CI_COMMIT_BRANCH
  script:
    - npm run test:front
  coverage: /All files[^|]*\|[^|]*\s+([\d\.]+)/
  artifacts:
    when: on_success
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/front/cobertura-coverage.xml
    paths:
      - coverage/front/lcov.info

test-e2e:
  image: node:18
  stage: test
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
      when: never
    - if: $CI_COMMIT_BRANCH == "main" && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
      when: never
    - if: $CI_COMMIT_BRANCH
  script:
    - npm run test:e2e
  artifacts:
    when: on_failure
    expire_in: 3 days
    paths:
      - test-results/
  allow_failure: true

sonarqube-check:
  stage: sonar
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [ "" ]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner
  allow_failure: true
  only:
    - development

build:
  image: docker:26.1.3
  services:
    - docker:26.1.3
  stage: build
  cache: []
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
      when: never
    - if: $CI_COMMIT_BRANCH == "main"
      when: never
    - if: $CI_COMMIT_BRANCH
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build --pull --build-arg BASE_PATH_ARG=/$CI_COMMIT_REF_SLUG -t $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

deploy-on-lab:
  image: ubuntu
  stage: deploy
  cache: []
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
      when: never
    - if: $CI_COMMIT_BRANCH == "main"
      when: never
    - if: $CI_COMMIT_BRANCH == "development"
  before_script:
    - apt update && apt install -y curl
    - curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
  script:
    - |
      if [[ "$CI_COMMIT_BRANCH" == "development" ]]; then
        export COMPLIANCE_URL=$COMPLIANCE_DEV_BASE_URL 
      else 
        export COMPLIANCE_URL=$COMPLIANCE_BASE_URL 
      fi
    - helm upgrade --install -n "$CI_COMMIT_REF_SLUG" --create-namespace gx-signing-tool ./k8s/gx-signing-tool --set "nameOverride=$CI_COMMIT_REF_SLUG,ingress.hosts[0].host=wizard.lab.gaia-x.eu,ingress.hosts[0].paths[0].path=/$CI_COMMIT_REF_SLUG,image.tag=$CI_COMMIT_REF_SLUG,ingress.hosts[0].paths[0].pathType=Prefix,REGISTRY_API_BASE_URL=$REGISTRY_API_BASE_URL,COMPLIANCE_BASE_URL=$COMPLIANCE_URL,REGISTRATION_NUMBER_NOTARY_BASE_URL=$REGISTRATION_NUMBER_NOTARY_BASE_URL,AWS_ENDPOINT=$AWS_ENDPOINT,AWS_REGION=$AWS_REGION,AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY,BASE_URL=$BASE_URL,DID_X509_ROOT_CERTIFICATE=$DID_X509_ROOT_CERTIFICATE,DID_X509_ROOT_PRIVATE_KEY=$DID_X509_ROOT_PRIVATE_KEY,STORAGE_PROVIDER=$STORAGE_PROVIDER,DISK_STORAGE_PATH=$DISK_STORAGE_PATH,STORAGE_S3_VCS_BUCKET=$STORAGE_S3_VCS_BUCKET,STORAGE_S3_DID_BUCKET=$STORAGE_S3_DID_BUCKET,STORAGE_S3_X509_BUCKET=$STORAGE_S3_X509_BUCKET,KEYCLOAK_BASE_URL=$KEYCLOAK_BASE_URL,KEYCLOAK_CLIENT_ID=$KEYCLOAK_CLIENT_ID,KEYCLOAK_CLIENT_SECRET=$KEYCLOAK_CLIENT_SECRET,OIDC_SERVICE_BASE_URL=$OIDC_SERVICE_BASE_URL,MEMBERSHIP_CREDENTIAL_PORTAL_URL=$MEMBERSHIP_CREDENTIAL_PORTAL_URL" --kubeconfig "$ORANGE_KUBECONFIG"

make-release:
  image: node:18
  stage: release
  rules:
    - if: $CI_COMMIT_BRANCH == "main" && $CI_COMMIT_MESSAGE =~ /^chore\(release\):/
      when: never
    - if: $CI_COMMIT_BRANCH == "main"
  before_script:
    - apt-get update -y && apt-get install -yqqf openssh-client git unzip sshpass rsync --fix-missing
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client git -y )'
    - eval $(ssh-agent -s)
    - echo "$CI_SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh

    - ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts

    - git config --global user.email "cto@gaia-x.eu"
    - git config --global user.name "semantic-release-bot"
  script:
    - ./node_modules/.bin/semantic-release

build-tag:
  image: docker:26.1.3
  services:
    - docker:26.1.3
  stage: build
  cache: []
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build --pull -t $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG .
    - docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  only:
    - tags

deploy-tag-on-lab:
  image: ubuntu
  stage: deploy
  cache: []
  when: manual
  before_script:
    - apt update && apt install -y curl
    - curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
  script:
    - helm upgrade --install -n "${CI_COMMIT_TAG%%.*}" --create-namespace gx-signing-tool ./k8s/gx-signing-tool --set "ingress.hosts[0].host=wizard.lab.gaia-x.eu,ingress.hosts[0].paths[0].path=/,image.tag=$CI_COMMIT_TAG,ingress.hosts[0].paths[0].pathType=Prefix,REGISTRY_API_BASE_URL=$REGISTRY_API_BASE_URL,COMPLIANCE_BASE_URL=$COMPLIANCE_BASE_URL,REGISTRATION_NUMBER_NOTARY_BASE_URL=$REGISTRATION_NUMBER_NOTARY_BASE_URL,AWS_ENDPOINT=$AWS_ENDPOINT,AWS_REGION=$AWS_REGION,AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID,AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY,BASE_URL=$BASE_URL,DID_X509_ROOT_CERTIFICATE=$DID_X509_ROOT_CERTIFICATE,DID_X509_ROOT_PRIVATE_KEY=$DID_X509_ROOT_PRIVATE_KEY,KEYCLOAK_BASE_URL=$KEYCLOAK_BASE_URL,KEYCLOAK_CLIENT_ID=$KEYCLOAK_CLIENT_ID,KEYCLOAK_CLIENT_SECRET=$KEYCLOAK_CLIENT_SECRET,OIDC_SERVICE_BASE_URL=$OIDC_SERVICE_BASE_URL,MEMBERSHIP_CREDENTIAL_PORTAL_URL=$MEMBERSHIP_CREDENTIAL_PORTAL_URL" --kubeconfig "$ORANGE_KUBECONFIG"
  only:
    - tags
