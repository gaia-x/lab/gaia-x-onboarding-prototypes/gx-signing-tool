/** @type {import('next').NextConfig} */
const nextConfig = {
  output: 'standalone',
  reactStrictMode: false,
  basePath: process.env.BASE_PATH ? process.env.BASE_PATH : undefined
}

module.exports = nextConfig
